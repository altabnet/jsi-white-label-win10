﻿
' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 

Imports JSI_Altab.Altab_WS
Imports Newtonsoft.Json
Imports Windows.Storage

Public NotInheritable Class Login_Page
    Inherits Page


    Dim _MiRicordo As Boolean = False

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

    Dim _myIDE_Done As String = "Thank You! Your submission has benn received!"
    Dim _myIDE_Failed As String = "Ooops! Something went wrong while submitting the form"
    Dim _myIDE_Invalid As String = "Invalid username and/or password"
    Dim _myIDE_Misssing As String = "UserName or Password missing"
    Dim _myIDE_Skip As String = "Skip"
    Dim _myIDE_DataMissing As String = "You have to connect the app to the internet at the least one time before using it."

    Dim m_Alert_Warning As String = "Warning"
    Dim m_Alert_OK As String = "Ok"
    Dim m_Log_DataMissing As String = "You have to connect the app to the internet at the least one time before using it."

    Dim _PossoAgire As Boolean = False
    Dim _PrimaInstallazione As Boolean = False

    Public Property Colore_Check As SolidColorBrush
    Public Property Colore_Bordo_Check As SolidColorBrush

    Private Async Sub Btn_Login_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Login.Click

        _PossoAgire = True
        App.Da_Scaricare = New List(Of String)
        App.Thumbs_SCARICATE = New List(Of String)


        If Me.Txt_Username.Text.Trim.Length = 0 Then
            Me.Lbl_MessaggioLogin.Text = _myIDE_Invalid
            Exit Sub
        End If
        If Me.Txt_Password.Password.Length = 0 Then
            Me.Lbl_MessaggioLogin.Text = _myIDE_Invalid
            Exit Sub
        End If
        Me.Lbl_Installazione_02.Text = "Please wait a moment ... "
        Me.Lbl_Installazione_02.Visibility = Visibility.Visible

        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------
        Dim MyView As ApplicationView = ApplicationView.GetForCurrentView
        If Not MyView.IsFullScreenMode Then
            MyView.TryEnterFullScreenMode()
        End If
        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------
        '-----------------------------------------------------------
        ' Configura i Colori dell Title Bar
        '-----------------------------------------------------------
        Dim titlebar As ApplicationViewTitleBar = ApplicationView.GetForCurrentView().TitleBar
        'titlebar.ButtonBackgroundColor = Windows.UI.Color.FromArgb(255, 126, 188, 66)
        titlebar.ButtonBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonHoverBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonHoverForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonInactiveBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonInactiveForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonPressedBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonPressedForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonForegroundColor = Windows.UI.Colors.White
        '-----------------------------------------------------------
        ' Configura i Colori dell Title Bar
        '-----------------------------------------------------------



        Me.Btn_Login.Visibility = Visibility.Collapsed

        Me.BarraProgresso.Minimum = 0
        Me.BarraProgresso.Maximum = 10
        Me.BarraProgresso.Value = 0
        Me.BarraProgresso.Visibility = Visibility.Visible
        Task.Delay(10)

        App.Oggetti_in_Downloading = New List(Of Funzioni_Dati.File_da_Scaricare)

        App.Lista_File_in_Downloading = New List(Of Funzioni_Generali.File_in_Downloading)
        App.Lista_File_in_Downloading_File = New List(Of Funzioni_Generali.File_in_Downloading)

        '--------------------------------------------------------------
        ' Recupera l'ultima lingua selezionata
        Dim Lang_NoRete_Code As Object = localSettings.Values("Lang_NoRete_Code")
        Dim Lang_NoRete_Id As Object = localSettings.Values("Lang_NoRete_Id")

        If Not Lang_NoRete_Code Is Nothing Then App.m_Lang_NoRete_Code = Lang_NoRete_Code
        If Not Lang_NoRete_Id Is Nothing Then App.m_Lang_NoRete_Id = CInt(Lang_NoRete_Id)
        '--------------------------------------------------------------

        AggiornaBarra(1)
        'Task.Delay(10)
        App.m_Username = Me.Txt_Username.Text
        App.Categorie_da_Scaricare = New List(Of File_Down.Categorie_To_Down)

        If Funzioni_Generali.Connessione_Internet_Attiva Then

            Dim MioLogin As Funzioni_Login.Login


            Dim client As MobileServiceClient = New MobileServiceClient()
            Dim _Ritorno As String = ""
            Try
                _Ritorno = Await client.LoginAsync(Me.Txt_Username.Text, Me.Txt_Password.Password, "")  ', App.Firm_HdHash)

                Dim json As String = _Ritorno  '.Result
                MioLogin = Funzioni_Login.Convert_JSON_ReturnedFrom_Login(json)
            Catch ex As Exception
                MioLogin = New Funzioni_Login.Login
                MioLogin.p_HashId = "NULL"
            End Try

            '_Ritorno.Wait()

            App.Caricamento_DatiStruttura_Effettuto = False
            App.Is_First_Connection = False

            If MioLogin.p_HashId.Trim.ToUpper = "NULL" Then

                'Se Utente NON Trovato ESCE
                Me.Lbl_MessaggioLogin.Text = _myIDE_Invalid

                Me.BarraProgresso.Value = 0
                Me.BarraProgresso.Visibility = Visibility.Collapsed
                Me.Btn_Login.Visibility = Visibility.Visible
                Me.Lbl_Installazione_02.Visibility = Visibility.Collapsed
                Exit Sub
            Else
                AggiornaBarra(3)
                'Task.Delay(10)

                '---------------------------------------------
                'Gestione del Remember
                '---------------------------------------------

                'If Me.Op_Remember.IsChecked Then
                If _MiRicordo Then
                        localSettings.Values("Login_UserName") = Me.Txt_Username.Text
                        localSettings.Values("Login_Password") = Me.Txt_Password.Password
                        localSettings.Values("Login_RememberMe") = True
                    Else
                        localSettings.Values("Login_UserName") = ""
                    localSettings.Values("Login_Password") = ""
                    localSettings.Values("Login_RememberMe") = False
                End If


                '---------------------------------------------------------


                '------------------------------------------------------------------------------------------
                ' Se Utente Diverso dall'ultimo acceduto Reset dell'applicazione
                '------------------------------------------------------------------------------------------
                Dim _mYash As Object = localSettings.Values("Login_NoRete_HashID")
                Dim _MioHash As String = ""
                If Not _mYash Is Nothing Then _MioHash = _mYash.ToString

                If _MioHash <> MioLogin.p_HashId Or _PrimaInstallazione Then
                    localSettings.Values("Login_PrimaInstallazione") = "SI"
                    App.Is_First_Connection = True
                    'Va Resettato il DB e Vanno cancellate tutte le SottoDirectories

                    'Cancellazione DB
                    If Funzioni_Dati.LocalDatabase.Database_Exists() Then
                        Funzioni_Dati.LocalDatabase.DeleteDatabase()
                    End If

                    'Cancellazione delle SottoDirectory
                    Await Funzioni_Dati.Struttura.Eliminazione_Directory()
                Else
                    localSettings.Values("Login_PrimaInstallazione") = "NO"
                End If
            End If



            '------------------------------------------------------------------------------------------
            ' Verifica e Creazione della Struttura
            '------------------------------------------------------------------------------------------
            Azzera_Variabili_Globali()
            AggiornaBarra(5)


            'Verifica che siano presenti le Sottodirectory e Crearle
            Await Funzioni_Dati.Struttura.Creazione_Directory()

            'Verifica che ci sia il DB altrimenti Crearlo
            If Not Funzioni_Dati.LocalDatabase.Database_Exists() Then
                App.Is_First_Connection = True
                Funzioni_Dati.LocalDatabase.CreateDatabase()
            Else
                Funzioni_Dati.LocalDatabase.CreateDatabase()
            End If



            '----------------------------------------------------------------------------------------------------------
            'salva i dati per il recall se non ci fosse linea e per rendere disponibili i dati in tutta la APP
            '----------------------------------------------------------------------------------------------------------
            localSettings.Values("Login_NORete_UserName") = Me.Txt_Username.Text
            localSettings.Values("Login_NORete_Password") = Me.Txt_Password.Password
            localSettings.Values("Login_NoRete_HashID") = MioLogin.p_HashId
            localSettings.Values("Login_NoRete_rootHashID") = MioLogin.p_rootHashID
            localSettings.Values("Login_NoRete_EnableToSend") = MioLogin.p_EnableToSend


            localSettings.Values("Scelta_Id") = 0
            localSettings.Values("Scelta_Title") = ""
            localSettings.Values("Scelta_FK") = 0
            localSettings.Values("Effettuato_FK") = 0
            localSettings.Values("SecIDE_TopThank") = ""


            AggiornaBarra(7)
            'MessaggioConnesso()


            '--------------------------------------------------------------------------------
            ' Caricamento delle Lingue Disponibili e Reso il dato fruibile in tutta la APP
            '       Senza connessione la parte delle Ligue Non deve essere visibile
            '       Si utilizzerà l'ultima lingua selezionata
            '--------------------------------------------------------------------------------
            Funzioni_Lingue.Carica_Lingue(App.Lingue_Diponibili, MioLogin.p_HashId)
            AggiornaBarra(8)

            Await Funzioni_Lingue.Dammi_Lingua_Utilizzabile(App.m_Lang_NoRete_Code)

            '--------------------------------------------------------------
            ' Recupero Dati  della struttura   e  salvataggio nel DB
            '--------------------------------------------------------------

            'Se il Dato esiste già va recuperato dal DB ... altrimenti dalla struttura generale
            App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Principali_Call(App.m_Lang_NoRete_Id)

            If App.LeSezioni_Da_DataBase.Count = 0 Or App.Is_First_Connection Then


                '-------------------------------------------------
                '  (Menù non presente nel DB)
                'Recupero del Dato dal Alteb_WS  (Server)
                '-------------------------------------------------
                Dim Testo_Struttura As String = Await (Funct_RecuperoDati.Recupero_Dati_e_Struttura())
                AggiornaBarra(9)
                'Me.BarraProgresso.Value = 8
                'Task.Delay(50)


                '------------------------------------
                ' Conversione del JSON in Struttura
                '------------------------------------
                App.Struttura_Globale = JsonConvert.DeserializeObject(Of UpdateSync.Generale)(Testo_Struttura)

                ''---------------------------------------------------------
                ''Verificare se c'e' Connessione Internet
                ''---------------------------------------------------------
                App.Firm_HdHash = App.Struttura_Globale.BaseInfo.HqHashing
                localSettings.Values("Login_NoRete_HqHashing") = App.Struttura_Globale.BaseInfo.HqHashing

                If Funzioni_Generali.Connessione_Internet_Attiva() Then
                    'Scarica Json layout 
                    Await Funzioni_Layout.Scarica_Json_Layout(App.Struttura_Globale.BaseInfo.HqHashing)
                End If



                ''------------------------------------
                '' Conversione del JSON in Layout
                ''------------------------------------
                Dim _TestoLayout As String = Await Funzioni_Layout.Dammi_Testo_File()
                If _TestoLayout.Trim.Length > 0 Then
                    App.Layout_Globale = JsonConvert.DeserializeObject(Of Layout.RootObject)(_TestoLayout)
                End If

                '----------------------------------------------------
                ' Recupero delle sezioni e salvataggio nel DB
                '----------------------------------------------------
                App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Genera_da_NuovoStruttura()
                Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Save(App.LeSezioni_Da_DataBase)
                App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Principali_Call(App.m_Lang_NoRete_Id)

                '------------------------------------------------
                ' Settaggio delle Variabili 
                '------------------------------------------------
                App.Menu_Aggiornato = False
                App.Caricamento_DatiStruttura_Effettuto = True

                AggiornaBarra(10)
                'Me.BarraProgresso.Value = 10
                'Task.Delay(20)



                If App.Is_First_Connection Then
                    Me.Lbl_Installazione_02.Text = "Please wait a few minutes ... "
                    Me.Lbl_Installazione_01.Visibility = Visibility.Visible
                    Me.Lbl_Installazione_02.Visibility = Visibility.Visible
                    Me.Lbl_Installazione_03.Visibility = Visibility.Visible
                    Me.Lbl_Installazione_04.Visibility = Visibility.Visible


                    '-----------------------------------------------------------------
                    '---------------------------------------------
                    'Cancella tutti i Json
                    '-----------------------------------------------------------------
                    '---------------------------------------------
                    Await Funzioni_Layout.Cancella_Tutti_Json


                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "

                    '-----------------------------------------------------------------
                    '---------------------------------------------
                    ' 1 - Carica i dati da JSON
                    '---------------------------------------------
                    Funct_RecuperoDati.Recupero_e_Save_Dati_From_TheHome()

                    App.Le_Mie_Relazioni_Page = New List(Of Funzioni_Dati.TEMP_Relazione_Brochure_Page)

                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "

                    '---------------------------------------------
                    ' Effettua Altri Aggiornamenti - Mettin nella corretta struttura
                    '---------------------------------------------
                    Dim Video_FileScaricato As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Genera_da_NuovoStruttura()
                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "
                    Dim Immagini_FileScaricato As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Genera_da_NuovoStruttura()
                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "
                    Dim Brochure_FileScaricato As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochures_Genera_da_NuovoStruttura()
                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "
                    Dim SitiWeb_FileScaricato As List(Of Funzioni_Dati.SitiWeb) = Funzioni_Dati.LocalDatabase.SitiWeb.SitiWeb_Genera_da_NuovoStruttura()
                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "
                    Dim InfoBase_FileScaricato As Funzioni_Dati.InfoBase = Funzioni_Dati.LocalDatabase.InfoBase.InfoBase_Genera_da_NuovoStruttura()
                    Me.Lbl_Installazione_03.Text = "Step  1 / 5  "

                    '---------------------------------------------
                    ' Aggiorna e Scarica le Thumbs
                    '---------------------------------------------
                    App.Thumbs_Da_Scaricare = New List(Of String)

                    Dim W_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Thumbs"))
                    Me.Lbl_Installazione_03.Text = "Step  2 /  5 "
                    Await Funzioni_Dati.LocalDatabase.Immagini.Immagini_Aggiorna_Dati_e_Thumbs(Immagini_FileScaricato, W_DestinationFolder)
                    Me.Lbl_Installazione_03.Text = "Step  3 / 5  "

                    Await Funzioni_Dati.LocalDatabase.Video.Video_Aggiorna_Dati_e_Thumbs(Video_FileScaricato, W_DestinationFolder)
                    Me.Lbl_Installazione_03.Text = "Step  4  / 5  "

                    Await Funzioni_Dati.LocalDatabase.Brochures.Brochures_Aggiorna_Dati_e_Thumbs(Brochure_FileScaricato, W_DestinationFolder)
                    Me.Lbl_Installazione_03.Text = "Step   5 / 5  "

                    'Salva il File degli Oggetti da Scaricare
                    '---------------------------------------------
                    Await Funzioni_Dati.File_da_Scaricare.Carica_File_da_Scaricare_ALL(App.Da_Scaricare)
                    Me.Lbl_Installazione_03.Text = "Step   5 / 5  "
                    App.Caricamento_DatiStruttura_Effettuto = True
                    Me.Lbl_Installazione_01.Visibility = Visibility.Collapsed
                    Me.Lbl_Installazione_02.Visibility = Visibility.Collapsed
                    Me.Lbl_Installazione_03.Visibility = Visibility.Collapsed
                    Me.Lbl_Installazione_04.Visibility = Visibility.Collapsed

                End If
            Else
                ''---------------------------------------------------------
                ''Verificare se c'e' Connessione Internet
                ''---------------------------------------------------------
                If Funzioni_Generali.Connessione_Internet_Attiva() Then

                    Dim Testo_Struttura As String = Await (Funct_RecuperoDati.Recupero_Dati_e_Struttura())
                    App.Struttura_Globale = JsonConvert.DeserializeObject(Of UpdateSync.Generale)(Testo_Struttura)

                    'Scarica Json layout 
                    App.Firm_HdHash = App.Struttura_Globale.BaseInfo.HqHashing
                    Await Funzioni_Layout.Scarica_Json_Layout(App.Struttura_Globale.BaseInfo.HqHashing)
                    localSettings.Values("Login_NoRete_HqHashing") = App.Struttura_Globale.BaseInfo.HqHashing

                End If

                ''------------------------------------
                '' Conversione del JSON in Layout
                ''------------------------------------
                Dim _TestoLayout As String = Await Funzioni_Layout.Dammi_Testo_File()
                If _TestoLayout.Trim.Length > 0 Then
                    App.Layout_Globale = JsonConvert.DeserializeObject(Of Layout.RootObject)(_TestoLayout)
                End If

            End If

            App.Oggetti_da_Inviare = New List(Of Funzioni_Dati.File_da_Inviare)
            Funzioni_Dati.File_To_Send.Carica_Tutti_Elementi_to_Send()

            Await Funzioni_Lingue.Setta_Controlli_Lingua(App.m_Lang_NoRete_Code)
            Await Funzioni_Lingue.Scarica_Tutti_Json_delle_Lingue()


            Me.Frame.Navigate(GetType(The_Home_Page))
            'Me.Frame.Navigate(GetType(Maschera_Test))

        Else

            Dim _MyUN As Object = localSettings.Values("Login_NORete_UserName")
            Dim _MyPWD As Object = localSettings.Values("Login_NORete_Password")
            App.Firm_HdHash = localSettings.Values("Login_NoRete_HqHashing")

            If _MyUN Is Nothing OrElse _MyPWD Is Nothing Then
                Me.Btn_Login.Visibility = Visibility.Visible
                Me.Lbl_MessaggioLogin.Text = _myIDE_DataMissing
                Me.Lbl_Installazione_02.Visibility = Visibility.Collapsed
                Exit Sub
            Else

                ''------------------------------------
                '' Conversione del JSON in Layout
                ''------------------------------------

                Dim _TestoLayout As String = Await Funzioni_Layout.Dammi_Testo_File()
                If _TestoLayout.Trim.Length > 0 Then
                    App.Layout_Globale = JsonConvert.DeserializeObject(Of Layout.RootObject)(_TestoLayout)
                End If


                If _MyUN.ToString.ToUpper = Me.Txt_Username.Text.Trim.ToUpper AndAlso
                    _MyPWD = Me.Txt_Password.Password Then


                    App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Principali_Call(App.m_Lang_NoRete_Id)
                    '------------------------------------------------
                    ' Settaggio delle Variabili 
                    '------------------------------------------------
                    App.Menu_Aggiornato = False
                    App.Caricamento_DatiStruttura_Effettuto = False

                    App.Oggetti_da_Inviare = New List(Of Funzioni_Dati.File_da_Inviare)
                    Funzioni_Dati.File_To_Send.Carica_Tutti_Elementi_to_Send()


                    'Carica_Le_Lingue
                    Funzioni_Lingue.Carica_Lingue_dal_DB()
                    Await Funzioni_Lingue.Setta_Controlli_Lingua_OffLine(App.m_Lang_NoRete_Code)


                    'Richiama Maschera principale
                    Me.Frame.Navigate(GetType(The_Home_Page))
                Else
                    Me.Lbl_Installazione_02.Visibility = Visibility.Collapsed
                    Me.Lbl_MessaggioLogin.Text = _myIDE_Invalid
                    Me.Btn_Login.Visibility = Visibility.Visible
                    Exit Sub
                End If
            End If
        End If

    End Sub


    Private Sub AggiornaBarra(ByVal Valore As Integer)
        Me.BarraProgresso.Value = Valore
    End Sub
    Private Sub MessaggioConnesso()
        Me.Lbl_MessaggioLogin.Text = "Connesso"
    End Sub

    Private Async Sub Loginb_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded


        Me.Lbl_Installazione_01.Visibility = Visibility.Collapsed
        Me.Lbl_Installazione_02.Visibility = Visibility.Collapsed
        Me.Lbl_Installazione_03.Visibility = Visibility.Collapsed
        Me.Lbl_Installazione_04.Visibility = Visibility.Collapsed

        'Me.ProRing_Login.IsActive = False
        Me.ProRing_Login.Visibility = Visibility.Collapsed


        Dim Alert_Warning As Object = localSettings.Values("AleIDE_Warning")
        Dim Alert_OK As Object = localSettings.Values("AleIDE_OK")
        Dim LOG_DataMissing As Object = localSettings.Values("AleIDE_NoConnection")

        If Not LOG_DataMissing Is Nothing Then m_Log_DataMissing = LOG_DataMissing
        If Not Alert_Warning Is Nothing Then m_Alert_Warning = Alert_Warning
        If Not Alert_OK Is Nothing Then m_Alert_OK = Alert_OK


        '---------------------------------------------------------
        '  Recupero del remember di userName e Password
        '---------------------------------------------------------
        Dim _MyUN As Object = localSettings.Values("Login_UserName")
        Dim _MyPWD As Object = localSettings.Values("Login_Password")
        Dim _MyRM As Object = localSettings.Values("Login_RememberMe")
        Dim _MyPrima As Object = localSettings.Values("Login_PrimaInstallazione")
        If Not _MyUN Is Nothing Then
            Me.Txt_Username.Text = _MyUN.ToString
        End If
        If Not _MyPWD Is Nothing Then
            Me.Txt_Password.Password = _MyPWD.ToString
        End If
        If Not _MyRM Is Nothing Then
            Me.Op_Remember.IsChecked = _MyRM.ToString
        Else
            Me.Op_Remember.IsChecked = False
        End If
        _MiRicordo = Not Me.Op_Remember.IsChecked
        Me.Txt_Remeber_Tapped(Nothing, Nothing)


        If Not _MyPrima Is Nothing Then
            If _MyPrima.ToString.ToUpper = "SI" Then
                _PrimaInstallazione = True
            Else
                _PrimaInstallazione = False
            End If
        Else
            _PrimaInstallazione = True
        End If

        '---------------------------------------------------------
        '  Recupero dei testi in lingua dei controlli
        '       Ultima Lingua usata
        '---------------------------------------------------------
        Dim _MyIDUser As Object = localSettings.Values("LogIDE_Username")
        Dim _MyIDPwd As Object = localSettings.Values("LogIDE_Password")
        Dim _MyIDRem As Object = localSettings.Values("LogIDE_Remember")
        Dim _MyIDLog As Object = localSettings.Values("LogIDE_Login")
        Dim _MyIDFor As Object = localSettings.Values("LogIDE_Forgot")
        Dim _MyIDReg As Object = localSettings.Values("LogIDE_Register")

        If Not _MyIDUser Is Nothing Then
            Me.Lbl_UserName.Text = _MyIDUser.ToString
        End If
        If Not _MyIDPwd Is Nothing Then
            Me.Lbl_Password.Text = _MyIDPwd.ToString
        End If
        If Not _MyIDRem Is Nothing Then
            Me.Lbl_Remember.Text = _MyIDRem.ToString
        End If
        If Not _MyIDLog Is Nothing Then
            Me.Btn_Login.Content = _MyIDLog.ToString
        End If
        If Not _MyIDFor Is Nothing Then
            Me.Lbl_Forget.Content = _MyIDFor.ToString
        End If
        If Not _MyIDReg Is Nothing Then
            Me.Lbl_ClickHere.Content = _MyIDReg.ToString
        End If

        Dim myIDE_Done As Object = localSettings.Values("LogIDE_Done")
        Dim myIDE_Failed As Object = localSettings.Values("LogIDE_Failed")
        Dim myIDE_Invalid As Object = localSettings.Values("LogIDE_Invalid")

        Dim myIDE_Misssing As Object = localSettings.Values("LogIDE_Missing")
        Dim myIDE_Skip As Object = localSettings.Values("LogIDE_Skip")
        Dim myIDE_DataMissing As Object = localSettings.Values("LogIDE_DataMissing")

        If Not myIDE_Done Is Nothing Then _myIDE_Done = myIDE_Done.ToString
        If Not myIDE_Failed Is Nothing Then _myIDE_Failed = myIDE_Failed.ToString
        If Not myIDE_Invalid Is Nothing Then _myIDE_Invalid = myIDE_Invalid.ToString

        If Not myIDE_Misssing Is Nothing Then _myIDE_Misssing = myIDE_Misssing.ToString
        If Not myIDE_Skip Is Nothing Then _myIDE_Skip = myIDE_Skip.ToString
        If Not myIDE_DataMissing Is Nothing Then _myIDE_DataMissing = myIDE_DataMissing.ToString
        '---------------------------------------------------------
        '  FINE  -  Recupero dei testi in lingua dei controlli
        '                Ultima Lingua usata
        '---------------------------------------------------------



    End Sub

    Private Async Sub Lbl_ClickHere_Click(sender As Object, e As RoutedEventArgs) Handles Lbl_ClickHere.Click

        If Not Funzioni_Generali.Connessione_Internet_Attiva Then
            '-------------------------------------------------------------------
            ' Individuazione di un errore nell'invio
            '-------------------------------------------------------------------
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Log_DataMissing
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync
            Exit Sub
        End If
        '------------------------------------------
        ' Inserimento Nuovo Utente
        '------------------------------------------
        If Funzioni_Generali.Connessione_Internet_Attiva Then
            Frame.Navigate(GetType(Registration_Page))
        End If
    End Sub

    Private Sub Txt_Password_GotFocus(sender As Object, e As RoutedEventArgs) Handles Txt_Password.GotFocus, Txt_Username.GotFocus
        '------------------------------------------------------------------------------------
        ' Toglie il messaggio dopo che è stato confermato
        '------------------------------------------------------------------------------------
        If _PossoAgire Then
            Me.Lbl_MessaggioLogin.Text = ""
        End If
    End Sub

    Private Async Sub Lbl_Forget_Click(sender As Object, e As RoutedEventArgs) Handles Lbl_Forget.Click

        If Not Funzioni_Generali.Connessione_Internet_Attiva Then
            '-------------------------------------------------------------------
            ' Individuazione di un errore nell'invio
            '-------------------------------------------------------------------
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Log_DataMissing
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync
            Exit Sub
        End If

        '------------------------------------------
        ' Richiesta Nuova Password
        '------------------------------------------
        If Funzioni_Generali.Connessione_Internet_Attiva Then
            Frame.Navigate(GetType(ResetForgot_Password))
        End If
    End Sub

    Private Sub Login_Page_SizeChanged(sender As Object, e As SizeChangedEventArgs) Handles Me.SizeChanged

        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------
        Dim MyView As ApplicationView = ApplicationView.GetForCurrentView
        If Not MyView.IsFullScreenMode Then
            MyView.TryEnterFullScreenMode()
        End If
        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------


        '-----------------------------------------------------------
        ' Configura i Colori dell Title Bar
        '-----------------------------------------------------------
        Dim titlebar As ApplicationViewTitleBar = ApplicationView.GetForCurrentView().TitleBar
        'titlebar.ButtonBackgroundColor = Windows.UI.Color.FromArgb(255, 126, 188, 66)
        titlebar.ButtonBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonHoverBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonHoverForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonInactiveBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonInactiveForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonPressedBackgroundColor = Windows.UI.Colors.White
        titlebar.ButtonPressedForegroundColor = Windows.UI.Colors.White
        titlebar.ButtonForegroundColor = Windows.UI.Colors.White
        '-----------------------------------------------------------
        ' Configura i Colori dell Title Bar
        '-----------------------------------------------------------


    End Sub

    Private Sub Login_Page_LostFocus(sender As Object, e As RoutedEventArgs) Handles Me.LostFocus

        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------
        Dim MyView As ApplicationView = ApplicationView.GetForCurrentView
        If Not MyView.IsFullScreenMode Then
            MyView.TryEnterFullScreenMode()
        End If
        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------

    End Sub

    Private Async Sub Login_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        Try
            App.Firm_HdHash = localSettings.Values("Login_NoRete_HqHashing")
        Catch ex As Exception
            App.Firm_HdHash = ""
        End Try



        If App.Layout_Globale Is Nothing Then
            ''------------------------------------
            '' Conversione del JSON in Layout
            ''------------------------------------

            Dim _TestoLayout As String = Await Funzioni_Layout.Dammi_Testo_File()
            If _TestoLayout.Trim.Length > 0 Then
                App.Layout_Globale = JsonConvert.DeserializeObject(Of Layout.RootObject)(_TestoLayout)
            End If
        End If

        If Not App.Layout_Globale Is Nothing Then
            ''------------------------------------
            '' UTILIZZO del JSON  Layout
            ''------------------------------------

            '--------------------------------------------
            ' Globale
            '--------------------------------------------
            Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Grd_Globale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Pnl_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)
            Me.Pnl_Centrale.CornerRadius = New CornerRadius(CDbl(App.Layout_Globale.pageLogin.loginPanelRadius))


            Me.BarraProgresso.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.BarraProgresso.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)
            'Me.BarraProgresso.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)


            '--------------------------------------------
            ' label
            '--------------------------------------------
            Me.Lbl_Password.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_UserName.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_ClickHere.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_Forget.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)

            Me.Lbl_Installazione_01.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_Installazione_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_Installazione_03.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_Installazione_04.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)

            Me.Lbl_MessaggioLogin.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Lbl_Remember.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)

            Me.Op_Remember.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            'Me.Op_Remember.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
            Me.Op_Remember.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Colore_Bordo_Check = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)

            Me.Op_Remember.FocusVisualPrimaryBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)
            Me.Op_Remember.FocusVisualSecondaryBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)

            Me.Txt_Remeber_NO.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
            Me.Txt_Remeber_SI.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)

            '--------------------------------------------
            ' text Box
            '--------------------------------------------
            Me.Txt_Password.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
            Me.Txt_Username.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)

            Me.Txt_Password.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)
            Me.Txt_Username.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)

            Me.Txt_Password.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)
            Me.Txt_Username.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)

            'Me.Txt_Password.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)
            'Me.Txt_Username.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)


            'App.Layout_Globale.pageLogin.loginEditTxtRadius
            If CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth) > 8 Then
                Me.Txt_Password.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
                Me.Txt_Username.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
            End If


            '--------------------------------------------
            ' Bottoni
            '--------------------------------------------
            Me.Btn_Login.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)
            Me.Btn_Login.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBorderColor)
            Me.Btn_Login.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageLogin.loginBtnBorderWidth))
            'Me.Btn_Login.Radius = App.Layout_Globale.pageLogin.loginBtnRadius
            Me.Btn_Login.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnTxtColor)


            Me.Freccia_01.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)
            Me.Freccia_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)


            Me.Txt_Login_01.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)
            Me.Txt_Login_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)



            '--------------------------------------------------
            ' LOGO
            '--------------------------------------------------
            Try
                Me.Logo_Login.Source = Nothing
                Dim uriSource = New Uri("ms-appx://JSI_ATAB/Assets/StoreLogo.png")
                Me.Logo_Login.CacheMode = Nothing
                Me.Logo_Login.Source = New BitmapImage(uriSource)
                Me.Logo_Login.UpdateLayout()
            Catch ex As Exception
                Me.Logo_Login.Source = Nothing
            End Try


            Dim _FileName As String = App.Firm_HdHash.Trim & ".jpeg"  'App.Layout_Globale.general.panelLogo
            'Dim _FileName As String = App.Layout_Globale.general.panelLogo
            Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
            Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)
            Try
                If MyFileInfo.Length > 0 Then
                    Dim uriSource = New Uri(_FileControllo)
                    Me.Logo_Login.Source = New BitmapImage(uriSource)
                Else
                    Me.Logo_Login.Source = Nothing
                End If

            Catch ex As Exception
                Me.Logo_Login.Source = Nothing
            End Try
            '--------------------------------------------------
            ' LOGO
            '--------------------------------------------------
        End If


    End Sub

    Private Sub Txt_Remeber_Tapped(sender As Object, e As TappedRoutedEventArgs) Handles Txt_Remeber_SI.Tapped, Txt_Remeber_NO.Tapped
        If _MiRicordo Then
            _MiRicordo = False
            Me.Txt_Remeber_NO.Visibility = Visibility.Visible
            Me.Txt_Remeber_SI.Visibility = Visibility.Collapsed
        Else
            _MiRicordo = True
            Me.Txt_Remeber_SI.Visibility = Visibility.Visible
            Me.Txt_Remeber_NO.Visibility = Visibility.Collapsed
        End If
    End Sub



    'Private Sub Login_Page_GotFocus(sender As Object, e As RoutedEventArgs) Handles Me.GotFocus

    '    Me.Login_Page_SizeChanged(Nothing, Nothing)
    'End Sub

    'Private Sub Login_Page_LayoutUpdated(sender As Object, e As Object) Handles Me.LayoutUpdated
    '    Me.Login_Page_SizeChanged(Nothing, Nothing)
    'End Sub


    Private Sub Azzera_Variabili_Globali()
        Try
            App.LeSezioni_Da_DataBase.Clear()
        Catch ex As Exception

        End Try

        Try
            App.Sezioni_Secondarie.Clear()
        Catch ex As Exception

        End Try

        Try
            App.Lingue_Diponibili.Clear()
        Catch ex As Exception

        End Try

        Try
            App.MFly_Languages = Nothing
        Catch ex As Exception

        End Try

        Try
            App.MFly_Stats = Nothing
        Catch ex As Exception

        End Try


        App.Title_SettorePrincipale = ""
        App.Title_SettoreSecondario = ""
    End Sub

End Class
