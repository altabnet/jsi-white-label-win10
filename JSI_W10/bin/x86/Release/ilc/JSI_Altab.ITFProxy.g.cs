using Mcg.System;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;



namespace __InterfaceProxies
{
	public class ServiceChannelProxy_IMobileService : global::System.ServiceModel.Channels.ServiceChannelProxy_P, global::JSI_Altab.Altab_WS.IMobileService_P
	{
		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.LoginAsync(
					string u, 
					string p, 
					string hqHash)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					u, 
					p, 
					hqHash
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.UpdateSyncAsync(string uHash)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.GetCreditsLeftAsync(string uHash)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P_1 global::JSI_Altab.Altab_WS.IMobileService_P.ParseWebHookAsync(string json)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					json
			};
			global::System.Threading.Tasks.Task_P_1 retval = ((global::System.Threading.Tasks.Task_P_1)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.UserDenyContentToSendAsync(string uHash)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SendAsync(
					string uHash, 
					string name, 
					string surn, 
					string mail, 
					string note, 
					global::System.Collections.ObjectModel.ObservableCollection_P<long> ids, 
					string additionalText)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash, 
					name, 
					surn, 
					mail, 
					note, 
					ids, 
					additionalText
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SendMultipleAsync(
					string uHash, 
					global::System.Collections.ObjectModel.ObservableCollection_P<string> mail, 
					global::System.Collections.ObjectModel.ObservableCollection_P<long> ids, 
					string additionalText)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash, 
					mail, 
					ids, 
					additionalText
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<global::JSI_Altab.Altab_WS.CreateUserResponse_P> global::JSI_Altab.Altab_WS.IMobileService_P.CreateUserAsync(
					string name, 
					string surn, 
					string mail, 
					string company, 
					int fkHq, 
					int fkMailTpl, 
					int fkRoot)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company, 
					fkHq, 
					fkMailTpl, 
					fkRoot
			};
			global::System.Threading.Tasks.Task_P<global::JSI_Altab.Altab_WS.CreateUserResponse_P> retval = ((global::System.Threading.Tasks.Task_P<global::JSI_Altab.Altab_WS.CreateUserResponse_P>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SignUpGeAsync(
					string name, 
					string surn, 
					string mail, 
					string company)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SignUpGeDTSAsync(
					string name, 
					string surn, 
					string mail, 
					string company)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.ResetPasswordAsync(
					string rootHashID, 
					string mail)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					rootHashID, 
					mail
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SignUpSaipemAsync(
					string name, 
					string surn, 
					string mail, 
					string company)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.ResetPasswordSaipemAsync(
					string rootHashID, 
					string mail)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					rootHashID, 
					mail
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SignUpModulaAsync(
					string name, 
					string surn, 
					string mail, 
					string company)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.ResetPasswordModulaAsync(
					string rootHashID, 
					string mail)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					rootHashID, 
					mail
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.SignUpSystemAsync(
					string name, 
					string surn, 
					string mail, 
					string company)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					name, 
					surn, 
					mail, 
					company
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.ResetPasswordSystemAsync(
					string rootHashID, 
					string mail)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					rootHashID, 
					mail
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.GetAvailableLanguagesAsync(string uHash)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<string> global::JSI_Altab.Altab_WS.IMobileService_P.UpdateSyncByLangAsync(
					string uHash, 
					long languageId)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					uHash, 
					languageId
			};
			global::System.Threading.Tasks.Task_P<string> retval = ((global::System.Threading.Tasks.Task_P<string>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}
	}

	[global::System.Runtime.CompilerServices.EagerStaticClassConstruction]
	[global::System.Runtime.CompilerServices.DependencyReductionRoot]
	public static class __DispatchProxyImplementationData
	{
		static global::System.Reflection.DispatchProxyEntry[] s_entryTable = new global::System.Reflection.DispatchProxyEntry[] {
				new global::System.Reflection.DispatchProxyEntry() {
					ProxyClassType = typeof(global::System.ServiceModel.Channels.ServiceChannelProxy_P).TypeHandle,
					InterfaceType = typeof(global::JSI_Altab.Altab_WS.IMobileService_P).TypeHandle,
					ImplementationClassType = typeof(global::__InterfaceProxies.ServiceChannelProxy_IMobileService).TypeHandle,
				}
		};
		static __DispatchProxyImplementationData()
		{
			global::System.Reflection.DispatchProxyHelpers.RegisterImplementations(s_entryTable);
		}
	}
}

namespace JSI_Altab.Altab_WS
{
	[global::System.Runtime.InteropServices.McgRedirectedType("JSI_Altab.Altab_WS.IMobileService, JSI_Altab, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
	public interface IMobileService_P
	{
		global::System.Threading.Tasks.Task_P<string> LoginAsync(
					string u, 
					string p, 
					string hqHash);

		global::System.Threading.Tasks.Task_P<string> UpdateSyncAsync(string uHash);

		global::System.Threading.Tasks.Task_P<string> GetCreditsLeftAsync(string uHash);

		global::System.Threading.Tasks.Task_P_1 ParseWebHookAsync(string json);

		global::System.Threading.Tasks.Task_P<string> UserDenyContentToSendAsync(string uHash);

		global::System.Threading.Tasks.Task_P<string> SendAsync(
					string uHash, 
					string name, 
					string surn, 
					string mail, 
					string note, 
					global::System.Collections.ObjectModel.ObservableCollection_P<long> ids, 
					string additionalText);

		global::System.Threading.Tasks.Task_P<string> SendMultipleAsync(
					string uHash, 
					global::System.Collections.ObjectModel.ObservableCollection_P<string> mail, 
					global::System.Collections.ObjectModel.ObservableCollection_P<long> ids, 
					string additionalText);

		global::System.Threading.Tasks.Task_P<global::JSI_Altab.Altab_WS.CreateUserResponse_P> CreateUserAsync(
					string name, 
					string surn, 
					string mail, 
					string company, 
					int fkHq, 
					int fkMailTpl, 
					int fkRoot);

		global::System.Threading.Tasks.Task_P<string> SignUpGeAsync(
					string name, 
					string surn, 
					string mail, 
					string company);

		global::System.Threading.Tasks.Task_P<string> SignUpGeDTSAsync(
					string name, 
					string surn, 
					string mail, 
					string company);

		global::System.Threading.Tasks.Task_P<string> ResetPasswordAsync(
					string rootHashID, 
					string mail);

		global::System.Threading.Tasks.Task_P<string> SignUpSaipemAsync(
					string name, 
					string surn, 
					string mail, 
					string company);

		global::System.Threading.Tasks.Task_P<string> ResetPasswordSaipemAsync(
					string rootHashID, 
					string mail);

		global::System.Threading.Tasks.Task_P<string> SignUpModulaAsync(
					string name, 
					string surn, 
					string mail, 
					string company);

		global::System.Threading.Tasks.Task_P<string> ResetPasswordModulaAsync(
					string rootHashID, 
					string mail);

		global::System.Threading.Tasks.Task_P<string> SignUpSystemAsync(
					string name, 
					string surn, 
					string mail, 
					string company);

		global::System.Threading.Tasks.Task_P<string> ResetPasswordSystemAsync(
					string rootHashID, 
					string mail);

		global::System.Threading.Tasks.Task_P<string> GetAvailableLanguagesAsync(string uHash);

		global::System.Threading.Tasks.Task_P<string> UpdateSyncByLangAsync(
					string uHash, 
					long languageId);
	}
}

namespace System.Reflection
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Reflection.DispatchProxy, System.Private.Interop, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f" +
		"7f11d50a3a")]
	public class DispatchProxy_P
	{
	}
}

namespace System.ServiceModel.Channels
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.ServiceModel.Channels.ServiceChannelProxy, System.Private.ServiceModel, Version=4.1.0.0, Culture=neutral," +
		" PublicKeyToken=b03f5f7f11d50a3a")]
	public class ServiceChannelProxy_P : global::System.Reflection.DispatchProxy
	{
		protected override object Invoke(
					global::System.Reflection.MethodInfo targetMethodInfo, 
					object[] args)
		{
			return null;
		}
	}
}

namespace System.Threading.Tasks
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Threading.Tasks.Task`1, System.Private.Threading, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f" +
		"7f11d50a3a")]
	public class Task_P<TResult>
	{
	}
}

namespace System.Threading.Tasks
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Threading.Tasks.Task, System.Private.Threading, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f" +
		"11d50a3a")]
	public class Task_P_1
	{
	}
}

namespace System.Collections.ObjectModel
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Collections.ObjectModel.ObservableCollection`1, System.ObjectModel, Version=4.0.10.0, Culture=neutral, Pu" +
		"blicKeyToken=b03f5f7f11d50a3a")]
	public class ObservableCollection_P<T>
	{
	}
}

namespace JSI_Altab.Altab_WS
{
	[global::System.Runtime.InteropServices.McgRedirectedType("JSI_Altab.Altab_WS.CreateUserResponse, JSI_Altab, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
	public class CreateUserResponse_P
	{
	}
}
