﻿

' From Global page   10/01/2017   Funzionante  

    Private Async Sub Button_Click_1(sender As Object, e As RoutedEventArgs)

        If Me.Lbl_Down.Text.Trim.Length > 0 Then
            Exit Sub
        End If

        Dim _MioBottone As Button = CType(sender, Button)
        Dim _idAccesso As String = _MioBottone.Name


        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        Dim lancialo As Boolean = True
        Dim ListaFile As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.ID_Section = _idAccesso).ToList


        Dim Quanti As Integer = 0
        If App.Oggetti_in_Downloading.Count = 0 Then
            App.Contatore_Generale = 0
        End If
        Dim SelezioneOggetti As New List(Of Funzioni_Dati.File_da_Scaricare)

        For Each Lista As Funzioni_Dati.File_da_Scaricare In ListaFile
            Dim AggiungiFile As New Funzioni_Dati.File_da_Scaricare
            AggiungiFile = Lista
            AggiungiFile.p_Scaricato = False
            App.Oggetti_in_Downloading.Add(AggiungiFile)
            'Quanti += 1


            Dim AggiungiFile2 As New Funzioni_Dati.File_da_Scaricare
            AggiungiFile2 = Lista
            AggiungiFile2.p_Scaricato = False
            SelezioneOggetti.Add(AggiungiFile)
        Next

        Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count

        Dim File_Scaricato As Boolean = False
        For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In SelezioneOggetti    'App.Oggetti_in_Downloading

            File_Scaricato = False
            If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "IMG" Then
                File_Scaricato = Await Scarica_File(OggettoSelezionato, ImageFolder, _UrlBase)
            End If
            If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "VID" Then
                File_Scaricato = Await Scarica_File(OggettoSelezionato, VideoFolder, _UrlBase)
            End If
            If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "BRO" Then
                File_Scaricato = Await Scarica_e_Unzippa_File(OggettoSelezionato, BrochureFolderZIP, _UrlBase)
                'Await Task.Delay(10000)
            End If




            ''Diminuisci dalla Lista del Settore
            ''---------------------------------------------------
            'For Each Settore As File_Down.Categorie_To_Down In Me.Categorie_Down
            '    If Settore.ID_Section = _idAccesso Then
            '        If Settore.NumeroOggetti = 1 Then
            '            Me.Categorie_Down.Remove(Settore)
            '        Else
            '            Settore.NumeroOggetti = Settore.NumeroOggetti - 1
            '        End If
            '        Exit For
            '    End If
            'Next


            'Ricarica la lista
            '---------------------------------------------------
            Me.Lista_Down.ItemsSource = Nothing
            Me.Lista_Down.ItemsSource = Categorie_Down

            Quanti += 1
            App.Contatore_Generale += 1
            Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count

        Next
        Me.Lbl_Down.Text = ""

        'If App.Contatore_Generale = App.Oggetti_in_Downloading.Count Then

        '    Dim SPippo As Integer = 0
        '    For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In App.Oggetti_in_Downloading
        '        If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "BRO" Then
        '            File_Scaricato = Await Unzippa_File(OggettoSelezionato, BrochureFolderZIP, _UrlBase)
        '            'Await Task.Delay(10000)
        '        End If
        '    Next
        'End If


        'Dim Indice As Integer = 0
        'For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In App.Oggetti_in_Downloading
        '    Indice += 1

        '    If Indice = 2 Then
        '        Dim Consideralo_Scaricato As Boolean = False
        '        If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "IMG" Then

        '            'Scarica IMG
        '            Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
        '            Dim _InputUrl As String = _UrlBase & Stringa_Thumb

        '            Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
        '            Dim Lungo As Integer = Stringa_Thumb.Trim.Length
        '            Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

        '            Consideralo_Scaricato = Await (Gestore_DownLoad.StartDownload(_InputUrl, ImageFolder, _Filename))

        '        Else
        '            If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "VID" Then
        '                'Scarica VID

        '                Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
        '                Dim _InputUrl As String = _UrlBase & Stringa_Thumb

        '                Dim Inizio As Integer = Stringa_Thumb.LastIndexOf("/")
        '                Dim Lungo As Integer = Stringa_Thumb.Trim.Length
        '                Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

        '                Consideralo_Scaricato = Await (Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename))

        '            Else
        '                If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "BRO" Then
        '                    'Scarica BRO

        '                    ' Gestione del Download
        '                    '------------------------------------------------------------
        '                    Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
        '                    Dim _InputUrl As String = _UrlBase & Stringa_Thumb

        '                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
        '                    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
        '                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

        '                    Dim BuonFine As Boolean = Await (Gestore_DownLoad.StartDownload(_InputUrl, BrochureFolderZIP, _Filename))
        '                    '------------------------------------------------------------
        '                    ' FINE -  Gestione del Download
        '                    '------------------------------------------------------------

        '                    If BuonFine Then
        '                        'Gestione dell'unzip
        '                        '------------------------------------------------------------


        '                        Dim archivio = Await localFolder.GetFileAsync("Pages\7z\" & _Filename)
        '                        If Not archivio Is Nothing Then

        '                            Dim MYStream As System.IO.Stream = New System.IO.FileStream(archivio.Path, FileMode.Open)

        '                            If Not MYStream Is Nothing Then
        '                                Dim MioArchivio As SharpCompress.Archive.SevenZip.SevenZipArchive =
        '                            SharpCompress.Archive.SevenZip.SevenZipArchive.Open(MYStream)

        '                                If Not MioArchivio Is Nothing Then

        '                                    'Gestione dell'unzip
        '                                    '------------------------------------------------------------
        '                                    Dim PrimaElaborazione As Boolean = True

        '                                    For Each Oggetto As SharpCompress.Archive.SevenZip.SevenZipArchiveEntry In MioArchivio.Entries

        '                                        Oggetto.OpenEntryStream()

        '                                        Dim _cartella As String = localFolder.Path
        '                                        Dim _dove1 As Integer = Oggetto.Key.IndexOf("/")
        '                                        Dim _dove As Integer = Oggetto.Key.LastIndexOf("/")
        '                                        If _dove > 0 Then
        '                                            Dim _cartella1 As String = Oggetto.Key.Substring(0, _dove1)
        '                                            _cartella = Oggetto.Key.Substring(0, _dove)
        '                                            'Try

        '                                            '    Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync(_cartella1.Replace("/", "\"), CreationCollisionOption.FailIfExists)
        '                                            'Catch ex As Exception
        '                                            '    Dim PIppo As String = ""
        '                                            'End Try
        '                                            Try

        '                                                Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync(_cartella.Replace("/", "\"), CreationCollisionOption.FailIfExists)
        '                                            Catch ex As Exception
        '                                                Dim PIppo As String = ""
        '                                            End Try
        '                                        End If

        '                                        Dim _Ilpath As String = localFolder.Path & "\" & Oggetto.Key

        '                                        Dim fs As FileStream = Await Funzioni_Generali.CreaFile(_Ilpath)

        '                                        Oggetto.WriteTo(fs)

        '                                        ''Oggetto.OpenEntryStream()

        '                                        'Dim _cartella As String = localFolder.Path
        '                                        'Dim _dove1 As Integer = Oggetto.Key.IndexOf("/")
        '                                        'Dim _dove As Integer = Oggetto.Key.LastIndexOf("/")
        '                                        'If _dove > 0 Then
        '                                        '    Dim _cartella1 As String = Oggetto.Key.Substring(0, _dove1)
        '                                        '    _cartella = Oggetto.Key.Substring(0, _dove)

        '                                        '    If PrimaElaborazione Then
        '                                        '        PrimaElaborazione = False
        '                                        '        Try
        '                                        '            Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync(_cartella.Replace("/", "\"), CreationCollisionOption.FailIfExists)
        '                                        '        Catch ex As Exception
        '                                        '        End Try
        '                                        '    End If

        '                                        'End If

        '                                        'Dim _Ilpath As String = localFolder.Path & "\" & Oggetto.Key

        '                                        'If _Ilpath.ToUpper.EndsWith("0031.JPG") Then
        '                                        '    _Ilpath = _Ilpath
        '                                        'End If

        '                                        'Dim fs As System.IO.FileStream = Await Funzioni_Generali.CreaFile(_Ilpath)

        '                                        'If Not fs Is Nothing Then
        '                                        '    Try
        '                                        '        'Await Scrivilo(Oggetto, fs)
        '                                        '        Oggetto.WriteTo(fs)

        '                                        '        'Await Task.Delay(10000)


        '                                        '        Consideralo_Scaricato = False
        '                                        '    Catch ex As Exception
        '                                        '        Consideralo_Scaricato = False
        '                                        '        Exit For
        '                                        '    End Try
        '                                        'Else
        '                                        '    Consideralo_Scaricato = False
        '                                        '    Exit For
        '                                        'End If
        '                                    Next
        '                                    '------------------------------------------------------------
        '                                    '  FINE  Gestione dell'unzip
        '                                    '------------------------------------------------------------

        '                                End If
        '                            End If
        '                        End If

        '                    End If
        '                End If

        '            End If

        '        End If

        '        ' Togli dalla Lista degli Oggetti da sacricare
        '        '---------------------------------------------------
        '        If Consideralo_Scaricato Then
        '            '    Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID_Object)
        '        End If



        '        'Diminuisci dalla Lista del Settore
        '        '---------------------------------------------------
        '        For Each Settore As File_Down.Categorie_To_Down In Me.Categorie_Down
        '            If Settore.ID_Section = _idAccesso Then
        '                If Settore.NumeroOggetti = 1 Then
        '                    Me.Categorie_Down.Remove(Settore)
        '                Else
        '                    Settore.NumeroOggetti = Settore.NumeroOggetti - 1
        '                End If
        '                Exit For
        '            End If
        '        Next


        '        'Ricarica la lista
        '        '---------------------------------------------------
        '        Me.Lista_Down.ItemsSource = Nothing
        '        Me.Lista_Down.ItemsSource = Categorie_Down
        '        Try

        '            ' Togli dalla Lista del DB dei file da scaricare
        '            '---------------------------------------------------
        '            OggettoSelezionato.p_Scaricato = True
        '            'App.Oggetti_in_Downloading.Remove(App.Oggetti_in_Downloading(0))
        '        Catch ex As Exception

        '        End Try

        '        Exit For
        '    End If
        'Next


        'Try
        '    'Await Funzioni_Generali.Download_Massivo()
        'Catch ex As Exception

        'End Try

    End Sub
