﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 
Imports JSI_Altab.Altab_WS
Imports Windows.Storage
Imports System.Net
Imports Windows.Storage.Pickers
Imports System.Net.Http


Public NotInheritable Class Maschera_Test
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

    Public DownloadedFile As StorageFile

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

        Dim _UserHash As Object = localSettings.Values("Login_NoRete_HashID")
        If _UserHash Is Nothing Then
            Exit Sub
        End If

        Dim client As MobileServiceClient = New MobileServiceClient()
        Dim _Ritorno As Task(Of String) = client.GetAvailableLanguagesAsync(_UserHash.ToString)
        _Ritorno.Wait()

        Dim json As String = _Ritorno.Result
        Dim Mie_Lingue As List(Of Funzioni_Lingue.Lingue_Disponibili) = Funzioni_Lingue.Convert_JSON_ReturnedFrom_AvailableLanguages(json)

    End Sub

    Private Sub RecuperoTesti_Click(sender As Object, e As RoutedEventArgs)

    End Sub


    Public Async Sub DownLoadFile(ByVal Il_Link As String,
                                         ByVal IlBottone_Sender As Button,
                                         ByVal BarraProgresso As ProgressBar)

        'Dim _StringaUrl As String = "http://data.just-send-it/"
        '_StringaUrl &= localSettings.Values("Login_NoRete_rootHashID")
        ''_StringaUrl &= App.Firm_HdHash
        '_StringaUrl &= "/lang/en.json"
        ''Dim _FileName As String = "en.json"

        ''Dim _StringaUrl As String = "http://icalendario.it/media/stampare/2017/annuale/calendario-2017.png"
        'Dim _FileName As String = "calendario2017.jpeg"

        'Funzioni_Generali.StartDownload(_StringaUrl, _FileName)
        'Exit Sub






        '76EA4EBAF60C4A740A33060950698BACC27153E9
        Il_Link = "http://data.just-send-it/"
        Il_Link &= localSettings.Values("Login_NoRete_rootHashID")
        Il_Link &= "/lang/en.json"
        'Dim _FileName As String = ""
        _FileName = ""
        Dim _FileExtension As String = ""
        Try
            _FileName = System.IO.Path.GetFileName(Il_Link).Substring(0, System.IO.Path.GetFileName(Il_Link).Trim.Length - 5)
            _FileExtension = Il_Link.Trim.Substring(Il_Link.LastIndexOf("."), 5)


        Catch ex As Exception

            'Dim Messaggio As New MessageDialog()
        End Try

        If _FileName <> "" And _FileExtension <> "" Then
            Dim MioPicker As FolderPicker = New FolderPicker()
            MioPicker.SuggestedStartLocation = PickerLocationId.Desktop
            MioPicker.ViewMode = PickerViewMode.Thumbnail
            MioPicker.FileTypeFilter.Add("*")

            Dim MioFolder As StorageFolder = Await MioPicker.PickSingleFolderAsync

            If Not MioFolder Is DBNull.Value Then
                IlBottone_Sender.IsEnabled = False
                BarraProgresso.IsIndeterminate = True
                BarraProgresso.Visibility = Visibility.Visible

                Dim MyAddress As Uri = New Uri(Il_Link, UriKind.Absolute)
                Dim My_Request As HttpWebRequest = WebRequest.Create(MyAddress)
                Dim MyResponse As HttpWebResponse = Await My_Request.GetResponseAsync
                Dim MyStream As Stream = MyResponse.GetResponseStream
                Dim MyFile As StorageFile = Await MioFolder.CreateFileAsync(_FileName + _FileExtension, CreationCollisionOption.GenerateUniqueName)
                Await Windows.Storage.FileIO.WriteBytesAsync(MyFile, MyReadStream(MyStream))
                DownloadedFile = MyFile


                'Dim MyAddress As Uri = New Uri(Il_Link, UriKind.Absolute)
                ''                Uri Uri = New Uri("http://i.stack.imgur.com/ZfLdV.png?s=128&g=1");
                ''String fileName = "daniel2.png";

                'Dim destinationFile As StorageFile = Await KnownFolders.PicturesLibrary.CreateFileAsync(_FileName + _FileExtension, CreationCollisionOption.GenerateUniqueName)
                ''          StorageFile destinationFile = Await KnownFolders.PicturesLibrary.CreateFileAsync(
                ''fileName, CreationCollisionOption.GenerateUniqueName);


                'Dim Client As HttpClient = New HttpClient
                ''HttpClient client = New HttpClient();

                'Dim Buffer = Await Client.GetByteArrayAsync(MyAddress)
                ''var Buffer = Await client.GetBufferAsync(Uri);

                'Await Windows.Storage.FileIO.WriteBytesAsync(destinationFile, Buffer)
                ''Await Windows.Storage.FileIO.WriteBufferAsync(destinationFile, Buffer);

                'DownloadedFile = destinationFile

                IlBottone_Sender.IsEnabled = True
                BarraProgresso.IsIndeterminate = False
                BarraProgresso.Visibility = Visibility.Collapsed

            End If

        Else
        End If

    End Sub


    Private Shared Function MyReadStream(ByVal LoStream As Stream) As Byte()

        Dim buffer As Byte() = New Byte(16 * 1024 - 1) {}
        Using ms As New MemoryStream()
            Dim Read As Integer

            While (Read = LoStream.Read(buffer, 0, buffer.Length)) > 0

                ms.Write(buffer, 0, Read)
            End While

            Return ms.ToArray
        End Using


    End Function

    Private Sub GuardaFile_Click(sender As Object, e As RoutedEventArgs) Handles GuardaFile.Click
        If Not DownloadedFile Is DBNull.Value Then
            'Windows.System.Launcher.LaunchFileAsync(DownloadedFile)
        End If
    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs) Handles Btn_Download.Click

        DownLoadFile(Me.Txt_FileDaScaricare.Text, Me.Btn_Download, BarraProgresso)
    End Sub
End Class
