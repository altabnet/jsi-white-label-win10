﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 


Public NotInheritable Class MainPage
    Inherits Page

    Public HashId As String = ""

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Application.Current.Resources("ToggleButtonBackgroundChecked") = New SolidColorBrush(Windows.UI.Colors.Red)
        Application.Current.Resources("ToggleButtonBackgroundCheckedPointerOver") = New SolidColorBrush(Windows.UI.Colors.Red)
        Application.Current.Resources("ToggleButtonBackgroundCheckedPressed") = New SolidColorBrush(Windows.UI.Colors.Red)

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub MainPage_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Me.Frame.Navigate(GetType(Login_Page))
    End Sub
End Class
