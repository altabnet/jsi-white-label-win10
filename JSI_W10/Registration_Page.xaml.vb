﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 

Imports JSI_Altab.Altab_WS

Public NotInheritable Class Registration_Page
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder
    Dim _ConnessionePresente As Boolean = True

    Dim _myIDE_Name As String = "Name"
    Dim _myIDE_SurName As String = "Surname"
    Dim _myIDE_EMail As String = "e-mail"
    Dim _myIDE_Company As String = "Company"
    Dim _myIDE_Register As String = "Register"
    Dim _myIDE_Login As String = "Back to Login"
    Dim _myIDE_Done As String = "Congratulations! Your registration was succefull. You will receive an email with your username and password."
    Dim _myIDE_AlredyExist As String = "There's another user registered with this mail. Please recover password."
    Dim _myIDE_MissingField As String = "Please Fill In all fields."
    Dim m_For_InvalidMail As String = "Bad email address provided!"


    Private Sub Lbl_BackLogin_Click(sender As Object, e As RoutedEventArgs) Handles Lbl_BackLogin.Click
        '---------------------------------------------------------
        ' Ritorno al Login
        '---------------------------------------------------------
        Frame.Navigate(GetType(Login_Page))
    End Sub

    Private Async Sub Btn_Register_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Register.Click

        If Me.Txt_Company.Text.Trim.Length = 0 OrElse
                     Me.Txt_email.Text.Trim.Length = 0 OrElse
                      Me.Txt_Name.Text.Trim.Length = 0 OrElse
                       Me.Txt_Surname.Text.Trim.Length = 0 Then

            Me.Lbl_MessaggioLogin.Text = _myIDE_MissingField
            Exit Sub
        End If

        If Not Funzioni_Generali.ValidaEmail(Me.Txt_email.Text) Then
            Me.Lbl_MessaggioLogin.Text = m_For_InvalidMail
            Exit Sub
        End If

        Dim client As MobileServiceClient = New MobileServiceClient()
        Dim _Ritorno As Task(Of String) = client.SignUpSystemAsync(Me.Txt_Name.Text, Me.Txt_Surname.Text, Me.Txt_email.Text, Me.Txt_Company.Text)
        _Ritorno.Wait()

        Dim json As String = _Ritorno.Result

        Dim _Messaggio As String = ""
        If json.IndexOf("OK") > 0 Then
            _Messaggio = _myIDE_Done
        Else
            _Messaggio = _myIDE_AlredyExist
        End If
        Me.Lbl_MessaggioLogin.Text = _Messaggio
        If json.IndexOf("OK") > 0 Then

            Await Task.Delay(3000)
            Frame.Navigate(GetType(Login_Page))
        End If


    End Sub


    Private Sub Txt_Company_GotFocus(sender As Object, e As RoutedEventArgs) Handles Txt_Company.GotFocus, Txt_Name.GotFocus, Txt_Surname.GotFocus, Txt_email.GotFocus
        Me.Lbl_MessaggioLogin.Text = ""
    End Sub

    Private Sub Registration_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        '---------------------------------------------------------
        '  Recupero dei testi in lingua dei controlli
        '       Ultima Lingua usata
        '---------------------------------------------------------

        Dim myIDE_Name As Object = localSettings.Values("RegIDE_Name")
        Dim myIDE_SurName As Object = localSettings.Values("RegIDE_Surname")
        Dim myIDE_EMail As Object = localSettings.Values("RegIDE_Email")

        Dim myIDE_Company As Object = localSettings.Values("RegIDE_Company")
        Dim myIDE_Register As Object = localSettings.Values("RegIDE_register")
        Dim myIDE_Login As Object = localSettings.Values("RegIDE_Login")

        Dim myIDE_Done As Object = localSettings.Values("RegIDE_Done")
        Dim myIDE_AlredyExist As Object = localSettings.Values("RegIDE_AlredyExist")
        Dim myIDE_MissingField As Object = localSettings.Values("RegIDE_MissingFields")


        If Not myIDE_Name Is Nothing Then _myIDE_Name = myIDE_Name.ToString
        If Not myIDE_SurName Is Nothing Then _myIDE_SurName = myIDE_SurName.ToString
        If Not myIDE_EMail Is Nothing Then _myIDE_EMail = myIDE_EMail.ToString

        If Not myIDE_Company Is Nothing Then _myIDE_Company = myIDE_Company.ToString
        If Not myIDE_Register Is Nothing Then _myIDE_Register = myIDE_Register.ToString
        If Not myIDE_Login Is Nothing Then _myIDE_Login = myIDE_Login.ToString

        If Not myIDE_Done Is Nothing Then _myIDE_Done = myIDE_Done.ToString
        If Not myIDE_AlredyExist Is Nothing Then _myIDE_AlredyExist = myIDE_AlredyExist.ToString
        If Not myIDE_MissingField Is Nothing Then _myIDE_MissingField = myIDE_MissingField.ToString

        Me.Lbl_Name.Text = _myIDE_Name
        Me.Lbl_Surname.Text = _myIDE_SurName
        Me.Lbl_Email.Text = _myIDE_EMail

        Me.Lbl_Company.Text = _myIDE_Company
        Me.Btn_Register.Content = _myIDE_Register
        Me.Lbl_BackLogin.Content = _myIDE_Login
        '---------------------------------------------------------
        '  FINE  -  Recupero dei testi in lingua dei controlli
        '                 Ultima Lingua usata
        '---------------------------------------------------------

        '------------------------------------------
        ' Usco la variabile del Respond
        '------------------------------------------
        Dim _For_InvalidMail As Object = localSettings.Values("ForIDE_InvalidMail")
        If Not _For_InvalidMail Is Nothing Then m_For_InvalidMail = _For_InvalidMail

    End Sub

    Private Sub Registration_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        '------------------------------------------
        ' SETTAGGI LAYOUT
        '------------------------------------------

        Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)


        Me.Pnl_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)
        Me.Pnl_Centrale.CornerRadius = New CornerRadius(CDbl(App.Layout_Globale.pageLogin.loginPanelRadius))

        Me.Btn_Register.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)
        Me.Btn_Register.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBorderColor)
        Me.Btn_Register.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageLogin.loginBtnBorderWidth))
        'Me.Btn_Login.Radius = App.Layout_Globale.pageLogin.loginBtnRadius
        Me.Btn_Register.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnTxtColor)


        Me.Freccia_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)

        Me.Txt_Login_01.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)
        Me.Txt_Login_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)


        Me.Txt_Company.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
        Me.Txt_email.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
        Me.Txt_Name.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
        Me.Txt_Surname.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)


        Me.Txt_Company.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)
        Me.Txt_email.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)
        Me.Txt_Name.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)
        Me.Txt_Surname.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)

        Me.Txt_Company.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)
        Me.Txt_email.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)
        Me.Txt_Name.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)
        Me.Txt_Surname.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)

        'App.Layout_Globale.pageLogin.loginEditTxtRadius
        If CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth) > 0 Then
            Me.Txt_Company.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
            Me.Txt_email.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
            Me.Txt_Name.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
            Me.Txt_Surname.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
        End If

        Me.Lbl_BackLogin.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_Company.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_MessaggioLogin.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_Name.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_Surname.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_Email.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)



    End Sub
End Class
