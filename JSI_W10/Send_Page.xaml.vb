﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 
Imports JSI_Altab.Altab_WS


Public NotInheritable Class Send_Page
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

    Dim Oggetti_da_Inviare As List(Of File_Send.File_da_Inviare)
    Dim Testo_Vuoto_Brush As SolidColorBrush
    Dim Testo_Pieno_Brush As SolidColorBrush

    Dim m_Send_Heading As String = "EMAIL ADDRESS BELOW, THEN PRESS SEND:"
    Dim m_Send_PlaceHolder As String = "Email"
    Dim m_Send_Contact_Later As String = "Contact me later"
    Dim m_Send_Consent As String = "I indicate my consente to MODULA's Privacy Policy Terms"
    Dim m_Send_Policy_ERR As String = "You must accept the MODULS's Privacy Policy Terms"
    Dim m_Send_Send As String = "SEND"
    Dim m_Send_Close As String = "close"
    Dim m_Send_Thank As String = "Thank you!"

    '-------------------------------------------------------------------
    '-------------------------------------------------------------------
    '-------------------------------------------------------------------

    Dim m_Alert_Warning As String = "Warning"
    Dim m_Alert_Confirm As String = "Confirm"
    Dim m_Alert_Cancel As String = "Cancel"

    Dim m_Alert_Alert As String = "Warning"
    Dim m_Alert_Deleting_Entity As String = "You are about to delete an entity from th cart. Are you sure ?"
    Dim m_Alert_Email_Required As String = "The email address is required"

    Dim m_Alert_Email_Invalid As String = "Insert a valid email adress"
    Dim m_Alert_No_Entities As String = "there are no elements to send!"
    Dim m_Alert_Yes As String = "Yes"

    Dim m_Alert_NO As String = "No"
    Dim m_Alert_OK As String = "Ok"
    Dim m_Policy_Text As String = "Policy"



    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Oggetti_da_Inviare = New List(Of File_Send.File_da_Inviare)
        For Each Oggetto As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare

            Dim OggettoNuovo As New File_Send.File_da_Inviare
            OggettoNuovo.File_da_Inviare = Oggetto.File_da_Inviare
            OggettoNuovo.File_Thumbs = Oggetto.File_Thumbs
            OggettoNuovo.ID_Object = Oggetto.ID_Object
            OggettoNuovo.ID_Section = Oggetto.ID_Section
            OggettoNuovo.ID_Send = Oggetto.ID_Send
            OggettoNuovo.Title = Oggetto.Title
            Oggetti_da_Inviare.Add(OggettoNuovo)

        Next

    End Sub


    Private Async Sub Btn_Close_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Close.Click
        App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(Windows.Storage.ApplicationData.Current.LocalSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))
        Me.Frame.Navigate(GetType(The_Global_Page))

    End Sub

    Private Sub Send_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        Me.Btn_Close.Content = App.Bottone_Close
        Me.Btn_Send.Content = App.Bottone_Send

        Dim m_Heading As Object = localSettings.Values("SendIDE_Heading")
        Dim m_PlaceHolder As Object = localSettings.Values("SendIDE_PlaceHolder")
        Dim m_Contact_Later As Object = localSettings.Values("SendIDE_ContactLater")

        Dim m_Consent As Object = localSettings.Values("SendIDE_Consent")
        Dim m_Policy_ERR As Object = localSettings.Values("SendIDE_PolicyErr")
        Dim m_Send As Object = localSettings.Values("SendIDE_Send")

        Dim m_Close As Object = localSettings.Values("SendIDE_Close")
        Dim m_Thank As Object = localSettings.Values("SendIDE_Thank")

        Dim m_Policy As Object = localSettings.Values("PolIDE_Close")



        If Not m_Heading Is Nothing Then m_Send_Heading = m_Heading
        If Not m_PlaceHolder Is Nothing Then m_Send_PlaceHolder = m_PlaceHolder
        If Not m_Contact_Later Is Nothing Then m_Send_Contact_Later = m_Contact_Later

        If Not m_Consent Is Nothing Then m_Send_Consent = m_Consent
        If Not m_Policy_ERR Is Nothing Then m_Send_Policy_ERR = m_Policy_ERR
        If Not m_Send Is Nothing Then m_Send_Send = m_Send

        If Not m_Close Is Nothing Then m_Send_Close = m_Close
        If Not m_Thank Is Nothing Then m_Send_Thank = m_Thank

        If Not m_Policy Is Nothing Then m_Policy_Text = m_Policy


        Me.Tgl_Contact_OFF.Text = m_Send_Contact_Later
        Me.Tgl_Contact_ON.Text = m_Send_Contact_Later

        'Me.Tgl_Privacy_OFF.Text = m_Send_Consent
        'Me.Tgl_Privacy_ON.Text = m_Send_Consent
        'Me.Lnk_Privacy.Content = m_Send_Consent


        Me.Lbl_MailAddress.Text = m_Send_Heading


        'Me.Tgl_ContactMe.OffContent = m_Send_Contact_Later
        'Me.Tgl_ContactMe.OnContent = m_Send_Contact_Later

        'Me.Tgl_Privacy.OffContent = m_Send_Consent
        'Me.Tgl_Privacy.OffContent = m_Send_Consent

        Me.Bottone_Privacy.Content = m_Send_Consent

        Me.Btn_Close.Content = m_Send_Close
        Me.Btn_Close_Policy.Content = m_Send_Close
        Me.Btn_Send.Content = m_Send_Send

        '--------------------------------------------------------------------
        '--------------------------------------------------------------------
        '--------------------------------------------------------------------

        Dim m_Warning As Object = localSettings.Values("AleIDE_Warning")
        Dim m_Confirm As Object = localSettings.Values("AleIDE_Confirm")
        Dim m_Cancel As Object = localSettings.Values("AleIDE_Cancel")

        Dim m_Alert As Object = localSettings.Values("AleIDE_Alert")
        Dim m_Deleting_Entity As Object = localSettings.Values("AleIDE_DeleteEntity")
        Dim m_Email_Required As Object = localSettings.Values("AleIDE_EmailRequired")

        Dim m_Email_Invalid As Object = localSettings.Values("AleIDE_EmailInvalid")
        Dim m_No_Entities As Object = localSettings.Values("AleIDE_NoEntities")
        Dim m_Yes As Object = localSettings.Values("AleIDE_YES")

        Dim m_NO As Object = localSettings.Values("AleIDE_NO")
        Dim m_OK As Object = localSettings.Values("AleIDE_OK")


        If Not m_Warning Is Nothing Then m_Alert_Warning = m_Warning
        If Not m_Confirm Is Nothing Then m_Alert_Confirm = m_Confirm
        If Not m_Cancel Is Nothing Then m_Alert_Cancel = m_Cancel

        If Not m_Alert Is Nothing Then m_Alert_Alert = m_Alert
        If Not m_Deleting_Entity Is Nothing Then m_Alert_Deleting_Entity = m_Deleting_Entity
        If Not m_Email_Required Is Nothing Then m_Alert_Email_Required = m_Email_Required

        If Not m_Email_Invalid Is Nothing Then m_Alert_Email_Invalid = m_Email_Invalid
        If Not m_No_Entities Is Nothing Then m_Alert_No_Entities = m_No_Entities
        If Not m_Yes Is Nothing Then m_Yes = m_Alert_Yes

        If Not m_NO Is Nothing Then m_NO = m_Alert_NO
        If Not m_OK Is Nothing Then m_OK = m_Alert_OK


        '--------------------------------------------------------------------
        '--------------------------------------------------------------------
        '--------------------------------------------------------------------

        Testo_Pieno_Brush = Me.Txt_MailAddress.Foreground
        Testo_Vuoto_Brush = New SolidColorBrush(Windows.UI.Colors.Gray)
        Me.Txt_MailAddress.Foreground = Testo_Vuoto_Brush

        'If Funzioni_Generali.Connessione_Internet_Attiva Then
        Me.Btn_Send.Visibility = Visibility.Visible
        'Else
        '    Me.Btn_Send.Visibility = Visibility.Collapsed
        'End If

        Me.Lbl_Ritorno_Send.Visibility = Visibility.Collapsed
    End Sub

    'Private Sub Txt_MailAddress_GotFocus(sender As Object, e As RoutedEventArgs) Handles Txt_MailAddress.GotFocus
    '    If Me.Txt_MailAddress.Text = m_Send_PlaceHolder Then
    '        Me.Txt_MailAddress.Text = ""
    '        Me.Txt_MailAddress.Foreground = Testo_Pieno_Brush
    '    End If
    'End Sub

    'Private Sub Txt_MailAddress_LostFocus(sender As Object, e As RoutedEventArgs) Handles Txt_MailAddress.LostFocus
    '    If Me.Txt_MailAddress.Text.Trim.Length = 0 Then
    '        Me.Txt_MailAddress.Text = m_Send_PlaceHolder
    '        Me.Txt_MailAddress.Foreground = Testo_Vuoto_Brush
    '    End If
    'End Sub



    Private Async Sub Btn_Del_Click(sender As Object, e As RoutedEventArgs)

        Dim _MioBottone As Button = CType(sender, Button)
        Dim _idAccesso As String = _MioBottone.Name

        '------------------------------------------------------------
        '  Richiede conferma per la cancellazione di un item dalla lista
        '------------------------------------------------------------
        Dim Content As ContentDialog = New ContentDialog
        Content.Content = m_Alert_Deleting_Entity
        Content.PrimaryButtonText = m_Alert_Yes
        Content.SecondaryButtonText = m_Alert_NO
        Content.Title = m_Alert_Cancel

        Dim Ritorno As ContentDialogResult = Await Content.ShowAsync()
        If Ritorno = Ritorno.Primary Then

            For Each DaInviare As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare
                If DaInviare.ID_Send = _idAccesso Then

                    Funzioni_Dati.File_To_Send.Cancella_Un_Elemento_to_Send(DaInviare)
                    App.Oggetti_da_Inviare.Remove(DaInviare)

                    Exit For
                End If
            Next


            Me.Frame.Navigate(GetType(Send_Page))
        Else
            Exit Sub
        End If

    End Sub

    Private Async Sub Btn_Send_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Send.Click
        '------------------------------------------------------------
        ' Verifica Inserimento Email
        '------------------------------------------------------------
        If Me.Txt_MailAddress.Text.Trim.Length = 0 Then
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Alert_Email_Required
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync()
            Exit Sub
        End If

        '------------------------------------------------------------
        ' Verifica se ci sono oggetti selezionati
        '------------------------------------------------------------
        If Oggetti_da_Inviare.Count = 0 Then
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Alert_No_Entities
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync()
            Exit Sub
        End If

        '------------------------------------------------------------
        ' Verifica se sia stata accettata la privacy
        '------------------------------------------------------------
        If Not Me.Tgl_Privacy.IsOn Then
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Send_Policy_ERR
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync()
            Exit Sub
        End If


        '-----------------------------------------------------------------
        ' Crea Array degli Id_Entities
        '-----------------------------------------------------------------
        Dim Id_Selezionati As New ObservableCollection(Of Long)
        For Each Oggetto As File_Send.File_da_Inviare In Oggetti_da_Inviare
            Id_Selezionati.Add(Oggetto.ID_Object)
        Next

        '-----------------------------------------------------------------
        ' Crea Array delle email..e verifica correttezza
        '-----------------------------------------------------------------
        Dim EMail_Selezionate As New ObservableCollection(Of String)
        Dim MieEmail As String() = Me.Txt_MailAddress.Text.Split({CChar(","), CChar(";")})
        For Each MiaMail As String In MieEmail
            EMail_Selezionate.Add(MiaMail)
            If Not Funzioni_Generali.ValidaEmail(MiaMail) Then
                Dim Content As ContentDialog = New ContentDialog
                Content.Content = m_Alert_Email_Invalid
                Content.PrimaryButtonText = m_Alert_OK
                Content.Title = m_Alert_Warning

                Await Content.ShowAsync()
                Exit Sub
            End If
        Next

        If Funzioni_Generali.Connessione_Internet_Attiva Then

            '------------------------------------------------------------
            '  Procede all'invio
            '------------------------------------------------------------
            Dim client As MobileServiceClient = New MobileServiceClient()
            Dim _Ritorno As String = ""
            Try
                _Ritorno = Await client.SendMultipleAsync(localSettings.Values("Login_NoRete_HashID"), EMail_Selezionate, Id_Selezionati, Me.Txt_Note.Text)

                '------------------------------------
                ' Conversione del JSON in Struttura
                '------------------------------------
                Dim result = Newtonsoft.Json.JsonConvert.DeserializeObject(_Ritorno)
                Dim MioRitorno As List(Of File_Send.Risposta) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of File_Send.Risposta))(_Ritorno)


                Dim _EsisteErrore As Boolean = False
                For Each _RispostaRitorno As File_Send.Risposta In MioRitorno
                    If _RispostaRitorno.Status.ToString.ToUpper = "INVALID" Then
                        _EsisteErrore = True
                    End If
                Next

                Me.Grd_Send.Visibility = Visibility.Collapsed
                If _EsisteErrore Then
                    '-------------------------------------------------------------------
                    ' Individuazione di un errore nell'invio
                    '-------------------------------------------------------------------
                    Dim Content As ContentDialog = New ContentDialog
                    Content.Content = m_Alert_Warning
                    Content.PrimaryButtonText = m_Alert_OK
                    Content.Title = m_Alert_Warning

                    Await Content.ShowAsync()
                    Exit Sub
                Else
                    '-------------------------------------------------------------------
                    ' Invio andato a buon fine
                    '-------------------------------------------------------------------
                    Me.Pnl_Email.Visibility = Visibility.Collapsed
                    Me.Lbl_Ritorno_Send.Text = m_Send_Thank
                    Me.Lbl_Ritorno_Send.Visibility = Visibility.Visible
                    Me.Btn_Send.Visibility = Visibility.Collapsed
                    Me.Grd_Send.Visibility = Visibility.Collapsed
                    App.Oggetti_da_Inviare.Clear()


                    Funzioni_Dati.File_To_Send.Cancella_Tutti_Elementi_to_Send()



                    Await Task.Delay(3000)
                    Frame.Navigate(GetType(The_Home_Page))

                    Exit Sub

                End If

            Catch ex As Exception

            End Try

        Else
            '-------------------------------------------------------------------
            ' Inserisci Nelle Code di Invio
            '-------------------------------------------------------------------
            Inserisci_in_Waiting(Me.Txt_MailAddress.Text, Id_Selezionati)


            Me.Pnl_Email.Visibility = Visibility.Collapsed
            Me.Lbl_Ritorno_Send.Text = m_Send_Thank & vbCrLf & vbCrLf & " ... Will SEND when Internet Connection is Available .... "
            Me.Lbl_Ritorno_Send.Visibility = Visibility.Visible
            Me.Btn_Send.Visibility = Visibility.Collapsed
            Me.Grd_Send.Visibility = Visibility.Collapsed
            App.Oggetti_da_Inviare.Clear()

            Funzioni_Dati.File_To_Send.Cancella_Tutti_Elementi_to_Send()


            Await Task.Delay(5000)
            Frame.Navigate(GetType(The_Home_Page))

            Exit Sub

        End If

    End Sub


    Public Sub Inserisci_in_Waiting(ByVal MailAddress As String, ByVal Id_Selezionati As ObservableCollection(Of Long))

        Dim _CodiceUnivoco As String = CStr(Date.Now.Year).Trim & CStr(Date.Now.Month).Trim & CStr(Date.Now.Day).Trim & CStr(Date.Now.Hour).Trim & CStr(Date.Now.Minute).Trim & CStr(Date.Now.Second).Trim

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        For Indice As Integer = 0 To Id_Selezionati.Count - 1

            Dim _MY_to_Send As New Funzioni_Dati.Waiting_To_Send

            _MY_to_Send.ID_Selezionato = Id_Selezionati(Indice)
            _MY_to_Send.Tutte_Email_Selezionate = MailAddress
            _MY_to_Send.Codice_Univoco = _CodiceUnivoco
            _MY_to_Send.Posizione = (Indice + 1)


            Conn_SQL.Insert(_MY_to_Send)

        Next
    End Sub

    Private Sub Txt_MailAddress_TextChanging(sender As TextBox, args As TextBoxTextChangingEventArgs) Handles Txt_MailAddress.TextChanging

        If Txt_MailAddress Is Nothing Then
            Exit Sub
        End If
        If Txt_MailAddress.Text.EndsWith(" ") Then
            If Txt_MailAddress.Text.Length > 1 Then
                If Txt_MailAddress.Text.Substring(Txt_MailAddress.Text.Length - 2, 1) = "," Then

                Else
                    If Txt_MailAddress.Text.Substring(Txt_MailAddress.Text.Length - 2, 1) = " " Then
                        Me.Txt_MailAddress.Text = Txt_MailAddress.Text.Substring(0, Txt_MailAddress.Text.Length - 1).Trim
                    Else
                        Me.Txt_MailAddress.Text = Txt_MailAddress.Text.Substring(0, Txt_MailAddress.Text.Length - 1).Trim & " , "
                    End If
                    Txt_MailAddress.SelectionStart = Txt_MailAddress.Text.Length
                    Txt_MailAddress.SelectionLength = 0
                End If
            End If
            'Else
            '    Txt_MailAddress.Text = Txt_MailAddress.Text.Replace(" ", ",")
            '    Txt_MailAddress.SelectionStart = Txt_MailAddress.Text.Length
            '    Txt_MailAddress.SelectionLength = 0
        End If
    End Sub

    Private Sub Send_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Me.Txt_MailAddress.PlaceholderText = m_Send_PlaceHolder
    End Sub

    'Private Async Sub Lnk_Privacy_Click(sender As Object, e As RoutedEventArgs) Handles Lnk_Privacy.Click


    ''-------------------------------------------------------------------
    '' Individuazione di un errore nell'invio
    '''-------------------------------------------------------------------
    'Dim Content As ContentDialog = New ContentDialog
    '    'Content.Content = m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf)
    '    'Content.Content &= m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf)

    '    'Content.Content = m_Policy_Text.Replace("<br>", vbCrLf)
    '    Content.Content = App.Testo_Privacy.Replace("<br>", vbCrLf)


    '    Content.PrimaryButtonText = m_Alert_OK

    '    'Content.MaxHeight = 500

    '    Content.Title = "" 'm_Alert_Warning
    '    Content.Background = New SolidColorBrush(Windows.UI.Colors.DeepSkyBlue)
    '    Content.Foreground = New SolidColorBrush(Windows.UI.Colors.White)

    '    Await Content.ShowAsync()



    '    'Dim MyMessage As New Windows.UI.Popups.MessageDialog(Content.Content)
    '    'Await MyMessage.ShowAsync()


    '    'Me.Scrolla.Text = Content.Content
    '    'Await Me.MyContentDialog.ShowAsync

    '    'Exit Sub

    '    'Me.lbl_Policy.Text = m_Policy_Text.Replace("<br>", vbCrLf)
    '    'Me.Pnl_Privacy.Visibility = Visibility.Visible

    'End Sub

    Private Async Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Me.Label_Privacy.Text = App.Testo_Privacy.Replace("<br>", vbCrLf)
        Me.Label_Privacy.Text = Me.Label_Privacy.Text.Replace("<BR>", vbCrLf)

        '''-------------------------------------------------------------------
        ''' Individuazione di un errore nell'invio
        '''-------------------------------------------------------------------
        'Dim Content As ContentDialog = New ContentDialog
        ''Content.Content = m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf)
        ''Content.Content &= m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf) & m_Policy_Text.Replace("<br>", vbCrLf)

        ''Content.Content = m_Policy_Text.Replace("<br>", vbCrLf)
        'Content.Content = App.Testo_Privacy.Replace("<br>", vbCrLf)


        'Content.PrimaryButtonText = m_Alert_OK

        ''Content.MaxHeight = 500

        'Content.Title = "" 'm_Alert_Warning
        'Content.Background = New SolidColorBrush(Windows.UI.Colors.DeepSkyBlue)
        'Content.Foreground = New SolidColorBrush(Windows.UI.Colors.White)

        'Await Content.ShowAsync()

    End Sub

    Private Sub Btn_Close_Policy_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Close_Policy.Click
        'Me.Bottone_Privacy.Focus(FocusState.Programmatic)
        Me.Bottone_Privacy_Click(Nothing, Nothing)
        Me.Fly_Policy.Hide()
    End Sub

    Private Sub Bottone_Privacy_Click(sender As Object, e As RoutedEventArgs) Handles Bottone_Privacy.Click
        '    Me.Pnl_Privacy.Visibility = Visibility.Collapsed

    End Sub

    Private Sub Send_Page_Loading(sender As Object, e As RoutedEventArgs) Handles Me.Loading


        If Not App.Layout_Globale Is Nothing Then

            '-----------------------------
            ' Globale
            '-----------------------------
            Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Stack_01.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Stack_02.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Pnl_Email.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Grd_Globale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Grd_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Griglia_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Griglia_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Pnl_Iniziale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.headerColor)

            Me.Stack_Privacy.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Label_Privacy.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Label_Privacy.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.privacyTextColor)


            '-----------------------------
            ' Bottoni
            '-----------------------------
            'Me.Tgl_ContactMe.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBkgColor)
            'Me.Tgl_ContactMe.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBkgColor)


            'Me.Tgl_Privacy.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBkgColor)
            'Me.Tgl_Privacy.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBkgColor)

            Application.Current.Resources("ToggleSwitchFillOnPointerOver") = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendTitleTxtColor)
            Application.Current.Resources("ToggleSwitchFillOn") = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendTitleTxtColor)
            Me.Btn_Close_Policy.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnBkgColor)
            Me.Btn_Close_Policy.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnBorderColor)
            Me.Btn_Close_Policy.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageSend.closeBtnBorderWidth))
            Me.Btn_Close_Policy.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnTxtColor)


            Me.Btn_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBkgColor)
            Me.Btn_Send.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnBorderColor)
            'Me.Btn_Send.Background = App.Layout_Globale.pageSend.sendBtnBorderRadius
            Me.Btn_Send.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageSend.sendBtnBorderWidth))
            Me.Btn_Send.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendBtnTxtColor)

            'App.Layout_Globale.pageSend.contactTextColor

            Me.Btn_Close.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnBkgColor)
            Me.Btn_Close.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnBorderColor)
            'Me.Btn_Close.Background = App.Layout_Globale.pageSend.closeBtnBorderRadius
            Me.Btn_Close.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageSend.closeBtnBorderWidth))
            Me.Btn_Close.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.closeBtnTxtColor)


            Me.Lbl_MailAddress.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendTitleTxtColor)
            Me.Tgl_ContactMe.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.contactTextColor)
            Me.Tgl_Privacy.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.privacyTextColor)

            Me.Bottone_Privacy.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.sendTitleTxtColor)



            Me.Txt_Note.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.commentsBkgColor)
            Me.Txt_Note.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.commentsBorderColor)
            'Me.Txt_Note.Background = App.Layout_Globale.pageSend.commentsBorderRadius
            Me.Txt_Note.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageSend.commentsBorderWidth))
            'Me.Txt_Note.Background = App.Layout_Globale.pageSend.commentsPlcColor
            Me.Txt_Note.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.commentsTextColor)

            'App.Layout_Globale.pageSend.boxEmailBkg


            Me.Txt_MailAddress.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.emailBkgColor)
            Me.Txt_MailAddress.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.emailBorderColor)
            'Me.Txt_MailAddress.Background = App.Layout_Globale.pageSend.emailBorderRadius
            Me.Txt_MailAddress.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageSend.emailBorderWidth))
            'Me.Txt_MailAddress.Background = App.Layout_Globale.pageSend.emailPlcColor
            Me.Txt_MailAddress.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageSend.emailTextColor)

        End If
    End Sub
End Class

