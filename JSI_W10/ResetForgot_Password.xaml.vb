﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 

Imports JSI_Altab.Altab_WS


Public NotInheritable Class ResetForgot_Password
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder
    Dim _ConnessionePresente As Boolean = True

    Dim m_For_Email As String = "e-mail"
    Dim m_For_SubMit As String = "Submit"
    Dim m_For_ToLogin As String = "Back to login"
    Dim m_For_Done As String = "Request submitted correctly!"
    Dim m_For_UserNotFound As String = "User does not exist!"
    Dim m_For_InsertMail As String = "Email address not provided!"
    Dim m_For_InvalidMail As String = "Bad email address provided!"


    Private Sub ResetForgot_Password_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        Dim _For_Email As Object = localSettings.Values("ForIDE_Email")
        Dim _For_SubMit As Object = localSettings.Values("ForIDE_Submit")
        Dim _For_ToLogin As Object = localSettings.Values("ForIDE_ToLogin")

        Dim _For_Done As Object = localSettings.Values("ForIDE_Done")
        Dim _For_UserNotFound As Object = localSettings.Values("ForIDE_UserNotFound")
        Dim _For_InsertMail As Object = localSettings.Values("ForIDE_InsertMail")
        Dim _For_InvalidMail As Object = localSettings.Values("ForIDE_InvalidMail")

        If Not _For_Email Is Nothing Then m_For_Email = _For_Email
        If Not _For_SubMit Is Nothing Then m_For_SubMit = _For_SubMit
        If Not _For_ToLogin Is Nothing Then m_For_ToLogin = _For_ToLogin

        If Not _For_Done Is Nothing Then m_For_Done = _For_Done
        If Not _For_UserNotFound Is Nothing Then m_For_UserNotFound = _For_UserNotFound
        If Not _For_InsertMail Is Nothing Then m_For_InsertMail = _For_InsertMail
        If Not _For_InvalidMail Is Nothing Then m_For_InvalidMail = _For_InvalidMail


        Me.Lbl_BackLogin.Content = m_For_ToLogin
        Me.Lbl_Email.Text = m_For_Email
        Me.Btn_Register.Content = m_For_SubMit

    End Sub

    Private Sub Btn_Register_LayoutUpdated(sender As Object, e As Object) Handles Btn_Register.Click

        If Me.Txt_email.Text.Trim.Length = 0 Then
            Me.Lbl_MessaggioLogin.Text = m_For_InsertMail
            Exit Sub
        End If

        If Not Funzioni_Generali.ValidaEmail(Me.Txt_email.Text) Then
            Me.Lbl_MessaggioLogin.Text = m_For_InvalidMail
            Exit Sub
        End If

        Dim _UserHash As Object = localSettings.Values("Login_NoRete_RootHashID")
        If _UserHash Is Nothing Then
            Exit Sub
        End If

        Dim client As MobileServiceClient = New MobileServiceClient()
        Dim _Ritorno As Task(Of String) = client.ResetPasswordSystemAsync(_UserHash, Me.Txt_email.Text)
        _Ritorno.Wait()

        Dim json As String = _Ritorno.Result

        Dim MiaRisposta As Funzioni_Login.Response_Message = Funzioni_Login.Convert_JSON_ReturnedFrom_ResetPassword(json)
        Dim _Messaggio As String = ""
        If MiaRisposta.p_Status.ToUpper = "OK" Then
            _Messaggio = m_For_Done
        Else
            _Messaggio = m_For_UserNotFound
        End If
        Me.Lbl_MessaggioLogin.Text = _Messaggio


    End Sub

    Private Sub Txt_email_GotFocus(sender As Object, e As RoutedEventArgs) Handles Txt_email.GotFocus
        Me.Lbl_MessaggioLogin.Text = ""
    End Sub

    Private Sub Lbl_BackLogin_Click(sender As Object, e As RoutedEventArgs) Handles Lbl_BackLogin.Click
        '---------------------------------------------------------
        ' Ritorno al Login
        '---------------------------------------------------------
        Frame.Navigate(GetType(Login_Page))
    End Sub

    Private Sub ResetForgot_Password_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        '------------------------------------------
        ' SETTAGGI LAYOUT
        '------------------------------------------

        Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)


        Me.Pnl_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.panelColor)
        Me.Pnl_Centrale.CornerRadius = New CornerRadius(CDbl(App.Layout_Globale.pageLogin.loginPanelRadius))

        Me.Btn_Register.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)
        Me.Btn_Register.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBorderColor)
        Me.Btn_Register.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageLogin.loginBtnBorderWidth))
        'Me.Btn_Login.Radius = App.Layout_Globale.pageLogin.loginBtnRadius
        Me.Btn_Register.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnTxtColor)


        Me.Freccia_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginBtnBkgColor)

        Me.Txt_Login_01.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)
        Me.Txt_Login_02.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginTitleColor)


        Me.Txt_email.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditBkgColor)
        Me.Txt_email.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtBorderColor)
        Me.Txt_email.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginEditTxtColor)

        If CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth) > 0 Then
            Me.Txt_email.FontSize = CDbl(App.Layout_Globale.pageLogin.loginEditTxtWidth)
        End If

        Me.Lbl_BackLogin.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_MessaggioLogin.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)
        Me.Lbl_Email.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageLogin.loginLblTxtColor)


    End Sub
End Class
