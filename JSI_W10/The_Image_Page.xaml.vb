﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

Imports Windows.Storage
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class The_Image_Page
    Inherits Page

    Dim Gestiscilo As Boolean = False
    Dim m_Alert_Warning As String = "Warning"
    Dim m_Alert_OK As String = "Ok"
    Dim m_Log_DataMissing As String = "You have to connect the app to the internet at the least one time before using it."
    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings


    Private Sub Imm_Visualizzata_ManipulationCompleted(sender As Object, e As ManipulationCompletedRoutedEventArgs) Handles Imm_Visualizzata.ManipulationCompleted
        Imm_Visualizzata.Opacity = 1
    End Sub

    Private Sub Imm_Visualizzata_ManipulationDelta(sender As Object, e As ManipulationDeltaRoutedEventArgs) Handles Imm_Visualizzata.ManipulationDelta
        'Image_Transform.TranslateX += e.Delta.Translation.X
        'Image_Transform.TranslateY += e.Delta.Translation.Y
    End Sub

    Private Sub Imm_Visualizzata_ManipulationStarted(sender As Object, e As ManipulationStartedRoutedEventArgs) Handles Imm_Visualizzata.ManipulationStarted
        Imm_Visualizzata.Opacity = 0.5
    End Sub

    Private Sub Imm_Visualizzata_PointerWheelChanged(sender As Object, e As PointerRoutedEventArgs) Handles Imm_Visualizzata.PointerWheelChanged


        '------------------------------------
        ' VIDEO di Riferimento
        '****************************************
        'https://www.youtube.com/watch?v=eaafmJhJXos
        '
        '****************************************
        '
        ' https://www.youtube.com/watch?v=U8gact4pLMo
        '****************************************
        ' VIDEO di Riferimento
        '------------------------------------

        Dim Delta_Scroll As Double = -1 * e.GetCurrentPoint(Imm_Visualizzata).Properties.MouseWheelDelta
        Dim posX As Double = e.GetCurrentPoint(Imm_Visualizzata).Position.X
        Dim posY As Double = e.GetCurrentPoint(Imm_Visualizzata).Position.Y

        Dim Now_Width As Double = Imm_Visualizzata.ActualWidth
        Dim Now_Height As Double = Imm_Visualizzata.ActualHeight

        If Delta_Scroll < 0 Then
            Delta_Scroll = 0.8
        Else
            Delta_Scroll = 1.2
        End If

        Dim New_ScaleX As Double = Image_Transform.ScaleX * Delta_Scroll
        Dim New_ScaleY As Double = Image_Transform.ScaleY * Delta_Scroll


        Dim NewTraslate_Scale_X As Double = 0
        Dim NewTraslate_Scale_Y As Double = 0

        Dim Modificatore As Double = 0
        If Delta_Scroll > 1 Then
            Modificatore = 0.2
        Else
            Modificatore = -0.2
        End If
        NewTraslate_Scale_X = (Image_Transform.TranslateX - (posX * Modificatore * Image_Transform.ScaleX))
        NewTraslate_Scale_Y = (Image_Transform.TranslateY - (posX * Modificatore * Image_Transform.ScaleY))

        'If New_ScaleX <= 0.4 OrElse New_ScaleY <= 0.4 Then
        '    New_ScaleX = 0.4
        '    New_ScaleY = 0.4
        '    NewTraslate_Scale_X = 0
        '    NewTraslate_Scale_Y = 0
        'End If
        If New_ScaleX <= 1 OrElse New_ScaleY <= 1 Then
            New_ScaleX = 1
            New_ScaleY = 1
            NewTraslate_Scale_X = 0
            NewTraslate_Scale_Y = 0
        End If

        If New_ScaleX > 1 Then
            MyScrollViewer.HorizontalScrollMode = ScrollMode.Auto
            MyScrollViewer.VerticalScrollMode = ScrollMode.Auto
        Else
            MyScrollViewer.HorizontalScrollMode = ScrollMode.Disabled
            MyScrollViewer.VerticalScrollMode = ScrollMode.Disabled
        End If

        Image_Transform.ScaleX = New_ScaleX
        Image_Transform.ScaleY = New_ScaleY
        Image_Transform.TranslateX = NewTraslate_Scale_X
        Image_Transform.TranslateY = NewTraslate_Scale_Y



    End Sub

    Private Sub The_Image_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        Me.Btn_Close.Content = App.Bottone_Close
        Me.Btn_AddCart.Content = App.Bottone_Add

        Me.Lbl_Titolo.Text = App._Image_Title
        Me.Imm_Visualizzata.Source = New BitmapImage(New Uri(App._Image_Path))

        'Me.Imm_Visualizzata.Height = Me.MyScrollViewer.Height
        'Me.Imm_Visualizzata.Width = Me.MyScrollViewer.Width


        Me.Btn_AddCart.Content = App.Bottone_Add
        For Each DaInviare As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare
            If DaInviare.ID_Section = App.Selected_IdSector AndAlso DaInviare.ID_Object = App.Selected_Id Then
                Me.Btn_AddCart.Content = App.Bottone_Saved
                Exit For
            End If
        Next


        Dim Alert_Warning As Object = localSettings.Values("AleIDE_Warning")
        Dim Alert_OK As Object = localSettings.Values("AleIDE_OK")
        Dim LOG_DataMissing As Object = localSettings.Values("AleIDE_NoConnection")


        If Not Alert_Warning Is Nothing Then m_Alert_Warning = Alert_Warning
        If Not Alert_OK Is Nothing Then m_Alert_OK = Alert_OK
        If Not LOG_DataMissing Is Nothing Then m_Log_DataMissing = LOG_DataMissing


        If Not App.Layout_Globale Is Nothing Then

            Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Griglia_Immagine.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Btn_AddCart.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageAddBtnBkgColor)
            Me.Btn_AddCart.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageAddBtnBorderColor)
            'Me.Btn_AddCart.Background = App.Layout_Globale.pageBrochure.brochureAddBtnBorderRadius
            Me.Btn_AddCart.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageImage.imageAddBtnBorderWidth))
            Me.Btn_AddCart.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageAddBtnTxtColor)


            Me.Btn_Close.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnBkgColor)
            Me.Btn_Close.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnBorderColor)
            'Me.Btn_Close.Background = App.Layout_Globale.pageBrochure.brochureCloseBtnBorderRadius
            Me.Btn_Close.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageImage.imageCloseBtnBorderWidth))
            Me.Btn_Close.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnTxtColor)

            Me.Pnl_Repeat.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnBkgColor)
            Me.Pnl_Repeat.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnBorderColor)
            'Me.Btn_Close.Background = App.Layout_Globale.pageBrochure.brochureCloseBtnBorderRadius
            Me.Pnl_Repeat.BorderThickness = New Thickness(CDbl(App.Layout_Globale.pageImage.imageCloseBtnBorderWidth))

            Me.Lbl_repeat.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageCloseBtnTxtColor)


            'App.Layout_Globale.pageBrochure.brochureHeaderBkgColor

            'App.Layout_Globale.pageBrochure.brochureSavedBtnBkgColor
            'App.Layout_Globale.pageBrochure.brochureSavedBtnBorderColor
            'App.Layout_Globale.pageBrochure.brochureSavedBtnBorderRadius
            'App.Layout_Globale.pageBrochure.brochureSavedBtnBorderWidth
            'App.Layout_Globale.pageBrochure.brochureSavedBtnTxtColor

            Me.Lbl_Titolo.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageImage.imageTitleTxt)

        End If



    End Sub

    Private Async Sub Btn_Close_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Close.Click

        If App.Visualizzo_Grid_Ricerca = True Then
            'If Me.Frame.CanGoBack Then
            '    Me.Frame.GoBack()
            'End If
            Me.Frame.Navigate(GetType(The_Global_Page))
        Else
            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(Windows.Storage.ApplicationData.Current.LocalSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))
            Me.Frame.Navigate(GetType(The_Global_Page))
        End If

    End Sub

    Private Sub Btn_AddCart_Click(sender As Object, e As RoutedEventArgs) Handles Btn_AddCart.Click

        If Me.Btn_AddCart.Content.ToString.ToUpper = App.Bottone_Add.Trim.ToUpper Then

            Me.Btn_AddCart.Content = App.Bottone_Saved

            Dim _OggettoInvio As New Funzioni_Dati.File_da_Inviare
            _OggettoInvio.ID_Send = Funzioni_Generali.MaxId(App.Oggetti_da_Inviare, "Id_Send") + 1
            _OggettoInvio.File_da_Inviare = App.Selected_Object.File_Reale
            _OggettoInvio.File_Thumbs = App.Selected_Object.Thumbs
            _OggettoInvio.ID_Object = App.Selected_Object.ID
            _OggettoInvio.ID_Section = App.Selected_Object.IDSection
            _OggettoInvio.Title = App.Selected_Object.Title
            App.Oggetti_da_Inviare.Add(_OggettoInvio)

            Funzioni_Dati.File_To_Send.Aggiungi_Un_Elemento_to_Send(_OggettoInvio)

        Else

            For Each DaInviare As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare
                If DaInviare.ID_Section = App.Selected_Object.IDSection AndAlso DaInviare.ID_Object = App.Selected_Object.ID Then

                    Funzioni_Dati.File_To_Send.Cancella_Un_Elemento_to_Send(DaInviare)
                    App.Oggetti_da_Inviare.Remove(DaInviare)

                    Exit For
                End If
            Next

            Me.Btn_AddCart.Content = App.Bottone_Add
        End If

    End Sub

    'Private Sub Imm_Visualizzata_SizeChanged(sender As Object, e As SizeChangedEventArgs) Handles Imm_Visualizzata.SizeChanged


    '    Dim Delta_Scroll As Double = 0  ' -1 * e.GetCurrentPoint(Imm_Visualizzata).Properties.MouseWheelDelta
    '    Dim posX As Double = 0  ' e.GetCurrentPoint(Imm_Visualizzata).Position.X
    '    Dim posY As Double = 0  ' e.GetCurrentPoint(Imm_Visualizzata).Position.Y


    '    Dim Now_Width As Double = Imm_Visualizzata.ActualWidth
    '    Dim Now_Height As Double = Imm_Visualizzata.ActualHeight

    '    If Delta_Scroll < 0 Then
    '        Delta_Scroll = 0.8
    '    Else
    '        Delta_Scroll = 1.2
    '    End If

    '    Dim New_ScaleX As Double = Image_Transform.ScaleX * Delta_Scroll
    '    Dim New_ScaleY As Double = Image_Transform.ScaleY * Delta_Scroll


    '    Dim NewTraslate_Scale_X As Double = 0
    '    Dim NewTraslate_Scale_Y As Double = 0

    '    Dim Modificatore As Double = 0
    '    If Delta_Scroll > 1 Then
    '        Modificatore = 0.2
    '    Else
    '        Modificatore = -0.2
    '    End If
    '    NewTraslate_Scale_X = (Image_Transform.TranslateX - (posX * Modificatore * Image_Transform.ScaleX))
    '    NewTraslate_Scale_Y = (Image_Transform.TranslateY - (posX * Modificatore * Image_Transform.ScaleY))

    '    'If New_ScaleX <= 0.4 OrElse New_ScaleY <= 0.4 Then
    '    '    New_ScaleX = 0.4
    '    '    New_ScaleY = 0.4
    '    '    NewTraslate_Scale_X = 0
    '    '    NewTraslate_Scale_Y = 0
    '    'End If
    '    If New_ScaleX <= 1 OrElse New_ScaleY <= 1 Then
    '        New_ScaleX = 1
    '        New_ScaleY = 1
    '        NewTraslate_Scale_X = 0
    '        NewTraslate_Scale_Y = 0
    '    End If

    '    If New_ScaleX > 1 Then
    '        MyScrollViewer.HorizontalScrollMode = ScrollMode.Auto
    '        MyScrollViewer.VerticalScrollMode = ScrollMode.Auto
    '    Else
    '        MyScrollViewer.HorizontalScrollMode = ScrollMode.Disabled
    '        MyScrollViewer.VerticalScrollMode = ScrollMode.Disabled
    '    End If

    '    Image_Transform.ScaleX = New_ScaleX
    '    Image_Transform.ScaleY = New_ScaleY
    '    Image_Transform.TranslateX = NewTraslate_Scale_X
    '    Image_Transform.TranslateY = NewTraslate_Scale_Y

    'End Sub

    Private Sub The_Image_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        Dim bounds As Rect = ApplicationView.GetForCurrentView().VisibleBounds
        Dim scaleFactor As Double = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel
        'Me.Imm_Visualizzata.Width = (bounds.Width * scaleFactor) - 80
        'Me.Imm_Visualizzata.Height = (bounds.Height * scaleFactor) - 150
        Me.Imm_Visualizzata.Width = (bounds.Width) - 80
        Me.Imm_Visualizzata.Height = (bounds.Height) - 150

        Gestiscilo = True

        Me.The_Image_Page_LayoutUpdated(Nothing, Nothing)

        If Funzioni_Generali.Connessione_Internet_Attiva Then
            Me.Lbl_repeat.Visibility = Visibility.Visible
            Me.Img_Repeat.Visibility = Visibility.Visible
            Me.Pnl_Repeat.Visibility = Visibility.Visible
        Else
            Me.Lbl_repeat.Visibility = Visibility.Collapsed
            Me.Img_Repeat.Visibility = Visibility.Collapsed
            Me.Pnl_Repeat.Visibility = Visibility.Collapsed
        End If

        GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(1, App.Selected_Object.Title)  'Titolo
        GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(2, App.Selected_Object.ID)  'ID
        GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(3, App.m_Lang_NoRete_Id)  ' Lingua
        GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(4, App.m_Username)  'Username
        'GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(5, "")  'Testo
        GoogleAnalytics.EasyTracker.GetTracker().SendView("Image")



        'Dim _TrakerId As String = "UA-96098690-2"
        'Dim MyTraker As GoogleAnalytics.Tracker = GoogleAnalytics.AnalyticsManager.Current.CreateTracker(_TrakerId)
        'MyTraker.ScreenName = "Image3"

        ''Dim Hit = GoogleAnalytics.HitBuilder.CreateScreenView.Build
        ''Hit.Add(1, App.Selected_Object.Title)
        ''Hit.Add(2, App.Selected_Object.ID)  'ID
        ''Hit.Add(3, App.m_Lang_NoRete_Id)  ' Lingua
        ''Hit.Add(4, App.m_Username)  'Username

        'Dim Vista = GoogleAnalytics.HitBuilder.CreateScreenView
        'Vista.SetCustomDimension(1, App.Selected_Object.Title)
        'Vista.SetCustomDimension(2, App.Selected_Object.ID)
        ''Dim Hit = Vista.Build()

        ''MyTraker.Send(Vista)
        'MyTraker.Send(Vista.Build)


    End Sub

    Private Sub The_Image_Page_LayoutUpdated(sender As Object, e As Object) Handles Me.LayoutUpdated

        If Gestiscilo AndAlso IsMobile Then
            Try
                Dim bounds As Rect = ApplicationView.GetForCurrentView().VisibleBounds
                Dim scaleFactor As Double = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel

                Me.Imm_Visualizzata.Width = (bounds.Width) - 80
                Me.Imm_Visualizzata.Height = (bounds.Height) - 150
                Me.Imm_Visualizzata.Source = New BitmapImage(New Uri(App._Image_Path))
                Gestiscilo = False
            Catch ex As Exception

            End Try
        Else
            Gestiscilo = True
        End If
        'Dim Pippo As String = ""

    End Sub

    Public Shared ReadOnly Property IsMobile() As Boolean
        Get
            Dim qualifiers = Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().QualifierValues
            Return (qualifiers.ContainsKey("DeviceFamily") AndAlso qualifiers("DeviceFamily") = "Mobile")
        End Get
    End Property

    'Private Async Sub Btn_Repeat_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Repeat.Click

    '    If Me.Btn_Repeat.Content = "** Downloading **" Then
    '        Exit Sub
    '    End If


    '    Dim _testo As String = Me.Btn_Repeat.Content.ToString
    '    Me.Btn_Repeat.Content = "** Downloading **"
    '    Me.Btn_Repeat.Background = Funzioni_Generali.GetSolidColorBrush("#CFDFF0")

    '    '-------------------------------------------------------------
    '    '-------------------------------------------------------------

    '    Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"
    '    Dim Stringa_Thumb As String = App.Selected_Object.File_Reale_SoloFile
    '    Dim _InputUrl As String = _UrlBase & "Images/" & Stringa_Thumb
    '    Dim VideoFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))

    '    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
    '    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
    '    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

    '    Await (Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename))

    '    Me.Btn_Repeat.Background = Me.Btn_Close.Background
    '    Me.Btn_Repeat.Content = _testo

    '    The_Image_Page_Loading(Nothing, Nothing)


    '    '-------------------------------------------------------------
    '    '-------------------------------------------------------------


    'End Sub


    Private Sub Image_Tapped(sender As Object, e As TappedRoutedEventArgs)
        Lbl_repeat_Tapped(Nothing, Nothing)
    End Sub

    Private Async Sub Lbl_repeat_Tapped(sender As Object, e As TappedRoutedEventArgs) Handles Lbl_repeat.Tapped

        If Not Funzioni_Generali.Connessione_Internet_Attiva Then
            '-------------------------------------------------------------------
            ' Individuazione di un errore nell'invio
            '-------------------------------------------------------------------
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Log_DataMissing
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync
            Exit Sub
        End If

        If Me.Lbl_repeat.Text = "** Downloading **" Then
            Exit Sub
        End If


        Dim _testo As String = Me.Lbl_repeat.Text.ToString
        Me.Lbl_repeat.Text = "** Downloading **"
        Me.Lbl_repeat.Foreground = Funzioni_Generali.GetSolidColorBrush("#CFDFF0")

        '-------------------------------------------------------------
        '-------------------------------------------------------------

        Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"
        Dim Stringa_Thumb As String = App.Selected_Object.File_Reale_SoloFile
        Dim _InputUrl As String = _UrlBase & "Images/" & Stringa_Thumb
        Dim VideoFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))

        Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
        Dim Lungo As Integer = Stringa_Thumb.Trim.Length
        Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

        Try
            Await (Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename))

            Me.Lbl_repeat.Foreground = Me.Btn_Close.Foreground
            Me.Lbl_repeat.Text = _testo

            The_Image_Page_Loading(Nothing, Nothing)
        Catch ex As Exception
        End Try


        '-------------------------------------------------------------
        '-------------------------------------------------------------


    End Sub

End Class
