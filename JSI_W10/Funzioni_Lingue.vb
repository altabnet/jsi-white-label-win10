﻿Imports JSI_Altab.Altab_WS
Imports Windows.Storage

Public Class Funzioni_Lingue


    Public Class Controlli

        '----------------------------------------------------------------
        '----------------------------------------------------------------
        ' Template
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Temp_Languages As String = "Languages"
        Dim m_Temp_Results_For As String = "Results for"
        Dim m_Temp_No_Result As String = "Your Search '{{ serachText }}' did not match any documents. "
        Dim m_Temp_Home As String = "Homepage"
        Dim m_Temp_Logout As String = "Logout"
        Dim m_Temp_Stats As String = "Stats"

        Public Property p_Temp_Languages As String
            Get
                Return m_Temp_Languages
            End Get
            Set(value As String)
                m_Temp_Languages = value
            End Set
        End Property

        Public Property p_Temp_Results_For As String
            Get
                Return m_Temp_Results_For
            End Get
            Set(value As String)
                m_Temp_Results_For = value
            End Set
        End Property

        Public Property p_Temp_No_Result As String
            Get
                Return m_Temp_No_Result
            End Get
            Set(value As String)
                m_Temp_No_Result = value
            End Set
        End Property

        Public Property p_Temp_Home As String
            Get
                Return m_Temp_Home
            End Get
            Set(value As String)
                m_Temp_Home = value
            End Set
        End Property

        Public Property p_Temp_Logout As String
            Get
                Return m_Temp_Logout
            End Get
            Set(value As String)
                m_Temp_Logout = value
            End Set
        End Property

        Public Property p_Temp_Stats As String
            Get
                Return m_Temp_Stats
            End Get
            Set(value As String)
                m_Temp_Stats = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        ' LOGIN
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Log_User As String = "Username"
        Dim m_Log_Pwd As String = "Password"
        Dim m_Log_Rem As String = "Remember"
        Dim m_Log_Log As String = "Login"
        Dim m_Log_For As String = "Forgot/change password"
        Dim m_Log_Reg As String = "Click here to register"
        Dim m_Log_Done As String = "Thank You! Your submission has benn received!"
        Dim m_Log_Failed As String = "Ooops! Something went wrong while submitting the form"
        Dim m_Log_Invalid As String = "Invalid username and/or password"
        Dim m_Log_Misssing As String = "UserName or Password missing"
        Dim m_Log_Skip As String = "Skip"
        Dim m_Log_DataMissing As String = "You have to connect the app to the internet at the least one time before using it."

        Public Property p_Log_User As String
            Get
                Return m_Log_User
            End Get
            Set(value As String)
                m_Log_User = value
            End Set
        End Property

        Public Property p_Log_Pwd As String
            Get
                Return m_Log_Pwd
            End Get
            Set(value As String)
                m_Log_Pwd = value
            End Set
        End Property

        Public Property p_Log_Rem As String
            Get
                Return m_Log_Rem
            End Get
            Set(value As String)
                m_Log_Rem = value
            End Set
        End Property

        Public Property p_Log_Log As String
            Get
                Return m_Log_Log
            End Get
            Set(value As String)
                m_Log_Log = value
            End Set
        End Property

        Public Property p_Log_For As String
            Get
                Return m_Log_For
            End Get
            Set(value As String)
                m_Log_For = value
            End Set
        End Property

        Public Property p_Log_Reg As String
            Get
                Return m_Log_Reg
            End Get
            Set(value As String)
                m_Log_Reg = value
            End Set
        End Property

        Public Property p_Log_Done As String
            Get
                Return m_Log_Done
            End Get
            Set(value As String)
                m_Log_Done = value
            End Set
        End Property

        Public Property p_Log_Failed As String
            Get
                Return m_Log_Failed
            End Get
            Set(value As String)
                m_Log_Failed = value
            End Set
        End Property

        Public Property p_Log_Invalid As String
            Get
                Return m_Log_Invalid
            End Get
            Set(value As String)
                m_Log_Invalid = value
            End Set
        End Property

        Public Property p_Log_Misssing As String
            Get
                Return m_Log_Misssing
            End Get
            Set(value As String)
                m_Log_Misssing = value
            End Set
        End Property

        Public Property p_Log_Skip As String
            Get
                Return m_Log_Skip
            End Get
            Set(value As String)
                m_Log_Skip = value
            End Set
        End Property

        Public Property p_Log_DataMissing As String
            Get
                Return m_Log_DataMissing
            End Get
            Set(value As String)
                m_Log_DataMissing = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        ' Register
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Reg_Name As String = "Name"
        Dim m_Reg_SurName As String = "Surname"
        Dim m_Reg_EMail As String = "e-mail"
        Dim m_Reg_Company As String = "Company"
        Dim m_Reg_Register As String = "Register"
        Dim m_Reg_Login As String = "Back to Login"
        Dim m_Reg_Done As String = "Congratulations! Your registration was succefull. You will receive an email with your username and password."
        Dim m_Reg_AlredyExist As String = "There's another user registered with this mail. Please recover password."
        Dim m_Reg_MissingField As String = "Please Fill In all fields."

        Public Property p_Reg_Name As String
            Get
                Return m_Reg_Name
            End Get
            Set(value As String)
                m_Reg_Name = value
            End Set
        End Property

        Public Property p_Reg_SurName As String
            Get
                Return m_Reg_SurName
            End Get
            Set(value As String)
                m_Reg_SurName = value
            End Set
        End Property

        Public Property p_Reg_EMail As String
            Get
                Return m_Reg_EMail
            End Get
            Set(value As String)
                m_Reg_EMail = value
            End Set
        End Property

        Public Property p_Reg_Company As String
            Get
                Return m_Reg_Company
            End Get
            Set(value As String)
                m_Reg_Company = value
            End Set
        End Property

        Public Property p_Reg_Register As String
            Get
                Return m_Reg_Register
            End Get
            Set(value As String)
                m_Reg_Register = value
            End Set
        End Property

        Public Property p_Reg_Login As String
            Get
                Return m_Reg_Login
            End Get
            Set(value As String)
                m_Reg_Login = value
            End Set
        End Property

        Public Property p_Reg_Done As String
            Get
                Return m_Reg_Done
            End Get
            Set(value As String)
                m_Reg_Done = value
            End Set
        End Property

        Public Property p_Reg_AlredyExist As String
            Get
                Return m_Reg_AlredyExist
            End Get
            Set(value As String)
                m_Reg_AlredyExist = value
            End Set
        End Property

        Public Property p_Reg_MissingField As String
            Get
                Return m_Reg_MissingField
            End Get
            Set(value As String)
                m_Reg_MissingField = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        ' FORGOT
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_For_Email As String = "e-mail"
        Dim m_For_SubMit As String = "Submit"
        Dim m_For_ToLogin As String = "Back to login"
        Dim m_For_Done As String = "Request submitted correctly!"
        Dim m_For_UserNotFound As String = "User does not exist!"
        Dim m_For_InsertMail As String = "Email address not provided!"
        Dim m_For_InvalidMail As String = "Bad email address provided!"

        Public Property p_For_Email As String
            Get
                Return m_For_Email
            End Get
            Set(value As String)
                m_For_Email = value
            End Set
        End Property

        Public Property p_For_SubMit As String
            Get
                Return m_For_SubMit
            End Get
            Set(value As String)
                m_For_SubMit = value
            End Set
        End Property

        Public Property p_For_ToLogin As String
            Get
                Return m_For_ToLogin
            End Get
            Set(value As String)
                m_For_ToLogin = value
            End Set
        End Property

        Public Property p_For_Done As String
            Get
                Return m_For_Done
            End Get
            Set(value As String)
                m_For_Done = value
            End Set
        End Property

        Public Property p_For_UserNotFound As String
            Get
                Return m_For_UserNotFound
            End Get
            Set(value As String)
                m_For_UserNotFound = value
            End Set
        End Property

        Public Property p_For_InsertMail As String
            Get
                Return m_For_InsertMail
            End Get
            Set(value As String)
                m_For_InsertMail = value
            End Set
        End Property

        Public Property p_For_InvalidMail As String
            Get
                Return m_For_InvalidMail
            End Get
            Set(value As String)
                m_For_InvalidMail = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        'SECTIONS
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Sec_TopThank As String = "Welcome to Modula APP"
        Dim m_Sec_ButtomThank As String = ""
        Dim m_Sec_ADD As String = "Add to Cart"
        Dim m_Sec_Send As String = "SEND"
        Dim m_Sec_Saved As String = "Saved"
        Dim m_Sec_Updating As String = "Updating..."
        Dim m_Sec_Updated As String = "New update available"

        Public Property p_Sec_TopThank As String
            Get
                Return m_Sec_TopThank
            End Get
            Set(value As String)
                m_Sec_TopThank = value
            End Set
        End Property

        Public Property p_Sec_ButtomThank As String
            Get
                Return m_Sec_ButtomThank
            End Get
            Set(value As String)
                m_Sec_ButtomThank = value
            End Set
        End Property

        Public Property p_Sec_ADD As String
            Get
                Return m_Sec_ADD
            End Get
            Set(value As String)
                m_Sec_ADD = value
            End Set
        End Property

        Public Property p_Sec_Send As String
            Get
                Return m_Sec_Send
            End Get
            Set(value As String)
                m_Sec_Send = value
            End Set
        End Property

        Public Property p_Sec_Saved As String
            Get
                Return m_Sec_Saved
            End Get
            Set(value As String)
                m_Sec_Saved = value
            End Set
        End Property

        Public Property p_Sec_Updating As String
            Get
                Return m_Sec_Updating
            End Get
            Set(value As String)
                m_Sec_Updating = value
            End Set
        End Property

        Public Property p_Sec_Updated As String
            Get
                Return m_Sec_Updated
            End Get
            Set(value As String)
                m_Sec_Updated = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        'DETAILS
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Details_Close As String = "Close"
        Dim m_Details_Add As String = "Add to cart"
        Dim m_Details_Saved As String = "Saved"

        Public Property p_Details_Close As String
            Get
                Return m_Details_Close
            End Get
            Set(value As String)
                m_Details_Close = value
            End Set
        End Property

        Public Property p_Details_Add As String
            Get
                Return m_Details_Add
            End Get
            Set(value As String)
                m_Details_Add = value
            End Set
        End Property

        Public Property p_Details_Saved As String
            Get
                Return m_Details_Saved
            End Get
            Set(value As String)
                m_Details_Saved = value
            End Set
        End Property



        '----------------------------------------------------------------
        '----------------------------------------------------------------
        'SEND
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Send_Heading As String = "EMAIL ADDRESS BELOW, THEN PRESS SEND:"
        Dim m_Send_PlaceHolder As String = "Email"
        Dim m_Send_ContactLater As String = "Contact me later"
        Dim m_Send_Consent As String = "I indicate my consent to MODULA's privacy Policy Terms"
        Dim m_Send_PolicyERR As String = "You must accept the MODULA's Privacy Policy Terms"
        Dim m_Send_Send As String = "SEND"
        Dim m_Send_Close As String = "close"
        Dim m_Send_Thank As String = "Thank you!"

        Public Property p_Send_Heading As String
            Get
                Return m_Send_Heading
            End Get
            Set(value As String)
                m_Send_Heading = value
            End Set
        End Property

        Public Property p_Send_PlaceHolder As String
            Get
                Return m_Send_PlaceHolder
            End Get
            Set(value As String)
                m_Send_PlaceHolder = value
            End Set
        End Property

        Public Property p_Send_ContactLater As String
            Get
                Return m_Send_ContactLater
            End Get
            Set(value As String)
                m_Send_ContactLater = value
            End Set
        End Property

        Public Property p_Send_Consent As String
            Get
                Return m_Send_Consent
            End Get
            Set(value As String)
                m_Send_Consent = value
            End Set
        End Property

        Public Property p_Send_PolicyERR As String
            Get
                Return m_Send_PolicyERR
            End Get
            Set(value As String)
                m_Send_PolicyERR = value
            End Set
        End Property

        Public Property p_Send_Send As String
            Get
                Return m_Send_Send
            End Get
            Set(value As String)
                m_Send_Send = value
            End Set
        End Property

        Public Property p_Send_Close As String
            Get
                Return m_Send_Close
            End Get
            Set(value As String)
                m_Send_Close = value
            End Set
        End Property

        Public Property p_Send_Thank As String
            Get
                Return m_Send_Thank
            End Get
            Set(value As String)
                m_Send_Thank = value
            End Set
        End Property


        '----------------------------------------------------------------
        '----------------------------------------------------------------
        'POLICY
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Policy_Text As String = "Policy text"

        Public Property p_Policy_Text As String
            Get
                Return m_Policy_Text
            End Get
            Set(value As String)
                m_Policy_Text = value
            End Set
        End Property



        '----------------------------------------------------------------
        '----------------------------------------------------------------
        'ALERT
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_Alert_Warning As String = "Warning"
        Dim m_Alert_Confirm As String = "Confirm"
        Dim m_Alert_Cancel As String = "Cancel"
        Dim m_Alert_Alert As String = "Warning"
        Dim m_Alert_DeleteEntity As String = "You are about to delete an entity from the cart. Are you sure ?"
        Dim m_Alert_EmailRequired As String = "The email address is required!"
        Dim m_Alert_EmailInvalid As String = "Insert a valid email address"
        Dim m_Alert_NoEntities As String = "There are no elements to send!"
        Dim m_Alert_YES As String = "Yes"
        Dim m_Alert_NO As String = "No"
        Dim m_Alert_OK As String = "Ok"

        Dim m_Alert_NO_Results As String = "No Results Found"
        Dim m_Alert_NO_DownLoad As String = "No Active Downloads!"
        Dim m_Alert_NO_Connection As String = "No internet connection!"
        Dim m_Alert_EMAIL_NOT_SEND As String = "There are no email to send!"
        Dim m_Alert_EMAIL_SEND As String = "Emails sent successfully!"


        Public Property p_Alert_Warning As String
            Get
                Return m_Alert_Warning
            End Get
            Set(value As String)
                m_Alert_Warning = value
            End Set
        End Property

        Public Property p_Alert_Confirm As String
            Get
                Return m_Alert_Confirm
            End Get
            Set(value As String)
                m_Alert_Confirm = value
            End Set
        End Property

        Public Property p_Alert_Cancel As String
            Get
                Return m_Alert_Cancel
            End Get
            Set(value As String)
                m_Alert_Cancel = value
            End Set
        End Property

        Public Property p_Alert_Alert As String
            Get
                Return m_Alert_Alert
            End Get
            Set(value As String)
                m_Alert_Alert = value
            End Set
        End Property

        Public Property p_Alert_DeleteEntity As String
            Get
                Return m_Alert_DeleteEntity
            End Get
            Set(value As String)
                m_Alert_DeleteEntity = value
            End Set
        End Property

        Public Property p_Alert_EmailRequired As String
            Get
                Return m_Alert_EmailRequired
            End Get
            Set(value As String)
                m_Alert_EmailRequired = value
            End Set
        End Property

        Public Property p_Alert_EmailInvalid As String
            Get
                Return m_Alert_EmailInvalid
            End Get
            Set(value As String)
                m_Alert_EmailInvalid = value
            End Set
        End Property

        Public Property p_Alert_NoEntities As String
            Get
                Return m_Alert_NoEntities
            End Get
            Set(value As String)
                m_Alert_NoEntities = value
            End Set
        End Property

        Public Property p_Alert_YES As String
            Get
                Return m_Alert_YES
            End Get
            Set(value As String)
                m_Alert_YES = value
            End Set
        End Property

        Public Property p_Alert_NO As String
            Get
                Return m_Alert_NO
            End Get
            Set(value As String)
                m_Alert_NO = value
            End Set
        End Property

        Public Property p_Alert_OK As String
            Get
                Return m_Alert_OK
            End Get
            Set(value As String)
                m_Alert_OK = value
            End Set
        End Property


        Public Property p_Alert_NO_Results As String
            Get
                Return m_Alert_NO_Results
            End Get
            Set(value As String)
                m_Alert_NO_Results = value
            End Set
        End Property
        Public Property p_Alert_NO_DownLoad As String
            Get
                Return m_Alert_NO_DownLoad
            End Get
            Set(value As String)
                m_Alert_NO_DownLoad = value
            End Set
        End Property
        Public Property p_Alert_NO_Connection As String
            Get
                Return m_Alert_NO_Connection
            End Get
            Set(value As String)
                m_Alert_NO_Connection = value
            End Set
        End Property
        Public Property p_Alert_EMAIL_NOT_SEND As String
            Get
                Return m_Alert_EMAIL_NOT_SEND
            End Get
            Set(value As String)
                m_Alert_EMAIL_NOT_SEND = value
            End Set
        End Property
        Public Property p_Alert_EMAIL_SEND As String
            Get
                Return m_Alert_EMAIL_SEND
            End Get
            Set(value As String)
                m_Alert_EMAIL_SEND = value
            End Set
        End Property



        '----------------------------------------------------------------
        '----------------------------------------------------------------
        '404
        '----------------------------------------------------------------
        '----------------------------------------------------------------
        Dim m_404_TopText As String = "Page not found"
        Dim m_404_Text As String = "The page request is not available."

        Public Property p_404_TopText As String
            Get
                Return m_404_TopText
            End Get
            Set(value As String)
                m_404_TopText = value
            End Set
        End Property

        Public Property p_404_Text As String
            Get
                Return m_404_Text
            End Get
            Set(value As String)
                m_404_Text = value
            End Set
        End Property

    End Class


    Public Enum Sezioni
        Template
        Login
        Register
        Forgot
        Sections
        Details
        Send
        Policy
        Alert
        Err_404
    End Enum

    Public Shared Function Convert_JSON_ReturnedFrom_Words(jsonString As String) As Controlli

        ' Mi creo un Array  di tanti eventuali risultati (nel caso fosse una tabella)
        Dim jsonParts As String() = jsonString.Replace("[", "").Replace("]", "").Split("},{")


        'Individuo  la Sezione di Interesse
        Dim _Sezione As Sezioni = Sezioni.Template


        'Mi carico la struttura di Controlli
        Dim RigaLogin As New Controlli
        App.Testo_Privacy = ""


        For Each jp As String In jsonParts


            If _Sezione = Sezioni.Policy Then
                _Sezione = Sezioni.Template
            End If

            If jp.Trim.Length > 0 Then
                'Splitto ogni campo della stessa riga
                Dim propData As String() = jp.Replace("{", "").Replace("}", "").Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)

                For Each rowData As String In propData

                    If _Sezione = Sezioni.Policy Then
                        App.Testo_Privacy = App.Testo_Privacy & rowData
                    End If


                    Dim idx As Integer = 0
                    Dim n As String = ""
                    Dim v As String = ""

                    Dim idx2 As Integer = 0
                    Dim n2 As String = ""
                    Dim v2 As String = ""

                    Dim idx3 As Integer = 0
                    Dim n3 As String = ""
                    Dim v3 As String = ""


                    Try
                        idx = rowData.IndexOf(":")
                        n = rowData.Substring(0, idx - 1).Replace("""", "").Trim
                        v = rowData.Substring(idx + 1).Replace("""", "").Trim


                        idx2 = rowData.IndexOf(":", idx + 1)
                        If idx2 > idx Then
                            n2 = rowData.Substring(idx + 1, idx2 - 1 - idx).Replace("""", "").Trim
                            v2 = rowData.Substring(idx2 + 1).Replace("""", "").Trim

                            idx3 = 0
                            n3 = ""
                            If n.ToUpper <> "Policy".ToUpper Then
                                idx3 = rowData.IndexOf(":", idx2 + 1)
                            End If

                            If idx3 > idx2 Then
                                n3 = rowData.Substring(idx2 + 1, idx3 - 1 - idx2).Replace("""", "").Trim
                                v3 = rowData.Substring(idx3 + 1).Replace("""", "").Trim
                            End If

                            If n3.Trim.Length > 0 AndAlso v3.Trim.Length > 0 Then
                                n = n2
                            End If


                            If n.ToUpper = "Template".ToUpper Then
                                _Sezione = Sezioni.Template
                            End If
                            If n.ToUpper = "Sections".ToUpper Then
                                _Sezione = Sezioni.Sections
                            End If
                            If n.ToUpper = "Login".ToUpper Then
                                _Sezione = Sezioni.Login
                            End If
                            If n.ToUpper = "Send".ToUpper Then
                                _Sezione = Sezioni.Send
                            End If
                            If n.ToUpper = "Alert".ToUpper Then
                                _Sezione = Sezioni.Alert
                            End If
                            If n.ToUpper = "Detail".ToUpper Then
                                _Sezione = Sezioni.Details
                            End If
                            If n.ToUpper = "404".ToUpper Then
                                _Sezione = Sezioni.Err_404
                            End If
                            If n.ToUpper = "Forgot".ToUpper Then
                                _Sezione = Sezioni.Forgot
                            End If
                            If n.ToUpper = "Policy".ToUpper Then
                                _Sezione = Sezioni.Policy
                            End If
                            If n.ToUpper = "Register".ToUpper Then
                                _Sezione = Sezioni.Register
                            End If

                            If n3.Trim.Length > 0 AndAlso v3.Trim.Length > 0 Then
                                n2 = n3
                                v2 = v3
                            End If

                        End If

                        If n2.Trim.Length > 0 AndAlso v2.Trim.Length > 0 Then
                            n = n2
                            v = v2
                        End If



                        If _Sezione = Sezioni.Template Then
                            If n.ToUpper = "LANGUAGES" Then RigaLogin.p_Temp_Languages = v
                            If n.ToUpper = "RESULTS_FOR" Then RigaLogin.p_Temp_Results_For = v
                            If n.ToUpper = "NO_RESULTS" Then RigaLogin.p_Temp_No_Result = v
                            If n.ToUpper = "HOME" Then RigaLogin.p_Temp_Home = v
                            If n.ToUpper = "LOGOUT" Then RigaLogin.p_Temp_Logout = v
                            If n.ToUpper = "STATS" Then RigaLogin.p_Temp_Stats = v
                        End If

                        If _Sezione = Sezioni.Login Then
                            If n.ToUpper = "USERNAME" Then RigaLogin.p_Log_User = v
                            If n.ToUpper = "PASSWORD" Then RigaLogin.p_Log_Pwd = v
                            If n.ToUpper = "REMEMBER" Then RigaLogin.p_Log_Rem = v
                            If n.ToUpper = "LOGIN" Then RigaLogin.p_Log_Log = v
                            If n.ToUpper = "FORGOT" Then RigaLogin.p_Log_For = v
                            If n.ToUpper = "REGISTER" Then RigaLogin.p_Log_Reg = v
                            If n.ToUpper = "DONE" Then RigaLogin.p_Log_Done = v
                            If n.ToUpper = "FAIL" Then RigaLogin.p_Log_Failed = v
                            If n.ToUpper = "INVALID_CREDENTIALS" Then RigaLogin.p_Log_Invalid = v
                            If n.ToUpper = "MISSING_CREDENTIALS" Then RigaLogin.p_Log_Misssing = v
                            If n.ToUpper = "SKIP" Then RigaLogin.p_Log_Skip = v
                            If n.ToUpper = "DATA_MISSING" Then RigaLogin.p_Log_DataMissing = v
                        End If

                        If _Sezione = Sezioni.Register Then
                            If n.ToUpper = "NAME" Then RigaLogin.p_Reg_Name = v
                            If n.ToUpper = "SURNAME" Then RigaLogin.p_Reg_SurName = v
                            If n.ToUpper = "EMAIL" Then RigaLogin.p_Reg_EMail = v
                            If n.ToUpper = "COMPANY" Then RigaLogin.p_Reg_Company = v
                            If n.ToUpper = "REGISTER" Then RigaLogin.p_Reg_Register = v
                            If n.ToUpper = "TO_LOGIN" Then RigaLogin.p_Reg_Login = v
                            If n.ToUpper = "DONE" Then RigaLogin.p_Reg_Done = v
                            If n.ToUpper = "EMAIL_ALREADY_EXISTS" Then RigaLogin.p_Reg_AlredyExist = v
                            If n.ToUpper = "MISSING_FIELDS" Then RigaLogin.p_Reg_MissingField = v
                        End If

                        If _Sezione = Sezioni.Forgot Then
                            If n.ToUpper = "EMAIL" Then RigaLogin.p_For_Email = v
                            If n.ToUpper = "SUBMIT" Then RigaLogin.p_For_SubMit = v
                            If n.ToUpper = "TO_LOGIN" Then RigaLogin.p_For_ToLogin = v
                            If n.ToUpper = "DONE" Then RigaLogin.p_For_Done = v
                            If n.ToUpper = "USER_NOT_FOUND" Then RigaLogin.p_For_UserNotFound = v
                            If n.ToUpper = "INSERT_MAIL" Then RigaLogin.p_For_InsertMail = v
                            If n.ToUpper = "INVALID_MAIL" Then RigaLogin.p_For_InvalidMail = v
                        End If

                        If _Sezione = Sezioni.Sections Then
                            If n.ToUpper = "TOP_THANK" Then RigaLogin.p_Sec_TopThank = v
                            If n.ToUpper = "BOTTOM_THANK" Then RigaLogin.p_Sec_ButtomThank = v
                            If n.ToUpper = "ADD" Then RigaLogin.p_Sec_ADD = v
                            If n.ToUpper = "SEND" Then RigaLogin.p_Sec_Send = v
                            If n.ToUpper = "SAVED" Then RigaLogin.p_Sec_Saved = v
                            If n.ToUpper = "UPDATING" Then RigaLogin.p_Sec_Updating = v
                            If n.ToUpper = "UPDATED" Then RigaLogin.p_Sec_Updated = v
                        End If

                        If _Sezione = Sezioni.Details Then
                            If n.ToUpper = "CLOSE" Then RigaLogin.p_Details_Close = v
                            If n.ToUpper = "ADD" Then RigaLogin.p_Details_Add = v
                            If n.ToUpper = "SAVED" Then RigaLogin.p_Details_Saved = v
                        End If

                        If _Sezione = Sezioni.Send Then
                            If n.ToUpper = "HEADING" Then RigaLogin.p_Send_Heading = v
                            If n.ToUpper = "PLACEHOLDER" Then RigaLogin.p_Send_PlaceHolder = v
                            If n.ToUpper = "CONTACT_LATER" Then RigaLogin.p_Send_ContactLater = v
                            If n.ToUpper = "CONSENT" Then RigaLogin.p_Send_Consent = v
                            If n.ToUpper = "POLICY_ERR" Then RigaLogin.p_Send_PolicyERR = v
                            If n.ToUpper = "SEND" Then RigaLogin.p_Send_Send = v
                            If n.ToUpper = "CLOSE" Then RigaLogin.p_Send_Close = v
                            If n.ToUpper = "THANK" Then RigaLogin.p_Send_Thank = v
                        End If

                        If _Sezione = Sezioni.Policy Then
                            If n.ToUpper = "TEXT" Then
                                RigaLogin.p_Policy_Text = v
                                App.Testo_Privacy = v
                            End If
                        End If

                        If _Sezione = Sezioni.Alert Then
                            If n.ToUpper = "WARNING" Then RigaLogin.p_Alert_Warning = v
                            If n.ToUpper = "CONFIRM" Then RigaLogin.p_Alert_Confirm = v
                            If n.ToUpper = "CANCEL" Then RigaLogin.p_Alert_Cancel = v
                            If n.ToUpper = "ALERT" Then RigaLogin.p_Alert_Alert = v
                            If n.ToUpper = "DELETING_ENTITY" Then RigaLogin.p_Alert_DeleteEntity = v
                            If n.ToUpper = "EMAIL_REQUIRED" Then RigaLogin.p_Alert_EmailRequired = v
                            If n.ToUpper = "EMAIL_INVALID" Then RigaLogin.p_Alert_EmailInvalid = v
                            If n.ToUpper = "NO_ENTITIES" Then RigaLogin.p_Alert_NoEntities = v
                            If n.ToUpper = "YES" Then RigaLogin.p_Alert_YES = v
                            If n.ToUpper = "NO" Then RigaLogin.p_Alert_NO = v
                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v

                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v
                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v
                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v
                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v
                            If n.ToUpper = "OK" Then RigaLogin.p_Alert_OK = v

                            If n.ToUpper = "NO_Result".ToUpper Then RigaLogin.p_Alert_NO_Results = v
                            If n.ToUpper = "NO_DownLoads".ToUpper Then RigaLogin.p_Alert_NO_DownLoad = v
                            If n.ToUpper = "NO_Connection".ToUpper Then RigaLogin.p_Alert_NO_Connection = v
                            If n.ToUpper = "EMAIL_NOT_SEND" Then RigaLogin.p_Alert_EMAIL_NOT_SEND = v
                            If n.ToUpper = "EMAIL_SEND" Then RigaLogin.p_Alert_EMAIL_SEND = v
                        End If

                        If _Sezione = Sezioni.Err_404 Then
                            If n.ToUpper = "TOP_TEXT" Then RigaLogin.p_404_TopText = v
                            If n.ToUpper = "TEXT" Then RigaLogin.p_404_Text = v
                        End If

                    Catch ex As Exception
                        Continue For
                    End Try
                Next
            End If
        Next

        Return RigaLogin

    End Function


    Public Shared Sub Salva_Settings_Words(ByVal Testo_Controllli As Controlli)

        Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
        Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder



        '---------------------------------------------------------------
        'Template
        '---------------------------------------------------------------
        localSettings.Values("TemIDE_Languages") = Testo_Controllli.p_Temp_Languages
        localSettings.Values("TemIDE_ResultsFor") = Testo_Controllli.p_Temp_Results_For
        localSettings.Values("TemIDE_NoResults") = Testo_Controllli.p_Temp_No_Result
        localSettings.Values("TemIDE_Home") = Testo_Controllli.p_Temp_Home
        localSettings.Values("TemIDE_LogOut") = Testo_Controllli.p_Temp_Logout
        localSettings.Values("TemIDE_Stats") = Testo_Controllli.p_Temp_Stats


        '---------------------------------------------------------------
        'Login
        '---------------------------------------------------------------
        localSettings.Values("LogIDE_Username") = Testo_Controllli.p_Log_User
        localSettings.Values("LogIDE_Password") = Testo_Controllli.p_Log_Pwd
        localSettings.Values("LogIDE_Remember") = Testo_Controllli.p_Log_Rem

        localSettings.Values("LogIDE_Login") = Testo_Controllli.p_Log_Log
        localSettings.Values("LogIDE_Forgot") = Testo_Controllli.p_Log_For
        localSettings.Values("LogIDE_Register") = Testo_Controllli.p_Log_Reg

        localSettings.Values("LogIDE_Done") = Testo_Controllli.p_Log_Done
        localSettings.Values("LogIDE_Failed") = Testo_Controllli.p_Log_Failed
        localSettings.Values("LogIDE_Invalid") = Testo_Controllli.p_Log_Invalid

        localSettings.Values("LogIDE_Missing") = Testo_Controllli.p_Log_Misssing
        localSettings.Values("LogIDE_Skip") = Testo_Controllli.p_Log_Skip
        localSettings.Values("LogIDE_DataMissing") = Testo_Controllli.p_Log_DataMissing


        '---------------------------------------------------------------
        'Register
        '---------------------------------------------------------------
        localSettings.Values("RegIDE_Name") = Testo_Controllli.p_Reg_Name
        localSettings.Values("RegIDE_Surname") = Testo_Controllli.p_Reg_SurName
        localSettings.Values("RegIDE_Email") = Testo_Controllli.p_Reg_EMail

        localSettings.Values("RegIDE_Company") = Testo_Controllli.p_Reg_Company
        localSettings.Values("RegIDE_Register") = Testo_Controllli.p_Reg_Register
        localSettings.Values("RegIDE_Login") = Testo_Controllli.p_Reg_Login

        localSettings.Values("RegIDE_Done") = Testo_Controllli.p_Reg_Done
        localSettings.Values("RegIDE_AlredyExists") = Testo_Controllli.p_Reg_AlredyExist
        localSettings.Values("RegIDE_MissingField") = Testo_Controllli.p_Reg_MissingField


        '---------------------------------------------------------------
        'Forgot
        '---------------------------------------------------------------
        localSettings.Values("ForIDE_Email") = Testo_Controllli.p_For_Email
        localSettings.Values("ForIDE_Submit") = Testo_Controllli.p_For_SubMit
        localSettings.Values("ForIDE_ToLogin") = Testo_Controllli.p_For_ToLogin
        localSettings.Values("ForIDE_Done") = Testo_Controllli.p_For_Done
        localSettings.Values("ForIDE_UserNotFound") = Testo_Controllli.p_For_UserNotFound
        localSettings.Values("ForIDE_InsertMail") = Testo_Controllli.p_For_InsertMail
        localSettings.Values("ForIDE_InvalidMail") = Testo_Controllli.p_For_InvalidMail


        '---------------------------------------------------------------
        'Sections
        '---------------------------------------------------------------
        localSettings.Values("SecIDE_TopThank") = Testo_Controllli.p_Sec_TopThank
        localSettings.Values("SecIDE_BottomThank") = Testo_Controllli.p_Sec_ButtomThank
        localSettings.Values("SecIDE_Add") = Testo_Controllli.p_Sec_ADD
        localSettings.Values("SecIDE_Send") = Testo_Controllli.p_Sec_Send
        localSettings.Values("SecIDE_Saved") = Testo_Controllli.p_Sec_Saved
        localSettings.Values("SecIDE_Updating") = Testo_Controllli.p_Sec_Updating
        localSettings.Values("SecIDE_Updated") = Testo_Controllli.p_Sec_Updated


        '---------------------------------------------------------------
        'Details
        '---------------------------------------------------------------
        localSettings.Values("DetIDE_Close") = Testo_Controllli.p_Details_Close
        localSettings.Values("DetIDE_Add") = Testo_Controllli.p_Details_Add
        localSettings.Values("DetIDE_Saved") = Testo_Controllli.p_Details_Saved


        '---------------------------------------------------------------
        'Send
        '---------------------------------------------------------------
        localSettings.Values("SendIDE_Heading") = Testo_Controllli.p_Send_Heading
        localSettings.Values("SendIDE_PlaceHolder") = Testo_Controllli.p_Send_PlaceHolder
        localSettings.Values("SendIDE_ContactLater") = Testo_Controllli.p_Send_ContactLater
        localSettings.Values("SendIDE_Consent") = Testo_Controllli.p_Send_Consent
        localSettings.Values("SendIDE_PolicyErr") = Testo_Controllli.p_Send_PolicyERR
        localSettings.Values("SendIDE_Send") = Testo_Controllli.p_Send_Send
        localSettings.Values("SendIDE_Close") = Testo_Controllli.p_Send_Close
        localSettings.Values("SendIDE_Thank") = Testo_Controllli.p_Send_Thank


        '---------------------------------------------------------------
        'Policy
        '---------------------------------------------------------------
        localSettings.Values("PolIDE_Close") = Testo_Controllli.p_Policy_Text


        '---------------------------------------------------------------
        'Alert
        '---------------------------------------------------------------
        localSettings.Values("AleIDE_Warning") = Testo_Controllli.p_Alert_Warning
        localSettings.Values("AleIDE_Confirm") = Testo_Controllli.p_Alert_Confirm
        localSettings.Values("AleIDE_Cancel") = Testo_Controllli.p_Alert_Cancel
        localSettings.Values("AleIDE_Alert") = Testo_Controllli.p_Alert_Alert
        localSettings.Values("AleIDE_DeleteEntity") = Testo_Controllli.p_Alert_DeleteEntity
        localSettings.Values("AleIDE_EmailRequired") = Testo_Controllli.p_Alert_EmailRequired
        localSettings.Values("AleIDE_EmailInvalid") = Testo_Controllli.p_Alert_EmailInvalid
        localSettings.Values("AleIDE_NoEntities") = Testo_Controllli.p_Alert_NoEntities
        localSettings.Values("AleIDE_YES") = Testo_Controllli.p_Alert_YES
        localSettings.Values("AleIDE_NO") = Testo_Controllli.p_Alert_NO
        localSettings.Values("AleIDE_OK") = Testo_Controllli.p_Alert_OK

        localSettings.Values("AleIDE_NoResults") = Testo_Controllli.p_Alert_NO_Results
        localSettings.Values("AleIDE_NoDownloads") = Testo_Controllli.p_Alert_NO_DownLoad
        localSettings.Values("AleIDE_NoConnection") = Testo_Controllli.p_Alert_NO_Connection
        localSettings.Values("AleIDE_EmailNotSend") = Testo_Controllli.p_Alert_EMAIL_NOT_SEND
        localSettings.Values("AleIDE_EmailSend") = Testo_Controllli.p_Alert_EMAIL_SEND


        '---------------------------------------------------------------
        ' 404
        '---------------------------------------------------------------
        localSettings.Values("Err404IDE_TopText") = Testo_Controllli.p_404_TopText
        localSettings.Values("Err404IDE_Text") = Testo_Controllli.p_404_Text

    End Sub

    Public Shared Async Function Scarica_Tutti_Json_delle_Lingue() As Task

        For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

            Dim CodiceLingua As String = App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
            Dim _Uri As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/lang/" & CodiceLingua.Trim & ".json"
            '(va usato l'HashID   generico della company)
            Dim _FileName As String = CodiceLingua.Trim & ".json"

            'Scarica il File di Interesse
            '-----------------------------------
            Await Gestore_DownLoad.StartDownload(_Uri, _FileName)

        Next

    End Function



    Public Shared Async Function Setta_Controlli_Lingua_OffLine(ByVal CodiceLingua As String) As Task

        '(va usato l'HashID   generico della company)
        Dim _FileName As String = CodiceLingua.Trim & ".json"


        Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
        Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)

        If MyFileInfo.Length > 0 Then

            'Metti in Stringa il Contenuto del File
            '-----------------------------------
            Await (Funzioni_Dati.Leggere_Testo_File_From_LocalFolder(_FileName))

            Dim TestoContenuto As String = App.Stringa_Contenuto_File.ToString()

            'Legge il file
            '-----------------------------------
            Dim ControlliForm As Controlli = Convert_JSON_ReturnedFrom_Words(TestoContenuto)

            'Salva nei Settaggi
            '-----------------------------------
            Salva_Settings_Words(ControlliForm)

        End If
    End Function


    Public Shared Async Function Dammi_Lingua_Utilizzabile(ByVal CodiceLingua As String) As Task(Of String)

        Dim Lingua_che_Utilizzo As String = ""
        Dim _Id_Lingua As Integer = 0

        Dim Trovato As Boolean = False
        For Each LaLingua In App.Lingue_Diponibili
            If LaLingua.p_LanguageCode.ToUpper = CodiceLingua.ToUpper Then
                _Id_Lingua = LaLingua.p_LanguageID
                Trovato = True
            End If
        Next

        If Not Trovato Then
            For Each LaLingua In App.Lingue_Diponibili
                If LaLingua.p_LanguageCode.ToUpper = "EN" Then
                    CodiceLingua = LaLingua.p_LanguageCode.ToUpper
                    _Id_Lingua = LaLingua.p_LanguageID
                    Exit For
                Else
                    CodiceLingua = LaLingua.p_LanguageCode.ToUpper
                    _Id_Lingua = LaLingua.p_LanguageID
                End If
            Next


        End If

        App.m_Lang_NoRete_Code = CodiceLingua
        App.m_Lang_NoRete_Id = _Id_Lingua

        Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
        localSettings.Values("Lang_NoRete_Code") = CodiceLingua
        localSettings.Values("Lang_NoRete_Id") = _Id_Lingua


        Lingua_che_Utilizzo = CodiceLingua
        Return Lingua_che_Utilizzo

    End Function


    Public Shared Async Function Setta_Controlli_Lingua(ByVal CodiceLingua As String) As Task


        CodiceLingua = Await Dammi_Lingua_Utilizzabile(CodiceLingua)

        'Dim Trovato As Boolean = False
        'For Each LaLingua In App.Lingue_Diponibili
        '    If LaLingua.p_LanguageCode.ToUpper = CodiceLingua.ToUpper Then
        '        Trovato = True
        '    End If
        'Next

        'If Not Trovato Then
        '    For Each LaLingua In App.Lingue_Diponibili
        '        If LaLingua.p_LanguageCode.ToUpper = "EN" Then
        '            CodiceLingua = LaLingua.p_LanguageCode.ToUpper
        '            Exit For
        '        Else
        '            CodiceLingua = LaLingua.p_LanguageCode.ToUpper
        '        End If
        '    Next

        '    App.m_Lang_NoRete_Code = CodiceLingua
        'End If

        Dim _Uri As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/lang/" & CodiceLingua.Trim & ".json"
        '(va usato l'HashID   generico della company)
        Dim _FileName As String = CodiceLingua.Trim & ".json"

        'Scarica il File di Interesse
        '-----------------------------------
        Await Gestore_DownLoad.StartDownload(_Uri, _FileName)

        Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
        Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)

        If MyFileInfo.Length > 0 Then

            'Metti in Stringa il Contenuto del File
            '-----------------------------------
            Await (Funzioni_Dati.Leggere_Testo_File_From_LocalFolder(_FileName))

            Dim TestoContenuto As String = App.Stringa_Contenuto_File.ToString()

            'Legge il file
            '-----------------------------------
            Dim ControlliForm As Controlli = Convert_JSON_ReturnedFrom_Words(TestoContenuto)

            'Salva nei Settaggi
            '-----------------------------------
            Salva_Settings_Words(ControlliForm)

        End If

    End Function


    Public Class Lingue_Disponibili

        Dim m_LanguageID As Integer = 0
        Dim m_LanguageCode As String = ""
        Dim m_DisplayName As String = ""
        Dim m_CultureInfo As String = ""
        Dim m_CountryCode As String = ""
        Dim m_CountryDisplayName As String = ""

        Public Property p_LanguageID As Integer
            Get
                Return m_LanguageID
            End Get
            Set(value As Integer)
                m_LanguageID = value
            End Set
        End Property

        Public Property p_LanguageCode As String
            Get
                Return m_LanguageCode
            End Get
            Set(value As String)
                m_LanguageCode = value
            End Set
        End Property

        Public Property p_DisplayName As String
            Get
                Return m_DisplayName
            End Get
            Set(value As String)
                m_DisplayName = value
            End Set
        End Property

        Public Property p_CultureInfo As String
            Get
                Return m_CultureInfo
            End Get
            Set(value As String)
                m_CultureInfo = value
            End Set
        End Property

        Public Property p_CountryCode As String
            Get
                Return m_CountryCode
            End Get
            Set(value As String)
                m_CountryCode = value
            End Set
        End Property

        Public Property p_CountryDisplayName As String
            Get
                Return m_CountryDisplayName
            End Get
            Set(value As String)
                m_CountryDisplayName = value
            End Set
        End Property

    End Class


    Public Shared Function Convert_JSON_ReturnedFrom_AvailableLanguages(jsonString As String) As List(Of Lingue_Disponibili)
        ' Mi creo un Array  di tanti eventuali risultati (nel caso fosse una tabella)
        Dim jsonParts As String() = jsonString.Replace("[", "").Replace("]", "").Split("},{")

        'Mi carico la struttura delle Lingue
        Dim ListaLingue As New List(Of Lingue_Disponibili)
        Dim RigaLogin As Lingue_Disponibili

        For Each jp As String In jsonParts

            If jp.Trim.Length > 0 Then
                'Splitto ogni campo della stessa riga
                Dim propData As String() = jp.Replace("{", "").Replace("}", "").Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)

                RigaLogin = New Lingue_Disponibili
                Dim Is_Country As Boolean = False
                For Each rowData As String In propData
                    Try
                        Dim idx As Integer = 0
                        Dim idx2 As Integer = 0
                        Dim n As String = ""
                        Dim n2 As String = ""
                        Dim v As String = ""
                        Dim v2 As String = ""

                        idx = rowData.IndexOf(":")
                        If idx >= 0 Then
                            n = rowData.Substring(0, idx - 1).Replace("""", "")
                            v = rowData.Substring(idx + 1).Replace("""", "")

                            idx2 = rowData.IndexOf(":", idx + 1)
                            If idx2 > idx Then
                                n2 = rowData.Substring(idx + 1, idx2 - 1 - idx).Replace("""", "")
                                v2 = rowData.Substring(idx2 + 1).Replace("""", "")
                            End If
                            If n.ToUpper = "COUNTRY" Then
                                Is_Country = True
                            End If

                            If n2.Trim.Length > 0 AndAlso v2.Trim.Length > 0 Then
                                n = n2
                                v = v2
                            End If

                            If Not Is_Country Then
                                If n.ToUpper = "LANGUAGEID" Then RigaLogin.p_LanguageID = v
                                If n.ToUpper = "LANGUAGECODE" Then RigaLogin.p_LanguageCode = v
                                If n.ToUpper = "DISPLAYNAME" Then RigaLogin.p_DisplayName = v
                                If n.ToUpper = "CULTUREINFO" Then RigaLogin.p_CultureInfo = v
                            Else
                                If n.ToUpper = "COUNTRYCODE" Then RigaLogin.p_CountryCode = v
                                If n.ToUpper = "DISPLAYNAME" Then RigaLogin.p_CountryDisplayName = v
                            End If
                        End If
                    Catch ex As Exception
                        Continue For
                    End Try

                Next
                ListaLingue.Add(RigaLogin)
            End If
        Next
        Return ListaLingue

    End Function


    Public Shared Sub Carica_Lingue(ByRef Array_Lingue As List(Of Funzioni_Lingue.Lingue_Disponibili), ByVal uHash As String)

        Dim client_Lingue As MobileServiceClient = New MobileServiceClient()
        Dim _Ritorno_Lingue As Task(Of String) = client_Lingue.GetAvailableLanguagesAsync(uHash)
        _Ritorno_Lingue.Wait()

        Dim json_Lingue As String = _Ritorno_Lingue.Result
        Array_Lingue = Funzioni_Lingue.Convert_JSON_ReturnedFrom_AvailableLanguages(json_Lingue)

        Salva_Lingue_nel_DB(Array_Lingue)

    End Sub



    Public Shared Sub Salva_Lingue_nel_DB(ByVal Array_Lingue As List(Of Funzioni_Lingue.Lingue_Disponibili))


        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        Using conn As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
            If Not Funzioni_Dati.isTableExists("Lingue_Disponibili") Then conn.CreateTable(Of Funzioni_Lingue.Lingue_Disponibili)()
        End Using

        Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from Lingue_Disponibili")
        _Cm1.ExecuteNonQuery()


        For Indice As Integer = 0 To Array_Lingue.Count - 1

            Dim StrInsert As String = "Insert INTO Lingue_Disponibili ("
            StrInsert &= "  p_LanguageID  , p_LanguageCode ,  p_DisplayName ,  p_CultureInfo , p_CountryCode , p_CountryDisplayName  "
            StrInsert &= " ) VALUES ( "
            StrInsert &= Array_Lingue(Indice).p_LanguageID.ToString.Trim & " , "
            StrInsert &= "'" & Array_Lingue(Indice).p_LanguageCode & "' , "
            StrInsert &= "'" & Array_Lingue(Indice).p_DisplayName & "' , "
            StrInsert &= "'" & Array_Lingue(Indice).p_CultureInfo & "' , "
            StrInsert &= "'" & Array_Lingue(Indice).p_CountryCode & "' , "
            StrInsert &= "'" & Array_Lingue(Indice).p_CountryDisplayName & "'"
            StrInsert &= " ) "

            _Cm1 = Conn_SQL.CreateCommand(StrInsert)
            _Cm1.ExecuteNonQuery()

        Next

    End Sub



    Public Shared Sub Carica_Lingue_dal_DB()

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        App.Lingue_Diponibili = (From p In Conn_SQL.Table(Of Funzioni_Lingue.Lingue_Disponibili)).ToList

    End Sub

End Class
