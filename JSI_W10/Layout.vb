﻿Public Class Layout
    'Public Class General


    '    Dim m_appName As String
    '    Dim m_analyticsId As String
    '    Dim m_headerColor As String
    '    Dim m_backgroundColor As String
    '    Dim m_panelColor As String
    '    Dim m_footerBackgroundColor As String
    '    Dim m_backgroundImage As String
    '    Dim m_panelLogo As String
    '    Dim m_favoriteButtonImage As String
    '    Dim m_settingsButtonImage As String
    '    Dim m_favoriteOffImage As String
    '    Dim m_favoriteOnImage As String
    '    Dim m_removeItemImage As String
    '    Dim m_removeEmailImage As String
    '    Dim m_notificationImage As String
    '    Dim m_nextMenuImage As String
    '    Dim m_prevMenuImage As String

    '    Public Property appName As String
    '        Get
    '            Return m_appName
    '        End Get
    '        Set(value As String)
    '            m_appName = value
    '        End Set
    '    End Property

    '    Public Property analyticsId As String
    '        Get
    '            Return m_analyticsId
    '        End Get
    '        Set(value As String)
    '            m_analyticsId = value
    '        End Set
    '    End Property


    '    Public Property headerColor As String
    '        Get
    '            Return m_headerColor
    '        End Get
    '        Set(value As String)
    '            m_headerColor = value
    '        End Set
    '    End Property

    '    Public Property backgroundColor As String
    '        Get
    '            Return m_backgroundColor
    '        End Get
    '        Set(value As String)
    '            m_backgroundColor = value
    '        End Set
    '    End Property

    '    Public Property panelColor As String
    '        Get
    '            Return m_panelColor
    '        End Get
    '        Set(value As String)
    '            m_panelColor = value
    '        End Set
    '    End Property

    '    Public Property footerBackgroundColor As String
    '        Get
    '            Return m_footerBackgroundColor
    '        End Get
    '        Set(value As String)
    '            m_footerBackgroundColor = value
    '        End Set
    '    End Property

    '    Public Property backgroundImage As String
    '        Get
    '            Return m_backgroundImage
    '        End Get
    '        Set(value As String)
    '            m_backgroundImage = value
    '        End Set
    '    End Property

    '    Public Property panelLogo As String
    '        Get
    '            Return m_panelLogo
    '        End Get
    '        Set(value As String)
    '            m_panelLogo = value
    '        End Set
    '    End Property

    '    Public Property favoriteButtonImage As String
    '        Get
    '            Return m_favoriteButtonImage
    '        End Get
    '        Set(value As String)
    '            m_favoriteButtonImage = value
    '        End Set
    '    End Property

    '    Public Property settingsButtonImage As String
    '        Get
    '            Return m_settingsButtonImage
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonImage = value
    '        End Set
    '    End Property

    '    Public Property favoriteOffImage As String
    '        Get
    '            Return m_favoriteOffImage
    '        End Get
    '        Set(value As String)
    '            m_favoriteOffImage = value
    '        End Set
    '    End Property

    '    Public Property favoriteOnImage As String
    '        Get
    '            Return m_favoriteOnImage
    '        End Get
    '        Set(value As String)
    '            m_favoriteOnImage = value
    '        End Set
    '    End Property

    '    Public Property removeItemImage As String
    '        Get
    '            Return m_removeItemImage
    '        End Get
    '        Set(value As String)
    '            m_removeItemImage = value
    '        End Set
    '    End Property

    '    Public Property removeEmailImage As String
    '        Get
    '            Return m_removeEmailImage
    '        End Get
    '        Set(value As String)
    '            m_removeEmailImage = value
    '        End Set
    '    End Property

    '    Public Property notificationImage As String
    '        Get
    '            Return m_notificationImage
    '        End Get
    '        Set(value As String)
    '            m_notificationImage = value
    '        End Set
    '    End Property

    '    Public Property nextMenuImage As String
    '        Get
    '            Return m_nextMenuImage
    '        End Get
    '        Set(value As String)
    '            m_nextMenuImage = value
    '        End Set
    '    End Property

    '    Public Property prevMenuImage As String
    '        Get
    '            Return m_prevMenuImage
    '        End Get
    '        Set(value As String)
    '            m_prevMenuImage = value
    '        End Set
    '    End Property


    'End Class


    'Public Class PageLogin

    '    Dim m_loginTitleColor As String
    '    Dim m_loginBtnTxtColor As String
    '    Dim m_loginBtnBkgColor As String
    '    Dim m_loginBtnRadius As String
    '    Dim m_loginBtnBorderWidth As String
    '    Dim m_loginBtnBorderColor As String
    '    Dim m_loginLblTxtColor As String
    '    Dim m_loginEditTxtColor As String
    '    Dim m_loginEditTxtRadius As String
    '    Dim m_loginEditTxtWidth  As String
    '    Dim m_loginEditTxtBorderColor As String
    '    Dim m_loginEditBkgColor As String
    '    Dim m_loginPanelRadius As String
    '    Dim m_switchTrackColor As String
    '    Dim m_switchColorOn As String
    '    Dim m_switchColorOff As String

    '    Public Property loginTitleColor As String
    '        Get
    '            Return m_loginTitleColor
    '        End Get
    '        Set(value As String)
    '            m_loginTitleColor = value
    '        End Set
    '    End Property

    '    Public Property loginBtnTxtColor As String
    '        Get
    '            Return m_loginBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_loginBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property loginBtnBkgColor As String
    '        Get
    '            Return m_loginBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_loginBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property loginBtnRadius As String
    '        Get
    '            Return m_loginBtnRadius
    '        End Get
    '        Set(value As String)
    '            m_loginBtnRadius = value
    '        End Set
    '    End Property

    '    Public Property loginBtnBorderWidth As String
    '        Get
    '            Return m_loginBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_loginBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property loginBtnBorderColor As String
    '        Get
    '            Return m_loginBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_loginBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property loginLblTxtColor As String
    '        Get
    '            Return m_loginLblTxtColor
    '        End Get
    '        Set(value As String)
    '            m_loginLblTxtColor = value
    '        End Set
    '    End Property

    '    Public Property loginEditTxtColor As String
    '        Get
    '            Return m_loginEditTxtColor
    '        End Get
    '        Set(value As String)
    '            m_loginEditTxtColor = value
    '        End Set
    '    End Property

    '    Public Property loginEditTxtRadius As String
    '        Get
    '            Return m_loginEditTxtRadius
    '        End Get
    '        Set(value As String)
    '            m_loginEditTxtRadius = value
    '        End Set
    '    End Property

    '    Public Property loginEditTxtWidth As String
    '        Get
    '            Return m_loginEditTxtWidth
    '        End Get
    '        Set(value As String)
    '            m_loginEditTxtWidth = value
    '        End Set
    '    End Property

    '    Public Property loginEditTxtBorderColor As String
    '        Get
    '            Return m_loginEditTxtBorderColor
    '        End Get
    '        Set(value As String)
    '            m_loginEditTxtBorderColor = value
    '        End Set
    '    End Property

    '    Public Property loginEditBkgColor As String
    '        Get
    '            Return m_loginEditBkgColor
    '        End Get
    '        Set(value As String)
    '            m_loginEditBkgColor = value
    '        End Set
    '    End Property

    '    Public Property loginPanelRadius As String
    '        Get
    '            Return m_loginPanelRadius
    '        End Get
    '        Set(value As String)
    '            m_loginPanelRadius = value
    '        End Set
    '    End Property

    '    Public Property switchTrackColor As String
    '        Get
    '            Return m_switchTrackColor
    '        End Get
    '        Set(value As String)
    '            m_switchTrackColor = value
    '        End Set
    '    End Property

    '    Public Property switchColorOn As String
    '        Get
    '            Return m_switchColorOn
    '        End Get
    '        Set(value As String)
    '            m_switchColorOn = value
    '        End Set
    '    End Property

    '    Public Property switchColorOff As String
    '        Get
    '            Return m_switchColorOff
    '        End Get
    '        Set(value As String)
    '            m_switchColorOff = value
    '        End Set
    '    End Property


    'End Class

    'Public Class SettingsPage

    '    Dim m_settingsButtonTextColor As String
    '    Dim m_settingsButtonBackgroundColor As String
    '    Dim m_settingsButtonBorderColor As String
    '    Dim m_settingsButtonBorderWidth As String
    '    Dim m_settingsButtonBorderRadius As String
    '    Dim m_settingsLabelTextColor As String

    '    Public Property settingsButtonTextColor As String
    '        Get
    '            Return m_settingsButtonTextColor
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonTextColor = value
    '        End Set
    '    End Property

    '    Public Property settingsButtonBackgroundColor As String
    '        Get
    '            Return m_settingsButtonBackgroundColor
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonBackgroundColor = value
    '        End Set
    '    End Property

    '    Public Property settingsButtonBorderColor As String
    '        Get
    '            Return m_settingsButtonBorderColor
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonBorderColor = value
    '        End Set
    '    End Property

    '    Public Property settingsButtonBorderWidth As String
    '        Get
    '            Return m_settingsButtonBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property settingsButtonBorderRadius As String
    '        Get
    '            Return m_settingsButtonBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_settingsButtonBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property settingsLabelTextColor As String
    '        Get
    '            Return m_settingsLabelTextColor
    '        End Get
    '        Set(value As String)
    '            m_settingsLabelTextColor = value
    '        End Set
    '    End Property


    'End Class

    'Public Class PageMain

    '    Dim m_settingsMenuTxtColor As String
    '    Dim m_settingsMenuBkgColor As String
    '    Dim m_languageTextColor As String
    '    Dim m_languageMenuTxtColor As String
    '    Dim m_languageMenuBkgColor As String
    '    Dim m_newUpdateTxtColor As String
    '    Dim m_newUpdateBkgColor As String
    '    Dim m_welcomeTxtColor As String
    '    Dim m_menuBkgColor As String
    '    Dim m_menuTxtColor As String
    '    Dim m_menuSeparatorColor As String
    '    Dim m_subMenuBkgColor As String
    '    Dim m_subMenuTxtColor As String
    '    Dim m_subMenuSeparatorColor As String
    '    Dim m_mainSendButtonTextColor As String
    '    Dim m_mainSendButtonBackgroundColor As String
    '    Dim m_mainSendButtonBorderColor As String
    '    Dim m_mainSendButtonBorderWidth As String
    '    Dim m_mainSendButtonBorderRadius As String

    '    Public Property settingsMenuTxtColor As String
    '        Get
    '            Return m_settingsMenuTxtColor
    '        End Get
    '        Set(value As String)
    '            m_settingsMenuTxtColor = value
    '        End Set
    '    End Property

    '    Public Property settingsMenuBkgColor As String
    '        Get
    '            Return m_settingsMenuBkgColor
    '        End Get
    '        Set(value As String)
    '            m_settingsMenuBkgColor = value
    '        End Set
    '    End Property

    '    Public Property languageTextColor As String
    '        Get
    '            Return m_languageTextColor
    '        End Get
    '        Set(value As String)
    '            m_languageTextColor = value
    '        End Set
    '    End Property

    '    Public Property languageMenuTxtColor As String
    '        Get
    '            Return m_languageMenuTxtColor
    '        End Get
    '        Set(value As String)
    '            m_languageMenuTxtColor = value
    '        End Set
    '    End Property

    '    Public Property languageMenuBkgColor As String
    '        Get
    '            Return m_languageMenuBkgColor
    '        End Get
    '        Set(value As String)
    '            m_languageMenuBkgColor = value
    '        End Set
    '    End Property

    '    Public Property newUpdateTxtColor As String
    '        Get
    '            Return m_newUpdateTxtColor
    '        End Get
    '        Set(value As String)
    '            m_newUpdateTxtColor = value
    '        End Set
    '    End Property

    '    Public Property newUpdateBkgColor As String
    '        Get
    '            Return m_newUpdateBkgColor
    '        End Get
    '        Set(value As String)
    '            m_newUpdateBkgColor = value
    '        End Set
    '    End Property

    '    Public Property welcomeTxtColor As String
    '        Get
    '            Return m_welcomeTxtColor
    '        End Get
    '        Set(value As String)
    '            m_welcomeTxtColor = value
    '        End Set
    '    End Property

    '    Public Property menuBkgColor As String
    '        Get
    '            Return m_menuBkgColor
    '        End Get
    '        Set(value As String)
    '            m_menuBkgColor = value
    '        End Set
    '    End Property

    '    Public Property menuTxtColor As String
    '        Get
    '            Return m_menuTxtColor
    '        End Get
    '        Set(value As String)
    '            m_menuTxtColor = value
    '        End Set
    '    End Property

    '    Public Property menuSeparatorColor As String
    '        Get
    '            Return m_menuSeparatorColor
    '        End Get
    '        Set(value As String)
    '            m_menuSeparatorColor = value
    '        End Set
    '    End Property

    '    Public Property subMenuBkgColor As String
    '        Get
    '            Return m_subMenuBkgColor
    '        End Get
    '        Set(value As String)
    '            m_subMenuBkgColor = value
    '        End Set
    '    End Property

    '    Public Property subMenuTxtColor As String
    '        Get
    '            Return m_subMenuTxtColor
    '        End Get
    '        Set(value As String)
    '            m_subMenuTxtColor = value
    '        End Set
    '    End Property

    '    Public Property subMenuSeparatorColor As String
    '        Get
    '            Return m_subMenuSeparatorColor
    '        End Get
    '        Set(value As String)
    '            m_subMenuSeparatorColor = value
    '        End Set
    '    End Property

    '    Public Property mainSendButtonTextColor As String
    '        Get
    '            Return m_mainSendButtonTextColor
    '        End Get
    '        Set(value As String)
    '            m_mainSendButtonTextColor = value
    '        End Set
    '    End Property

    '    Public Property mainSendButtonBackgroundColor As String
    '        Get
    '            Return m_mainSendButtonBackgroundColor
    '        End Get
    '        Set(value As String)
    '            m_mainSendButtonBackgroundColor = value
    '        End Set
    '    End Property

    '    Public Property mainSendButtonBorderColor As String
    '        Get
    '            Return m_mainSendButtonBorderColor
    '        End Get
    '        Set(value As String)
    '            m_mainSendButtonBorderColor = value
    '        End Set
    '    End Property

    '    Public Property mainSendButtonBorderWidth As String
    '        Get
    '            Return m_mainSendButtonBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_mainSendButtonBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property mainSendButtonBorderRadius As String
    '        Get
    '            Return m_mainSendButtonBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_mainSendButtonBorderRadius = value
    '        End Set
    '    End Property


    'End Class

    'Public Class PageContent

    '    Dim m_sectionTitleTxtColor As String
    '    Dim m_sectionDescrTxtColor As String
    '    Dim m_filterBorderRadius As String
    '    Dim m_filterOnTxtColor As String
    '    Dim m_filterOnBkgColor As String
    '    Dim m_filterOnBorderWidth As String
    '    Dim m_filterOnBorderColor As String
    '    Dim m_filterOffTxtColor As String
    '    Dim m_filterOffBkgColor As String
    '    Dim m_filterOffBorderWidth As String
    '    Dim m_filterOffBorderColor As String
    '    Dim m_itemTxtColor As String
    '    Dim m_addCartTxtColor As String
    '    Dim m_addCartBkgColor As String
    '    Dim m_addCartBorderRadius As String
    '    Dim m_addCartBorderWidth As String
    '    Dim m_addCartBorderColor As String
    '    Dim m_savedCartTxtColor As String
    '    Dim m_savedCartBkgColor As String
    '    Dim m_savedCartBorderRadius As String
    '    Dim m_savedCartBorderWidth As String
    '    Dim m_savedCartBorderColor As String
    '    Dim m_savedIconBkgColor As String
    '    Dim m_searchTxtColor As String
    '    Dim m_searchPlcTxtColor As String
    '    Dim m_searchBkgColor As String
    '    Dim m_searchNoResultTxtColor As String
    '    Dim m_searchResTitleTxtColor As String
    '    Dim m_searchResTitleBkgColor As String
    '    Dim m_searchLabelTxtColor As String
    '    Dim m_searchSpinColor As String

    '    Public Property sectionTitleTxtColor As String
    '        Get
    '            Return m_sectionTitleTxtColor
    '        End Get
    '        Set(value As String)
    '            m_sectionTitleTxtColor = value
    '        End Set
    '    End Property

    '    Public Property sectionDescrTxtColor As String
    '        Get
    '            Return m_sectionDescrTxtColor
    '        End Get
    '        Set(value As String)
    '            m_sectionDescrTxtColor = value
    '        End Set
    '    End Property

    '    Public Property filterBorderRadius As String
    '        Get
    '            Return m_filterBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_filterBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property filterOnTxtColor As String
    '        Get
    '            Return m_filterOnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_filterOnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property filterOnBkgColor As String
    '        Get
    '            Return m_filterOnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_filterOnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property filterOnBorderWidth As String
    '        Get
    '            Return m_filterOnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_filterOnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property filterOnBorderColor As String
    '        Get
    '            Return m_filterOnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_filterOnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property filterOffTxtColor As String
    '        Get
    '            Return m_filterOffTxtColor
    '        End Get
    '        Set(value As String)
    '            m_filterOffTxtColor = value
    '        End Set
    '    End Property

    '    Public Property filterOffBkgColor As String
    '        Get
    '            Return m_filterOffBkgColor
    '        End Get
    '        Set(value As String)
    '            m_filterOffBkgColor = value
    '        End Set
    '    End Property

    '    Public Property filterOffBorderWidth As String
    '        Get
    '            Return m_filterOffBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_filterOffBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property filterOffBorderColor As String
    '        Get
    '            Return m_filterOffBorderColor
    '        End Get
    '        Set(value As String)
    '            m_filterOffBorderColor = value
    '        End Set
    '    End Property

    '    Public Property itemTxtColor As String
    '        Get
    '            Return m_itemTxtColor
    '        End Get
    '        Set(value As String)
    '            m_itemTxtColor = value
    '        End Set
    '    End Property

    '    Public Property addCartTxtColor As String
    '        Get
    '            Return m_addCartTxtColor
    '        End Get
    '        Set(value As String)
    '            m_addCartTxtColor = value
    '        End Set
    '    End Property

    '    Public Property addCartBkgColor As String
    '        Get
    '            Return m_addCartBkgColor
    '        End Get
    '        Set(value As String)
    '            m_addCartBkgColor = value
    '        End Set
    '    End Property

    '    Public Property addCartBorderRadius As String
    '        Get
    '            Return m_addCartBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_addCartBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property addCartBorderWidth As String
    '        Get
    '            Return m_addCartBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_addCartBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property addCartBorderColor As String
    '        Get
    '            Return m_addCartBorderColor
    '        End Get
    '        Set(value As String)
    '            m_addCartBorderColor = value
    '        End Set
    '    End Property

    '    Public Property savedCartTxtColor As String
    '        Get
    '            Return m_savedCartTxtColor
    '        End Get
    '        Set(value As String)
    '            m_savedCartTxtColor = value
    '        End Set
    '    End Property

    '    Public Property savedCartBkgColor As String
    '        Get
    '            Return m_savedCartBkgColor
    '        End Get
    '        Set(value As String)
    '            m_savedCartBkgColor = value
    '        End Set
    '    End Property

    '    Public Property savedCartBorderRadius As String
    '        Get
    '            Return m_savedCartBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_savedCartBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property savedCartBorderWidth As String
    '        Get
    '            Return m_savedCartBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_savedCartBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property savedCartBorderColor As String
    '        Get
    '            Return m_v
    '        End Get
    '        Set(value As String)
    '            m_savedCartBorderColor = value
    '        End Set
    '    End Property

    '    Public Property savedIconBkgColor As String
    '        Get
    '            Return m_savedIconBkgColor
    '        End Get
    '        Set(value As String)
    '            m_savedIconBkgColor = value
    '        End Set
    '    End Property

    '    Public Property searchTxtColor As String
    '        Get
    '            Return m_searchTxtColor
    '        End Get
    '        Set(value As String)
    '            m_searchTxtColor = value
    '        End Set
    '    End Property

    '    Public Property searchPlcTxtColor As String
    '        Get
    '            Return m_searchPlcTxtColor
    '        End Get
    '        Set(value As String)
    '            m_searchPlcTxtColor = value
    '        End Set
    '    End Property

    '    Public Property searchBkgColor As String
    '        Get
    '            Return m_searchBkgColor
    '        End Get
    '        Set(value As String)
    '            m_searchBkgColor = value
    '        End Set
    '    End Property

    '    Public Property searchNoResultTxtColor As String
    '        Get
    '            Return m_searchNoResultTxtColor
    '        End Get
    '        Set(value As String)
    '            m_searchNoResultTxtColor = value
    '        End Set
    '    End Property

    '    Public Property searchResTitleTxtColor As String
    '        Get
    '            Return m_searchResTitleTxtColor
    '        End Get
    '        Set(value As String)
    '            m_searchResTitleTxtColor = value
    '        End Set
    '    End Property

    '    Public Property searchResTitleBkgColor As String
    '        Get
    '            Return m_searchResTitleBkgColor
    '        End Get
    '        Set(value As String)
    '            m_searchResTitleBkgColor = value
    '        End Set
    '    End Property

    '    Public Property searchLabelTxtColor As String
    '        Get
    '            Return m_searchLabelTxtColor
    '        End Get
    '        Set(value As String)
    '            m_searchLabelTxtColor = value
    '        End Set
    '    End Property

    '    Public Property searchSpinColor As String
    '        Get
    '            Return m_searchSpinColor
    '        End Get
    '        Set(value As String)
    '            m_searchSpinColor = value
    '        End Set
    '    End Property

    'End Class

    'Public Class PageSend

    '    Dim m_closeBtnTxtColor As String
    '    Dim m_closeBtnBkgColor As String
    '    Dim m_closeBtnBorderRadius As String
    '    Dim m_closeBtnBorderWidth As String
    '    Dim m_closeBtnBorderColor As String
    '    Dim m_sendBtnTxtColor As String
    '    Dim m_sendBtnBkgColor As String
    '    Dim m_sendBtnBorderRadius As String
    '    Dim m_sendBtnBorderWidth As String
    '    Dim m_sendBtnBorderColor As String
    '    Dim m_sendTitleTxtColor As String
    '    Dim m_emailTextColor As String
    '    Dim m_emailBkgColor As String
    '    Dim m_emailPlcColor As String
    '    Dim m_emailBorderColor As String
    '    Dim m_emailBorderRadius As String
    '    Dim m_emailBorderWidth As String
    '    Dim m_commentsTextColor As String
    '    Dim m_commentsBkgColor As String
    '    Dim m_commentsPlcColor As String
    '    Dim m_commentsBorderColor As String
    '    Dim m_commentsBorderRadius As String
    '    Dim m_commentsBorderWidth As String
    '    Dim m_contactTextColor As String
    '    Dim m_privacyTextColor As String
    '    Dim m_boxEmailBkg As String

    '    Public Property closeBtnTxtColor As String
    '        Get
    '            Return m_closeBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_closeBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property closeBtnBkgColor As String
    '        Get
    '            Return m_closeBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_closeBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property closeBtnBorderRadius As String
    '        Get
    '            Return m_closeBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_closeBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property closeBtnBorderWidth As String
    '        Get
    '            Return m_closeBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_closeBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property closeBtnBorderColor As String
    '        Get
    '            Return m_closeBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_closeBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property sendBtnTxtColor As String
    '        Get
    '            Return m_sendBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_sendBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property sendBtnBkgColor As String
    '        Get
    '            Return m_sendBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_sendBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property sendBtnBorderRadius As String
    '        Get
    '            Return m_sendBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_sendBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property sendBtnBorderWidth As String
    '        Get
    '            Return m_sendBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_sendBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property sendBtnBorderColor As String
    '        Get
    '            Return m_sendBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_sendBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property sendTitleTxtColor As String
    '        Get
    '            Return m_sendTitleTxtColor
    '        End Get
    '        Set(value As String)
    '            m_sendTitleTxtColor = value
    '        End Set
    '    End Property

    '    Public Property emailTextColor As String
    '        Get
    '            Return m_emailTextColor
    '        End Get
    '        Set(value As String)
    '            m_emailTextColor = value
    '        End Set
    '    End Property

    '    Public Property emailBkgColor As String
    '        Get
    '            Return m_emailBkgColor
    '        End Get
    '        Set(value As String)
    '            m_emailBkgColor = value
    '        End Set
    '    End Property

    '    Public Property emailPlcColor As String
    '        Get
    '            Return m_emailPlcColor
    '        End Get
    '        Set(value As String)
    '            m_emailPlcColor = value
    '        End Set
    '    End Property

    '    Public Property emailBorderColor As String
    '        Get
    '            Return m_emailBorderColor
    '        End Get
    '        Set(value As String)
    '            m_emailBorderColor = value
    '        End Set
    '    End Property

    '    Public Property emailBorderRadius As String
    '        Get
    '            Return m_emailBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_emailBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property emailBorderWidth As String
    '        Get
    '            Return m_emailBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_emailBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property commentsTextColor As String
    '        Get
    '            Return m_commentsTextColor
    '        End Get
    '        Set(value As String)
    '            m_commentsTextColor = value
    '        End Set
    '    End Property

    '    Public Property commentsBkgColor As String
    '        Get
    '            Return m_commentsBkgColor
    '        End Get
    '        Set(value As String)
    '            m_commentsBkgColor = value
    '        End Set
    '    End Property

    '    Public Property commentsPlcColor As String
    '        Get
    '            Return m_commentsPlcColor
    '        End Get
    '        Set(value As String)
    '            m_commentsPlcColor = value
    '        End Set
    '    End Property

    '    Public Property commentsBorderColor As String
    '        Get
    '            Return m_commentsBorderColor
    '        End Get
    '        Set(value As String)
    '            m_commentsBorderColor = value
    '        End Set
    '    End Property

    '    Public Property commentsBorderRadius As String
    '        Get
    '            Return m_commentsBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_commentsBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property commentsBorderWidth As String
    '        Get
    '            Return m_commentsBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_commentsBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property contactTextColor As String
    '        Get
    '            Return m_contactTextColor
    '        End Get
    '        Set(value As String)
    '            m_contactTextColor = value
    '        End Set
    '    End Property

    '    Public Property privacyTextColor As String
    '        Get
    '            Return m_privacyTextColor
    '        End Get
    '        Set(value As String)
    '            m_privacyTextColor = value
    '        End Set
    '    End Property

    '    Public Property boxEmailBkg As String
    '        Get
    '            Return m_boxEmailBkg
    '        End Get
    '        Set(value As String)
    '            m_boxEmailBkg = value
    '        End Set
    '    End Property

    'End Class

    'Public Class UpdatePanel

    '    Dim m_panelBkgColor As String
    '    Dim m_panelCloseTxtColor As String
    '    Dim m_panelCloseBkgColor As String
    '    Dim m_panelCloseBorderColor As String
    '    Dim m_panelCloseBorderWidth As String
    '    Dim m_panelCloseBorderRadius As String
    '    Dim m_progressTxtColor As String
    '    Dim m_progressColor As String
    '    Dim m_panelCategoryTxtColor As String
    '    Dim m_panelDownloadingTxtColor As String
    '    Dim m_panelDownloadBtnTxtColor As String
    '    Dim m_panelDownloadBtnBkgColor As String
    '    Dim m_panelDownloadBtnBorderColor As String
    '    Dim m_panelDownloadBtnBorderRadius As String
    '    Dim m_panelDownloadBtnBorderWidth As String

    '    Public Property panelBkgColor As String
    '        Get
    '            Return m_panelBkgColor
    '        End Get
    '        Set(value As String)
    '            m_panelBkgColor = value
    '        End Set
    '    End Property

    '    Public Property panelCloseTxtColor As String
    '        Get
    '            Return m_panelCloseTxtColor
    '        End Get
    '        Set(value As String)
    '            m_panelCloseTxtColor = value
    '        End Set
    '    End Property

    '    Public Property panelCloseBkgColor As String
    '        Get
    '            Return m_panelCloseBkgColor
    '        End Get
    '        Set(value As String)
    '            m_panelCloseBkgColor = value
    '        End Set
    '    End Property

    '    Public Property panelCloseBorderColor As String
    '        Get
    '            Return m_panelCloseBorderColor
    '        End Get
    '        Set(value As String)
    '            m_panelCloseBorderColor = value
    '        End Set
    '    End Property

    '    Public Property panelCloseBorderWidth As String
    '        Get
    '            Return m_panelCloseBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_panelCloseBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property panelCloseBorderRadius As String
    '        Get
    '            Return m_panelCloseBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_panelCloseBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property progressTxtColor As String
    '        Get
    '            Return m_progressTxtColor
    '        End Get
    '        Set(value As String)
    '            m_progressTxtColor = value
    '        End Set
    '    End Property

    '    Public Property progressColor As String
    '        Get
    '            Return m_progressColor
    '        End Get
    '        Set(value As String)
    '            m_progressColor = value
    '        End Set
    '    End Property

    '    Public Property panelCategoryTxtColor As String
    '        Get
    '            Return m_panelCategoryTxtColor
    '        End Get
    '        Set(value As String)
    '            m_panelCategoryTxtColor = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadingTxtColor As String
    '        Get
    '            Return m_panelDownloadingTxtColor
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadingTxtColor = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadBtnTxtColor As String
    '        Get
    '            Return m_panelDownloadBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadBtnBkgColor As String
    '        Get
    '            Return m_panelDownloadBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadBtnBorderColor As String
    '        Get
    '            Return m_panelDownloadBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadBtnBorderRadius As String
    '        Get
    '            Return m_panelDownloadBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property panelDownloadBtnBorderWidth As String
    '        Get
    '            Return m_panelDownloadBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_panelDownloadBtnBorderWidth = value
    '        End Set
    '    End Property

    'End Class

    'Public Class PageBrochure

    '    Dim m_brochureHeaderBkgColor As String
    '    Dim m_brochureTitleTxt As String
    '    Dim m_brochureCloseBtnTxtColor As String
    '    Dim m_brochureCloseBtnBkgColor As String
    '    Dim m_brochureCloseBtnBorderRadius As String
    '    Dim m_brochureCloseBtnBorderWidth As String
    '    Dim m_brochureCloseBtnBorderColor As String
    '    Dim m_brochureAddBtnTxtColor As String
    '    Dim m_brochureAddBtnBkgColor As String
    '    Dim m_brochureAddBtnBorderRadius As String
    '    Dim m_brochureAddBtnBorderWidth As String
    '    Dim m_brochureAddBtnBorderColor As String
    '    Dim m_brochureSavedBtnTxtColor As String
    '    Dim m_brochureSavedBtnBkgColor As String
    '    Dim m_brochureSavedBtnBorderRadius As String
    '    Dim m_brochureSavedBtnBorderWidth As String
    '    Dim m_brochureSavedBtnBorderColor As String

    '    Public Property brochureHeaderBkgColor As String
    '        Get
    '            Return m_brochureHeaderBkgColor
    '        End Get
    '        Set(value As String)
    '            m_brochureHeaderBkgColor = value
    '        End Set
    '    End Property

    '    Public Property brochureTitleTxt As String
    '        Get
    '            Return m_brochureTitleTxt
    '        End Get
    '        Set(value As String)
    '            m_brochureTitleTxt = value
    '        End Set
    '    End Property

    '    Public Property brochureCloseBtnTxtColor As String
    '        Get
    '            Return m_brochureCloseBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_brochureCloseBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property brochureCloseBtnBkgColor As String
    '        Get
    '            Return m_brochureCloseBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_brochureCloseBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property brochureCloseBtnBorderRadius As String
    '        Get
    '            Return m_brochureCloseBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_brochureCloseBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property brochureCloseBtnBorderWidth As String
    '        Get
    '            Return m_brochureCloseBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_brochureCloseBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property brochureCloseBtnBorderColor As String
    '        Get
    '            Return m_brochureCloseBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_brochureCloseBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property brochureAddBtnTxtColor As String
    '        Get
    '            Return m_brochureAddBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_brochureAddBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property brochureAddBtnBkgColor As String
    '        Get
    '            Return m_brochureAddBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_brochureAddBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property brochureAddBtnBorderRadius As String
    '        Get
    '            Return m_brochureAddBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_brochureAddBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property brochureAddBtnBorderWidth As String
    '        Get
    '            Return m_brochureAddBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_brochureAddBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property brochureAddBtnBorderColor As String
    '        Get
    '            Return m_brochureAddBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_brochureAddBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property brochureSavedBtnTxtColor As String
    '        Get
    '            Return m_brochureSavedBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_brochureSavedBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property brochureSavedBtnBkgColor As String
    '        Get
    '            Return m_brochureSavedBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_brochureSavedBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property brochureSavedBtnBorderRadius As String
    '        Get
    '            Return m_brochureSavedBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_brochureSavedBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property brochureSavedBtnBorderWidth As String
    '        Get
    '            Return m_brochureSavedBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_brochureSavedBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property brochureSavedBtnBorderColor As String
    '        Get
    '            Return m_brochureSavedBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_brochureSavedBtnBorderColor = value
    '        End Set
    '    End Property

    'End Class

    'Public Class PageImage

    '    Dim m_imageHeaderBkgColor As String
    '    Dim m_imageTitleTxt As String
    '    Dim m_imageCloseBtnTxtColor As String
    '    Dim m_imageCloseBtnBkgColor As String
    '    Dim m_imageCloseBtnBorderRadius As String
    '    Dim m_imageCloseBtnBorderWidth As String
    '    Dim m_imageCloseBtnBorderColor As String
    '    Dim m_imageAddBtnTxtColor As String
    '    Dim m_imageAddBtnBkgColor As String
    '    Dim m_imageAddBtnBorderRadius As String
    '    Dim m_imageAddBtnBorderWidth As String
    '    Dim m_imageAddBtnBorderColor As String
    '    Dim m_imageSavedBtnTxtColor As String
    '    Dim m_imageSavedBtnBkgColor As String
    '    Dim m_imageSavedBtnBorderRadius As String
    '    Dim m_imageSavedBtnBorderWidth As String
    '    Dim m_imageSavedBtnBorderColor As String

    '    Public Property imageHeaderBkgColor As String
    '        Get
    '            Return m_imageHeaderBkgColor
    '        End Get
    '        Set(value As String)
    '            m_imageHeaderBkgColor = value
    '        End Set
    '    End Property

    '    Public Property imageTitleTxt As String
    '        Get
    '            Return m_imageTitleTxt
    '        End Get
    '        Set(value As String)
    '            m_imageTitleTxt = value
    '        End Set
    '    End Property

    '    Public Property imageCloseBtnTxtColor As String
    '        Get
    '            Return m_imageCloseBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_imageCloseBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property imageCloseBtnBkgColor As String
    '        Get
    '            Return m_imageCloseBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_imageCloseBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property imageCloseBtnBorderRadius As String
    '        Get
    '            Return m_imageCloseBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_imageCloseBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property imageCloseBtnBorderWidth As String
    '        Get
    '            Return m_imageCloseBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_imageCloseBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property imageCloseBtnBorderColor As String
    '        Get
    '            Return m_imageCloseBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_imageCloseBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property imageAddBtnTxtColor As String
    '        Get
    '            Return m_imageAddBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_imageAddBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property imageAddBtnBkgColor As String
    '        Get
    '            Return m_imageAddBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_imageAddBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property imageAddBtnBorderRadius As String
    '        Get
    '            Return m_imageAddBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_imageAddBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property imageAddBtnBorderWidth As String
    '        Get
    '            Return m_imageAddBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_imageAddBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property imageAddBtnBorderColor As String
    '        Get
    '            Return m_imageAddBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_imageAddBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property imageSavedBtnTxtColor As String
    '        Get
    '            Return m_imageSavedBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_imageSavedBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property imageSavedBtnBkgColor As String
    '        Get
    '            Return m_imageSavedBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_imageSavedBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property imageSavedBtnBorderRadius As String
    '        Get
    '            Return m_imageSavedBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_imageSavedBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property imageSavedBtnBorderWidth As String
    '        Get
    '            Return m_imageSavedBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_imageSavedBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property imageSavedBtnBorderColor As String
    '        Get
    '            Return m_imageSavedBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_imageSavedBtnBorderColor = value
    '        End Set
    '    End Property

    'End Class

    'Public Class PageVideo

    '    Dim m_videoHeaderBkgColor As String
    '    Dim m_videoTitleTxt As String
    '    Dim m_videoCloseBtnTxtColor As String
    '    Dim m_videoCloseBtnBkgColor As String
    '    Dim m_videoCloseBtnBorderRadius As String
    '    Dim m_videoCloseBtnBorderWidth As String
    '    Dim m_videoCloseBtnBorderColor As String
    '    Dim m_videoAddBtnTxtColor As String
    '    Dim m_videoAddBtnBkgColor As String
    '    Dim m_videoAddBtnBorderRadius As String
    '    Dim m_videoAddBtnBorderWidth As String
    '    Dim m_videoAddBtnBorderColor As String
    '    Dim m_videoSavedBtnTxtColor As String
    '    Dim m_videoSavedBtnBkgColor As String
    '    Dim m_videoSavedBtnBorderRadius As String
    '    Dim m_videoSavedBtnBorderWidth As String
    '    Dim m_videoSavedBtnBorderColor As String

    '    Public Property videoHeaderBkgColor As String
    '        Get
    '            Return m_videoHeaderBkgColor
    '        End Get
    '        Set(value As String)
    '            m_videoHeaderBkgColor = value
    '        End Set
    '    End Property

    '    Public Property videoTitleTxt As String
    '        Get
    '            Return m_videoTitleTxt
    '        End Get
    '        Set(value As String)
    '            m_videoTitleTxt = value
    '        End Set
    '    End Property

    '    Public Property videoCloseBtnTxtColor As String
    '        Get
    '            Return m_videoCloseBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_videoCloseBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property videoCloseBtnBkgColor As String
    '        Get
    '            Return m_videoCloseBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_videoCloseBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property videoCloseBtnBorderRadius As String
    '        Get
    '            Return m_videoCloseBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_videoCloseBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property videoCloseBtnBorderWidth As String
    '        Get
    '            Return m_videoCloseBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_videoCloseBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property videoCloseBtnBorderColor As String
    '        Get
    '            Return m_videoCloseBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_videoCloseBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property videoAddBtnTxtColor As String
    '        Get
    '            Return m_videoAddBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_videoAddBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property videoAddBtnBkgColor As String
    '        Get
    '            Return m_videoAddBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_videoAddBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property videoAddBtnBorderRadius As String
    '        Get
    '            Return m_videoAddBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_videoAddBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property videoAddBtnBorderWidth As String
    '        Get
    '            Return m_videoAddBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_videoAddBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property videoAddBtnBorderColor As String
    '        Get
    '            Return m_videoAddBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_videoAddBtnBorderColor = value
    '        End Set
    '    End Property

    '    Public Property videoSavedBtnTxtColor As String
    '        Get
    '            Return m_videoSavedBtnTxtColor
    '        End Get
    '        Set(value As String)
    '            m_videoSavedBtnTxtColor = value
    '        End Set
    '    End Property

    '    Public Property videoSavedBtnBkgColor As String
    '        Get
    '            Return m_videoSavedBtnBkgColor
    '        End Get
    '        Set(value As String)
    '            m_videoSavedBtnBkgColor = value
    '        End Set
    '    End Property

    '    Public Property videoSavedBtnBorderRadius As String
    '        Get
    '            Return m_videoSavedBtnBorderRadius
    '        End Get
    '        Set(value As String)
    '            m_videoSavedBtnBorderRadius = value
    '        End Set
    '    End Property

    '    Public Property videoSavedBtnBorderWidth As String
    '        Get
    '            Return m_videoSavedBtnBorderWidth
    '        End Get
    '        Set(value As String)
    '            m_videoSavedBtnBorderWidth = value
    '        End Set
    '    End Property

    '    Public Property videoSavedBtnBorderColor As String
    '        Get
    '            Return m_videoSavedBtnBorderColor
    '        End Get
    '        Set(value As String)
    '            m_videoSavedBtnBorderColor = value
    '        End Set
    '    End Property

    'End Class


    'Public Class RootObject

    '    Dim m_General As General
    '    Dim m_PageLogin As PageLogin
    '    Dim m_SettingsPage As SettingsPage
    '    Dim m_PageMain As PageMain
    '    Dim m_PageContent As PageContent
    '    Dim m_PageSend As PageSend
    '    Dim m_UpdatePanel As UpdatePanel
    '    Dim m_PageBrochure As PageBrochure
    '    Dim m_PageImage As PageImage
    '    Dim m_PageVideo As PageVideo

    '    Public Property General As General
    '        Get
    '            Return m_General
    '        End Get
    '        Set(value As General)
    '            m_General = value
    '        End Set
    '    End Property

    '    Public Property PageLogin As PageLogin
    '        Get
    '            Return m_PageLogin
    '        End Get
    '        Set(value As PageLogin)
    '            m_PageLogin = value
    '        End Set
    '    End Property

    '    Public Property SettingsPage As SettingsPage
    '        Get
    '            Return m_SettingsPage
    '        End Get
    '        Set(value As SettingsPage)
    '            m_SettingsPage = value
    '        End Set
    '    End Property

    '    Public Property PageMain As PageMain
    '        Get
    '            Return m_PageMain
    '        End Get
    '        Set(value As PageMain)
    '            m_PageMain = value
    '        End Set
    '    End Property

    '    Public Property PageContent As PageContent
    '        Get
    '            Return m_PageContent
    '        End Get
    '        Set(value As PageContent)
    '            m_PageContent = value
    '        End Set
    '    End Property

    '    Public Property PageSend As PageSend
    '        Get
    '            Return m_PageSend
    '        End Get
    '        Set(value As PageSend)
    '            m_PageSend = value
    '        End Set
    '    End Property

    '    Public Property UpdatePanel As UpdatePanel
    '        Get
    '            Return m_UpdatePanel
    '        End Get
    '        Set(value As UpdatePanel)
    '            m_UpdatePanel = value
    '        End Set
    '    End Property

    '    Public Property PageBrochure As PageBrochure
    '        Get
    '            Return m_PageBrochure
    '        End Get
    '        Set(value As PageBrochure)
    '            m_PageBrochure = value
    '        End Set
    '    End Property

    '    Public Property PageImage As PageImage
    '        Get
    '            Return m_PageImage
    '        End Get
    '        Set(value As PageImage)
    '            m_PageImage = value
    '        End Set
    '    End Property

    '    Public Property PageVideo As PageVideo
    '        Get
    '            Return m_PageVideo
    '        End Get
    '        Set(value As PageVideo)
    '            m_PageVideo = value
    '        End Set
    '    End Property

    'End Class


    Public Class General
        Public Property appName As String
        Public Property analyticsId As String
        Public Property headerColor As String
        Public Property backgroundColor As String
        Public Property panelColor As String
        Public Property footerBackgroundColor As String
        Public Property backgroundImage As String
        Public Property panelLogo As String
        Public Property favoriteButtonImage As String
        Public Property settingsButtonImage As String
        Public Property favoriteOffImage As String
        Public Property favoriteOnImage As String
        Public Property removeItemImage As String
        Public Property removeEmailImage As String
        Public Property notificationImage As String
        Public Property nextMenuImage As String
        Public Property prevMenuImage As String
    End Class

    Public Class PageLogin
        Public Property loginTitleColor As String
        Public Property loginBtnTxtColor As String
        Public Property loginBtnBkgColor As String
        Public Property loginBtnRadius As String
        Public Property loginBtnBorderWidth As String
        Public Property loginBtnBorderColor As String
        Public Property loginLblTxtColor As String
        Public Property loginEditTxtColor As String
        Public Property loginEditTxtRadius As String
        Public Property loginEditTxtWidth As String
        Public Property loginEditTxtBorderColor As String
        Public Property loginEditBkgColor As String
        Public Property loginPanelRadius As String
        Public Property switchTrackColor As String
        Public Property switchColorOn As String
        Public Property switchColorOff As String
    End Class

    Public Class SettingsPage
        Public Property settingsButtonTextColor As String
        Public Property settingsButtonBackgroundColor As String
        Public Property settingsButtonBorderColor As String
        Public Property settingsButtonBorderWidth As String
        Public Property settingsButtonBorderRadius As String
        Public Property settingsLabelTextColor As String
    End Class

    Public Class PageMain
        Public Property settingsMenuTxtColor As String
        Public Property settingsMenuBkgColor As String
        Public Property languageTextColor As String
        Public Property languageMenuTxtColor As String
        Public Property languageMenuBkgColor As String
        Public Property newUpdateTxtColor As String
        Public Property newUpdateBkgColor As String
        Public Property welcomeTxtColor As String
        Public Property menuBkgColor As String
        Public Property menuTxtColor As String
        Public Property menuSeparatorColor As String
        Public Property subMenuBkgColor As String
        Public Property subMenuTxtColor As String
        Public Property subMenuSeparatorColor As String
        Public Property mainSendButtonTextColor As String
        Public Property mainSendButtonBackgroundColor As String
        Public Property mainSendButtonBorderColor As String
        Public Property mainSendButtonBorderWidth As String
        Public Property mainSendButtonBorderRadius As String
    End Class

    Public Class PageContent
        Public Property sectionTitleTxtColor As String
        Public Property sectionDescrTxtColor As String
        Public Property filterBorderRadius As String
        Public Property filterOnTxtColor As String
        Public Property filterOnBkgColor As String
        Public Property filterOnBorderWidth As String
        Public Property filterOnBorderColor As String
        Public Property filterOffTxtColor As String
        Public Property filterOffBkgColor As String
        Public Property filterOffBorderWidth As String
        Public Property filterOffBorderColor As String
        Public Property itemTxtColor As String
        Public Property addCartTxtColor As String
        Public Property addCartBkgColor As String
        Public Property addCartBorderRadius As String
        Public Property addCartBorderWidth As String
        Public Property addCartBorderColor As String
        Public Property savedCartTxtColor As String
        Public Property savedCartBkgColor As String
        Public Property savedCartBorderRadius As String
        Public Property savedCartBorderWidth As String
        Public Property savedCartBorderColor As String
        Public Property savedIconBkgColor As String
        Public Property searchTxtColor As String
        Public Property searchPlcTxtColor As String
        Public Property searchBkgColor As String
        Public Property searchNoResultTxtColor As String
        Public Property searchResTitleTxtColor As String
        Public Property searchResTitleBkgColor As String
        Public Property searchLabelTxtColor As String
        Public Property searchSpinColor As String
    End Class

    Public Class PageSend
        Public Property closeBtnTxtColor As String
        Public Property closeBtnBkgColor As String
        Public Property closeBtnBorderRadius As String
        Public Property closeBtnBorderWidth As String
        Public Property closeBtnBorderColor As String
        Public Property sendBtnTxtColor As String
        Public Property sendBtnBkgColor As String
        Public Property sendBtnBorderRadius As String
        Public Property sendBtnBorderWidth As String
        Public Property sendBtnBorderColor As String
        Public Property sendTitleTxtColor As String
        Public Property emailTextColor As String
        Public Property emailBkgColor As String
        Public Property emailPlcColor As String
        Public Property emailBorderColor As String
        Public Property emailBorderRadius As String
        Public Property emailBorderWidth As String
        Public Property commentsTextColor As String
        Public Property commentsBkgColor As String
        Public Property commentsPlcColor As String
        Public Property commentsBorderColor As String
        Public Property commentsBorderRadius As String
        Public Property commentsBorderWidth As String
        Public Property contactTextColor As String
        Public Property privacyTextColor As String
        Public Property boxEmailBkg As String
    End Class

    Public Class UpdatePanel
        Public Property panelBkgColor As String
        Public Property panelCloseTxtColor As String
        Public Property panelCloseBkgColor As String
        Public Property panelCloseBorderColor As String
        Public Property panelCloseBorderWidth As String
        Public Property panelCloseBorderRadius As String
        Public Property progressTxtColor As String
        Public Property progressColor As String
        Public Property panelCategoryTxtColor As String
        Public Property panelDownloadingTxtColor As String
        Public Property panelDownloadBtnTxtColor As String
        Public Property panelDownloadBtnBkgColor As String
        Public Property panelDownloadBtnBorderColor As String
        Public Property panelDownloadBtnBorderRadius As String
        Public Property panelDownloadBtnBorderWidth As String
    End Class

    Public Class PageBrochure
        Public Property brochureHeaderBkgColor As String
        Public Property brochureTitleTxt As String
        Public Property brochureCloseBtnTxtColor As String
        Public Property brochureCloseBtnBkgColor As String
        Public Property brochureCloseBtnBorderRadius As String
        Public Property brochureCloseBtnBorderWidth As String
        Public Property brochureCloseBtnBorderColor As String
        Public Property brochureAddBtnTxtColor As String
        Public Property brochureAddBtnBkgColor As String
        Public Property brochureAddBtnBorderRadius As String
        Public Property brochureAddBtnBorderWidth As String
        Public Property brochureAddBtnBorderColor As String
        Public Property brochureSavedBtnTxtColor As String
        Public Property brochureSavedBtnBkgColor As String
        Public Property brochureSavedBtnBorderRadius As String
        Public Property brochureSavedBtnBorderWidth As String
        Public Property brochureSavedBtnBorderColor As String
    End Class

    Public Class PageImage
        Public Property imageHeaderBkgColor As String
        Public Property imageTitleTxt As String
        Public Property imageCloseBtnTxtColor As String
        Public Property imageCloseBtnBkgColor As String
        Public Property imageCloseBtnBorderRadius As String
        Public Property imageCloseBtnBorderWidth As String
        Public Property imageCloseBtnBorderColor As String
        Public Property imageAddBtnTxtColor As String
        Public Property imageAddBtnBkgColor As String
        Public Property imageAddBtnBorderRadius As String
        Public Property imageAddBtnBorderWidth As String
        Public Property imageAddBtnBorderColor As String
        Public Property imageSavedBtnTxtColor As String
        Public Property imageSavedBtnBkgColor As String
        Public Property imageSavedBtnBorderRadius As String
        Public Property imageSavedBtnBorderWidth As String
        Public Property imageSavedBtnBorderColor As String
    End Class

    Public Class PageVideo
        Public Property videoHeaderBkgColor As String
        Public Property videoTitleTxt As String
        Public Property videoCloseBtnTxtColor As String
        Public Property videoCloseBtnBkgColor As String
        Public Property videoCloseBtnBorderRadius As String
        Public Property videoCloseBtnBorderWidth As String
        Public Property videoCloseBtnBorderColor As String
        Public Property videoAddBtnTxtColor As String
        Public Property videoAddBtnBkgColor As String
        Public Property videoAddBtnBorderRadius As String
        Public Property videoAddBtnBorderWidth As String
        Public Property videoAddBtnBorderColor As String
        Public Property videoSavedBtnTxtColor As String
        Public Property videoSavedBtnBkgColor As String
        Public Property videoSavedBtnBorderRadius As String
        Public Property videoSavedBtnBorderWidth As String
        Public Property videoSavedBtnBorderColor As String
    End Class

    Public Class RootObject
        Public Property general As General
        Public Property pageLogin As PageLogin
        Public Property settingsPage As SettingsPage
        Public Property pageMain As PageMain
        Public Property pageContent As PageContent
        Public Property pageSend As PageSend
        Public Property updatePanel As UpdatePanel
        Public Property pageBrochure As PageBrochure
        Public Property pageImage As PageImage
        Public Property pageVideo As PageVideo
    End Class


End Class
