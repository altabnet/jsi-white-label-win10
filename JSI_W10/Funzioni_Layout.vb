﻿Imports Newtonsoft.Json

Module Funzioni_Layout

    Public Async Function Cancella_Tutti_Json() As Task

        Try
            Dim MioFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder
            Dim IFiles() As String = System.IO.Directory.GetFiles(MioFolder.Path)

            For Each IlFile As String In IFiles
                If IlFile.ToUpper.IndexOf("layout".ToUpper) <= 0 AndAlso IlFile.ToUpper.EndsWith(".JSON") Then
                    System.IO.File.Delete(IlFile)
                End If
            Next

        Catch ex As Exception

        End Try


    End Function

    Public Async Function Scarica_Json_Layout(ByVal Codice_HQ As String) As Task

        Dim _Uri As String = "http://data.just-send-it.com/" & Codice_HQ & "/layout/layout.json"
        'C81552F04617F895A253DDAE7547FE2EDA148591

        '(va usato l'HashID   generico della company)
        Dim _FileName As String = "layout.json"

        'Scarica il File di Interesse
        '-----------------------------------
        Await Gestore_DownLoad.StartDownload(_Uri, _FileName)


        '-----------------------------------
        'Scarica_Le_Immagini
        '-----------------------------------

        Dim _TestoLayout As String = Await Funzioni_Layout.Dammi_Testo_File()
        If _TestoLayout.Trim.Length > 0 Then
            Dim LayAll As Layout.RootObject = JsonConvert.DeserializeObject(Of Layout.RootObject)(_TestoLayout)

            'Dim _FileName As String = "layout.json"
            'Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
            'Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)

            Dim laBase As String = App.Struttura_Globale.BaseInfo.UrlPath
            Dim _Origine As String = laBase & LayAll.general.panelLogo
            'Dim _Destinazione As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, LayAll.general.panelLogo)
            Dim _Destinazione As String = App.Firm_HdHash.Trim & ".jpeg"  ' LayAll.general.panelLogo.Replace("/", "\")
            'Dim _Destinazione As String = LayAll.general.panelLogo.Replace("/", "\")

            '   "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

            Try
                Dim MioFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder
                System.IO.File.Delete((MioFolder.Path & "\" & _Destinazione).Replace("/", "\"))
            Catch ex As Exception
            End Try


            'Scarica Logo Panel
            If LayAll.general.panelLogo.Trim.Length > 0 Then
                Await Gestore_DownLoad.StartDownload(_Origine, _Destinazione)
            End If

            'Scarica Logo Panel
            'Gestore_DownLoad.StartDownload(_Origine, _Destinazione)

            'Scarica Logo Panel
            'Gestore_DownLoad.StartDownload(_Origine, _Destinazione)

        End If


    End Function

    Public Async Function Dammi_Testo_File() As Task(Of String)

        Dim DammiTestoFile As String = ""

        Dim _FileName As String = "layout.json"
        Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
        Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)
        Try
            If MyFileInfo.Length > 0 Then

                'Metti in Stringa il Contenuto del File
                '-----------------------------------
                Await (Funzioni_Dati.Leggere_Testo_File_From_LocalFolder(_FileName))
                DammiTestoFile = App.Stringa_Contenuto_File.ToString()

            End If
        Catch ex As Exception
        End Try

        Return DammiTestoFile

    End Function



End Module
