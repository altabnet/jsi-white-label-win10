﻿
Imports Windows.Storage
Imports Windows.Storage.Pickers
Imports Windows.Networking.BackgroundTransfer
Imports System.Text.RegularExpressions
Imports SharpCompress.Archive
Imports JSI_Altab.Altab_WS
Imports Windows.System.Threading
'Imports SharpCompress.Common




Public Class Funzioni_Generali

    Public Shared Function Connessione_Internet_Attiva() As Boolean

        Try
            Dim connectionProfile As Windows.Networking.Connectivity.ConnectionProfile
            connectionProfile = Windows.Networking.Connectivity.NetworkInformation.GetInternetConnectionProfile()
            If (connectionProfile.GetNetworkConnectivityLevel() = Windows.Networking.Connectivity.NetworkConnectivityLevel.InternetAccess) Then
                Connessione_Internet_Attiva = True
            Else
                Connessione_Internet_Attiva = False
            End If
        Catch ex As Exception
            Connessione_Internet_Attiva = False
        End Try
    End Function

    Public Shared Async Function Invia_Email_In_Waiting() As Task(Of Task)

        If Not Funzioni_Generali.Connessione_Internet_Attiva Then
            Exit Function
        End If

        If App.Sto_Eseguendo Then
            Exit Function
        End If

        Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
        Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder


        Dim _IlCodice As String = ""
        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
        Dim Lista_Wait_Send = (From p In Conn_SQL.Table(Of Funzioni_Dati.Waiting_To_Send) Order By p.Codice_Univoco).ToList

        If Lista_Wait_Send.Count = 0 Then
            Exit Function
        End If
        Sto_Eseguendo = True

        Try

            For Indice As Integer = 0 To Lista_Wait_Send.Count - 1
                If _IlCodice <> Lista_Wait_Send(Indice).Codice_Univoco Then
                    _IlCodice = Lista_Wait_Send(Indice).Codice_Univoco


                    Dim Id_Selezionati As New ObservableCollection(Of Long)
                    Dim _StringaEmail As String = ""
                    '-----------------------------------------------------------------
                    ' Crea Array degli Id_Entities
                    '-----------------------------------------------------------------
                    For Invia As Integer = 0 To Lista_Wait_Send.Count - 1
                        If _IlCodice = Lista_Wait_Send(Invia).Codice_Univoco Then
                            Id_Selezionati.Add(Lista_Wait_Send(Invia).ID_Selezionato)
                            _StringaEmail = Lista_Wait_Send(Invia).Tutte_Email_Selezionate
                        End If
                    Next

                    '-----------------------------------------------------------------
                    ' Crea Array delle email..e verifica correttezza
                    '-----------------------------------------------------------------
                    Dim EMail_Selezionate As New ObservableCollection(Of String)
                    Dim MieEmail As String() = _StringaEmail.Split({CChar(","), CChar(";")})
                    For Each MiaMail As String In MieEmail
                        EMail_Selezionate.Add(MiaMail)
                    Next

                    '------------------------------------------------------------
                    '  Procede all'invio
                    '------------------------------------------------------------
                    Dim client As MobileServiceClient = New MobileServiceClient()
                    Dim _Ritorno As String = ""
                    Try
                        _Ritorno = Await client.SendMultipleAsync(localSettings.Values("Login_NoRete_HashID"), EMail_Selezionate, Id_Selezionati, "")

                        '------------------------------------
                        ' Conversione del JSON in Struttura
                        '------------------------------------
                        Dim result = Newtonsoft.Json.JsonConvert.DeserializeObject(_Ritorno)
                        Dim MioRitorno As List(Of File_Send.Risposta) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of File_Send.Risposta))(_Ritorno)


                        Dim _EsisteErrore As Boolean = False
                        For Each _RispostaRitorno As File_Send.Risposta In MioRitorno
                            If _RispostaRitorno.Status.ToString.ToUpper = "INVALID" Then
                                _EsisteErrore = True
                            End If
                        Next

                        If _EsisteErrore Then
                            '-------------------------------------------------------------------
                            ' Individuazione di un errore nell'invio
                            '-------------------------------------------------------------------
                        Else
                            '-------------------------------------------------------------------
                            ' Invio andato a buon fine
                            '-------------------------------------------------------------------

                            'Cancella_dalla_Tabella_Waiting()
                            Dim _Stringa As String = "Delete From Waiting_To_Send WHERE Codice_Univoco='" & _IlCodice & "'"
                            Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand(_Stringa)
                            _Cm1.ExecuteNonQuery()

                        End If

                    Catch ex As Exception
                        Dim Stringa As String = ex.ToString
                    End Try


                End If


            Next
            App.Sto_Eseguendo = False

        Catch ex As Exception
            App.Sto_Eseguendo = False
        End Try



    End Function


    Public Shared Function ValidaEmail(ByVal indirizzo As String) As Boolean

        'Dim espressioneRegolare As String
        'espressioneRegolare = "^([\w-\.]+)@" &
        '                      "((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|" &
        '                      "(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        'Return Regex.IsMatch(indirizzo, espressioneRegolare)


        If indirizzo.IndexOf("@") < 1 Then
            Return False
        Else
            If indirizzo.LastIndexOf(".") < indirizzo.IndexOf("@") Then
                Return False
            Else
                If indirizzo.IndexOf("@") <> indirizzo.LastIndexOf("@") Then
                    Return False
                Else
                    Return True
                End If
            End If
        End If

    End Function

    Public Shared Function Dammi_Misura_Stringa(ByVal Testo As String, ByVal Il_Font As FontFamily, ByVal Grandezza_Font As Integer) As Size

        Dim Miotesto As New TextBlock
        Miotesto.FontFamily = Il_Font
        Miotesto.FontSize = Grandezza_Font
        Miotesto.Text = Testo

        Dim MioSize As New Size(Double.PositiveInfinity, Double.PositiveInfinity)
        Miotesto.Measure(MioSize)

        Dammi_Misura_Stringa = Miotesto.DesiredSize
    End Function




    'Private Sub Download_File_Controlli(ByVal _IsoLanguage As String)
    '    Dim remoteUri As String = "http://data.just-send.it/" & _IsoLanguage & "/lang/"
    '    Dim fileName As String = "" & ".json"

    '    Dim myStringWebResource As String = Nothing

    '    ' Create a new WebClient instance.
    '    Dim myWebClient As New Net.WebClient()


    '    ' Concatenate the domain with the Web resource filename. Because DownloadFile 
    '    'requires a fully qualified resource name, concatenate the domain with the Web resource file name.
    '    myStringWebResource = remoteUri + fileName

    '    'Console.WriteLine("Downloading File ""{0}"" from ""{1}"" ......." + ControlChars.Cr + ControlChars.Cr, fileName, myStringWebResource)



    '    ' The DownloadFile() method downloads the Web resource and saves it into the current file-system folder.
    '    myWebClient.DownloadFile(myStringWebResource, fileName)

    '    'Console.WriteLine("Successfully Downloaded file ""{0}"" from ""{1}""", fileName, myStringWebResource)
    '    'Console.WriteLine((ControlChars.Cr + "Downloaded file saved in the following file system folder:" + ControlChars.Cr + ControlChars.Tab + Application.StartupPath))
    'End Sub

    'Private Sub StartDownload_Click(ByVal inputURL As String)
    '    Try
    '        Dim source As New Uri(inputURL)

    '        'Dim destinationFile As StorageFile = Await KnownFolders.PicturesLibrary.CreateFileAsync(title.Text, CreationCollisionOption.GenerateUniqueName)
    '        Dim destinationFile As StorageFile = KnownFolders.PicturesLibrary.CreateFileAsync("Pippo", CreationCollisionOption.GenerateUniqueName)

    '        Dim downloader As New BackgroundDownloader()

    '        Dim download As DownloadOperation = downloader.CreateDownload(source, destinationFile)

    '        ' Attach progress and completion handlers.
    '        HandleDownloadAsync(download, True)
    '    Catch ex As Exception
    '        LogException("Download Error", ex)
    '    End Try
    'End Sub

    'Public Sub StartDownload_Click(ByVal uriString As String, ByVal FileName As String)

    '    Dim uri = Windows.Foundation.Uri(uriString)
    '    Dim downloader = New Windows.Networking.BackgroundTransfer.BackgroundDownloader()

    '    ' Create a new download operation.
    '    download = downloader.CreateDownload(uri, newFile)

    '    ' Start the download and persist the promise to be able to cancel the download.
    '    promise = download.startAsync().[then](complete, [error], Progress)


    '    Try
    '        'Windows.Storage.KnownFolders.PicturesLibrary.CreateFileAsync(FileName, _Opzione)

    '        'Windows.Storage.KnownFolders.PicturesLibrary.CreateFileAsync(FileName, Windows.Storage.CreationCollisionOption.GenerateUniqueName).done(
    '        'Function(newFile) {
    '        '                        var uri = Windows.Foundation.Uri(uriString);
    '        '            var downloader = New Windows.Networking.BackgroundTransfer.BackgroundDownloader();

    '        '            // Create a New download operation.
    '        '            download = downloader.createDownload(Uri, newFile);

    '        '            // Start the download And persist the promise to be able to cancel the download.
    '        '            promise = download.startAsync().then(complete, error, Progress);
    '        '        }, error);


    '    Catch ex As Exception

    '    End Try

    'End Sub

    '
    ' FUNZIONA
    '
    'Public Shared Async Function StartDownload(ByVal InputUrl As String, ByVal FileName As String) As Task


    '    Dim MioPicker As FolderPicker = New FolderPicker()
    '    MioPicker.SuggestedStartLocation = PickerLocationId.Desktop
    '    MioPicker.ViewMode = PickerViewMode.Thumbnail
    '    MioPicker.FileTypeFilter.Add("*")
    '    Dim MioFolder As StorageFolder = Await MioPicker.PickSingleFolderAsync

    '    Dim source As New Uri(InputUrl)

    '    Dim destinationFile As StorageFile = Await MioFolder.CreateFileAsync(FileName, CreationCollisionOption.GenerateUniqueName)

    '    Dim downloader As New BackgroundDownloader()
    '    Dim download As DownloadOperation = downloader.CreateDownload(source, destinationFile)

    '    ' Attach progress and completion handlers.
    '    'HandleDownloadAsync(download, True)

    'End Function



    Public Shared Function MaxId(ByVal Lista_da_Analizzare As List(Of Funzioni_Dati.File_da_Inviare), ByVal Nome_Campo_Id As String) As Integer

        MaxId = 0
        For Each Oggetto As Funzioni_Dati.File_da_Inviare In Lista_da_Analizzare
            If MaxId < Oggetto.ID_Send Then
                MaxId = Oggetto.ID_Send
            End If
        Next

        Return MaxId

    End Function


    Public Shared Async Function Download_Massivo() As Task

        Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"
        Dim ImageFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))
        Dim VideoFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Videos"))
        Dim BrochureFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Pages"))
        Dim BrochureFolderZIP As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Pages\7z"))
        Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

        Dim _Scaricato As Boolean = False

        Do
            _Scaricato = False
            For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In App.Oggetti_in_Downloading
                Try
                    If OggettoSelezionato.p_Scaricato = False Then

                        _Scaricato = True

                        Dim Consideralo_Scaricato As Boolean = False
                        If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "IMG" Then

                            'Scarica IMG
                            Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
                            Dim _InputUrl As String = _UrlBase & Stringa_Thumb

                            Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                            Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                            Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                            Consideralo_Scaricato = Await (Gestore_DownLoad.StartDownload(_InputUrl, ImageFolder, _Filename))

                        Else
                            If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "VID" Then
                                'Scarica VID

                                Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
                                Dim _InputUrl As String = _UrlBase & Stringa_Thumb

                                Dim Inizio As Integer = Stringa_Thumb.LastIndexOf("/")
                                Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                                Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                                Consideralo_Scaricato = Await (Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename))

                            Else
                                If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "BRO" Then
                                    'Scarica BRO

                                    ' Gestione del Download
                                    '------------------------------------------------------------
                                    Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
                                    Dim _InputUrl As String = _UrlBase & Stringa_Thumb

                                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                                    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                                    Dim BuonFine As Boolean = Await (Gestore_DownLoad.StartDownload(_InputUrl, BrochureFolderZIP, _Filename))
                                    '------------------------------------------------------------
                                    ' FINE -  Gestione del Download
                                    '------------------------------------------------------------

                                    '        If BuonFine Then
                                    '            'Gestione dell'unzip
                                    '            '------------------------------------------------------------


                                    '            Dim archivio = Await localFolder.GetFileAsync("Pages\7z\" & _Filename)
                                    '            If Not archivio Is Nothing Then

                                    '                Dim MYStream As System.IO.Stream = New System.IO.FileStream(archivio.Path, FileMode.Open)

                                    '                If Not MYStream Is Nothing Then
                                    '                    Dim MioArchivio As SharpCompress.Archive.SevenZip.SevenZipArchive =
                                    '        SharpCompress.Archive.SevenZip.SevenZipArchive.Open(MYStream)

                                    '                    If Not MioArchivio Is Nothing Then

                                    '                        'Gestione dell'unzip
                                    '                        '------------------------------------------------------------
                                    '                        Dim PrimaElaborazione As Boolean = True

                                    '                        For Each Oggetto As SharpCompress.Archive.SevenZip.SevenZipArchiveEntry In MioArchivio.Entries

                                    '                            'Oggetto.OpenEntryStream()

                                    '                            Dim _cartella As String = localFolder.Path
                                    '                            Dim _dove1 As Integer = Oggetto.Key.IndexOf("/")
                                    '                            Dim _dove As Integer = Oggetto.Key.LastIndexOf("/")
                                    '                            If _dove > 0 Then
                                    '                                Dim _cartella1 As String = Oggetto.Key.Substring(0, _dove1)
                                    '                                _cartella = Oggetto.Key.Substring(0, _dove)

                                    '                                If PrimaElaborazione Then
                                    '                                    PrimaElaborazione = False
                                    '                                    Try
                                    '                                        Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync(_cartella.Replace("/", "\"), CreationCollisionOption.FailIfExists)
                                    '                                    Catch ex As Exception
                                    '                                    End Try
                                    '                                End If

                                    '                            End If

                                    '                            Dim _Ilpath As String = localFolder.Path & "\" & Oggetto.Key

                                    '                            If _Ilpath.ToUpper.EndsWith("0031.JPG") Then
                                    '                                _Ilpath = _Ilpath
                                    '                            End If

                                    '                            Dim fs As System.IO.FileStream = Await CreaFile(_Ilpath)

                                    '                            If Not fs Is Nothing Then
                                    '                                Try
                                    '                                    'Await Scrivilo(Oggetto, fs)
                                    '                                    Oggetto.WriteTo(fs)

                                    '                                    'Await Task.Delay(10000)


                                    '                                    Consideralo_Scaricato = False
                                    '                                Catch ex As Exception
                                    '                                    Consideralo_Scaricato = False
                                    '                                    Exit For
                                    '                                End Try
                                    '                            Else
                                    '                                Consideralo_Scaricato = False
                                    '                                Exit For
                                    '                            End If
                                    '                        Next
                                    '                        '------------------------------------------------------------
                                    '                        '  FINE  Gestione dell'unzip
                                    '                        '------------------------------------------------------------

                                    '                    End If
                                    '                End If
                                    '            End If

                                    '        End If
                                End If

                            End If

                        End If

                        ' Togli dalla Lista degli Oggetti da sacricare
                        '---------------------------------------------------
                        If Consideralo_Scaricato Then
                            Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(Nothing, OggettoSelezionato.ID_Object)
                        End If



                        ' Diminuisci dalla Lista del Settore
                        '---------------------------------------------------
                        'For Each Settore As File_Down.Categorie_To_Down In Me.Categorie_Down
                        '    If Settore.ID_Section = _idAccesso Then
                        '        If Settore.NumeroOggetti = 1 Then
                        '            Me.Categorie_Down.Remove(Settore)
                        '        Else
                        '            Settore.NumeroOggetti = Settore.NumeroOggetti - 1
                        '        End If
                        '        Exit For
                        '    End If
                        'Next

                        '
                        ' Ricarica la lista
                        '---------------------------------------------------
                        'Me.Lista_Down.ItemsSource = Nothing
                        'Me.Lista_Down.ItemsSource = Categorie_Down
                        Try

                            ' Togli dalla Lista del DB dei file da scaricare
                            '---------------------------------------------------
                            OggettoSelezionato.p_Scaricato = True
                            'App.Oggetti_in_Downloading.Remove(App.Oggetti_in_Downloading(0))
                        Catch ex As Exception

                        End Try
                    End If

                Catch ex1 As Exception
                    Dim Pippo As String = ex1.ToString
                End Try


            Next
            If _Scaricato = False Then
                Exit Do
            End If
        Loop

    End Function

    Public Shared Async Function CreaFile(ByVal IlPath As String) As Task(Of FileStream)

        Dim fs As FileStream = File.Create(IlPath.Replace("/", "\"))

        Return fs
    End Function

    'Public Shared Async Function Scrivilo(ByVal Oggetto As SharpCompress.Archive.SevenZip.SevenZipArchiveEntry, ByVal fs As FileStream) As Task
    '    Oggetto.WriteTo(fs)
    'End Function



    Public Shared Function GetSolidColorBrush(hex As String) As SolidColorBrush
        hex = hex.Replace("#", String.Empty)
        'Dim a As Byte = CByte(Convert.ToUInt32(hex.Substring(0, 2), 16))
        Dim a As Byte = 255
        Dim r As Byte = CByte(Convert.ToUInt32(hex.Substring(0, 2), 16))
        Dim g As Byte = CByte(Convert.ToUInt32(hex.Substring(2, 2), 16))
        Dim b As Byte = CByte(Convert.ToUInt32(hex.Substring(4, 2), 16))

        If hex.Trim.Length = 8 Then
            'a = CByte(Convert.ToUInt32(hex.Substring(0, 2), 16))
            r = CByte(Convert.ToUInt32(hex.Substring(2, 2), 16))
            g = CByte(Convert.ToUInt32(hex.Substring(4, 2), 16))
            b = CByte(Convert.ToUInt32(hex.Substring(6, 2), 16))
        End If
        Dim myBrush As New SolidColorBrush(Windows.UI.Color.FromArgb(a, r, g, b))
        Return myBrush

    End Function


    Public Shared Function Dammi_Nome_File_Puro(ByVal Nome_File As String) As String
        Dammi_Nome_File_Puro = ""
        If Nome_File.Trim.Length = 0 Then
            Exit Function
        End If
        Nome_File = Nome_File.Trim.ToUpper.Replace("/", "\")

        Dim Lo_Slash As Integer = Nome_File.LastIndexOf("\")
        If Lo_Slash >= 0 Then
            Nome_File = Nome_File.Substring(Lo_Slash + 1, Nome_File.Length - Lo_Slash - 1)
        End If

        Dammi_Nome_File_Puro = Nome_File

    End Function


    Public Shared Function Sto_Scaricandolo(ByVal ID_Oggetto As Integer) As Boolean

        Sto_Scaricandolo = False
        For Indice As Integer = 0 To App.Lista_File_in_Downloading.Count - 1
            If App.Lista_File_in_Downloading(Indice).Id_Oggetto = ID_Oggetto Then
                Sto_Scaricandolo = True
                Exit For
            End If
        Next

    End Function

    Public Shared Function Sto_Scaricandolo_File(ByVal Nome_File As String) As Boolean
        Nome_File = Dammi_Nome_File_Puro(Nome_File)

        Sto_Scaricandolo_File = False
        For Indice As Integer = 0 To App.Lista_File_in_Downloading_File.Count - 1
            If App.Lista_File_in_Downloading_File(Indice).Nome_File = Nome_File Then
                Sto_Scaricandolo_File = True
                Exit For
            End If
        Next

    End Function

    Public Shared Async Function Attendi_Oggetto_da_Scaricare(ByVal ID_Oggetto As Integer) As Task

        Do

            Dim Sto_Scaricandolo As Boolean = False
            For Indice As Integer = 0 To App.Lista_File_in_Downloading.Count - 1
                If App.Lista_File_in_Downloading(Indice).Id_Oggetto = ID_Oggetto Then
                    Sto_Scaricandolo = True
                    Exit For
                End If
            Next

            Await Task.Delay(5000)

            If Not Sto_Scaricandolo Then
                Exit Do
            End If

        Loop

    End Function


    Public Shared Async Function Attendi_Oggetto_da_Scaricare_File(ByVal Nome_File As String) As Task
        Nome_File = Dammi_Nome_File_Puro(Nome_File)
        Do

            Dim Sto_Scaricandolo As Boolean = False
            For Indice As Integer = 0 To App.Lista_File_in_Downloading_File.Count - 1
                If App.Lista_File_in_Downloading_File(Indice).Nome_File = Nome_File Then
                    Sto_Scaricandolo = True
                    Exit For
                End If
            Next

            Await Task.Delay(5000)

            If Not Sto_Scaricandolo Then
                Exit Do
            End If

        Loop

    End Function



    Public Shared Sub Inserisci_Oggetto_da_Scaricare(ByVal ID_Oggetto As Integer)

        Dim Sto_Scaricandolo As Boolean = False
        For Indice As Integer = 0 To App.Lista_File_in_Downloading.Count - 1
            If App.Lista_File_in_Downloading(Indice).Id_Oggetto = ID_Oggetto Then
                Sto_Scaricandolo = True
                Exit For
            End If
        Next

        If Not Sto_Scaricandolo Then

            Dim _Myogg As New Funzioni_Generali.File_in_Downloading
            _Myogg.Id_Oggetto = ID_Oggetto
            App.Lista_File_in_Downloading.Add(_Myogg)
        End If


    End Sub

    Public Shared Sub Inserisci_Oggetto_da_Scaricare_File(ByVal Nome_File As String)
        Nome_File = Dammi_Nome_File_Puro(Nome_File)
        Dim Sto_Scaricandolo As Boolean = False
        For Indice As Integer = 0 To App.Lista_File_in_Downloading_File.Count - 1
            If App.Lista_File_in_Downloading_File(Indice).Nome_File = Nome_File Then
                Sto_Scaricandolo = True
                Exit For
            End If
        Next

        If Not Sto_Scaricandolo Then

            Dim _Myogg As New Funzioni_Generali.File_in_Downloading
            _Myogg.Nome_File = Nome_File.Trim.Replace("/", "\").ToUpper
            App.Lista_File_in_Downloading_File.Add(_Myogg)
        End If
    End Sub



    Public Shared Sub Elimina_Oggetto_da_Scaricare(ByVal ID_Oggetto As Integer)

        For Indice As Integer = 0 To App.Lista_File_in_Downloading.Count - 1
            If App.Lista_File_in_Downloading(Indice).Id_Oggetto = ID_Oggetto Then
                App.Lista_File_in_Downloading.Remove(App.Lista_File_in_Downloading(Indice))
                Exit For
            End If
        Next

    End Sub

    Public Shared Sub Elimina_Oggetto_da_Scaricare_File(ByVal Nome_File As String)
        Nome_File = Dammi_Nome_File_Puro(Nome_File)
        For Indice As Integer = 0 To App.Lista_File_in_Downloading_File.Count - 1
            If App.Lista_File_in_Downloading_File(Indice).Nome_File = Nome_File Then
                App.Lista_File_in_Downloading_File.Remove(App.Lista_File_in_Downloading_File(Indice))
                Exit For
            End If
        Next

    End Sub




    Public Class File_in_Downloading

        Public Property Id_Oggetto As Integer
        Public Property Nome_File As String

    End Class




    'Dim PeriodicTimer As ThreadPoolTimer = ThreadPoolTimer.CreatePeriodicTimer(Function(source)
    '                                                                               '
    '                                                                               ' TODO: Work
    '                                                                               '

    '                                                                               '
    '                                                                               ' Update the UI thread by using the UI core dispatcher.
    '                                                                               '
    '                                                                               '
    '                                                                               ' UI components can be accessed within this scope.
    '                                                                               '

    '                                                                               'Dispatcher.RunAsync(CoreDispatcherPriority.High, Function()


    '                                                                               '                                                 End Function)

    '                                                                           End Function, TimeSpan.FromSeconds(60))



    'Private Function FunzioneTemporale()

    'End Function

End Class
