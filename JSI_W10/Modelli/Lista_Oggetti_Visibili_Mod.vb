﻿Namespace Lista_Oggetti

    Public Class Lista_Oggetti_Visibili
        Implements INotifyPropertyChanged

        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Dim m_Id As Integer = 0
        Dim m_Tipologia As String = ""
        Dim m_Thumbs As String = ""
        Dim m_Opacita As Double = 0
        Dim m_Title As String = ""
        Dim m_Order As Integer = 0
        Dim m_File_Reale As String = ""
        Dim m_File_Reale_SoloFile As String = ""
        Dim m_Testo_Bottone As String = ""
        Dim m_Testo_Bottone_Appoggio As String = ""
        Dim m_Nome_01 As String = "TXT"
        Dim m_Nome_02 As String = "LBL"
        Dim m_Nome_03 As String = "MSG"
        Dim m_ColoreSfondoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreTestoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreBordoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreTestoLabel As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ThicknessBottone As Thickness = New Thickness(0)


        '<SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        'Public Property Tab_ID As Integer

        Public Property ID As Integer
            Get
                Return m_Id
            End Get
            Set(value As Integer)
                m_Id = value
            End Set
        End Property

        Public Property IDSection As Integer

        Public Property Tipologia As String
            Get
                Return m_Tipologia
            End Get
            Set(value As String)
                m_Tipologia = value
            End Set
        End Property

        Public Property Thumbs As String
            Get
                Return m_Thumbs
            End Get
            Set(value As String)
                m_Thumbs = value
            End Set
        End Property

        Public Property Opacita As Double
            Get
                Return m_Opacita
            End Get
            Set(value As Double)
                m_Opacita = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Opacita"))
            End Set
        End Property

        Public Property Title As String
            Get
                Return m_Title
            End Get
            Set(value As String)
                m_Title = value
            End Set
        End Property

        Public Property Order As Integer
            Get
                Return m_Order
            End Get
            Set(value As Integer)
                m_Order = value
            End Set
        End Property

        Public Property File_Reale As String
            Get
                Return m_File_Reale
            End Get
            Set(value As String)
                m_File_Reale = value
            End Set
        End Property

        Public Property File_Reale_SoloFile As String
            Get
                Return m_File_Reale_SoloFile
            End Get
            Set(value As String)
                m_File_Reale_SoloFile = value
            End Set
        End Property

        Public Property Nome_01 As String
            Get
                Return m_Nome_01
            End Get
            Set(value As String)
                m_Nome_01 = value
            End Set
        End Property

        Public Property Nome_02 As String
            Get
                Return m_Nome_02
            End Get
            Set(value As String)
                m_Nome_02 = value
            End Set
        End Property

        Public Property Nome_03 As String
            Get
                Return m_Nome_03
            End Get
            Set(value As String)
                m_Nome_03 = value
            End Set
        End Property

        Public Property Testo_Bottone As String
            Get
                Return m_Testo_Bottone
            End Get
            Set(value As String)
                m_Testo_Bottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Testo_Bottone"))
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Sfondo_Bottone"))
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Bottone"))
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Bordo_Bottone"))
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Label"))
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Thickness_Bottone"))
            End Set
        End Property

        Public Property Testo_Bottone_Appoggio As String
            Get
                Return m_Testo_Bottone_Appoggio
            End Get
            Set(value As String)
                m_Testo_Bottone_Appoggio = value
            End Set
        End Property


        Public Property Colore_Sfondo_Bottone As SolidColorBrush
            Get
                If Testo_Bottone.Trim.ToUpper = "Downloading".ToUpper Then
                    m_ColoreSfondoBottone = Funzioni_Generali.GetSolidColorBrush("#CFDFF0")
                Else
                    If Testo_Bottone.Trim.ToUpper = App.Bottone_Add.Trim.ToUpper Then
                        m_ColoreSfondoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.addCartBkgColor)
                        'm_ColoreSfondoBottone = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")   'New SolidColorBrush(Windows.UI.Colors.LightBlue)
                    Else
                        m_ColoreSfondoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.savedCartBkgColor)
                        'm_ColoreSfondoBottone = Funzioni_Generali.GetSolidColorBrush("#03476F")   'New SolidColorBrush(Windows.UI.Colors.CadetBlue)
                    End If
                End If
                Return m_ColoreSfondoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreSfondoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Sfondo_Bottone"))
            End Set
        End Property

        Public Property Colore_Testo_Bottone As SolidColorBrush
            Get
                If Testo_Bottone.Trim.ToUpper = "Downloading".ToUpper Then
                    m_ColoreTestoBottone = Funzioni_Generali.GetSolidColorBrush("#03476F")
                Else
                    If Testo_Bottone.Trim.ToUpper = App.Bottone_Add.Trim.ToUpper Then
                        m_ColoreTestoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.addCartTxtColor)
                        'm_ColoreTestoBottone = Funzioni_Generali.GetSolidColorBrush("#03476F")   'New SolidColorBrush(Windows.UI.Colors.LightBlue)
                    Else
                        m_ColoreTestoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.savedCartTxtColor)
                        'm_ColoreTestoBottone = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")   'New SolidColorBrush(Windows.UI.Colors.CadetBlue)
                    End If
                End If
                Return m_ColoreTestoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreTestoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Bottone"))
            End Set
        End Property

        Public Property Colore_Bordo_Bottone As SolidColorBrush
            Get
                If Testo_Bottone.Trim.ToUpper = "Downloading".ToUpper Then
                    m_ColoreBordoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.savedCartTxtColor)
                Else
                    If Testo_Bottone.Trim.ToUpper = App.Bottone_Add.Trim.ToUpper Then
                        m_ColoreBordoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.addCartTxtColor)
                    Else
                        m_ColoreBordoBottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.savedCartTxtColor)
                    End If
                End If

                Return m_ColoreBordoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreBordoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Bordo_Bottone"))
            End Set
        End Property



        Public Property Colore_Testo_Label As SolidColorBrush
            Get
                m_ColoreTestoLabel = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.itemTxtColor)
                Return m_ColoreTestoLabel
            End Get
            Set(value As SolidColorBrush)
                m_ColoreTestoLabel = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Label"))
            End Set
        End Property


        Public Property Thickness_Bottone As Thickness
            Get
                If Testo_Bottone.Trim.ToUpper = "Downloading".ToUpper Then
                    m_ThicknessBottone = New Thickness(App.Layout_Globale.pageContent.savedCartBorderWidth)
                Else
                    If Testo_Bottone.Trim.ToUpper = App.Bottone_Add.Trim.ToUpper Then
                        m_ThicknessBottone = New Thickness(App.Layout_Globale.pageContent.addCartBorderWidth)
                    Else
                        m_ThicknessBottone = New Thickness(App.Layout_Globale.pageContent.savedCartBorderWidth)
                    End If
                End If


                Return m_ThicknessBottone
            End Get
            Set(value As Thickness)
                m_ThicknessBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Thickness_Bottone"))
            End Set
        End Property
    End Class



End Namespace
