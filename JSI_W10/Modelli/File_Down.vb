﻿Namespace File_Down

    Public Class Categorie_To_Down
        Implements INotifyPropertyChanged

        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Dim m_Id_Section As Integer = 0
        Dim m_Title As String = ""
        Dim m_NumeroOggetti As Integer = 0
        Dim m_TestoBottone As String = ""
        Dim m_Visibile As Visibility = Visibility.Visible
        Dim m_ColoreSfondoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreTestoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreTestoLabel As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ColoreBordoBottone As SolidColorBrush = New SolidColorBrush(Windows.UI.Colors.LightBlue)
        Dim m_ThichnessBottone As Thickness = New Thickness(0)

        Public Property ID_Section As Integer
            Get
                Return m_Id_Section
            End Get
            Set(value As Integer)
                m_Id_Section = value
            End Set
        End Property

        Public Property NumeroOggetti As Integer
            Get
                Return m_NumeroOggetti
            End Get
            Set(value As Integer)
                m_NumeroOggetti = value
            End Set
        End Property

        Public Property Title As String
            Get
                Return m_Title
            End Get
            Set(value As String)
                m_Title = value
            End Set
        End Property

        Public Property Testo_Bottone As String
            Get
                Return m_Title & " (" & CStr(NumeroOggetti) & ") "
            End Get
            Set(value As String)
                m_TestoBottone = value
            End Set
        End Property

        Public Property Visibile As Visibility
            Get
                Return m_Visibile
            End Get
            Set(value As Visibility)
                m_Visibile = value
            End Set
        End Property


        Public Property Colore_Sfondo_Bottone As SolidColorBrush
            Get
                Return m_ColoreSfondoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreSfondoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Sfondo_Bottone"))
            End Set
        End Property

        Public Property Colore_Testo_Bottone As SolidColorBrush
            Get
                Return m_ColoreTestoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreTestoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Bottone"))
            End Set
        End Property


        Public Property Colore_Testo_Label As SolidColorBrush
            Get
                Return m_ColoreTestoLabel
            End Get
            Set(value As SolidColorBrush)
                m_ColoreTestoLabel = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Testo_Label"))
            End Set
        End Property

        Public Property Colore_Bordo_Bottone As SolidColorBrush
            Get
                Return m_ColoreBordoBottone
            End Get
            Set(value As SolidColorBrush)
                m_ColoreBordoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Colore_Bordo_Bottone"))
            End Set
        End Property

        Public Property Thickness_Bottone As Thickness
            Get
                Return m_ThicknessBottone
            End Get
            Set(value As Thickness)
                m_ThicknessBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Thickness_Bottone"))
            End Set
        End Property

    End Class

End Namespace


