﻿Namespace Bottone_Info_Update

    Public Class Bottone_Update
        Implements INotifyPropertyChanged

        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Dim m_TestoBottone As String = ""


        Public Property Testo_Bottone As String
            Get
                Return m_TestoBottone
            End Get
            Set(value As String)
                m_TestoBottone = value
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Testo_Bottone"))
            End Set
        End Property

    End Class

End Namespace
