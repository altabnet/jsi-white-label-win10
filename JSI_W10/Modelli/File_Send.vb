﻿Namespace File_Send

    Public Class File_da_Inviare
        Implements INotifyPropertyChanged

        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Dim m_Id_Send As Integer = 0
        Dim m_Id_Section As Integer = 0
        Dim m_Id_Object As Integer = 0
        Dim m_Title As String = ""
        Dim m_File_Da_Inviare As String = ""
        Dim m_File_Thumbs As String = ""

        Public Property ID_Send As Integer
            Get
                Return m_Id_Send
            End Get
            Set(value As Integer)
                m_Id_Send = value
            End Set
        End Property
        Public Property ID_Section As Integer
            Get
                Return m_Id_Section
            End Get
            Set(value As Integer)
                m_Id_Section = value
            End Set
        End Property
        Public Property ID_Object As Integer
            Get
                Return m_Id_Object
            End Get
            Set(value As Integer)
                m_Id_Object = value
            End Set
        End Property
        Public Property Title As String
            Get
                Return m_Title
            End Get
            Set(value As String)
                m_Title = value
            End Set
        End Property

        Public Property File_da_Inviare As String
            Get
                Return m_File_Da_Inviare
            End Get
            Set(value As String)
                m_File_Da_Inviare = value
            End Set
        End Property
        Public Property File_Thumbs As String
            Get
                Return m_File_Thumbs
            End Get
            Set(value As String)
                m_File_Thumbs = value
            End Set
        End Property

    End Class


    Public Class Return_From_Send

        Public Property Risposte As List(Of Risposta)

    End Class

    Public Class Risposta

        Dim m_Status As Object
        Dim m_Hashing As Object


        Public Property Status As Object
            Get
                If m_Status Is Nothing Then
                    m_Status = ""
                End If
                Return m_Status
            End Get
            Set(value As Object)
                m_Status = value
            End Set
        End Property


        Public Property Hashing As Object
            Get
                If m_Hashing Is Nothing Then
                    m_Hashing = ""
                End If
                Return m_Hashing
            End Get
            Set(value As Object)
                m_Hashing = value
            End Set
        End Property

    End Class


End Namespace


