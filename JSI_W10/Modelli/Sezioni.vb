﻿Namespace Sections

    'Public Class Sezioni

    '    Dim m_ID As Integer           ' Identificatore del menù
    '    Dim m_FK As Integer           ' Se il menù è un ChildMenù FK è l' ID del Parent
    '    Dim m_Title As String = ""    ' Descrizione della categoria
    '    Dim m_CategoryColor As String = ""  '   Non Usata
    '    Dim m_Pos As Integer        ' Posizione all'interno del Menù
    '    Dim m_LandScapeImage As String = ""  '   Non Usata
    '    Dim m_PortraitImage As String = ""  '   Non Usata
    '    Dim m_LanguageID As Integer     'Id del Liunguaggio Selezionato
    '    Dim m_UnAvaibleForSend As Boolean = False    'Always false


    '    Public Property p_ID As Integer           ' Identificatore del menù
    '        Get
    '            Return m_ID
    '        End Get
    '        Set(value As Integer)
    '            m_ID = value
    '        End Set
    '    End Property

    '    Public Property p_FK As Integer           ' Se il menù è un ChildMenù FK è l' ID del Parent
    '        Get
    '            Return m_FK
    '        End Get
    '        Set(value As Integer)
    '            m_FK = value
    '        End Set
    '    End Property

    '    Public Property p_Title As String     ' Descrizione della categoria
    '        Get
    '            Return m_Title
    '        End Get
    '        Set(value As String)
    '            m_Title = value
    '        End Set
    '    End Property

    '    Public Property p_CategoryColor As String   '   Non Usata
    '        Get
    '            Return m_CategoryColor
    '        End Get
    '        Set(value As String)
    '            m_CategoryColor = value
    '        End Set
    '    End Property

    '    Public Property p_Pos As Integer        ' Posizione all'interno del Menù
    '        Get
    '            Return m_Pos
    '        End Get
    '        Set(value As Integer)
    '            m_Pos = value
    '        End Set
    '    End Property

    '    Public Property p_LandScapeImage As String   '   Non Usata
    '        Get
    '            Return m_LandScapeImage
    '        End Get
    '        Set(value As String)
    '            m_LandScapeImage = value
    '        End Set
    '    End Property

    '    Public Property p_PortraitImage As String   '   Non Usata
    '        Get
    '            Return m_PortraitImage
    '        End Get
    '        Set(value As String)
    '            m_PortraitImage = value
    '        End Set
    '    End Property

    '    Public Property p_LanguageID As Integer     'Id del Liunguaggio Selezionato
    '        Get
    '            Return m_LanguageID
    '        End Get
    '        Set(value As Integer)
    '            m_LanguageID = value
    '        End Set
    '    End Property

    '    Public Property p_UnAvaibleForSend As Boolean     'Always false
    '        Get
    '            Return m_UnAvaibleForSend
    '        End Get
    '        Set(value As Boolean)
    '            m_UnAvaibleForSend = value
    '        End Set
    '    End Property

    'End Class


End Namespace

