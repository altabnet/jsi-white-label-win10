﻿Namespace Videos
    Public Class Video

        Dim m_Section As String     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo del Video
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As String = ""    'Nome del File da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello


    End Class
End Namespace

