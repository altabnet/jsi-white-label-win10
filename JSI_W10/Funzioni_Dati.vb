﻿Imports Windows.Storage


Public Class Funzioni_Dati



    Public Shared Function isTableExists(ByVal tableName As String) As Boolean
        If tableName Is Nothing Then
            Return False
        End If

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("Select Count(*) FROM sqlite_master WHERE type='table' AND name='" & tableName & "'")
        If _Cm1.ExecuteScalar(Of Integer) = 0 Then
            isTableExists = False
        Else
            isTableExists = True
        End If
    End Function


    Dim Conn_SQL As SQLite.Net.SQLiteConnection

    Public Shared Async Function Leggere_Testo_File_From_LocalFolder(ByVal IlFile As String) As Task

        App.Stringa_Contenuto_File = ""
        Dim file As StorageFile = Await ApplicationData.Current.LocalFolder.GetFileAsync(IlFile)

        Using inputStream = Await file.OpenReadAsync()
            Using classicStream = inputStream.AsStreamForRead()
                Using streamReader = New StreamReader(classicStream)
                    While streamReader.Peek() >= 0
                        App.Stringa_Contenuto_File &= streamReader.ReadLine()
                        'Debug.WriteLine(String.Format("the line is {0}", streamReader.ReadLine()))
                    End While
                End Using
            End Using
        End Using

    End Function


    Public Shared Function Is_Oggetto_Da_Scaricare(ByVal Id_Object As Integer) As Boolean

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        Dim ListaSezioni As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.ID_Object = Id_Object).ToList

        If ListaSezioni.Count = 0 Then
            Is_Oggetto_Da_Scaricare = False
        Else
            Is_Oggetto_Da_Scaricare = True
        End If

    End Function

    Public Shared Sub Elimina_Oggetto_Da_Scaricare(ByRef Label_da_Aggiornare As TextBlock, ByVal Id_Object As Integer)

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        'Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from File_da_Scaricare where Id_Object=" & CStr(Id_Object).Trim)
        '_Cm1.ExecuteNonQuery()


        'Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from File_da_Scaricare where p_File_da_Scaricare='" & File_DownLoad.Trim & "'")
        '_Cm1.ExecuteNonQuery()
        Dim ListaSezioni As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.ID_Object = Id_Object).ToList
        If ListaSezioni.Count > 0 Then

            If ListaSezioni(0).p_File_da_Scaricare.Trim.Length > 0 Then

                Windows.Storage.ApplicationData.Current.LocalSettings.Values("Effettuato_FK") = ListaSezioni(0).ID_Section

                Dim _IlFile As String = ListaSezioni(0).p_File_da_Scaricare

                Dim ListaSezioni_Per_Immagine As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.p_File_da_Scaricare.Equals(_IlFile)).ToList

                Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from File_da_Scaricare where p_File_da_Scaricare='" & _IlFile & "'")
                _Cm1.ExecuteNonQuery()

            End If
        End If


        'If Not Label_da_Aggiornare Is Nothing Then
        '    Label_da_Aggiornare.Text = Date.Now.ToString
        'End If

    End Sub




    Public Class Struttura

        Public Shared Async Function Creazione_Directory() As Task
            Dim NomeDirectory As New List(Of String)
            NomeDirectory.Add("Data")
            NomeDirectory.Add("Thumbs")
            NomeDirectory.Add("Images")
            NomeDirectory.Add("Videos")
            NomeDirectory.Add("Pages")
            NomeDirectory.Add("Pages\7z")
            NomeDirectory.Add("PDF")
            NomeDirectory.Add("Videos\Mp4")

            Dim localFolder As StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

            For Indice As Integer = 0 To NomeDirectory.Count - 1
                Try
                    Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync(NomeDirectory(Indice), CreationCollisionOption.FailIfExists)
                Catch ex As Exception
                End Try
            Next

        End Function

        Public Shared Async Function Eliminazione_Directory() As Task

            Dim NomeDirectory As New List(Of String)
            NomeDirectory.Add("Videos\Mp4")
            NomeDirectory.Add("Data")
            NomeDirectory.Add("Thumbs")
            NomeDirectory.Add("Images")
            NomeDirectory.Add("Videos")
            NomeDirectory.Add("PDF")
            NomeDirectory.Add("Pages\7z")
            NomeDirectory.Add("Pages")


            Dim localFolder As StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

            For Indice As Integer = 0 To NomeDirectory.Count - 1

                Try
                    Dim NewFolder As StorageFolder = Await (Windows.Storage.StorageFolder.GetFolderFromPathAsync(localFolder.Path & "\" & NomeDirectory(Indice)))
                    Await NewFolder.DeleteAsync()

                Catch ex As Exception
                    Dim pippo As String = ex.Message
                End Try
            Next
        End Function



        Public Shared Async Function Aggiorna_Tutti_i_Dati_DB_e_Thumbs() As Task

            '-------------------------------------------------------------------------
            'Cancella i Deleted  (Images / Video /  Brochures / Website)
            '-------------------------------------------------------------------------

            '------------------------------------------------------------------------
            ' Metti le Strutture da Object in  tipizzate (quelle del DB)
            '------------------------------------------------------------------------

            '------------------------------------------------------------------------
            ' Verifica le differenze  ed eventualmente aggiorna
            '------------------------------------------------------------------------

            '------------------------------------------------------------------------
            ' Crea Lista Thumbs da aggiornare
            '------------------------------------------------------------------------

            '------------------------------------------------------------------------
            ' Crea Lista Download da aggiornare
            '------------------------------------------------------------------------





        End Function


    End Class


    Class LocalDatabase
        Public Shared Async Sub CreateDatabase()

            Dim Messaggio As String = ""
            Try

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")

                Using conn As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                    'conn.CreateTable(Of Documenti_Catalogo)()

                    If Not Funzioni_Dati.isTableExists("Sezioni") Then conn.CreateTable(Of Funzioni_Dati.Sezioni)()
                    If Not Funzioni_Dati.isTableExists("Immagini") Then conn.CreateTable(Of Funzioni_Dati.Immagini)()
                    If Not Funzioni_Dati.isTableExists("Brochure") Then conn.CreateTable(Of Funzioni_Dati.Brochure)()
                    If Not Funzioni_Dati.isTableExists("Video") Then conn.CreateTable(Of Funzioni_Dati.Video)()
                    If Not Funzioni_Dati.isTableExists("SitiWeb") Then conn.CreateTable(Of Funzioni_Dati.SitiWeb)()

                    If Not Funzioni_Dati.isTableExists("InfoBase") Then conn.CreateTable(Of Funzioni_Dati.InfoBase)()
                    If Not Funzioni_Dati.isTableExists("Relazione_Brochure_ID") Then conn.CreateTable(Of Funzioni_Dati.Relazione_Brochure_ID)()
                    If Not Funzioni_Dati.isTableExists("Relazione_Brochure_Page") Then conn.CreateTable(Of Funzioni_Dati.Relazione_Brochure_Page)()

                    If Not Funzioni_Dati.isTableExists("Relazione_Immagini_ID") Then conn.CreateTable(Of Funzioni_Dati.Relazione_Immagini_ID)()
                    If Not Funzioni_Dati.isTableExists("Relazione_Video_ID") Then conn.CreateTable(Of Funzioni_Dati.Relazione_Video_ID)()
                    If Not Funzioni_Dati.isTableExists("File_da_Scaricare") Then conn.CreateTable(Of Funzioni_Dati.File_da_Scaricare)()
                    If Not Funzioni_Dati.isTableExists("File_To_Download") Then conn.CreateTable(Of Funzioni_Dati.File_To_Download)()

                    If Not Funzioni_Dati.isTableExists("File_To_Send") Then conn.CreateTable(Of Funzioni_Dati.File_To_Send)()
                    If Not Funzioni_Dati.isTableExists("Waiting_To_Send") Then conn.CreateTable(Of Funzioni_Dati.Waiting_To_Send)()

                    If Not Funzioni_Dati.isTableExists("Lingue_Disponibili") Then conn.CreateTable(Of Funzioni_Lingue.Lingue_Disponibili)()

                    'conn.CreateTable(Of Funzioni_Dati.Temp_Relazione_Brochure_ID)()
                    'conn.CreateTable(Of Funzioni_Dati.Temp_Relazione_Brochure_Page)()
                    'conn.CreateTable(Of Funzioni_Dati.Temp_Relazione_Immagini_ID)()
                    'conn.CreateTable(Of Funzioni_Dati.Temp_Relazione_Video_ID)()
                End Using

            Catch ex As Exception
                Messaggio = ex.Message
            End Try

            If Messaggio.Trim.Length > 0 Then
                Dim MioMessaggio As Windows.UI.Popups.MessageDialog = New Windows.UI.Popups.MessageDialog(Messaggio)
                Await (MioMessaggio.ShowAsync())
            End If

        End Sub

        Public Shared Sub DeleteDatabase()

            Try
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                File.Delete(sqlpath)
            Catch ex As Exception

            End Try

        End Sub


        Public Shared Function Database_Exists() As Boolean

            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Database_Exists = File.Exists(sqlpath)

        End Function

        Public Shared Async Function Scarica_Le_Thumbs(ByVal Thumbs_Folder As StorageFolder) As Task

            Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

            For Indice = 0 To App.Thumbs_Da_Scaricare.Count - 1

                Dim Stringa_Thumb As String = App.Thumbs_Da_Scaricare(Indice)
                'Esegui il DownLoad
                Dim _InputUrl As String = _UrlBase & Stringa_Thumb

                Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                Await Gestore_DownLoad.StartDownload(_InputUrl, Thumbs_Folder, _Filename)
            Next

        End Function

        'Public Shared Async Function Gestisci_Tabelle_TEMP() As Task

        '    Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        '    Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        '    '-------------------------------------
        '    ' Cancello tutti i dati e ce li riscrivo
        '    '-------------------------------------
        '    ' CANCELLAZIONE

        '    Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from TEMP_Relazione_Brochure_Page ")
        '    _Cm1.ExecuteNonQuery()

        '    Dim _Cm2 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from TEMP_Relazione_Brochure_ID ")
        '    _Cm2.ExecuteNonQuery()

        '    Dim _Cm3 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from TEMP_Relazione_Immagini_ID ")
        '    _Cm3.ExecuteNonQuery()

        '    Dim _Cm4 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from TEMP_Relazione_Video_ID ")
        '    _Cm4.ExecuteNonQuery()



        '    '------------------------
        '    ' RISCRIVO

        '    For Indi As Integer = 0 To App.Le_Mie_Relazioni_Brochure.Count - 1
        '        Conn_SQL.Insert(App.Le_Mie_Relazioni_Brochure(Indi))
        '    Next

        '    For Indi As Integer = 0 To App.Le_Mie_Relazioni_Page.Count - 1
        '        Conn_SQL.Insert(App.Le_Mie_Relazioni_Page(Indi))
        '    Next

        '    For Indi As Integer = 0 To App.Le_Mie_Relazioni_Immagini.Count - 1
        '        Conn_SQL.Insert(App.Le_Mie_Relazioni_Immagini(Indi))
        '    Next

        '    For Indi As Integer = 0 To App.Le_Mie_Relazioni_Video.Count - 1
        '        Conn_SQL.Insert(App.Le_Mie_Relazioni_Video(Indi))
        '    Next

        'End Function

        Public Class Video

            Public Shared Async Function Video_Aggiorna_Dati_e_Thumbs(ByVal Video_Scaricati As List(Of Funzioni_Dati.Video), ByVal VideoFolder As StorageFolder) As Task(Of List(Of Funzioni_Dati.Video))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

                Dim ListaVideo As List(Of Funzioni_Dati.Video)


                Dim Indice As Integer = 0
                Dim Scarica_Thumbs As Boolean = False
                Dim Scarica_File As Boolean = False

                Dim _Cm As SQLite.Net.SQLiteCommand
                Dim _StrUpd As String = ""
                Conn_SQL.BeginTransaction()
                Try

                    For Indice = 0 To Video_Scaricati.Count - 1

                        Dim _MYid As Integer = Video_Scaricati(Indice).p_ID
                        Dim _MYsec As Integer = Video_Scaricati(Indice).p_Section

                        ListaVideo = (From p In Conn_SQL.Table(Of Funzioni_Dati.Video) Where p.p_ID = _MYid And p.p_Section = _MYsec).ToList


                        If ListaVideo.Count = 0 Then
                            'Inserisci_Riga
                            '-------------------------------------------------------------------
                            Conn_SQL.Insert(Video_Scaricati(Indice))

                            'Scaricare la Thumbs
                            '-------------------------------------------------------------------
                            Scarica_Thumbs = True
                            Scarica_File = True
                        Else
                            'Verifica se VersioneMod è modificato
                            '-------------------------------------------------------------------
                            If ListaVideo(0).p_VersionMod <> Video_Scaricati(Indice).p_VersionMod Then
                                ' Se Modificato Aggiornare la Riga e riscaricare la Thumb
                                '-------------------------------------------------------------------

                                Conn_SQL.Delete(ListaVideo(0))
                                Conn_SQL.Insert(Video_Scaricati(Indice))

                                'Conn_SQL.Update(Video_Scaricati(Indice))

                                'Scaricare la Thumbs
                                Scarica_Thumbs = True

                            Else
                                Scarica_Thumbs = False
                            End If

                            ' Verifica se occorre scaricare il File completo
                            '-------------------------------------------------------------------
                            If ListaVideo(0).p_VersionFile <> Video_Scaricati(Indice).p_VersionFile Then
                                Scarica_File = True
                            Else
                                Scarica_File = False
                            End If
                        End If

                        '------
                        ' Scarico Immediato del File
                        '------'-------------------------------------------------------------------
                        If Scarica_Thumbs Then
                            Dim Stringa_Thumb As String = Video_Scaricati(Indice).p_Thumb.Trim
                            If Stringa_Thumb.Length > 0 Then

                                If App.Thumbs_SCARICATE.IndexOf(Stringa_Thumb) < 0 Then
                                    App.Thumbs_SCARICATE.Add(Stringa_Thumb)

                                    Dim result As IEnumerable(Of String) = App.Thumbs_Da_Scaricare.Where(Function(s) s.Contains(Stringa_Thumb))
                                    If result.Count = 0 Then

                                        'Esegui il DownLoad
                                        Dim _InputUrl As String = _UrlBase & Stringa_Thumb

                                        Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                                        Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                                        Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                                        Await Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename)

                                        'Segnato epr non riscaricarlo
                                        '------------------------------------------------------------------- 
                                        App.Thumbs_Da_Scaricare.Add(Stringa_Thumb)
                                    End If
                                End If
                            End If
                        End If


                        If Scarica_File Then
                            ' Inserisci in Array per Scaricare
                            Dim StringaComplessa As String = ""
                            StringaComplessa &= "VIDEO     "   'Primi 10 la tipologia
                            StringaComplessa &= (Video_Scaricati(Indice).p_ID & New String(" ", 10)).Substring(0, 10)  'ID specifico
                            StringaComplessa &= (Video_Scaricati(Indice).p_Section & New String(" ", 10)).Substring(0, 10)  'Categoria
                            StringaComplessa &= (Video_Scaricati(Indice).p_ToDownLoad & New String(" ", 20)).Substring(0, 20)   'Nome del File


                            Dim result As IEnumerable(Of String) = App.Da_Scaricare.Where(Function(s) s.Contains(StringaComplessa))
                            If result.Count = 0 Then
                                ' Segna come  da Scaricare
                                '-------------------------------------------------------------------
                                App.Da_Scaricare.Add(StringaComplessa)
                            End If

                        End If
                    Next

                    Conn_SQL.Commit()

                Catch ex As Exception
                    Conn_SQL.Rollback()
                Finally
                    Conn_SQL.Close()
                End Try


            End Function


            Public Shared Async Function Video_Deleted(ByVal Lista_Da_Cancellare As List(Of UpdateSync.VideosDeleted)) As Task(Of Task())
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

                Dim Indice As Integer = 0


                For Indice = 0 To Lista_Da_Cancellare.Count - 1

                    For CollSec As Integer = 0 To Lista_Da_Cancellare(Indice).Section.Count - 1
                        Dim _MYid As Integer = Lista_Da_Cancellare(Indice).ID
                        Dim _MYsec As Integer = Lista_Da_Cancellare(Indice).Section(CollSec)

                        'Delete_Video
                        Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from Video where p_Id=" & CStr(_MYid).Trim & " and p_Section=" & CStr(_MYsec).Trim)
                        _Cm1.ExecuteNonQuery()
                    Next
                Next

            End Function




            Public Shared Function Video_Genera_da_NuovoStruttura() As List(Of Funzioni_Dati.Video)

                Dim I_Miei_Video As New List(Of Funzioni_Dati.Video)

                For Indi As Integer = 0 To App.Struttura_Globale.Videos.Count - 1

                    For CollSec As Integer = 0 To App.Struttura_Globale.Videos(Indi).Section.Count - 1
                        Dim Nuovo As New Funzioni_Dati.Video
                        Nuovo.p_Section = App.Struttura_Globale.Videos(Indi).Section(CollSec)
                        Nuovo.p_ID = App.Struttura_Globale.Videos(Indi).ID
                        Nuovo.p_Title = App.Struttura_Globale.Videos(Indi).Title
                        Nuovo.p_HashID = App.Struttura_Globale.Videos(Indi).HashID
                        Nuovo.p_Tags = App.Struttura_Globale.Videos(Indi).Tags

                        Nuovo.p_VersionMod = App.Struttura_Globale.Videos(Indi).VersionMod
                        Nuovo.p_VersionFile = App.Struttura_Globale.Videos(Indi).VersionFile

                        Nuovo.p_Thumb = App.Struttura_Globale.Videos(Indi).Thumb
                        Nuovo.p_ToDownLoad = App.Struttura_Globale.Videos(Indi).ToDownLoad

                        If App.Struttura_Globale.Videos(Indi).ToExtract Is Nothing Then
                            Nuovo.p_ToExtract = True
                        Else
                            Nuovo.p_ToExtract = App.Struttura_Globale.Videos(Indi).ToExtract
                        End If

                        Nuovo.p_Active = App.Struttura_Globale.Videos(Indi).Active
                        Nuovo.p_Order = App.Struttura_Globale.Videos(Indi).Order
                        Nuovo.p_Deleted = App.Struttura_Globale.Videos(Indi).Deleted
                        Nuovo.p_LanguageID = App.Struttura_Globale.Videos(Indi).LanguageID
                        Nuovo.p_UnAvailableForSend = App.Struttura_Globale.Videos(Indi).UnAvailableForSend

                        I_Miei_Video.Add(Nuovo)

                    Next
                Next

                Return I_Miei_Video
            End Function


            Public Shared Function Video_Settore_Call(ByVal Id_Section As Integer, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Video)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Video) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Video) Where p.p_Section = Id_Section And p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Video_Filtro_Call(ByVal Filtro As String, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Video)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Video) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Video) Where (p.p_Title.Contains(Filtro) Or p.p_Tags.Contains(Filtro)) And p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Video_Call(ByVal Id_language As Integer) As List(Of Funzioni_Dati.Video)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Video) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Video) Where p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Video_Call_ALL() As List(Of Funzioni_Dati.Video)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Video) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Video)).ToList

                Return ListaSezioni

            End Function
        End Class

        Public Class Immagini

            Public Shared Async Function Immagini_Aggiorna_Dati_e_Thumbs(ByVal Immagini_Scaricate As List(Of Funzioni_Dati.Immagini), ByVal ImageFolder As StorageFolder) As Task(Of List(Of Funzioni_Dati.Immagini))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim ListaImmagini As List(Of Funzioni_Dati.Immagini)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

                Dim Indice As Integer = 0

                Dim _Cm As SQLite.Net.SQLiteCommand
                Dim _StrUpd As String = ""
                Conn_SQL.BeginTransaction()
                Try

                    For Indice = 0 To Immagini_Scaricate.Count - 1

                        Dim _MYid As Integer = Immagini_Scaricate(Indice).p_ID
                        Dim _MYsec As Integer = Immagini_Scaricate(Indice).p_Section

                        ListaImmagini = (From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini) Where p.p_ID = _MYid And p.p_Section = _MYsec).ToList

                        'If _MYsec = 498 Or _MYsec = 499 Then
                        '    Dim bito As Integer = 0
                        '    abito = 1
                        'End If


                        If ListaImmagini.Count = 0 Then
                            'Inserisci_Riga
                            '------------------------------------------------------

                            'Dim _StrIns As String = "Insert INTO Immagini ( "

                            '_StrIns &= " p_Section , p_ID, p_Title , "
                            '_StrIns &= " p_HashID , p_Tags, p_VersionMod, "
                            '_StrIns &= " p_VersionFile, p_Thumb, p_ToDownLoad, "
                            '_StrIns &= " p_ToExtract, p_Active, p_Order, "
                            '_StrIns &= " p_Deleted, p_LanguageID, p_UnAvailableForSend "

                            '_StrIns &= " ) VALUES ( "

                            ''_StrIns &= Immagini_Scaricate(Indice).p_Section & " ,  " & Immagini_Scaricate(Indice).p_ID & " , " & Immagini_Scaricate(Indice).p_Title & " , "
                            ''_StrIns &= Immagini_Scaricate(Indice).p_HashID & " ,  " & Immagini_Scaricate(Indice).p_Tags & " , " & Immagini_Scaricate(Indice).p_VersionMod & " , "
                            ''_StrIns &= Immagini_Scaricate(Indice).p_VersionFile & " , " & Immagini_Scaricate(Indice).p_Thumb & " , " & Immagini_Scaricate(Indice).p_ToDownLoad & " , "
                            ''_StrIns &= Immagini_Scaricate(Indice).p_ToExtract & " , " & Immagini_Scaricate(Indice).p_Active & " , " & Immagini_Scaricate(Indice).p_Order & " , "
                            ''_StrIns &= Immagini_Scaricate(Indice).p_Deleted & " , " & Immagini_Scaricate(Indice).p_LanguageID & " ,  " & Immagini_Scaricate(Indice).p_UnAvailableForSend
                            ''_StrIns &= " {1} ,  {2} ,  {3} , "
                            ''_StrIns &= " {4} ,  {5} ,  {6} , "
                            ''_StrIns &= " {7} ,  {8} ,  {9} , "
                            ''_StrIns &= " {10} ,  {11} ,  {12}  "

                            '_StrIns &= " ? ,  ? ,  ? , "
                            '_StrIns &= " ? ,  ? ,  ? , "
                            '_StrIns &= " ? ,  ? ,  ? , "
                            '_StrIns &= " ? ,  ? ,  ? , "
                            '_StrIns &= " ? ,  ? ,  ?  "

                            '_StrIns &= " ) "

                            'Dim Parametri(15) As Object
                            'Parametri(0) = Immagini_Scaricate(Indice).p_Section
                            'Parametri(1) = Immagini_Scaricate(Indice).p_ID
                            'Parametri(2) = Immagini_Scaricate(Indice).p_Title

                            'Parametri(3) = Immagini_Scaricate(Indice).p_HashID
                            'Parametri(4) = Immagini_Scaricate(Indice).p_Tags
                            'Parametri(5) = Immagini_Scaricate(Indice).p_VersionMod

                            'Parametri(6) = Immagini_Scaricate(Indice).p_VersionFile
                            'Parametri(7) = Immagini_Scaricate(Indice).p_Thumb
                            'Parametri(8) = Immagini_Scaricate(Indice).p_ToDownLoad

                            'Parametri(9) = Immagini_Scaricate(Indice).p_ToExtract
                            'Parametri(10) = Immagini_Scaricate(Indice).p_Active
                            'Parametri(11) = Immagini_Scaricate(Indice).p_Order

                            'Parametri(12) = Immagini_Scaricate(Indice).p_Deleted
                            'Parametri(13) = Immagini_Scaricate(Indice).p_LanguageID
                            'Parametri(14) = Immagini_Scaricate(Indice).p_UnAvailableForSend


                            '_Cm = Conn_SQL.CreateCommand(_StrIns, Parametri)
                            '_Cm.ExecuteNonQuery()


                            Conn_SQL.Insert(Immagini_Scaricate(Indice))

                            'Scaricare la Thumbs e il file
                            '------------------------------------------------------
                            Scarica_Thumbs = True
                            Scarica_File = True
                        Else
                            'Verifica se VersioneMod è modificato
                            '------------------------------------------------------
                            If ListaImmagini(0).p_VersionMod <> Immagini_Scaricate(Indice).p_VersionMod Then
                                ' Se Modificato Aggiornare la Riga e riscaricare la Thumb
                                '------------------------------------------------------

                                Conn_SQL.Delete(ListaImmagini(0))
                                Conn_SQL.Insert(Immagini_Scaricate(Indice))

                                'Conn_SQL.Update(Immagini_Scaricate(Indice))

                                'Scaricare la Thumbs
                                '------------------------------------------------------
                                Scarica_Thumbs = True

                                '-------------------------------------------------------------------
                                'Elimina Le Relazioni Precedenti
                                '-------------------------------------------------------------------
                                'Dim Relaz_Immagini As List(Of Funzioni_Dati.Relazione_Immagini_ID) = (From pRela In Conn_SQL.Table(Of Funzioni_Dati.Relazione_Immagini_ID) Where pRela.ID = _MYid).ToList
                                'Conn_SQL.Delete(Relaz_Immagini)

                            Else
                                Scarica_Thumbs = False
                            End If

                            'Verifica se inserire il File tra quelli da Scaricare 
                            '------------------------------------------------------
                            If ListaImmagini(0).p_VersionFile <> Immagini_Scaricate(Indice).p_VersionFile Then
                                Scarica_File = True
                            Else
                                Scarica_File = False
                            End If
                        End If


                        '------
                        ' Scarico Immediato del File Thumbs
                        '------------------------------------------------------
                        If Scarica_Thumbs Then
                            Dim Stringa_Thumb As String = Immagini_Scaricate(Indice).p_Thumb.Trim
                            If Stringa_Thumb.Length > 0 Then

                                If App.Thumbs_SCARICATE.IndexOf(Stringa_Thumb) < 0 Then
                                    App.Thumbs_SCARICATE.Add(Stringa_Thumb)

                                    Dim result As IEnumerable(Of String) = App.Thumbs_Da_Scaricare.Where(Function(s) s.Contains(Stringa_Thumb))
                                    If result.Count = 0 Then
                                        ''Esegui il DownLoad
                                        Dim _InputUrl As String = _UrlBase & Immagini_Scaricate(Indice).p_Thumb

                                        Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                                        Dim Lungo As Integer = Stringa_Thumb.Length
                                        Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                                        Await Gestore_DownLoad.StartDownload(_InputUrl, ImageFolder, _Filename)

                                        ' Registro il fiel scaricato epr non farlo riscaricare di nuovo
                                        '------------------------------------------------------
                                        App.Thumbs_Da_Scaricare.Add(Stringa_Thumb)
                                    End If
                                End If
                            End If
                        End If



                        If Scarica_File Then
                            ' Inserisci in Array per Scaricare  dal Download manager
                            '------------------------------------------------------
                            Dim StringaComplessa As String = ""
                            StringaComplessa &= "IMG       "   'Primi 10 la tipologia
                            StringaComplessa &= (Immagini_Scaricate(Indice).p_ID & New String(" ", 10)).Substring(0, 10)  'ID specifico
                            StringaComplessa &= (Immagini_Scaricate(Indice).p_Section & New String(" ", 10)).Substring(0, 10)  'Categoria
                            StringaComplessa &= (Immagini_Scaricate(Indice).p_ToDownLoad & New String(" ", 20)).Substring(0, 20)   'Nome del File


                            Dim result As IEnumerable(Of String) = App.Da_Scaricare.Where(Function(s) s.Contains(StringaComplessa))
                            If result.Count = 0 Then
                                App.Da_Scaricare.Add(StringaComplessa)
                            End If
                        End If
                    Next

                    Conn_SQL.Commit()

                Catch ex As Exception
                    Conn_SQL.Rollback()
                Finally
                    Conn_SQL.Close()
                End Try



            End Function


            Public Shared Async Function Immagini_Deleted(ByVal Lista_Da_Cancellare As List(Of UpdateSync.ImagesDeleted)) As Task(Of Task())

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

                Dim Indice As Integer = 0
                Dim Scarica_Thumbs As Boolean = False
                Dim Scarica_File As Boolean = False


                For Indice = 0 To Lista_Da_Cancellare.Count - 1

                    For CollSec As Integer = 0 To Lista_Da_Cancellare(Indice).Section.Count - 1
                        Dim _MYid As Integer = Lista_Da_Cancellare(Indice).ID
                        Dim _MYsec As Integer = Lista_Da_Cancellare(Indice).Section(CollSec)

                        'Delete_Image
                        Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from Immagini where p_Id=" & CStr(_MYid).Trim & " and p_Section=" & CStr(_MYsec).Trim)
                        _Cm1.ExecuteNonQuery()
                    Next
                Next

            End Function


            Public Shared Function Immagini_Genera_da_NuovoStruttura() As List(Of Funzioni_Dati.Immagini)

                Dim le_Mie_Immagini As New List(Of Funzioni_Dati.Immagini)

                For Indi As Integer = 0 To App.Struttura_Globale.Images.Count - 1

                    For CollSec As Integer = 0 To App.Struttura_Globale.Images(Indi).Section.Count - 1
                        Dim Nuovo As New Funzioni_Dati.Immagini
                        Nuovo.p_Section = App.Struttura_Globale.Images(Indi).Section(CollSec)
                        Nuovo.p_ID = App.Struttura_Globale.Images(Indi).ID
                        Nuovo.p_Title = App.Struttura_Globale.Images(Indi).Title
                        Nuovo.p_HashID = App.Struttura_Globale.Images(Indi).HashID
                        Nuovo.p_Tags = App.Struttura_Globale.Images(Indi).Tags

                        Nuovo.p_VersionMod = App.Struttura_Globale.Images(Indi).VersionMod
                        Nuovo.p_VersionFile = App.Struttura_Globale.Images(Indi).VersionFile

                        Nuovo.p_Thumb = App.Struttura_Globale.Images(Indi).Thumb
                        Nuovo.p_ToDownLoad = App.Struttura_Globale.Images(Indi).ToDownLoad

                        If App.Struttura_Globale.Images(Indi).ToExtract Is Nothing Then
                            Nuovo.p_ToExtract = True
                        Else
                            Nuovo.p_ToExtract = App.Struttura_Globale.Images(Indi).ToExtract
                        End If

                        Nuovo.p_Active = App.Struttura_Globale.Images(Indi).Active
                        Nuovo.p_Order = App.Struttura_Globale.Images(Indi).Order
                        Nuovo.p_Deleted = App.Struttura_Globale.Images(Indi).Deleted
                        Nuovo.p_LanguageID = App.Struttura_Globale.Images(Indi).LanguageID
                        Nuovo.p_UnAvailableForSend = App.Struttura_Globale.Images(Indi).UnAvailableForSend

                        le_Mie_Immagini.Add(Nuovo)

                    Next
                Next

                Return le_Mie_Immagini
            End Function


            Public Shared Function Immagini_Settore_Call(ByVal Id_Section As Integer, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Immagini)

                ' Richiama Le Immagini del Settore Indicato con la lingua attiva
                '------------------------------------------------------'------------------------------------------------------
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                '
                Dim ListaSezioni As List(Of Funzioni_Dati.Immagini) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini) Where p.p_LanguageID = Id_language And p.p_Section = Id_Section Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Immagini_Filtro_Call(ByVal Filtro As String, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Immagini)

                ' Richiama Le Immagini del Settore Indicato con la lingua attiva
                '------------------------------------------------------'------------------------------------------------------
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Immagini) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini) Where (p.p_Title.Contains(Filtro) Or p.p_Tags.Contains(Filtro)) And p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Immagini_Call(ByVal Id_language As Integer) As List(Of Funzioni_Dati.Immagini)

                ' Richiama Le Immagini del Settore Indicato con la lingua attiva
                '------------------------------------------------------'------------------------------------------------------
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Immagini) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini) Where p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Immagini_Call_ALL() As List(Of Funzioni_Dati.Immagini)

                ' Richiama Le Immagini del Settore Indicato con la lingua attiva
                '------------------------------------------------------'------------------------------------------------------
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Immagini) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini)).ToList

                Return ListaSezioni

            End Function

        End Class

        Public Class SitiWeb

            Public Shared Function SitiWeb_Genera_da_NuovoStruttura() As List(Of Funzioni_Dati.SitiWeb)

                Dim I_Miei_Siti As New List(Of Funzioni_Dati.SitiWeb)

                For Indi As Integer = 0 To App.Struttura_Globale.WebSites.Count - 1

                    For CollSec As Integer = 0 To App.Struttura_Globale.WebSites(Indi).Section.Count - 1
                        Dim Nuovo As New Funzioni_Dati.SitiWeb
                        Nuovo.p_UrlToOpen = App.Struttura_Globale.WebSites(Indi).UrlToOpen
                        'Nuovo.p_Section = New List(Of Integer)
                        'For CollSec As Integer = 0 To App.Struttura_Globale.WebSites(Indi).Section.Count - 1
                        '    Nuovo.p_Section.Add(App.Struttura_Globale.WebSites(Indi).Section(CollSec))
                        'Next
                        Nuovo.p_Active = App.Struttura_Globale.WebSites(Indi).Section(CollSec)
                        Nuovo.p_ID = App.Struttura_Globale.WebSites(Indi).ID
                        Nuovo.p_Title = App.Struttura_Globale.WebSites(Indi).Title
                        Nuovo.p_HashID = App.Struttura_Globale.WebSites(Indi).HashID
                        Nuovo.p_Tags = App.Struttura_Globale.WebSites(Indi).Tags

                        Nuovo.p_VersionMod = App.Struttura_Globale.WebSites(Indi).VersionMod
                        Nuovo.p_VersionFile = App.Struttura_Globale.WebSites(Indi).VersionFile

                        Nuovo.p_Thumb = App.Struttura_Globale.WebSites(Indi).Thumb
                        Nuovo.p_ToDownLoad = App.Struttura_Globale.WebSites(Indi).ToDownLoad

                        If App.Struttura_Globale.WebSites(Indi).ToExtract Is Nothing Then
                            Nuovo.p_ToExtract = True
                        Else
                            Nuovo.p_ToExtract = App.Struttura_Globale.WebSites(Indi).ToExtract
                        End If

                        Nuovo.p_Active = App.Struttura_Globale.WebSites(Indi).Active
                        Nuovo.p_Order = App.Struttura_Globale.WebSites(Indi).Order
                        Nuovo.p_Deleted = App.Struttura_Globale.WebSites(Indi).Deleted
                        Nuovo.p_LanguageID = App.Struttura_Globale.WebSites(Indi).LanguageID
                        Nuovo.p_UnAvailableForSend = App.Struttura_Globale.WebSites(Indi).UnAvailableForSend

                        I_Miei_Siti.Add(Nuovo)

                    Next
                Next

                Return I_Miei_Siti
            End Function

        End Class

        Public Class InfoBase

            Public Shared Function InfoBase_Genera_da_NuovoStruttura() As Funzioni_Dati.InfoBase

                Dim Le_Info As New Funzioni_Dati.InfoBase

                Dim Nuovo As New Funzioni_Dati.InfoBase
                Nuovo.p_HqHashing = App.Struttura_Globale.BaseInfo.HqHashing
                Nuovo.p_UrlPath = App.Struttura_Globale.BaseInfo.UrlPath

                Return Nuovo
            End Function

        End Class

        Public Class Brochures

            Public Shared Async Function Brochures_Aggiorna_Dati_e_Thumbs(ByVal Brochures_Scaricate As List(Of Funzioni_Dati.Brochure), ByVal BrochuresFolder As StorageFolder) As Task(Of List(Of Funzioni_Dati.Brochure))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim ListaBrochures As List(Of Funzioni_Dati.Brochure)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"


                Dim Indice As Integer = 0
                Dim Scarica_Thumbs As Boolean = False
                Dim Scarica_File As Boolean = False


                Dim _Cm As SQLite.Net.SQLiteCommand
                Dim _StrUpd As String = ""
                Conn_SQL.BeginTransaction()
                Try

                    For Indice = 0 To Brochures_Scaricate.Count - 1

                    Dim _MYid As Integer = Brochures_Scaricate(Indice).p_ID
                    Dim _MYsec As Integer = Brochures_Scaricate(Indice).p_Section
                    ListaBrochures = (From p In Conn_SQL.Table(Of Funzioni_Dati.Brochure) Where p.p_ID = _MYid And p.p_Section = _MYsec).ToList


                    If ListaBrochures.Count = 0 Then
                        'Inserisci_Riga
                        '------------------------------------------------------
                        Conn_SQL.Insert(Brochures_Scaricate(Indice))

                        'Scaricare la Thumbs
                        '------------------------------------------------------
                        Scarica_Thumbs = True
                        Scarica_File = True

                        '------------------------------------------
                        ' Registra le Page

                        Dim LaLista As List(Of Funzioni_Dati.TEMP_Relazione_Brochure_Page) = (From n In App.Le_Mie_Relazioni_Page Where n.ID = Brochures_Scaricate(Indice).p_ID).ToList

                        For Each Temp_Page As Funzioni_Dati.TEMP_Relazione_Brochure_Page In LaLista
                            Dim MiePage As New Funzioni_Dati.Relazione_Brochure_Page
                            MiePage.ID = Temp_Page.ID
                            MiePage.Order = Temp_Page.Order
                            MiePage.Pages = Temp_Page.Pages
                            Conn_SQL.Insert(MiePage)
                        Next
                    Else
                        '------------------------------------------
                        ' Registra le Page
                        If ListaBrochures(0).p_VersionMod <> Brochures_Scaricate(Indice).p_VersionMod Or
                                ListaBrochures(0).p_VersionFile <> Brochures_Scaricate(Indice).p_VersionFile Then
                            '-------------------------------------------------------------------
                            'Elimina Le Relazioni Precedenti
                            '-------------------------------------------------------------------
                            Dim Relaz_Brochure As List(Of Funzioni_Dati.Relazione_Brochure_Page) = (From pRela In Conn_SQL.Table(Of Funzioni_Dati.Relazione_Brochure_Page) Where pRela.ID = Brochures_Scaricate(Indice).p_ID).ToList
                            Conn_SQL.Delete(Relaz_Brochure)


                            Dim LaLista As List(Of Funzioni_Dati.TEMP_Relazione_Brochure_Page) = (From n In App.Le_Mie_Relazioni_Page Where n.ID = Brochures_Scaricate(Indice).p_ID).ToList

                            For Each Temp_Page As Funzioni_Dati.TEMP_Relazione_Brochure_Page In LaLista
                                Dim MiePage As New Funzioni_Dati.Relazione_Brochure_Page
                                MiePage.ID = Temp_Page.ID
                                MiePage.Order = Temp_Page.Order
                                MiePage.Pages = Temp_Page.Pages
                                Conn_SQL.Insert(MiePage)
                            Next
                        End If


                        'Verifica se VersioneMod è modificato
                        '------------------------------------------------------
                        If ListaBrochures(0).p_VersionMod <> Brochures_Scaricate(Indice).p_VersionMod Then
                                ' Se Modificato Aggiornare la Riga e riscaricare la Thumb
                                '------------------------------------------------------
                                'Conn_SQL.Update(Brochures_Scaricate(Indice))

                                Conn_SQL.Delete(ListaBrochures(0))
                                Conn_SQL.Insert(Brochures_Scaricate(Indice))

                                'Scaricare la Thumbs
                                '------------------------------------------------------
                                Scarica_Thumbs = True

                        Else
                            Scarica_Thumbs = False
                        End If

                        ' Verifica se Occorre riscaricare il File Completo
                        '------------------------------------------------------
                        If ListaBrochures(0).p_VersionFile <> Brochures_Scaricate(Indice).p_VersionFile Then
                            Scarica_File = True
                        Else
                            Scarica_File = False
                        End If
                    End If


                    '------
                    ' Scarico Immediato del File Thumbs
                    '------'------------------------------------------------------
                    If Scarica_Thumbs Then
                        Dim Stringa_Thumb As String = Brochures_Scaricate(Indice).p_Thumb.Trim
                        If Stringa_Thumb.Length > 0 Then
                            If App.Thumbs_SCARICATE.IndexOf(Stringa_Thumb) < 0 Then
                                App.Thumbs_SCARICATE.Add(Stringa_Thumb)

                                Dim result As IEnumerable(Of String) = App.Thumbs_Da_Scaricare.Where(Function(s) s.Contains(Stringa_Thumb))
                                If result.Count = 0 Then
                                    ''Esegui il DownLoad
                                    Dim _InputUrl As String = _UrlBase & Brochures_Scaricate(Indice).p_Thumb

                                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                                    Dim Lungo As Integer = Stringa_Thumb.Length
                                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                                    Await Gestore_DownLoad.StartDownload(_InputUrl, BrochuresFolder, _Filename)

                                    ' Indico che il file è scaricato per non riscricarlo ancora
                                    '------------------------------------------------------
                                    App.Thumbs_Da_Scaricare.Add(Stringa_Thumb)
                                End If
                            End If
                        End If
                    End If




                    If Scarica_File Then
                        ' Inserisci in Array per Scaricare
                        '------------------------------------------------------
                        Dim StringaComplessa As String = ""
                        StringaComplessa &= "BRO       "   'Primi 10 la tipologia
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_ID & New String(" ", 10)).Substring(0, 10)  'ID specifico
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_Section & New String(" ", 10)).Substring(0, 10)  'Categoria
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_ToDownLoad & New String(" ", 20)).Substring(0, 20)   'Nome del File


                        Dim result As IEnumerable(Of String) = App.Da_Scaricare.Where(Function(s) s.Contains(StringaComplessa))
                        If result.Count = 0 Then
                            App.Da_Scaricare.Add(StringaComplessa)
                        End If


                        'In questo caso inserisco anche il file PDF
                        '------------------------------------------------------
                        StringaComplessa = ""
                        StringaComplessa &= "BRO-PDF   "   'Primi 10 la tipologia
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_ID & New String(" ", 10)).Substring(0, 10)  'ID specifico
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_Section & New String(" ", 10)).Substring(0, 10)  'Categoria
                        StringaComplessa &= (Brochures_Scaricate(Indice).p_ToDownLoadPdf & New String(" ", 20)).Substring(0, 20)   'Nome del File


                        result = App.Da_Scaricare.Where(Function(s) s.Contains(StringaComplessa))
                        If result.Count = 0 Then
                            App.Da_Scaricare.Add(StringaComplessa)
                        End If

                    End If
                Next


                Conn_SQL.Commit()

                Catch ex As Exception
                Conn_SQL.Rollback()
                Finally
                Conn_SQL.Close()
                End Try


            End Function


            Public Shared Async Function Brochure_Deleted(ByVal Lista_Da_Cancellare As List(Of UpdateSync.BrochuresDeleted)) As Task(Of Task())
                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)
                Dim _UrlBase As String = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"

                Dim Indice As Integer = 0


                For Indice = 0 To Lista_Da_Cancellare.Count - 1

                    For CollSec As Integer = 0 To Lista_Da_Cancellare(Indice).Section.Count - 1
                        Dim _MYid As Integer = Lista_Da_Cancellare(Indice).ID
                        Dim _MYsec As Integer = Lista_Da_Cancellare(Indice).Section(CollSec)

                        'Delete_Brochure
                        Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from Brochure where p_Id=" & CStr(_MYid).Trim & " and p_Section=" & CStr(_MYsec).Trim)
                        _Cm1.ExecuteNonQuery()
                    Next
                Next

            End Function



            Public Shared Function Brochures_Genera_da_NuovoStruttura() As List(Of Funzioni_Dati.Brochure)

                Dim le_Mie_Brochure As New List(Of Funzioni_Dati.Brochure)

                For Indi As Integer = 0 To App.Struttura_Globale.Brochures.Count - 1


                    For CollSec As Integer = 0 To App.Struttura_Globale.Brochures(Indi).Section.Count - 1
                        Dim Nuovo As New Funzioni_Dati.Brochure
                        Nuovo.p_Section = App.Struttura_Globale.Brochures(Indi).Section(CollSec)
                        Nuovo.p_ID = App.Struttura_Globale.Brochures(Indi).ID
                        Nuovo.p_Title = App.Struttura_Globale.Brochures(Indi).Title
                        Nuovo.p_HashID = App.Struttura_Globale.Brochures(Indi).HashID
                        Nuovo.p_Tags = App.Struttura_Globale.Brochures(Indi).Tags

                        Nuovo.p_VersionMod = App.Struttura_Globale.Brochures(Indi).VersionMod
                        Nuovo.p_VersionFile = App.Struttura_Globale.Brochures(Indi).VersionFile

                        Nuovo.p_Thumb = App.Struttura_Globale.Brochures(Indi).Thumb

                        Nuovo.p_ToDownLoad = App.Struttura_Globale.Brochures(Indi).ToDownLoad

                        If App.Struttura_Globale.Brochures(Indi).ToExtract Is Nothing Then
                            Nuovo.p_ToExtract = True
                        Else
                            Nuovo.p_ToExtract = App.Struttura_Globale.Brochures(Indi).ToExtract
                        End If

                        Nuovo.p_Active = App.Struttura_Globale.Brochures(Indi).Active
                        Nuovo.p_Order = App.Struttura_Globale.Brochures(Indi).Order
                        Nuovo.p_ToDownLoadPdf = App.Struttura_Globale.Brochures(Indi).ToDownLoadPdf
                        Nuovo.p_Deleted = App.Struttura_Globale.Brochures(Indi).Deleted
                        Nuovo.p_LanguageID = App.Struttura_Globale.Brochures(Indi).LanguageID
                        Nuovo.p_UnAvailableForSend = App.Struttura_Globale.Brochures(Indi).UnAvailableForSend

                        le_Mie_Brochure.Add(Nuovo)


                        For CollSec3 As Integer = 0 To App.Struttura_Globale.Brochures(Indi).Pages.Count - 1
                            Dim Relazione As New TEMP_Relazione_Brochure_Page
                            Relazione.ID = App.Struttura_Globale.Brochures(Indi).ID
                            Relazione.Pages = App.Struttura_Globale.Brochures(Indi).Pages(CollSec3)
                            Relazione.Order = CollSec3

                            App.Le_Mie_Relazioni_Page.Add(Relazione)
                        Next

                    Next
                Next

                Return le_Mie_Brochure
            End Function



            Public Shared Function Brochure_Settore_Call(ByVal Id_Section As Integer, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Brochure)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Brochure) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Brochure) Where p.p_Section = Id_Section And p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Brochure_Filtro_Call(ByVal Filtro As String, ByVal Id_language As Integer) As List(Of Funzioni_Dati.Brochure)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Brochure) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Brochure) Where (p.p_Title.Contains(Filtro) Or p.p_Tags.Contains(Filtro)) And p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Brochure_Call(ByVal Id_language As Integer) As List(Of Funzioni_Dati.Brochure)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Brochure) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Brochure) Where p.p_LanguageID = Id_language Order By p.p_Order).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Brochure_Call_ALL() As List(Of Funzioni_Dati.Brochure)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Brochure) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Brochure)).ToList

                Return ListaSezioni

            End Function

        End Class

        Public Class Sezioni


            Public Shared Sub Sezioni_Save(ListaSezioni As List(Of Funzioni_Dati.Sezioni))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                For Indice As Integer = 0 To ListaSezioni.Count - 1

                    Dim Identificativo As Integer = ListaSezioni(Indice).p_ID

                    Dim LeSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = Identificativo).ToList
                    If LeSezioni.Count = 0 Then
                        Conn_SQL.Insert(ListaSezioni(Indice))
                    Else
                        Conn_SQL.Update(ListaSezioni(Indice))
                    End If
                Next

            End Sub

            Public Shared Async Function Sezioni_Deleted(ListaSezioni As List(Of UpdateSync.SectionsDeleted)) As Task(Of Task())


                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                For Indice As Integer = 0 To ListaSezioni.Count - 1

                    Dim Identificativo As Integer = ListaSezioni(Indice).ID

                    'Dim LeSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = Identificativo).ToList
                    'If LeSezioni.Count > 0 Then
                    'Elimina Sezione
                    Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from Sezioni where p_Id=" & CStr(Identificativo).Trim)
                        _Cm1.ExecuteNonQuery()

                    'End If
                Next

            End Function



            Public Shared Function Dammi_Descrizione_Sezione(ByVal IdSezione As Integer) As String

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = IdSezione And p.p_LanguageID = App.m_Lang_NoRete_Id Order By p.p_Pos).ToList

                Dammi_Descrizione_Sezione = ""
                If ListaSezioni.Count > 0 Then
                    Dammi_Descrizione_Sezione = ListaSezioni(0).p_Title
                End If

                Return Dammi_Descrizione_Sezione

            End Function

            Public Shared Function Is_Categoria_Attiva(ByVal IdSezione As Integer) As Boolean

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = IdSezione And p.p_LanguageID = App.m_Lang_NoRete_Id Order By p.p_Pos).ToList


                Return (ListaSezioni.Count > 0)

            End Function


            Public Shared Function Sezioni_Principali_Call(ByVal IdLanguage As Integer) As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_FK = 0 And p.p_LanguageID = IdLanguage Order By p.p_Pos).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Sezioni_Call(ByVal IdLanguage As Integer) As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_LanguageID = IdLanguage Order By p.p_Pos).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Sezioni_Call_ALL() As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni)).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Sezione_Specifica_Call(ByVal Id_Sezione As Integer, ByVal IdLanguage As Integer) As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = Id_Sezione And p.p_LanguageID = IdLanguage Order By p.p_Pos).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Sezioni_ALL_Call() As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni)).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Sezioni_SecondarieALL_Call(ByVal IdLanguage As Integer) As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_FK <> 0 And p.p_LanguageID = IdLanguage Order By p.p_Pos).ToList

                Return ListaSezioni

            End Function

            Public Shared Function Sezioni_Secondarie_Call(ByVal Id_SezionePrincipale As Integer, ByVal IdLanguage As Integer) As List(Of Funzioni_Dati.Sezioni)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_FK = Id_SezionePrincipale And p.p_LanguageID = IdLanguage Order By p.p_Pos).ToList

                Return ListaSezioni

            End Function


            Public Shared Function Sezioni_Genera_da_NuovoStruttura() As List(Of Funzioni_Dati.Sezioni)

                Dim LeMieSezioni As New List(Of Funzioni_Dati.Sezioni)

                For Indi As Integer = 0 To App.Struttura_Globale.Sections.Count - 1


                    Dim Nuovo As New Funzioni_Dati.Sezioni
                    Nuovo.p_ID = App.Struttura_Globale.Sections(Indi).ID
                    If App.Struttura_Globale.Sections(Indi).FK Is Nothing OrElse App.Struttura_Globale.Sections(Indi).FK.ToString = "null" OrElse App.Struttura_Globale.Sections(Indi).FK.ToString = "0" Then
                        Nuovo.p_FK = 0
                    Else
                        Nuovo.p_FK = CInt(App.Struttura_Globale.Sections(Indi).FK.ToString)
                    End If
                    Nuovo.p_Title = App.Struttura_Globale.Sections(Indi).Title
                    Nuovo.p_CategoryColor = App.Struttura_Globale.Sections(Indi).CategoryColor
                    Nuovo.p_Pos = App.Struttura_Globale.Sections(Indi).Pos
                    Nuovo.p_LandScapeImage = App.Struttura_Globale.Sections(Indi).LandScapeImage
                    Nuovo.p_PortraitImage = App.Struttura_Globale.Sections(Indi).PortraitImage
                    Nuovo.p_LanguageID = App.Struttura_Globale.Sections(Indi).LanguageID
                    Nuovo.p_UnAvaibleForSend = App.Struttura_Globale.Sections(Indi).UnAvaibleForSend

                    LeMieSezioni.Add(Nuovo)


                Next

                Return LeMieSezioni
            End Function
        End Class

    End Class






    Public Class Documenti_Catalogo

        '<Net.Attributes.PrimaryKey, Net.Attributes.AutoIncrement>
        'Public Property Id() As Integer
        '    Get
        '        Return m_ID
        '    End Get
        '    Set
        '        m_ID = Value
        '    End Set
        'End Property

        Dim m_Section As Integer    'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo della Brocheres
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim Pages                         '  Array di Pagine da visualizzare
        Dim m_ToDownLoad As String = ""    'Nome dello Zip da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_ToDownLoadPdf As String = ""      ''Nome del PDF della versione originale
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As Integer
            Get
                Return m_Section
            End Get
            Set(value As Integer)
                m_Section = value
            End Set
        End Property

        Public Property ID As Integer
            Get
                Return m_ID
            End Get
            Set(value As Integer)
                m_ID = value
            End Set
        End Property

        Public Property Title As String
            Get
                Return m_Title
            End Get
            Set(value As String)
                m_Title = value
            End Set
        End Property

        Public Property HashID As String
            Get
                Return m_HashID
            End Get
            Set(value As String)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As String
            Get
                Return m_Tags
            End Get
            Set(value As String)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Integer
            Get
                Return m_VersionMod
            End Get
            Set(value As Integer)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Integer
            Get
                Return m_VersionFile
            End Get
            Set(value As Integer)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As String
            Get
                Return m_Thumb
            End Get
            Set(value As String)
                m_Thumb = value
            End Set
        End Property

        'Dim Pages                         '  Array di Pagine da visualizzare

        Public Property ToDownLoad As String
            Get
                Return m_ToDownLoad
            End Get
            Set(value As String)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Boolean
            Get
                Return m_ToExtract
            End Get
            Set(value As Boolean)
                m_ToExtract = value
            End Set
        End Property


        Public Property Active As Boolean
            Get
                Return m_Active
            End Get
            Set(value As Boolean)
                m_Active = value
            End Set
        End Property


        Public Property Order As Integer
            Get
                Return m_Order
            End Get
            Set(value As Integer)
                m_Order = value
            End Set
        End Property

        Public Property ToDownLoadPdf As String
            Get
                Return m_ToDownLoadPdf
            End Get
            Set(value As String)
                m_ToDownLoadPdf = value
            End Set
        End Property

        Public Property Deleted As Boolean
            Get
                Return m_Deleted
            End Get
            Set(value As Boolean)
                m_Deleted = value
            End Set
        End Property


        Public Property LanguageID As Integer
            Get
                Return m_LanguageID
            End Get
            Set(value As Integer)
                m_LanguageID = value
            End Set
        End Property


        Public Property UnAvailableForSend As Boolean
            Get
                Return m_UnAvailableForSend
            End Get
            Set(value As Boolean)
                m_UnAvailableForSend = value
            End Set
        End Property


    End Class



    Public Class Sezioni

        Dim m_ID As Integer           ' Identificatore del menù
        Dim m_FK As Integer           ' Se il menù è un ChildMenù FK è l' ID del Parent
        Dim m_Title As String = ""    ' Descrizione della categoria
        Dim m_CategoryColor As String = ""  '   Non Usata
        Dim m_Pos As Integer        ' Posizione all'interno del Menù
        Dim m_LandScapeImage As String = ""  '   Non Usata
        Dim m_PortraitImage As String = ""  '   Non Usata
        Dim m_LanguageID As Integer     'Id del Liunguaggio Selezionato
        Dim m_UnAvaibleForSend As Boolean = False    'Always false

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property p_ID As Integer
            Get
                Return m_ID
            End Get
            Set(value As Integer)
                m_ID = value
            End Set
        End Property

        Public Property p_FK As Integer
            Get
                Return m_FK
            End Get
            Set(value As Integer)
                m_FK = value
            End Set
        End Property

        Public Property p_Title As String
            Get
                Return m_Title
            End Get
            Set(value As String)
                m_Title = value
            End Set
        End Property

        Public Property p_CategoryColor As String
            Get
                Return m_CategoryColor
            End Get
            Set(value As String)
                m_CategoryColor = value
            End Set
        End Property

        Public Property p_Pos As Integer
            Get
                Return m_Pos
            End Get
            Set(value As Integer)
                m_Pos = value
            End Set
        End Property

        Public Property p_LandScapeImage As String
            Get
                Return m_LandScapeImage
            End Get
            Set(value As String)
                m_LandScapeImage = value
            End Set
        End Property

        Public Property p_PortraitImage As String
            Get
                Return m_PortraitImage
            End Get
            Set(value As String)
                m_PortraitImage = value
            End Set
        End Property

        Public Property p_LanguageID As Integer
            Get
                Return m_LanguageID
            End Get
            Set(value As Integer)
                m_LanguageID = value
            End Set
        End Property

        Public Property p_UnAvaibleForSend As Boolean
            Get
                Return m_UnAvaibleForSend
            End Get
            Set(value As Boolean)
                m_UnAvaibleForSend = value
            End Set
        End Property

    End Class

    Public Class Immagini

        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo dell'Immagine
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As String = ""    'Nome del File da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello


        Public Property p_Section As Integer     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        <SQLite.Net.Attributes.AutoIncrement>
        Public Property p_IDentificativo As Integer


        Public Property p_ID As Integer
            Get
                Return m_ID
            End Get
            Set(value As Integer)
                m_ID = value
            End Set
        End Property


        Public Property p_Title As String     'Descrizione da evidenziare
        Public Property p_HashID As String     'Logical File identifier
        Public Property p_Tags As String       ' Lista separata da virgole di TAGS sui quali cercare
        Public Property p_VersionMod As Integer    ' Numero di version delle Modifiche
        Public Property p_VersionFile As Integer      'Numero di versione del File
        Public Property p_Thumb As String         'Nome del File da usare per la visualizzazione in Griglia
        Public Property p_ToDownLoad As String     'Nome del File da Scaricare
        Public Property p_ToExtract As Boolean           'Always TRUE
        Public Property p_Active As Boolean         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Public Property p_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Public Property p_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello

    End Class

    Public Class Video

        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo del Video
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As String = ""    'Nome del File da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello

        Public Property p_Section As Integer     'Identificativo della Sezione a cui si è collegati 
        <SQLite.Net.Attributes.PrimaryKey>
        <SQLite.Net.Attributes.AutoIncrement>
        Public Property p_IDentificativo As Integer
        Public Property p_ID As Integer        'Identificativo del Video
        Public Property p_Title As String     'Descrizione da evidenziare
        Public Property p_HashID As String    'Logical File identifier
        Public Property p_Tags As String       ' Lista separata da virgole di TAGS sui quali cercare
        Public Property p_VersionMod As Integer    ' Numero di version delle Modifiche
        Public Property p_VersionFile As Integer      'Numero di versione del File
        Public Property p_Thumb As String        'Nome del File da usare per la visualizzazione in Griglia
        Public Property p_ToDownLoad As String    'Nome del File da Scaricare
        Public Property p_ToExtract As Boolean          'Always TRUE
        Public Property p_Active As Boolean          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Public Property p_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Public Property p_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello


    End Class



    Public Class Brochure
        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo della Brocheres
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_Pages As List(Of String)                        '  Array di Pagine da visualizzare
        Dim m_ToDownLoad As String = ""    'Nome dello Zip da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_ToDownLoadPdf As String = ""      ''Nome del PDF della versione originale
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property p_Section As Integer     'Identificativo della Sezione a cui si è collegati 
        <SQLite.Net.Attributes.PrimaryKey>
        <SQLite.Net.Attributes.AutoIncrement>
        Public Property p_IDentificativo As Integer
        Public Property p_ID As Integer        'Identificativo della Brocheres
        Public Property p_Title As String = ""    'Descrizione da evidenziare
        Public Property p_HashID As String = ""    'Logical File identifier
        Public Property p_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Public Property p_VersionMod As Integer    ' Numero di version delle Modifiche
        Public Property p_VersionFile As Integer      'Numero di versione del File
        Public Property p_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        'Public Property p_Pages As List(Of String)                        '  Array di Pagine da visualizzare
        Public Property p_ToDownLoad As String = ""    'Nome dello Zip da Scaricare
        Public Property p_ToExtract As Boolean = True          'Always TRUE
        Public Property p_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Public Property p_ToDownLoadPdf As String = ""      ''Nome del PDF della versione originale
        Public Property p_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Public Property p_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello

    End Class


    Public Class InfoBase
        Dim m_UrlPath As String = ""   'path di partenza per i DownLoad               
        Dim m_HqHashing As String = ""   'hashing Code

        Public Property p_UrlPath As String   'path di partenza per i DownLoad               
        Public Property p_HqHashing As String   'hashing Code

    End Class


    Public Class SitiWeb
        Dim m_UrlToOpen As String = ""  'Indirizzo Url da aprire
        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo dell'Immagine
        Dim m_Title As String = ""    'Descrizione da evidenziare
        Dim m_HashID As String = ""    'Logical File identifier
        Dim m_Tags As String = ""      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Integer    ' Numero di version delle Modifiche
        Dim m_VersionFile As Integer      'Numero di versione del File
        Dim m_Thumb As String = ""        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As String = ""    'Nome del File da Scaricare
        Dim m_ToExtract As Boolean = True          'Always TRUE
        Dim m_Active As Boolean = True         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello


        Public Property p_UrlToOpen As String = ""  'Indirizzo Url da aprire
        Public Property p_Section As Integer     'Identificativo della Sezione a cui si è collegati 
        <SQLite.Net.Attributes.PrimaryKey>
        Public Property p_ID As Integer        'Identificativo dell'Immagine
        Public Property p_Title As String     'Descrizione da evidenziare
        Public Property p_HashID As String    'Logical File identifier
        Public Property p_Tags As String       ' Lista separata da virgole di TAGS sui quali cercare
        Public Property p_VersionMod As Integer    ' Numero di version delle Modifiche
        Public Property p_VersionFile As Integer      'Numero di versione del File
        Public Property p_Thumb As String        'Nome del File da usare per la visualizzazione in Griglia
        Public Property p_ToDownLoad As String     'Nome del File da Scaricare
        Public Property p_ToExtract As Boolean           'Always TRUE
        Public Property p_Active As Boolean          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_Order As Integer                  'Ordine di Visualizzaizone all'interno della sezione
        Public Property p_Deleted As Boolean               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Public Property p_LanguageID As Integer            'Contenuto visibile solo per questi ID_Lingua
        Public Property p_UnAvailableForSend As Boolean    'Se il contenuto può o meno essere aggiunto al carrello


    End Class



    Public Class Relazione_Immagini_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class Relazione_Video_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class Relazione_Brochure_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class Relazione_Brochure_Page

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property ID As Integer
        Public Property Pages As String
        Public Property Order As Integer

    End Class

    '------------------------------------------------------------
    '------------------------------------------------------------
    '------------------------------------------------------------

    Public Class TEMP_Relazione_Immagini_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class TEMP_Relazione_Video_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class TEMP_Relazione_Brochure_ID

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property Section As Integer
        Public Property ID As Integer

    End Class

    Public Class TEMP_Relazione_Brochure_Page

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property Tab_ID As Integer
        Public Property ID As Integer
        Public Property Pages As String
        Public Property Order As Integer

    End Class

    Public Class File_To_Download

        <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
        Public Property ID_Down As Integer
        Public Property ID_Section As Integer
        Public Property Nome_File As String
        Public Property Order As Integer

    End Class


    Public Class File_To_Send

        Public Property ID_Send As Integer
        Public Property ID_Object As Integer
        Public Property ID_Section As Integer
        Public Property Title As String
        Public Property File_Da_Inviare As String
        Public Property File_Thumbs As String


        Public Shared Sub Aggiungi_Un_Elemento_to_Send(ByVal Elemento_Send As Funzioni_Dati.File_da_Inviare)

            Dim _NUoEle As New File_To_Send
            _NUoEle.ID_Send = Elemento_Send.ID_Send
            _NUoEle.ID_Object = Elemento_Send.ID_Object
            _NUoEle.ID_Section = Elemento_Send.ID_Section
            _NUoEle.Title = Elemento_Send.Title
            _NUoEle.File_Da_Inviare = Elemento_Send.File_da_Inviare
            _NUoEle.File_Thumbs = Elemento_Send.File_Thumbs

            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

            Conn_SQL.Insert(_NUoEle)

        End Sub

        Public Shared Sub Cancella_Un_Elemento_to_Send(ByVal Elemento_Send As Funzioni_Dati.File_da_Inviare)

            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

            Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from File_To_Send where Id_Object=" & CStr(Elemento_Send.ID_Object).Trim & " and Id_Section=" & CStr(Elemento_Send.ID_Section).Trim)
            _Cm1.ExecuteNonQuery()


        End Sub

        Public Shared Sub Cancella_Tutti_Elementi_to_Send()

            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

            Dim _Cm1 As SQLite.Net.SQLiteCommand = Conn_SQL.CreateCommand("delete from File_To_Send")
            _Cm1.ExecuteNonQuery()

        End Sub

        Public Shared Sub Carica_Tutti_Elementi_to_Send()

            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

            Dim DaInviare As List(Of Funzioni_Dati.File_To_Send) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_To_Send)).ToList

            For Indice As Integer = 0 To DaInviare.Count - 1

                Dim _Nuovo As New Funzioni_Dati.File_da_Inviare

                _Nuovo.ID_Send = DaInviare(Indice).ID_Send
                _Nuovo.ID_Object = DaInviare(Indice).ID_Object
                _Nuovo.ID_Section = DaInviare(Indice).ID_Section
                _Nuovo.Title = DaInviare(Indice).Title
                _Nuovo.File_da_Inviare = DaInviare(Indice).File_Da_Inviare
                _Nuovo.File_Thumbs = DaInviare(Indice).File_Thumbs

                App.Oggetti_da_Inviare.Add(_Nuovo)
            Next

        End Sub

    End Class


    Public Class Waiting_To_Send
        Public Property Codice_Univoco As String
        Public Property Posizione As Integer
        Public Property ID_Selezionato As Long
        Public Property Tutte_Email_Selezionate As String
    End Class


    Public Class File_da_Scaricare

            <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
            Public Property ID_Down As Integer
            Public Property ID_Section As Integer
            Public Property ID_Object As Integer
            Dim m_Categoria As String = ""
            Dim m_File_da_Scaricare As String = ""
            Dim m_File_da_Unizippare As Boolean = False
            Dim m_File_Finale As String = ""
            Dim m_Tipologia As String = ""
            Dim m_Scaricato As Boolean = False
            Public Property p_Categoria As String
            Public Property p_File_da_Scaricare As String
            Public Property p_File_da_Unizippare As Boolean
            Public Property p_File_Finale As String
            Public Property p_Tipologia As String    'IMG / VID /  BRO
            Public Property p_Scaricato As Boolean


            Public Shared Async Function Carica_File_da_Scaricare_New(ByVal Lista_Da_Scaricare As List(Of String)) As Task(Of List(Of Funzioni_Dati.File_da_Scaricare))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                If Lista_Da_Scaricare.Count = 0 Then
                    Lista_Da_Scaricare = Elenco_Oggetti_da_Scaricare()
                End If

                Dim Lista_DaScaricare As New List(Of Funzioni_Dati.File_da_Scaricare)

                Dim _idLingua As Integer = App.m_Lang_NoRete_Id
                Dim _Progressivo As Integer = 0

                Dim _ListaImmagini As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Call(_idLingua)
                Dim _ListaVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Call(_idLingua)
                Dim _ListaBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Call(_idLingua)
                Dim _ListaSezioni As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Call(_idLingua)
                Dim _Scarico As New Funzioni_Dati.File_da_Scaricare

                For Each _Lista As String In Lista_Da_Scaricare
                    Dim _Settore As String = _Lista.Substring(0, 10).Trim.ToUpper
                    Dim _Id As Integer = CInt(_Lista.Substring(10, 10).Trim)
                    Dim _IdSezione As Integer = CInt(_Lista.Substring(20, 10).Trim)

                    Dim Sezione_Seconda As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_LanguageID = _idLingua And p.p_ID = _IdSezione).ToList
                    If Sezione_Seconda.Count > 0 Then
                        If Sezione_Seconda.Item(0).p_FK <> 0 Then

                            Dim Principale As Integer = Sezione_Seconda.Item(0).p_FK

                            Dim Sezione_Prima As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_LanguageID = _idLingua And p.p_ID = Principale).ToList
                            If Sezione_Prima.Count > 0 Then
                                _IdSezione = Sezione_Seconda.Item(0).p_ID
                            End If
                        End If
                    End If

                    If _Settore = "IMG" Then
                        Dim _Selezionato As New Funzioni_Dati.Immagini
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Immagini In _ListaImmagini
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "IMG"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If


                    If _Settore = "VIDEO" Then
                        Dim _Selezionato As New Funzioni_Dati.Video
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Video In _ListaVideo
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "VID"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If



                    If _Settore = "BRO" Then  'Or _Settore = "BRO-PDF"
                        Dim _Selezionato As New Funzioni_Dati.Brochure
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Brochure In _ListaBrochure
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            If _Settore = "BRO-PDF" Then
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoadPdf
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoadPdf
                            Else
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            End If
                            _Scarico.p_Tipologia = "BRO"
                            _Scarico.p_File_da_Unizippare = False
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If
                Next


                '---------------------------------------------------------
                '   Salvare la Lista sul DB Locale
                '---------------------------------------------------------

                For Each _Scaricare In Lista_DaScaricare

                    Conn_SQL.Insert(_Scaricare)

                Next


            End Function


            Public Shared Async Function Carica_File_da_Scaricare(ByVal Lista_Da_Scaricare As List(Of String)) As Task(Of List(Of Funzioni_Dati.File_da_Scaricare))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                If Lista_Da_Scaricare.Count = 0 Then
                    Lista_Da_Scaricare = Elenco_Oggetti_da_Scaricare()
                End If

                Dim Lista_DaScaricare As New List(Of Funzioni_Dati.File_da_Scaricare)

                Dim _idLingua As Integer = App.m_Lang_NoRete_Id
                Dim _Progressivo As Integer = 0

                Dim _ListaImmagini As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Call(_idLingua)
                Dim _ListaVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Call(_idLingua)
                Dim _ListaBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Call(_idLingua)
                Dim _ListaSezioni As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Call(_idLingua)
                Dim _Scarico As New Funzioni_Dati.File_da_Scaricare

                For Each _Lista As String In Lista_Da_Scaricare
                    Dim _Settore As String = _Lista.Substring(0, 10).Trim.ToUpper
                    Dim _Id As Integer = CInt(_Lista.Substring(10, 10).Trim)
                    Dim _IdSezione As Integer = CInt(_Lista.Substring(20, 10).Trim)

                    If _Settore = "IMG" Then
                        Dim _Selezionato As New Funzioni_Dati.Immagini
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Immagini In _ListaImmagini
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "IMG"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If


                    If _Settore = "VIDEO" Then
                        Dim _Selezionato As New Funzioni_Dati.Video
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Video In _ListaVideo
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "VID"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If



                    If _Settore = "BRO" Then  'Or _Settore = "BRO-PDF"
                        Dim _Selezionato As New Funzioni_Dati.Brochure
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Brochure In _ListaBrochure
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            If _Settore = "BRO-PDF" Then
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoadPdf
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoadPdf
                            Else
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            End If
                            _Scarico.p_Tipologia = "BRO"
                            _Scarico.p_File_da_Unizippare = False
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If
                Next


                '---------------------------------------------------------
                '   Salvare la Lista sul DB Locale
                '---------------------------------------------------------

                For Each _Scaricare In Lista_DaScaricare

                    Conn_SQL.Insert(_Scaricare)

                Next


            End Function



            Public Shared Async Function Carica_File_da_Scaricare_ALL_OLD(ByVal Lista_Da_Scaricare As List(Of String)) As Task(Of List(Of Funzioni_Dati.File_da_Scaricare))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                'If Lista_Da_Scaricare.Count = 0 Then
                '    Lista_Da_Scaricare = Elenco_Oggetti_da_Scaricare()
                'End If

                Dim Lista_DaScaricare As New List(Of Funzioni_Dati.File_da_Scaricare)

                Dim _idLingua As Integer = App.m_Lang_NoRete_Id
                Dim _Progressivo As Integer = 0

                Dim _ListaImmagini As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Call_ALL()
                Dim _ListaVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Call_ALL()
                Dim _ListaBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Call_ALL()
                Dim _ListaSezioni As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Call_ALL()
                Dim _Scarico As New Funzioni_Dati.File_da_Scaricare

                For Each _Lista As String In Lista_Da_Scaricare
                    Dim _Settore As String = _Lista.Substring(0, 10).Trim.ToUpper
                    Dim _Id As Integer = CInt(_Lista.Substring(10, 10).Trim)
                    Dim _IdSezione As Integer = CInt(_Lista.Substring(20, 10).Trim)

                    If _Settore = "IMG" Then
                        Dim _Selezionato As New Funzioni_Dati.Immagini
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Immagini In _ListaImmagini
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If


                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "IMG"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If


                    If _Settore = "VIDEO" Then
                        Dim _Selezionato As New Funzioni_Dati.Video
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Video In _ListaVideo
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If


                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "VID"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If



                    If _Settore = "BRO" Then  'Or _Settore = "BRO-PDF"
                        Dim _Selezionato As New Funzioni_Dati.Brochure
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Brochure In _ListaBrochure
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            If _Settore = "BRO-PDF" Then
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoadPdf
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoadPdf
                            Else
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            End If
                            _Scarico.p_Tipologia = "BRO"
                            _Scarico.p_File_da_Unizippare = False
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If
                Next


                '---------------------------------------------------------
                '   Salvare la Lista sul DB Locale
                '---------------------------------------------------------

                For Each _Scaricare In Lista_DaScaricare

                    Conn_SQL.Insert(_Scaricare)

                Next


            End Function



            Public Shared Async Function Carica_File_da_Scaricare_ALL(ByVal Lista_Da_Scaricare As List(Of String)) As Task(Of List(Of Funzioni_Dati.File_da_Scaricare))

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                'If Lista_Da_Scaricare.Count = 0 Then
                '    Lista_Da_Scaricare = Elenco_Oggetti_da_Scaricare()
                'End If

                Dim Lista_DaScaricare As New List(Of Funzioni_Dati.File_da_Scaricare)

                Dim _idLingua As Integer = App.m_Lang_NoRete_Id
                Dim _Progressivo As Integer = 0

                Dim _ListaImmagini As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Call_ALL()
                Dim _ListaVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Call_ALL()
                Dim _ListaBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Call_ALL()
                Dim _ListaSezioni As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Call_ALL()
                Dim _Scarico As New Funzioni_Dati.File_da_Scaricare

                For Each _Lista As String In Lista_Da_Scaricare
                    Dim _Settore As String = _Lista.Substring(0, 10).Trim.ToUpper
                    Dim _Id As Integer = CInt(_Lista.Substring(10, 10).Trim)
                    Dim _IdSezione As Integer = CInt(_Lista.Substring(20, 10).Trim)

                    'Dim Sezione_Seconda As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_LanguageID = _idLingua And p.p_ID = _IdSezione).ToList
                    Dim Sezione_Seconda As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = _IdSezione).ToList
                    If Sezione_Seconda.Count > 0 Then
                        If Sezione_Seconda.Item(0).p_FK <> 0 Then

                            Dim Principale As Integer = Sezione_Seconda.Item(0).p_FK

                            'Dim Sezione_Prima As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_LanguageID = _idLingua And p.p_ID = Principale).ToList
                            Dim Sezione_Prima As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = Principale).ToList
                            If Sezione_Prima.Count > 0 Then
                                _IdSezione = Sezione_Prima.Item(0).p_ID
                            End If
                        End If
                    End If

                    If _Settore = "IMG" Then
                        Dim _Selezionato As New Funzioni_Dati.Immagini
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Immagini In _ListaImmagini
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If


                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "IMG"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If


                    If _Settore = "VIDEO" Then
                        Dim _Selezionato As New Funzioni_Dati.Video
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Video In _ListaVideo
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If


                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            _Scarico.p_Tipologia = "VID"
                            _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                            _Scarico.p_File_da_Unizippare = False
                            _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If



                    If _Settore = "BRO" Then  'Or _Settore = "BRO-PDF"
                        Dim _Selezionato As New Funzioni_Dati.Brochure
                        Dim _LaSezione As New Funzioni_Dati.Sezioni
                        Dim _Trovato As Boolean = False

                        ' Se è presnete nelle lista prende i dati
                        '----------------------------------------------
                        For Each Oggetto As Funzioni_Dati.Brochure In _ListaBrochure
                            If Oggetto.p_ID = _Id Then
                                _Selezionato = Oggetto
                                _Trovato = True
                                Exit For
                            End If
                        Next

                        If _Trovato Then
                            _Trovato = False
                            ' Se presente nelle sezioni Prende i dati
                            '----------------------------------------------
                            For Each LaSezione As Funzioni_Dati.Sezioni In _ListaSezioni
                                If LaSezione.p_ID = _IdSezione Then
                                    _LaSezione = LaSezione
                                    _Trovato = True
                                    Exit For
                                End If
                            Next
                        End If

                        'Evitare che ci sia + volte lo stesso oggetto
                        '-----------------------------------------------------------------
                        If _Trovato Then
                            For Each _LaLista As File_da_Scaricare In Lista_DaScaricare
                                If _LaLista.ID_Object = _Id Then
                                    _Trovato = False
                                    Exit For
                                End If
                            Next
                        End If

                        If _Trovato Then
                            ' Aggiungere alla lista dei File da scaricare
                            '----------------------------------------------
                            _Progressivo += 1
                            _Scarico = New Funzioni_Dati.File_da_Scaricare
                            _Scarico.ID_Down = _Progressivo
                            _Scarico.ID_Object = _Selezionato.p_ID
                            _Scarico.ID_Section = _LaSezione.p_ID
                            _Scarico.p_Categoria = _LaSezione.p_Title
                            If _Settore = "BRO-PDF" Then
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoadPdf
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoadPdf
                            Else
                                _Scarico.p_File_da_Scaricare = _Selezionato.p_ToDownLoad
                                _Scarico.p_File_Finale = _Selezionato.p_ToDownLoad
                            End If
                            _Scarico.p_Tipologia = "BRO"
                            _Scarico.p_File_da_Unizippare = False
                            Lista_DaScaricare.Add(_Scarico)
                        End If
                    End If
                Next


            '---------------------------------------------------------
            '   Salvare la Lista sul DB Locale
            '---------------------------------------------------------

            Dim _Cm As SQLite.Net.SQLiteCommand
            Dim _StrUpd As String = ""
            Conn_SQL.BeginTransaction()
            Try

                For Each _Scaricare In Lista_DaScaricare

                    Conn_SQL.Insert(_Scaricare)

                Next

                Conn_SQL.Commit()

            Catch ex As Exception
                Conn_SQL.Rollback()
            Finally
                Conn_SQL.Close()
            End Try


        End Function

            Public Shared Function Elenco_Oggetti_da_Scaricare() As List(Of String)

                Elenco_Oggetti_da_Scaricare = New List(Of String)

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare)).ToList

                For Each _Listamelo As Funzioni_Dati.File_da_Scaricare In ListaSezioni
                    Dim StringaComplessa As String = ""


                    If _Listamelo.p_Tipologia = "IMG" Then StringaComplessa &= "IMG       "   'Primi 10 la tipologia
                    If _Listamelo.p_Tipologia = "VID" Then StringaComplessa &= "VIDEO     "   'Primi 10 la tipologia
                    If _Listamelo.p_Tipologia = "BRO" Then StringaComplessa &= "BRO       "   'Primi 10 la tipologia


                    StringaComplessa &= (_Listamelo.ID_Object & New String(" ", 10)).Substring(0, 10)  'ID specifico
                    StringaComplessa &= (_Listamelo.ID_Section & New String(" ", 10)).Substring(0, 10)  'Categoria
                    StringaComplessa &= (_Listamelo.p_File_da_Scaricare & New String(" ", 20)).Substring(0, 20)   'Nome del File


                    Dim result As IEnumerable(Of String) = Elenco_Oggetti_da_Scaricare.Where(Function(s) s.Contains(StringaComplessa))
                    If result.Count = 0 Then
                        ' Segna come  da Scaricare
                        '-------------------------------------------------------------------
                        Elenco_Oggetti_da_Scaricare.Add(StringaComplessa)
                    End If

                Next

                Return Elenco_Oggetti_da_Scaricare

            End Function



            Public Shared Function Is_da_Scaricare(ByVal Id_Object As Integer) As Boolean

                Is_da_Scaricare = False

                Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
                Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

                Dim ListaSezioni As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.ID_Object = Id_Object).ToList


                Return (ListaSezioni.Count > 0)

            End Function


        End Class

        Public Class File_da_Inviare

            <SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement>
            Public Property ID_Send As Integer
            Public Property ID_Section As Integer
            Public Property ID_Object As Integer
            Public Property Title As String
            Public Property File_da_Inviare As String
            Public Property File_Thumbs As String

        End Class

    End Class
