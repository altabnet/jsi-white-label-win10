﻿Imports System.Threading
Imports Windows.Networking.BackgroundTransfer
Imports Windows.Storage
Imports Windows.Storage.Pickers

Public Class Gestore_DownLoad

    Public Shared Async Function StartDownload(ByVal InputUrl As String, ByVal FileName As String) As Task

        '---------------------------------------------------------------------------
        '---------------------------------------------------------------------------
        ' Consentirebbe la scelta dell'utente
        '---------------------------------------------------------------------------
        '---------------------------------------------------------------------------
        'Dim MioPicker As FolderPicker = New FolderPicker()
        'MioPicker.SuggestedStartLocation = PickerLocationId.Desktop
        'MioPicker.ViewMode = PickerViewMode.Thumbnail
        'MioPicker.FileTypeFilter.Add("*")
        'Dim MioFolder As StorageFolder = Await MioPicker.PickSingleFolderAsync

        Dim MioFolder As StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

        Dim source As New Uri(InputUrl)
        Dim destinationFile As StorageFile = Await MioFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting)
        'Dim destinationFile As StorageFile = Await MioFolder.CreateFileAsync(FileName, CreationCollisionOption.GenerateUniqueName)

        Dim downloader As New BackgroundDownloader()
        Dim download As DownloadOperation = downloader.CreateDownload(source, destinationFile)

        Dim Progress As System.Progress(Of DownloadOperation) = New Progress(Of DownloadOperation)

        Dim CancellationToken As New CancellationTokenSource

        Try
            Dim LoScarico As DownloadOperation = Await download.StartAsync().AsTask(CancellationToken.Token, Progress)
        Catch ex As Exception

        End Try

    End Function


    Public Shared Async Function StartDownload(ByVal InputUrl As String, ByVal Destination_Folder As StorageFolder, ByVal FileName As String) As Task(Of Boolean)

        '---------------------------------------------------------------------------
        '---------------------------------------------------------------------------
        ' Consentirebbe la scelta dell'utente
        '---------------------------------------------------------------------------
        '---------------------------------------------------------------------------
        'Dim MioPicker As FolderPicker = New FolderPicker()
        'MioPicker.SuggestedStartLocation = PickerLocationId.Desktop
        'MioPicker.ViewMode = PickerViewMode.Thumbnail
        'MioPicker.FileTypeFilter.Add("*")
        'Dim MioFolder As StorageFolder = Await MioPicker.PickSingleFolderAsync

        Dim MioFolder As StorageFolder = Destination_Folder

        Dim source As New Uri(InputUrl)
        Dim destinationFile As StorageFile = Await MioFolder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting)

        Dim downloader As New BackgroundDownloader()
        Dim download As DownloadOperation = downloader.CreateDownload(source, destinationFile)

        Dim Progress As System.Progress(Of DownloadOperation) = New Progress(Of DownloadOperation)

        Dim CancellationToken As New CancellationTokenSource

        Try
            Dim LoScarico As DownloadOperation = Await download.StartAsync().AsTask(CancellationToken.Token, Progress)
            Return True
        Catch ex As Exception
            Dim Pippo As String = ex.Message
            Return False
        End Try

    End Function

End Class
