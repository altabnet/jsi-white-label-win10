﻿Public Class UpdateSync


    Public Class Generale

        Public Property Sections As List(Of Sections)
        Public Property SectionsDeleted As List(Of SectionsDeleted)

        Public Property Brochures As List(Of Brochures)
        Public Property BrochuresDeleted As List(Of BrochuresDeleted)

        Public Property Videos As List(Of Videos)
        Public Property VideosDeleted As List(Of VideosDeleted)

        Public Property Images As List(Of Images)
        Public Property ImagesDeleted As List(Of ImagesDeleted)

        Public Property WebSites As List(Of Websites)
        Public Property WebSitesDeteted As WebsitesDeleted

        Public Property BaseInfo As BaseInfo


    End Class


    Public Class Sections

        Dim m_ID As Integer           ' Identificatore del menù
        Dim m_FK As Object           ' Se il menù è un ChildMenù FK è l' ID del Parent
        Dim m_Title As Object    ' Descrizione della categoria
        Dim m_CategoryColor As Object  '   Non Usata
        Dim m_Pos As Object        ' Posizione all'interno del Menù
        Dim m_LandScapeImage As Object   '   Non Usata
        Dim m_PortraitImage As Object   '   Non Usata
        Dim m_LanguageID As Object     'Id del Liunguaggio Selezionato
        Dim m_UnAvaibleForSend As Object    'Always false

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer


        Public Property FK As Object
            Get
                If m_FK Is Nothing Then
                    m_FK = 0
                End If
                Return m_FK
            End Get
            Set(value As Object)
                m_FK = value
            End Set
        End Property


        Public Property Title As Object
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property


        Public Property CategoryColor As Object
            Get
                If m_CategoryColor Is Nothing Then
                    m_CategoryColor = ""
                End If
                Return m_CategoryColor
            End Get
            Set(value As Object)
                m_CategoryColor = value
            End Set
        End Property

        Public Property Pos As Object
            Get
                If m_Pos Is Nothing Then
                    m_Pos = ""
                End If
                Return m_Pos
            End Get
            Set(value As Object)
                m_Pos = value
            End Set
        End Property


        Public Property LandScapeImage As Object
            Get
                If m_LandScapeImage Is Nothing Then
                    m_LandScapeImage = ""
                End If
                Return m_LandScapeImage
            End Get
            Set(value As Object)
                m_LandScapeImage = value
            End Set
        End Property


        Public Property PortraitImage As Object
            Get
                If m_PortraitImage Is Nothing Then
                    m_PortraitImage = ""
                End If
                Return m_PortraitImage
            End Get
            Set(value As Object)
                m_PortraitImage = value
            End Set
        End Property


        Public Property LanguageID As Object
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property


        Public Property UnAvaibleForSend As Object
            Get
                If m_UnAvaibleForSend Is Nothing Then
                    m_UnAvaibleForSend = False
                End If
                Return m_UnAvaibleForSend
            End Get
            Set(value As Object)
                m_UnAvaibleForSend = value
            End Set
        End Property


    End Class

    Public Class SectionsDeleted

        Dim m_ID As Integer           ' Identificatore del menù
        Dim m_FK As Object           ' Se il menù è un ChildMenù FK è l' ID del Parent
        Dim m_Title As Object    ' Descrizione della categoria
        Dim m_CategoryColor As Object  '   Non Usata
        Dim m_Pos As Object        ' Posizione all'interno del Menù
        Dim m_LandScapeImage As Object  '   Non Usata
        Dim m_PortraitImage As Object  '   Non Usata
        Dim m_LanguageID As Object     'Id del Liunguaggio Selezionato
        Dim m_UnAvaibleForSend As Object    'Always false

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer


        Public Property FK As Object
            Get
                If m_FK Is Nothing Then
                    m_FK = 0
                End If
                Return m_FK
            End Get
            Set(value As Object)
                m_FK = value
            End Set
        End Property


        Public Property Title As Object
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property


        Public Property CategoryColor As Object
            Get
                If m_CategoryColor Is Nothing Then
                    m_CategoryColor = ""
                End If
                Return m_CategoryColor
            End Get
            Set(value As Object)
                m_CategoryColor = value
            End Set
        End Property

        Public Property Pos As Object
            Get
                If m_Pos Is Nothing Then
                    m_Pos = ""
                End If
                Return m_Pos
            End Get
            Set(value As Object)
                m_Pos = value
            End Set
        End Property


        Public Property LandScapeImage As Object
            Get
                If m_LandScapeImage Is Nothing Then
                    m_LandScapeImage = ""
                End If
                Return m_LandScapeImage
            End Get
            Set(value As Object)
                m_LandScapeImage = value
            End Set
        End Property


        Public Property PortraitImage As Object
            Get
                If m_PortraitImage Is Nothing Then
                    m_PortraitImage = ""
                End If
                Return m_PortraitImage
            End Get
            Set(value As Object)
                m_PortraitImage = value
            End Set
        End Property


        Public Property LanguageID As Object
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property


        Public Property UnAvaibleForSend As Object
            Get
                If m_UnAvaibleForSend Is Nothing Then
                    m_UnAvaibleForSend = False
                End If
                Return m_UnAvaibleForSend
            End Get
            Set(value As Object)
                m_UnAvaibleForSend = value
            End Set
        End Property

    End Class


    Public Class Videos

        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo del Video
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class

    Public Class VideosDeleted

        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo del Video
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class



    Public Class Images

        Dim m_Section As List(Of Object)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Object        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class

    Public Class ImagesDeleted

        Dim m_Section As List(Of Object)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Object        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class



    Public Class Websites

        Dim m_UrlToOpen As Object  'Indirizzo Url da aprire
        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello


        Public Property UrlToOpen As Object   'Indirizzo Url da aprire
            Get
                If m_UrlToOpen Is Nothing Then
                    m_UrlToOpen = ""
                End If
                Return m_UrlToOpen
            End Get
            Set(value As Object)
                m_UrlToOpen = value
            End Set
        End Property

        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class

    Public Class WebsitesDeleted

        Dim m_UrlToOpen As Object  'Indirizzo Url da aprire
        Dim m_Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Integer        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello


        Public Property UrlToOpen As Object   'Indirizzo Url da aprire
            Get
                If m_UrlToOpen Is Nothing Then
                    m_UrlToOpen = ""
                End If
                Return m_UrlToOpen
            End Get
            Set(value As Object)
                m_UrlToOpen = value
            End Set
        End Property

        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property

    End Class


    Public Class BaseInfo
        Dim m_UrlPath As String = ""   'path di partenza per i DownLoad               
        Dim m_HqHashing As String = ""   'hashing Code

        Public Property UrlPath As String    'path di partenza per i DownLoad               
        Public Property HqHashing As String   'hashing Code

    End Class


    Public Class Brochures

        Dim m_Section As List(Of Object)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Object        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_Pages As List(Of Object)                         '  Array di Pagine da visualizzare
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_ToDownLoadPdf As Object      ''Nome del PDF della versione originale
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property


        Public Property Pages As List(Of Object)                         '  Array di Pagine da visualizzare

        Public Property ToDownLoadPdf As Object       ''Nome del PDF della versione originale
            Get
                If m_ToDownLoadPdf Is Nothing Then
                    m_ToDownLoadPdf = ""
                End If
                Return m_ToDownLoadPdf
            End Get
            Set(value As Object)
                m_ToDownLoadPdf = value
            End Set
        End Property

    End Class

    Public Class BrochuresDeleted

        Dim m_Section As List(Of Object)     'Identificativo della Sezione a cui si è collegati 
        Dim m_ID As Object        'Identificativo dell'Immagine
        Dim m_Title As Object    'Descrizione da evidenziare
        Dim m_HashID As Object    'Logical File identifier
        Dim m_Tags As Object      ' Lista separata da virgole di TAGS sui quali cercare
        Dim m_VersionMod As Object    ' Numero di version delle Modifiche
        Dim m_VersionFile As Object      'Numero di versione del File
        Dim m_Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
        Dim m_Pages As List(Of Object)                         '  Array di Pagine da visualizzare
        Dim m_ToDownLoad As Object    'Nome del File da Scaricare
        Dim m_ToExtract As Object          'Always TRUE
        Dim m_Active As Object         'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
        Dim m_ToDownLoadPdf As Object      ''Nome del PDF della versione originale
        Dim m_Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
        Dim m_LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
        Dim m_UnAvailableForSend As Object    'Se il contenuto può o meno essere aggiunto al carrello



        Public Property Section As List(Of Integer)     'Identificativo della Sezione a cui si è collegati 

        <SQLite.Net.Attributes.PrimaryKey>
        Public Property ID As Integer        'Identificativo del Video

        Public Property Title As Object     'Descrizione da evidenziare
            Get
                If m_Title Is Nothing Then
                    m_Title = ""
                End If
                Return m_Title
            End Get
            Set(value As Object)
                m_Title = value
            End Set
        End Property

        Public Property HashID As Object    'Logical File identifier
            Get
                If m_HashID Is Nothing Then
                    m_HashID = ""
                End If
                Return m_HashID
            End Get
            Set(value As Object)
                m_HashID = value
            End Set
        End Property

        Public Property Tags As Object       ' Lista separata da virgole di TAGS sui quali cercare
            Get
                If m_Tags Is Nothing Then
                    m_Tags = ""
                End If
                Return m_Tags
            End Get
            Set(value As Object)
                m_Tags = value
            End Set
        End Property

        Public Property VersionMod As Object    ' Numero di version delle Modifiche
            Get
                If m_VersionMod Is Nothing Then
                    m_VersionMod = 0
                End If
                Return m_VersionMod
            End Get
            Set(value As Object)
                m_VersionMod = value
            End Set
        End Property

        Public Property VersionFile As Object      'Numero di versione del File
            Get
                If m_VersionFile Is Nothing Then
                    m_VersionFile = 0
                End If
                Return m_VersionFile
            End Get
            Set(value As Object)
                m_VersionFile = value
            End Set
        End Property

        Public Property Thumb As Object        'Nome del File da usare per la visualizzazione in Griglia
            Get
                If m_Thumb Is Nothing Then
                    m_Thumb = ""
                End If
                Return m_Thumb
            End Get
            Set(value As Object)
                m_Thumb = value
            End Set
        End Property

        Public Property ToDownLoad As Object     'Nome del File da Scaricare
            Get
                If m_ToDownLoad Is Nothing Then
                    m_ToDownLoad = ""
                End If
                Return m_ToDownLoad
            End Get
            Set(value As Object)
                m_ToDownLoad = value
            End Set
        End Property

        Public Property ToExtract As Object           'Always TRUE
            Get
                If m_ToExtract Is Nothing Then
                    m_ToExtract = False
                End If
                Return m_ToExtract
            End Get
            Set(value As Object)
                m_ToExtract = value
            End Set
        End Property

        Public Property Active As Object          'False se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Active Is Nothing Then
                    m_Active = True
                End If
                Return m_Active
            End Get
            Set(value As Object)
                m_Active = value
            End Set
        End Property

        Public Property Order As Object                  'Ordine di Visualizzaizone all'interno della sezione
            Get
                If m_Order Is Nothing Then
                    m_Order = 0
                End If
                Return m_Order
            End Get
            Set(value As Object)
                m_Order = value
            End Set
        End Property

        Public Property Deleted As Object               'True se deve essere disabilitato e quindi non visibile (come se fosse un ulteriore filtro)
            Get
                If m_Deleted Is Nothing Then
                    m_Deleted = False
                End If
                Return m_Deleted
            End Get
            Set(value As Object)
                m_Deleted = value
            End Set
        End Property

        Public Property LanguageID As Object            'Contenuto visibile solo per questi ID_Lingua
            Get
                If m_LanguageID Is Nothing Then
                    m_LanguageID = 0
                End If
                Return m_LanguageID
            End Get
            Set(value As Object)
                m_LanguageID = value
            End Set
        End Property

        Public Property UnAvailableForSend As Object    'Se il conten
            Get
                If m_UnAvailableForSend Is Nothing Then
                    m_UnAvailableForSend = False
                End If
                Return m_UnAvailableForSend
            End Get
            Set(value As Object)
                m_UnAvailableForSend = value
            End Set
        End Property


        Public Property Pages As List(Of Object)                         '  Array di Pagine da visualizzare

        Public Property ToDownLoadPdf As Object       ''Nome del PDF della versione originale
            Get
                If m_ToDownLoadPdf Is Nothing Then
                    m_ToDownLoadPdf = ""
                End If
                Return m_ToDownLoadPdf
            End Get
            Set(value As Object)
                m_ToDownLoadPdf = value
            End Set
        End Property

    End Class

End Class
