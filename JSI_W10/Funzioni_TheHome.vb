﻿Imports Windows.Storage

Module Funzioni_TheHome


    Public Sub Carica_Menu(ByVal TheHome As The_Home_Page)

        Dim Indice As Integer = 0
        Dim Il_Font As New FontFamily("Segoe UI")
        Dim LaGrandezza As Integer = 18
        Dim _Progressivo As Integer = 0

        For Indi As Integer = 0 To 99
            TheHome.IntestaMenu_Codice(Indi) = ""
            TheHome.IntestaMenu_Testo(Indi) = ""
            TheHome.IntestaMenu_Lunghezza(Indi) = 0
            TheHome.IntestaMenu_Partenza(Indi) = 0
        Next

        For Indice = 0 To App.LeSezioni_Da_DataBase.Count - 1
            TheHome.IntestaMenu_Codice(Indice) = App.LeSezioni_Da_DataBase(Indice).p_ID
            TheHome.IntestaMenu_Testo(Indice) = App.LeSezioni_Da_DataBase(Indice).p_Title
            Dim _SizeString As Size = Funzioni_Generali.Dammi_Misura_Stringa(TheHome.IntestaMenu_Testo(Indice), Il_Font, LaGrandezza)
            TheHome.IntestaMenu_Lunghezza(Indice) = _SizeString.Width + 10
            TheHome.IntestaMenu_Partenza(Indice) = 0
            _Progressivo += _SizeString.Width + 10
        Next

    End Sub

    Public Sub Carica_Menu_GB(ByVal TheHome As The_Global_Page)

        Dim Indice As Integer = 0
        Dim Il_Font As New FontFamily("Segoe UI")
        Dim LaGrandezza As Integer = 15
        Dim _Progressivo As Integer = 0

        For Indi As Integer = 0 To 99
            TheHome.IntestaMenu_Codice(Indi) = ""
            TheHome.IntestaMenu_Testo(Indi) = ""
            TheHome.IntestaMenu_Lunghezza(Indi) = 0
            TheHome.IntestaMenu_Partenza(Indi) = 0
        Next

        For Indice = 0 To App.LeSezioni_Da_DataBase.Count - 1
            TheHome.IntestaMenu_Codice(Indice) = App.LeSezioni_Da_DataBase(Indice).p_ID
            TheHome.IntestaMenu_Testo(Indice) = App.LeSezioni_Da_DataBase(Indice).p_Title
            Dim _SizeString As Size = Funzioni_Generali.Dammi_Misura_Stringa(TheHome.IntestaMenu_Testo(Indice), Il_Font, LaGrandezza)
            TheHome.IntestaMenu_Lunghezza(Indice) = _SizeString.Width + 10
            TheHome.IntestaMenu_Partenza(Indice) = 0
            _Progressivo += _SizeString.Width + 10
        Next

    End Sub


    Public Async Function Seleziona_Per_Categoria(ByVal Id_Settore As Integer, ByVal Immagini As Boolean,
                                                            ByVal Video As Boolean,
                                                            ByVal Brochures As Boolean,
                                                            ByVal Updating_in_Corso As Boolean) As Task(Of List(Of Lista_Oggetti.Lista_Oggetti_Visibili))

        Dim W_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Thumbs"))
        Dim I_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))
        Dim V_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Videos"))

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        Dim Oggetti As New List(Of Lista_Oggetti.Lista_Oggetti_Visibili)


        If Immagini Then
            Dim ListImg As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Settore_Call(Id_Settore, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListImg.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListImg(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListImg(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListImg(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListImg(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListImg(Indi).p_ID

                Oggetto.IDSection = Id_Settore
                Oggetto.Order = ListImg(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "IMG"
                Oggetto.Title = ListImg(Indi).p_Title

                Oggetto.File_Reale = I_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe

                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListImg(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else
                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Images\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If


                Oggetti.Add(Oggetto)
            Next

        End If


        If Video Then
            Dim ListVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Settore_Call(Id_Settore, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListVideo.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListVideo(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListVideo(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListVideo(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListVideo(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListVideo(Indi).p_ID

                Oggetto.IDSection = Id_Settore
                Oggetto.Order = ListVideo(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "VID"
                Oggetto.Title = ListVideo(Indi).p_Title

                Oggetto.File_Reale = V_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe
                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListVideo(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else
                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Videos\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If

                Oggetti.Add(Oggetto)
            Next
        End If


        If Brochures Then
            Dim ListBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Settore_Call(Id_Settore, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListBrochure.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListBrochure(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListBrochure(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListBrochure(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListBrochure(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListBrochure(Indi).p_ID

                Oggetto.IDSection = Id_Settore
                Oggetto.Order = ListBrochure(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "BRO"
                Oggetto.Title = ListBrochure(Indi).p_Title

                Oggetto.File_Reale = V_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe
                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListBrochure(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else

                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Pages\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If

                Oggetti.Add(Oggetto)
            Next
        End If


        For Each Oggetto As Lista_Oggetti.Lista_Oggetti_Visibili In Oggetti

            Dim _TestoBottone = App.Bottone_Add
            For Each DaInviare In App.Oggetti_da_Inviare
                If DaInviare.ID_Section = Oggetto.IDSection AndAlso DaInviare.ID_Object = Oggetto.ID Then
                    _TestoBottone = App.Bottone_Saved
                End If
            Next
            Oggetto.Testo_Bottone = _TestoBottone
        Next


        Return Oggetti
    End Function



    Public Async Function Seleziona_Per_Filtro(ByVal Filtro As String, ByVal Immagini As Boolean,
                                                            ByVal Video As Boolean,
                                                            ByVal Brochures As Boolean,
                                                            ByVal Updating_in_Corso As Boolean) As Task(Of List(Of Lista_Oggetti.Lista_Oggetti_Visibili))

        Dim W_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Thumbs"))
        Dim I_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))
        Dim V_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Videos"))

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        Dim Oggetti As New List(Of Lista_Oggetti.Lista_Oggetti_Visibili)


        If Immagini Then
            Dim ListImg As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Filtro_Call(Filtro, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListImg.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListImg(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListImg(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListImg(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListImg(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListImg(Indi).p_ID

                Oggetto.IDSection = ListImg(Indi).p_Section
                Oggetto.Order = ListImg(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "IMG"
                Oggetto.Title = ListImg(Indi).p_Title

                Oggetto.File_Reale = I_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe

                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListImg(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else
                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Images\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If


                Oggetti.Add(Oggetto)
            Next

        End If


        If Video Then
            Dim ListVideo As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Filtro_Call(Filtro, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListVideo.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListVideo(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListVideo(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListVideo(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListVideo(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListVideo(Indi).p_ID

                Oggetto.IDSection = ListVideo(Indi).p_Section
                Oggetto.Order = ListVideo(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "VID"
                Oggetto.Title = ListVideo(Indi).p_Title

                Oggetto.File_Reale = V_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe
                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListVideo(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else
                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Videos\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If

                Oggetti.Add(Oggetto)
            Next
        End If


        If Brochures Then
            Dim ListBrochure As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochure_Filtro_Call(Filtro, App.m_Lang_NoRete_Id)

            For Indi As Integer = 0 To ListBrochure.Count - 1

                Dim _FileName As String = ""
                Dim _FileEffe As String = ""

                '--------------------------------------
                ' Settaggio della Thumbs
                If ListBrochure(Indi).p_Thumb.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListBrochure(Indi).p_Thumb
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileName = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If

                '--------------------------------------
                ' Settaggio del File
                If ListBrochure(Indi).p_ToDownLoad.Trim.Length > 0 Then
                    Dim Stringa_Thumb As String = ListBrochure(Indi).p_ToDownLoad
                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Length
                    _FileEffe = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)
                End If


                Dim Oggetto As New Lista_Oggetti.Lista_Oggetti_Visibili
                Oggetto.ID = ListBrochure(Indi).p_ID

                Oggetto.IDSection = ListBrochure(Indi).p_Section
                Oggetto.Order = ListBrochure(Indi).p_Order
                Oggetto.Thumbs = W_DestinationFolder.Path & "\" & _FileName
                Oggetto.Tipologia = "BRO"
                Oggetto.Title = ListBrochure(Indi).p_Title

                Oggetto.File_Reale = V_DestinationFolder.Path & "\" & _FileEffe
                Oggetto.File_Reale_SoloFile = _FileEffe
                If Funzioni_Generali.Connessione_Internet_Attiva Then
                    If Funzioni_Dati.Is_Oggetto_Da_Scaricare(ListBrochure(Indi).p_ID) Or Updating_in_Corso Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                Else

                    Dim item = Await ApplicationData.Current.LocalFolder.TryGetItemAsync("\Pages\" & _FileEffe.Replace("/", "\"))
                    If item Is Nothing Then
                        Oggetto.Opacita = 0.5
                    Else
                        Oggetto.Opacita = 1
                    End If
                End If

                Oggetti.Add(Oggetto)
            Next
        End If


        For Each Oggetto As Lista_Oggetti.Lista_Oggetti_Visibili In Oggetti

            Dim _TestoBottone = App.Bottone_Add
            For Each DaInviare In App.Oggetti_da_Inviare
                If DaInviare.ID_Section = Oggetto.IDSection AndAlso DaInviare.ID_Object = Oggetto.ID Then
                    _TestoBottone = App.Bottone_Saved
                End If
            Next
            Oggetto.Testo_Bottone = _TestoBottone
        Next


        Return Oggetti
    End Function



    Public Function Genera_Lista_DownLoad()

        'Recupero dei File Salvati
        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        Dim Lista_Da_Scaricare = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Order By p.ID_Section).ToList


        'Colore dei Bottoni......




        If Lista_Da_Scaricare.Count = 0 Then
            App.Categorie_da_Scaricare = New List(Of File_Down.Categorie_To_Down)
            Exit Function

        Else
            'Inserire gli Oggetti da Scaricare
            App.Categorie_da_Scaricare = New List(Of File_Down.Categorie_To_Down)

            For Each OggettoDaScaricare As Funzioni_Dati.File_da_Scaricare In Lista_Da_Scaricare

                Dim _Trovato As Boolean = False
                For Each _Categoria As File_Down.Categorie_To_Down In App.Categorie_da_Scaricare
                    If _Categoria.ID_Section = OggettoDaScaricare.ID_Section Then

                        _Categoria.NumeroOggetti = _Categoria.NumeroOggetti + 1
                        _Categoria.Testo_Bottone = _Categoria.Title & " (" & CStr(_Categoria.NumeroOggetti) & " )"

                        'qui
                        _Categoria.Colore_Sfondo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnBkgColor)
                        _Categoria.Colore_Testo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnTxtColor)
                        _Categoria.Colore_Bordo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnBorderColor)
                        _Categoria.Thickness_Bottone = New Thickness(App.Layout_Globale.updatePanel.panelDownloadBtnBorderWidth)
                        _Categoria.Colore_Testo_Label = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelCategoryTxtColor)

                        _Trovato = True
                        Exit For
                    End If
                Next
                If Not _Trovato Then
                    Dim _Categoria As New File_Down.Categorie_To_Down
                    _Categoria.ID_Section = OggettoDaScaricare.ID_Section
                    _Categoria.Title = OggettoDaScaricare.p_Categoria
                    _Categoria.NumeroOggetti = 1
                    _Categoria.Testo_Bottone = _Categoria.Title & " (" & CStr(_Categoria.NumeroOggetti) & " )"


                    _Categoria.Colore_Sfondo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnBkgColor)
                    _Categoria.Colore_Testo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnTxtColor)
                    _Categoria.Colore_Bordo_Bottone = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelDownloadBtnBorderColor)
                    _Categoria.Thickness_Bottone = New Thickness(App.Layout_Globale.updatePanel.panelDownloadBtnBorderWidth)
                    _Categoria.Colore_Testo_Label = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.updatePanel.panelCategoryTxtColor)
                    App.Categorie_da_Scaricare.Add(_Categoria)
                End If
            Next

        End If

    End Function


End Module
