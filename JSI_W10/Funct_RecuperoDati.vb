﻿Imports JSI_Altab.Altab_WS

Public Class Funct_RecuperoDati



    Public Shared Async Function Recupero_Dati_e_Struttura() As Task(Of String)

        Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings

        Dim uHash As String = localSettings.Values("Login_NoRete_rootHashID").ToString

        Dim client As MobileServiceClient = New MobileServiceClient()
        Dim _Ritorno As Task(Of String) = client.UpdateSyncAsync(uHash)
        _Ritorno.Wait()

        Dim json As String = _Ritorno.Result

        Return json

    End Function



    Public Shared Async Sub Recupero_e_Save_Dati_From_TheHome()
        ''-------------------------------------------------
        ''  (Menù non presente nel DB)
        ''Recupero del Dato dal Alteb_WS  (Server)
        ''-------------------------------------------------
        'Dim Testo_Struttura As String = Await (Funct_RecuperoDati.Recupero_Dati_e_Struttura())

        ''------------------------------------
        '' Conversione del JSON in Struttura
        ''------------------------------------
        'App.Struttura_Globale = Newtonsoft.Json.JsonConvert.DeserializeObject(Of UpdateSync.Generale)(Testo_Struttura)

        '-----------------------------------------------------
        ' Verifica se ci sono state Modifiche nei Menù 
        '-----------------------------------------------------
        Dim MenuModificati As Boolean = False
        If App.Struttura_Globale.SectionsDeleted.Count = 0 Then
            MenuModificati = True
        Else
            Dim SezioniDB As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_ALL_Call()
            Dim SezioniConvertite As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Genera_da_NuovoStruttura
            MenuModificati = Verifica_Differenza_Menu(SezioniConvertite, SezioniDB)
        End If

    End Sub

    Public Shared Function Verifica_Differenza_Menu(ByVal Sezioni_Struttura As List(Of Funzioni_Dati.Sezioni), ByVal Sezioni_DB As List(Of Funzioni_Dati.Sezioni)) As Boolean

        Dim Differente As Boolean = False

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


        For Indice As Integer = 0 To Sezioni_Struttura.Count - 1

            Dim m_ID As Integer = Sezioni_Struttura(Indice).p_ID
            Dim m_Title As String = Sezioni_Struttura(Indice).p_Title
            Dim m_Languageid As Integer = Sezioni_Struttura(Indice).p_LanguageID

            Dim result As List(Of Funzioni_Dati.Sezioni) = (From p In Conn_SQL.Table(Of Funzioni_Dati.Sezioni) Where p.p_ID = m_ID And p.p_Title.Equals(m_Title) And p.p_LanguageID = m_Languageid).ToList


            If result.Count = 0 Then
                Differente = True
                Exit For
            End If

        Next

        Return Differente
    End Function

    Public Shared Function Convert_JSON_ReturnedFrom_UpDateSync_Sections(jsonString As String) As List(Of Funzioni_Dati.Sezioni)

        ' Mi creo un Array  di tanti eventuali risultati (nel caso fosse una tabella)
        Dim jsonParts As String() = jsonString.Replace("[", "").Replace("]", "").Split("},{")


        'Mi carico la struttura di Controlli
        Dim Le_Mie_Sezioni As New List(Of Funzioni_Dati.Sezioni)
        Dim Una_Sezione As Funzioni_Dati.Sezioni

        Dim Riga_Rilevante As Boolean = False
        Dim _PassatoDallaSezione As Boolean = False


        For Each jp As String In jsonParts

            If jp.Trim.Length > 0 Then
                'Splitto ogni campo della stessa riga
                Dim propData As String() = jp.Replace("{", "").Replace("}", "").Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)


                For Each rowData As String In propData

                    Dim idx As Integer = 0
                    Dim n As String = ""
                    Dim v As String = ""

                    Dim idx2 As Integer = 0
                    Dim n2 As String = ""
                    Dim v2 As String = ""

                    Dim idx3 As Integer = 0
                    Dim n3 As String = ""
                    Dim v3 As String = ""

                    Try
                        idx = rowData.IndexOf(":")
                        n = rowData.Substring(0, idx - 1).Replace("""", "").Trim
                        v = rowData.Substring(idx + 1).Replace("""", "").Trim


                        idx2 = rowData.IndexOf(":", idx + 1)
                        If idx2 > idx Then
                            n2 = rowData.Substring(idx + 1, idx2 - 1 - idx).Replace("""", "").Trim
                            v2 = rowData.Substring(idx2 + 1).Replace("""", "").Trim

                            idx3 = 0
                            n3 = ""
                            If n.ToUpper <> "Policy".ToUpper Then
                                idx3 = rowData.IndexOf(":", idx2 + 1)
                            End If

                            If idx3 > idx2 Then
                                n3 = rowData.Substring(idx2 + 1, idx3 - 1 - idx2).Replace("""", "").Trim
                                v3 = rowData.Substring(idx3 + 1).Replace("""", "").Trim
                            End If

                            If n3.Trim.Length > 0 AndAlso v3.Trim.Length > 0 Then
                                n = n2
                            End If


                            If n.ToUpper = "Sections".ToUpper Then
                                Riga_Rilevante = True
                                _PassatoDallaSezione = True
                            Else
                                Riga_Rilevante = False
                                If _PassatoDallaSezione Then
                                    Return Le_Mie_Sezioni
                                    Exit For
                                End If
                            End If


                            If n3.Trim.Length > 0 AndAlso v3.Trim.Length > 0 Then
                                n2 = n3
                                v2 = v3
                            End If

                        End If

                        If n2.Trim.Length > 0 AndAlso v2.Trim.Length > 0 Then
                            n = n2
                            v = v2
                        End If



                        If Riga_Rilevante Then

                            If v = "null" Then
                                If n.ToUpper = "ID" Or n.ToUpper = "FK" Or n.ToUpper = "LanguageId" Then
                                    v = 0
                                Else
                                    v = ""
                                End If
                            End If


                            If n.ToUpper = "ID" Then
                                Una_Sezione = New Funzioni_Dati.Sezioni
                            End If
                            If n.ToUpper = "ID" Then Una_Sezione.p_ID = v
                            If n.ToUpper = "FK" Then Una_Sezione.p_FK = v
                            If n.ToUpper = "TITLE" Then Una_Sezione.p_Title = v

                            If n.ToUpper = "CategoryColor".ToUpper Then Una_Sezione.p_CategoryColor = v
                            If n.ToUpper = "Pos".ToUpper Then Una_Sezione.p_Pos = v
                            If n.ToUpper = "LandScapeImage".ToUpper Then Una_Sezione.p_LandScapeImage = v

                            If n.ToUpper = "PortraitImage".ToUpper Then Una_Sezione.p_PortraitImage = v
                            If n.ToUpper = "LanguageId".ToUpper Then Una_Sezione.p_LanguageID = v
                            If n.ToUpper = "UnavailableForSend".ToUpper Then Una_Sezione.p_UnAvaibleForSend = False  '  v
                            If n.ToUpper = "UnavailableForSend".ToUpper Then
                                Le_Mie_Sezioni.Add(Una_Sezione)
                            End If
                        End If

                    Catch ex As Exception
                        Continue For
                    End Try

                Next
            End If
        Next

        Return Le_Mie_Sezioni

    End Function

End Class
