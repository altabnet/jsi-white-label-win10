﻿''' <summary>
''' Provides application-specific behavior to supplement the default Application class.
''' </summary>
NotInheritable Class App
    Inherits Application



    '--------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------
    '   Variabili Globali
    '--------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------

    'Public Shared G_Analytics_ID As String = "UA-96098690-1"


    Public Shared Struttura_Globale As UpdateSync.Generale

    Public Shared Layout_Globale As Layout.RootObject

    Public Shared Visualizza_Bottone_Aggiornamento As Boolean = True
    Public Shared Testo_Privacy As String = ""

    '*****   LOGIN   *****
    Public Shared Firm_HdHash As String = "C81552F04617F895A253DDAE7547FE2EDA148591"                     'Identificativo della Company

    Public Shared Lingue_Diponibili As List(Of Funzioni_Lingue.Lingue_Disponibili)
    Public Shared m_Lang_NoRete_Code As String = "EN"
    Public Shared m_Lang_NoRete_Id As Integer = 1
    Public Shared m_Username As String = ""

    Public Shared MFly_Stats As MenuFlyout
    Public Shared MFly_Languages As MenuFlyout

    Public Shared Stringa_Contenuto_File As String = ""
    Public Shared Is_First_Connection As Boolean = False
    '*****   LOGIN   *****

    '****  SEZIONI ****
    Public Shared Caricamento_DatiStruttura_Effettuto As Boolean = False
    Public Shared Menu_Aggiornato As Boolean = False
    Public Shared LeSezioni_Da_DataBase As List(Of Funzioni_Dati.Sezioni)
    Public Shared Sezioni_Secondarie As List(Of Funzioni_Dati.Sezioni)

    Public Shared Bottone_Add As String = "Add to cart"
    Public Shared Bottone_Saved As String = "Saved"
    Public Shared Bottone_Send As String = "SEND"
    Public Shared Bottone_Close As String = "Close"
    Public Shared Bottone_Updating As String = "Updating"
    Public Shared Bottone_Update As String = "Update"
    Public Shared Bottone_Aggiornamento As String = "Update"

    Public Shared Title_SettorePrincipale As String = ""
    Public Shared Title_SettoreSecondario As String = ""


    '----------------------------------------
    ' DownLoad
    '----------------------------------------
    Public Shared Thumbs_Da_Scaricare As List(Of String)
    Public Shared Thumbs_SCARICATE As List(Of String)
    Public Shared Da_Scaricare As List(Of String)
    Public Shared Le_Mie_Relazioni_Brochure As List(Of Funzioni_Dati.TEMP_Relazione_Brochure_ID)
    Public Shared Le_Mie_Relazioni_Immagini As List(Of Funzioni_Dati.TEMP_Relazione_Immagini_ID)
    Public Shared Le_Mie_Relazioni_Video As List(Of Funzioni_Dati.TEMP_Relazione_Video_ID)
    Public Shared Le_Mie_Relazioni_Page As List(Of Funzioni_Dati.TEMP_Relazione_Brochure_Page)
    Public Shared Contatore_Generale As Integer = 0


    '----------------------------------------
    ' Gestione
    '----------------------------------------
    Public Shared Oggetti_Selezionati As List(Of Lista_Oggetti.Lista_Oggetti_Visibili)
    Public Shared Oggetti_da_Inviare As List(Of Funzioni_Dati.File_da_Inviare)
    Public Shared Categorie_da_Scaricare As List(Of File_Down.Categorie_To_Down)
    Public Shared Oggetti_in_Downloading As List(Of Funzioni_Dati.File_da_Scaricare)
    Public Shared Visualizzo_Grid_Ricerca As Boolean = False
    Public Shared Oggetto_Search As Lista_Oggetti.Lista_Oggetti_Visibili
    Public Shared Testo_Oggetto_Search As String = ""

    Public Shared Dwonload_Massivo_dalla_Prima_Pagina As Boolean = False
    Public Shared Oggetti_Da_Inviare_dalla_Prima_Pagina As Boolean = False

    Public Shared Lista_File_in_Downloading As List(Of Funzioni_Generali.File_in_Downloading)
    Public Shared Lista_File_in_Downloading_File As List(Of Funzioni_Generali.File_in_Downloading)

    '--------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------
    '   Variabili Globali
    '--------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------

    Public Shared Sto_Eseguendo As Boolean = False


    '--------------------------------------------------------------------------------------------------------
    'Image page
    '--------------------------------------------------------------------------------------------------------
    Public Shared _Image_Path As String = ""
    Public Shared _Image_Title As String = ""

    Public Shared Selected_Id As Integer = 0
    Public Shared Selected_IdSector As Integer = 0
    Public Shared Selected_Object As Lista_Oggetti.Lista_Oggetti_Visibili

    Public Shared _Immagini As Boolean = False
    Public Shared _Video As Boolean = False
    Public Shared _Brochures As Boolean = False
    Public Shared _All As Boolean = False

    'Public Shared LaPagina As The_Global_Page

    'Shared _myProperty As Integer = 0
    'Public Shared Property MyProperty() As Integer
    '    Get
    '        Return _myProperty
    '    End Get
    '    Set(value As Integer)
    '        If _myProperty <> value Then
    '            The_Global_Page.Aggiorna_Label(App.LaPagina)
    '        End If
    '        _myProperty = value
    '    End Set
    'End Property


    Shared m_TestoBottone_Update As String = ""
    Public Shared Property Testo_Bottone_Update As String
        Get
            Return m_TestoBottone_Update
        End Get
        Set(value As String)
            m_TestoBottone_Update = value
            'RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Testo_Bottone"))
        End Set
    End Property



    ''' <summary>
    ''' Invoked when the application is launched normally by the end user.  Other entry points
    ''' will be used when the application is launched to open a specific file, to display
    ''' search results, and so forth.
    ''' </summary>
    ''' <param name="e">Details about the launch request and process.</param>
    Protected Overrides Sub OnLaunched(e As Windows.ApplicationModel.Activation.LaunchActivatedEventArgs)
        '#If DEBUG Then
        '        ' Show graphics profiling information while debugging.
        '        If System.Diagnostics.Debugger.IsAttached Then
        '            ' Display the current frame rate counters
        '            Me.DebugSettings.EnableFrameRateCounter = True
        '        End If
        '#End If

        Dim rootFrame As Frame = TryCast(Window.Current.Content, Frame)

        ' Do not repeat app initialization when the Window already has content,
        ' just ensure that the window is active

        If rootFrame Is Nothing Then
            ' Create a Frame to act as the navigation context and navigate to the first page
            rootFrame = New Frame()

            AddHandler rootFrame.NavigationFailed, AddressOf OnNavigationFailed

            If e.PreviousExecutionState = ApplicationExecutionState.Terminated Then
                ' TODO: Load state from previously suspended application
            End If
            ' Place the frame in the current Window
            Window.Current.Content = rootFrame
        End If

        If e.PrelaunchActivated = False Then
            If rootFrame.Content Is Nothing Then
                ' When the navigation stack isn't restored navigate to the first page,
                ' configuring the new page by passing required information as a navigation
                ' parameter
                rootFrame.Navigate(GetType(MainPage), e.Arguments)
            End If

            ' Ensure the current window is active
            Window.Current.Activate()
        End If
    End Sub

    ''' <summary>
    ''' Invoked when Navigation to a certain page fails
    ''' </summary>
    ''' <param name="sender">The Frame which failed navigation</param>
    ''' <param name="e">Details about the navigation failure</param>
    Private Sub OnNavigationFailed(sender As Object, e As NavigationFailedEventArgs)
        Throw New Exception("Failed to load Page " + e.SourcePageType.FullName)
    End Sub

    ''' <summary>
    ''' Invoked when application execution is being suspended.  Application state is saved
    ''' without knowing whether the application will be terminated or resumed with the contents
    ''' of memory still intact.
    ''' </summary>
    ''' <param name="sender">The source of the suspend request.</param>
    ''' <param name="e">Details about the suspend request.</param>
    Private Sub OnSuspending(sender As Object, e As SuspendingEventArgs) Handles Me.Suspending
        Dim deferral As SuspendingDeferral = e.SuspendingOperation.GetDeferral()
        ' TODO: Save application state and stop any background activity
        deferral.Complete()
    End Sub

End Class
