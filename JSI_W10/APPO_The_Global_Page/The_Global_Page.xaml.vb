﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
''' 
Imports JSI_W10.Lista_Oggetti
Imports Windows.Storage
Imports SharpCompress.Archive
Imports SharpCompress.Common


Public NotInheritable Class The_Global_Page
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

    Public Search_List As New List(Of List(Of Lista_Oggetti.Lista_Oggetti_Visibili))

    Dim OggettiFiltrati As List(Of Lista_Oggetti_Visibili)

    Dim m_Alert_Warning As String = "Warning"
    Dim m_Alert_OK As String = "Ok"

    Dim m_Temp_Languages As String = "Languages"
    Dim m_Temp_Results_For As String = "Results for"
    Dim m_Temp_No_Result As String = "Your Search '{{ serachText }}' did not match any documents. "
    Dim m_Temp_Home As String = "Homepage"
    Dim m_Temp_Logout As String = "Logout"
    Dim m_Temp_Stats As String = "Stats"
    Dim m_SecIDE_TopThank As String = ""

    Dim MStats_FlyOut(100) As Flyout
    Dim MStats_FlyOut_Codice(100) As String

    '------------------------------------------------------
    ' Variabili per la Gesione Dinamica del Menù
    '------------------------------------------------------
    Public IntestaMenu_Codice(100) As String
    Public IntestaMenu_Testo(100) As String
    Public IntestaMenu_Lunghezza(100) As Integer
    Public IntestaMenu_Partenza(100) As Integer
    Public Width_InizioMenu As Integer = 0

    Public IFlyOut(100) As Flyout
    Public IFlyOut_Codice(100) As String

    Public MTest As MenuFlyout

    Public Menu_Primo_Elemento As Integer = 0
    Public Menu_Primo_Elemento_Precedente As Integer = 0
    Public Menu_Ultimo_Elemento As Integer = 0
    '------------------------------------------------------
    ' Variabili per la Gesione Dinamica del Menù
    '------------------------------------------------------


    Public Oggetti_Da_Mostrare As List(Of Lista_Oggetti_Visibili)
    Public Categorie_Down As List(Of File_Down.Categorie_To_Down)
    Public File_to_Send As List(Of File_Send.File_da_Inviare)

    Public A_Oggetti_001 As List(Of Lista_Oggetti_Visibili)
    Dim _UrlBase As String = ""
    Dim ImageFolder As StorageFolder
    Dim VideoFolder As StorageFolder
    Dim BrochureFolder As StorageFolder
    Dim BrochureFolderZIP As StorageFolder

    Dim TB_Possono_Agire As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Oggetti_Da_Mostrare = App.Oggetti_Selezionati
        TB_Possono_Agire = False

    End Sub



    Private Async Sub The_Global_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        'Me.Lbl_MessaggioGenerico.Text = localSettings.Values("Scelta_Title").ToString
        _UrlBase = "http://data.just-send-it.com/" & App.Firm_HdHash & "/"
        ImageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Images"))
        VideoFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Videos"))
        BrochureFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Pages"))
        BrochureFolderZIP = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Pages\7z"))


        'Me.Button_Click_1(Nothing, Nothing)

        If App.Dwonload_Massivo_dalla_Prima_Pagina Then
            App.Dwonload_Massivo_dalla_Prima_Pagina = False
            If Me.Filtra_Oggetti.Text.Trim.Length > 0 Then
                Me.Filtra_Oggetti.Text = ""
                App.Testo_Oggetto_Search = ""
            Else
                Me.Btn_Aggiornamento_Click(Nothing, Nothing)
            End If
        End If

        If App.Visualizzo_Grid_Ricerca And App.Testo_Oggetto_Search.Trim.Length > 0 Then
            Me.Filtra_Oggetti_QuerySubmitted(Nothing, Nothing)
        End If


    End Sub

    Private Async Sub GridView_ItemClick(sender As Object, e As ItemClickEventArgs)

        Dim OggettoSelezionato As New Lista_Oggetti_Visibili

        If sender Is Nothing Then
            OggettoSelezionato = App.Oggetto_Search
        Else
            OggettoSelezionato = e.ClickedItem()
        End If




        If Funzioni_Dati.File_da_Scaricare.Is_da_Scaricare(OggettoSelezionato.ID) Then
            'If OggettoSelezionato.Opacita = 0.5 Then


            '------------------------------------------------------------
            ' Se il file non è ancora presente....
            '------------------------------------------------------------
            If Funzioni_Generali.Connessione_Internet_Attiva Then

                Dim TestoBT As String = OggettoSelezionato.Testo_Bottone
                OggettoSelezionato.Testo_Bottone = "DOWN"


                '****  IMG  
                'Scarica il File
                If OggettoSelezionato.Tipologia = "IMG" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then
                    Dim Stringa_Thumb As String = OggettoSelezionato.File_Reale_SoloFile
                    Dim _InputUrl As String = _UrlBase & "Images/" & Stringa_Thumb

                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                    Await (Gestore_DownLoad.StartDownload(_InputUrl, ImageFolder, _Filename))
                    OggettoSelezionato.Opacita = 1

                    Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID)
                End If
                '****  FINE  IMG  


                '****  VID  
                'Scarica il File
                If OggettoSelezionato.Tipologia = "VID" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then
                    Dim Stringa_Thumb As String = OggettoSelezionato.File_Reale_SoloFile
                    Dim _InputUrl As String = _UrlBase & "Videos/" & Stringa_Thumb

                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                    Await (Gestore_DownLoad.StartDownload(_InputUrl, VideoFolder, _Filename))
                    OggettoSelezionato.Opacita = 1

                    Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID)
                End If
                '****  FINE  VID  



                '****  BRO  
                'Scarica il File
                If OggettoSelezionato.Tipologia = "BRO" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then

                    ' Gestione del Download
                    '------------------------------------------------------------
                    Dim Stringa_Thumb As String = OggettoSelezionato.File_Reale_SoloFile
                    Dim _InputUrl As String = _UrlBase & "download/" & Stringa_Thumb

                    'Try
                    '    Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync("Pages", CreationCollisionOption.FailIfExists)
                    'Catch ex As Exception
                    'End Try
                    'Try
                    '    Dim newFolder As StorageFolder = Await localFolder.CreateFolderAsync("Pages\7z", CreationCollisionOption.FailIfExists)
                    'Catch ex As Exception
                    'End Try

                    Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
                    Dim Lungo As Integer = Stringa_Thumb.Trim.Length
                    Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

                    Await (Gestore_DownLoad.StartDownload(_InputUrl, BrochureFolderZIP, _Filename))
                    '------------------------------------------------------------
                    ' FINE -  Gestione del Download
                    '------------------------------------------------------------

                    'Gestione dell'unzip
                    '------------------------------------------------------------

                    Using stream = Await (Await localFolder.GetFileAsync("Pages\7z\" & _Filename)).OpenStreamForReadAsync()
                        Using archive = SharpCompress.Archives.SevenZip.SevenZipArchive.Open(stream)

                            For Each entry As SharpCompress.Archives.SevenZip.SevenZipArchiveEntry In archive.Entries
                                Using entryStream = entry.OpenEntryStream()
                                    Dim file = Await ApplicationData.Current.LocalFolder.CreateFileAsync(entry.Key.Replace("/", "\"), CreationCollisionOption.OpenIfExists)
                                    Using fileStream = Await file.OpenStreamForWriteAsync()
                                        entryStream.CopyTo(fileStream)
                                    End Using
                                End Using
                            Next
                        End Using
                    End Using

                    ''------------------------------------------------------------
                    ''  FINE  Gestione dell'unzip
                    ''------------------------------------------------------------

                    OggettoSelezionato.Opacita = 1

                    Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID)
                End If
                '****  FINE  BRO  

                OggettoSelezionato.Testo_Bottone = TestoBT

            End If


        Else
            OggettoSelezionato.Opacita = 1
            Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID)

            '------------------------------------------------------------
            ' Se il file è presente va in visualizzazione
            '------------------------------------------------------------
            '****  IMG  
            'Visualizza il File
            If OggettoSelezionato.Tipologia = "IMG" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then

                App._Image_Path = OggettoSelezionato.File_Reale.Trim
                App._Image_Title = OggettoSelezionato.Title
                App.Selected_Id = OggettoSelezionato.ID
                App.Selected_IdSector = OggettoSelezionato.IDSection
                App.Selected_Object = OggettoSelezionato

                Me.Frame.Navigate(GetType(The_Image_Page))

            End If
            '****  FINE  IMG  


            '****  VID
            'Visualizza il File
            If OggettoSelezionato.Tipologia = "VID" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then

                App._Image_Path = OggettoSelezionato.File_Reale.Trim.Replace("/", "\")
                App._Image_Title = OggettoSelezionato.Title
                App.Selected_Id = OggettoSelezionato.ID
                App.Selected_IdSector = OggettoSelezionato.IDSection
                App.Selected_Object = OggettoSelezionato

                Me.Frame.Navigate(GetType(The_Video_Page))

            End If
            '****  FINE  IMG  


            ''****  BRO
            ''Visualizza il File
            If OggettoSelezionato.Tipologia = "BRO" AndAlso OggettoSelezionato.File_Reale_SoloFile.Trim.Length > 3 Then

                App._Image_Path = OggettoSelezionato.File_Reale.Trim.Replace("/", "\")
                App._Image_Title = OggettoSelezionato.Title
                App.Selected_Id = OggettoSelezionato.ID
                App.Selected_IdSector = OggettoSelezionato.IDSection
                App.Selected_Object = OggettoSelezionato

                Me.Frame.Navigate(GetType(The_Brochure_Page))

            End If
            ''****  FINE  IMG  


        End If
        'Me.Primo.Content = CatalogoSelezionato.Titolo
    End Sub


    Private Async Sub The_Global_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading

        If App.Visualizzo_Grid_Ricerca = False Then
            Me.Griglia_Centrale.Visibility = Visibility.Visible
            Me.Griglia_Search.Visibility = Visibility.Collapsed
        Else
            Me.Griglia_Centrale.Visibility = Visibility.Collapsed
            Me.Griglia_Search.Visibility = Visibility.Visible
        End If


        If Not TB_Possono_Agire Then

            If App._Immagini AndAlso App._Video AndAlso App._Brochures Then
                App._All = True

                Colora_Tb_Button(Me.TB_All, True)
                Colora_Tb_Button(Me.TB_Brochere, False)
                Colora_Tb_Button(Me.TB_Immagini, False)
                Colora_Tb_Button(Me.TB_Video, False)
            Else
                App._All = False

                Colora_Tb_Button(Me.TB_All, False)
                Colora_Tb_Button(Me.TB_Brochere, App._Brochures)
                Colora_Tb_Button(Me.TB_Immagini, App._Immagini)
                Colora_Tb_Button(Me.TB_Video, App._Video)

            End If

            TB_Possono_Agire = True

            Metti_a_Video_Immagine_Sent()
        End If


        '----------------------------------------------------------------
        Dim Temp_Languages As Object = localSettings.Values("TemIDE_Languages")
        Dim Temp_Home As Object = localSettings.Values("TemIDE_Home")
        Dim Temp_Logout As Object = localSettings.Values("TemIDE_LogOut")
        Dim Temp_Stats As Object = localSettings.Values("TemIDE_Stats")
        Dim SecIDE_TopThank As Object = localSettings.Values("SecIDE_TopThank")

        Dim Temp_Results_For As Object = localSettings.Values("TemIDE_ResultsFor")
        Dim Temp_No_Result As Object = localSettings.Values("TemIDE_NoResults")


        Dim Alert_Warning As Object = localSettings.Values("AleIDE_Warning")
        Dim Alert_OK As Object = localSettings.Values("AleIDE_OK")


        If Not Temp_Languages Is Nothing Then m_Temp_Languages = Temp_Languages
        If Not Temp_Home Is Nothing Then m_Temp_Home = Temp_Home
        If Not Temp_Logout Is Nothing Then m_Temp_Logout = Temp_Logout
        If Not Temp_Stats Is Nothing Then m_Temp_Stats = Temp_Stats

        If Not Temp_Results_For Is Nothing Then m_Temp_Results_For = Temp_Results_For
        If Not Temp_No_Result Is Nothing Then m_Temp_No_Result = Temp_No_Result


        If Not SecIDE_TopThank Is Nothing Then m_SecIDE_TopThank = SecIDE_TopThank

        If Not Alert_Warning Is Nothing Then m_Alert_Warning = Alert_Warning
        If Not Alert_OK Is Nothing Then m_Alert_OK = Alert_OK

        Me.Lnk_Lingue.Content = m_Temp_Languages
        Me.Lbl_MessaggioGenerico_XXX.Text = m_SecIDE_TopThank


        Dim Bottone_01 As Object = localSettings.Values("SecIDE_Add")
        Dim Bottone_02 As Object = localSettings.Values("SecIDE_Saved")
        Dim Bottone_03 As Object = localSettings.Values("SecIDE_Send")
        Dim Bottone_04 As Object = localSettings.Values("DetIDE_Close")
        Dim Bottone_05 As Object = localSettings.Values("SecIDE_Updating")
        Dim Bottone_06 As Object = localSettings.Values("SecIDE_Updated")


        If Not Bottone_01 Is Nothing Then App.Bottone_Add = Bottone_01
        If Not Bottone_02 Is Nothing Then App.Bottone_Saved = Bottone_02
        If Not Bottone_03 Is Nothing Then App.Bottone_Send = Bottone_03
        If Not Bottone_04 Is Nothing Then App.Bottone_Close = Bottone_04
        If Not Bottone_05 Is Nothing Then App.Bottone_Updating = Bottone_05
        If Not Bottone_06 Is Nothing Then App.Bottone_Update = Bottone_06


        Me.Btn_Send.Content = App.Bottone_Send
        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento

        Menu_Primo_Elemento = 0
        Menu_Primo_Elemento_Precedente = 0
        Menu_Ultimo_Elemento = 0


        Me.Lnk_Lingue.Visibility = Visibility.Visible
        Me.Btn_Settings.Visibility = Visibility.Visible

        '-------------------------------------------------------------------
        '-------------------------------------------------------------------
        '  Creazioen Menu STATS
        '-------------------------------------------------------------------
        '-------------------------------------------------------------------


        If App.MFly_Stats Is Nothing Then
            App.MFly_Stats = New MenuFlyout()

            Dim mn As New MenuFlyoutItem()
            mn.Text = m_Temp_Home
            mn.Name = "Stats_01"
            AddHandler mn.Tapped, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            App.MFly_Stats.Items.Add(mn)

            mn = New MenuFlyoutItem()
            mn.Text = m_Temp_Logout
            mn.Name = "Stats_02"
            AddHandler mn.Tapped, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            App.MFly_Stats.Items.Add(mn)

            mn = New MenuFlyoutItem()
            mn.Text = m_Temp_Stats
            mn.Name = "Stats_03"
            AddHandler mn.Tapped, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            App.MFly_Stats.Items.Add(mn)
        End If


        If Funzioni_Generali.Connessione_Internet_Attiva Then
            '-------------------------------------------------------------------
            '-------------------------------------------------------------------
            ' CREZIONE MENU LINGUE
            '-------------------------------------------------------------------
            '-------------------------------------------------------------------
            If App.MFly_Languages Is Nothing Then
                If App.Lingue_Diponibili Is Nothing Then
                    Funzioni_Lingue.Carica_Lingue(App.Lingue_Diponibili, localSettings.Values("Login_NoRete_HashID").ToString)
                End If

                App.MFly_Languages = New MenuFlyout()
                For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

                    Dim mLang As New MenuFlyoutItem()
                    mLang.Text = App.Lingue_Diponibili(Indice).p_DisplayName.Trim
                    mLang.Name = "Btn_" & App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
                    AddHandler mLang.Tapped, AddressOf Lingua_Scelta
                    mLang.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                    mLang.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

                    App.MFly_Languages.Items.Add(mLang)

                Next
            End If
        Else
            Me.Lnk_Lingue.Visibility = Visibility.Collapsed
        End If


        Funzioni_TheHome.Carica_Menu_GB(Me)


        ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen

        Me.Lbl_SettorePrincipale.Text = App.Title_SettorePrincipale
        Me.Lbl_SettoreSecondario.Text = App.Title_SettoreSecondario


        Metti_a_Video_Immagine_Sent()

        Me.Griglia_Intestazione.Visibility = Visibility.Collapsed
        If App.Visualizzo_Grid_Ricerca Then

            Me.Filtra_Oggetti.Text = App.Testo_Oggetto_Search

            Me.Griglia_Centrale.Visibility = Visibility.Collapsed
            Me.Griglia_Search.Visibility = Visibility.Visible

        End If

        If App.Dwonload_Massivo_dalla_Prima_Pagina OrElse App.Oggetti_Da_Inviare_dalla_Prima_Pagina Then
            Me.Griglia_Centrale.Visibility = Visibility.Collapsed
            Me.Griglia_Search.Visibility = Visibility.Collapsed

            Me.Griglia_Intestazione.Visibility = Visibility.Visible
            Me.Pannello_SottoScelte.Visibility = Visibility.Collapsed

            App.Oggetti_Da_Inviare_dalla_Prima_Pagina = False
        End If

    End Sub

    Private Sub Colora_Tb_Button(ByRef Il_Bottone As Button, ByVal Selezionato As Boolean)

        If Selezionato Then
            Il_Bottone.BorderThickness = New Thickness(1, 1, 1, 1)
            Il_Bottone.BorderBrush = Funzioni_Generali.GetSolidColorBrush("#03476F")
            Il_Bottone.Foreground = Funzioni_Generali.GetSolidColorBrush("#03476F")
            Il_Bottone.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        Else
            Il_Bottone.BorderThickness = New Thickness(0, 0, 0, 0)
            Il_Bottone.Foreground = Funzioni_Generali.GetSolidColorBrush("#CFDFF0")
            Il_Bottone.Background = Funzioni_Generali.GetSolidColorBrush("#03476F")
        End If
    End Sub

    Private Async Sub TB_All_Click(sender As Object, e As RoutedEventArgs) Handles TB_All.Click

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        If Not App._All Then
            TB_Possono_Agire = False
            Colora_Tb_Button(Me.TB_All, True)
            Colora_Tb_Button(Me.TB_Brochere, False)
            Colora_Tb_Button(Me.TB_Immagini, False)
            Colora_Tb_Button(Me.TB_Video, False)

            TB_Possono_Agire = True

            App._All = True
            App._Immagini = True
            App._Video = True
            App._Brochures = True

            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(localSettings.Values("Scelta_Id"), True, True, True, (App.Bottone_Aggiornamento = App.Bottone_Updating)))
            Oggetti_Da_Mostrare = App.Oggetti_Selezionati

            Me.Frame.Navigate(GetType(The_Global_Page))

        Else
            TB_Possono_Agire = False
            Colora_Tb_Button(Me.TB_All, False)
            Colora_Tb_Button(Me.TB_Brochere, True)
            Colora_Tb_Button(Me.TB_Immagini, True)
            Colora_Tb_Button(Me.TB_Video, True)

            TB_Possono_Agire = True

            App._Immagini = True
            App._Video = True
            App._Brochures = True
            App._All = False

        End If

    End Sub

    Private Async Sub TB_Immagini_Click(sender As Object, e As RoutedEventArgs) Handles TB_Immagini.Click

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        If TB_Possono_Agire Then
            TB_Possono_Agire = False

            If App._All Then
                App._Immagini = True
                App._Video = False
                App._Brochures = False

            Else
                App._Immagini = Not App._Immagini
            End If


            If Not App._Immagini And Not App._All Then     'tb_immagini.is_checked = false
                App._All = False

                If App._Brochures = False And App._Video = False And App._Immagini = False Then
                    App._All = True
                End If
            Else
                If App._Brochures = False Or App._Video = False Then
                    App._All = False
                Else
                    App._All = True
                End If
            End If
            TB_Possono_Agire = True


            If App._All = True Then
                App._Immagini = True
                App._Video = True
                App._Brochures = True

                Colora_Tb_Button(Me.TB_All, True)
                Colora_Tb_Button(Me.TB_Brochere, False)
                Colora_Tb_Button(Me.TB_Immagini, False)
                Colora_Tb_Button(Me.TB_Video, False)
            Else
                Colora_Tb_Button(Me.TB_All, False)
                Colora_Tb_Button(Me.TB_Brochere, App._Brochures)
                Colora_Tb_Button(Me.TB_Immagini, App._Immagini)
                Colora_Tb_Button(Me.TB_Video, App._Video)
            End If


            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(localSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

            Me.Frame.Navigate(GetType(The_Global_Page))
        End If

    End Sub

    Private Async Sub TB_Video_Click(sender As Object, e As RoutedEventArgs) Handles TB_Video.Click

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        If TB_Possono_Agire Then
            TB_Possono_Agire = False

            If App._All Then
                App._Video = True
                App._Brochures = False
                App._Immagini = False

            Else
                App._Video = Not App._Video
            End If


            If Not App._Video And Not App._All Then     'tb_immagini.is_checked = false
                App._All = False

                If App._Brochures = False And App._Video = False And App._Immagini = False Then
                    App._All = True
                End If
            Else
                If App._Brochures = False Or App._Immagini = False Then
                    App._All = False
                Else
                    App._All = True
                End If
            End If
            TB_Possono_Agire = True


            If App._All = True Then
                App._Immagini = True
                App._Video = True
                App._Brochures = True

                Colora_Tb_Button(Me.TB_All, True)
                Colora_Tb_Button(Me.TB_Brochere, False)
                Colora_Tb_Button(Me.TB_Immagini, False)
                Colora_Tb_Button(Me.TB_Video, False)
            Else
                Colora_Tb_Button(Me.TB_All, False)
                Colora_Tb_Button(Me.TB_Brochere, App._Brochures)
                Colora_Tb_Button(Me.TB_Immagini, App._Immagini)
                Colora_Tb_Button(Me.TB_Video, App._Video)
            End If


            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(localSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

            Me.Frame.Navigate(GetType(The_Global_Page))
        End If


    End Sub



    Private Async Sub TB_Brochure_Click(sender As Object, e As RoutedEventArgs) Handles TB_Brochere.Click

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        If TB_Possono_Agire Then
            TB_Possono_Agire = False

            If App._All Then
                App._Brochures = True
                App._Video = False
                App._Immagini = False
            Else
                App._Brochures = Not App._Brochures
            End If


            If Not App._Brochures And Not App._All Then     'tb_immagini.is_checked = false
                App._All = False

                If App._Video = False And App._Video = False And App._Immagini = False Then
                    App._All = True
                End If
            Else
                If App._Video = False Or App._Immagini = False Then
                    App._All = False
                Else
                    App._All = True
                End If
            End If
            TB_Possono_Agire = True


            If App._All = True Then
                App._Immagini = True
                App._Video = True
                App._Brochures = True

                Colora_Tb_Button(Me.TB_All, True)
                Colora_Tb_Button(Me.TB_Brochere, False)
                Colora_Tb_Button(Me.TB_Immagini, False)
                Colora_Tb_Button(Me.TB_Video, False)
            Else
                Colora_Tb_Button(Me.TB_All, False)
                Colora_Tb_Button(Me.TB_Brochere, App._Brochures)
                Colora_Tb_Button(Me.TB_Immagini, App._Immagini)
                Colora_Tb_Button(Me.TB_Video, App._Video)
            End If


            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(localSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

            Me.Frame.Navigate(GetType(The_Global_Page))
        End If

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        Dim _MioBottone As Button = CType(sender, Button)
        Dim _idAccesso As String = _MioBottone.Name


        Dim LaLista As List(Of Lista_Oggetti_Visibili)
        If App.Visualizzo_Grid_Ricerca = True Then
            LaLista = OggettiFiltrati
        Else
            LaLista = Oggetti_Da_Mostrare
        End If

        For Each Oggetto As Lista_Oggetti_Visibili In LaLista
            If Oggetto.ID = _idAccesso Then
                If Oggetto.Testo_Bottone = App.Bottone_Add Then

                    Oggetto.Testo_Bottone = App.Bottone_Saved
                    'If Oggetto.Thumbs.Trim.Length > 0 Then
                    '    Me.Img_Sender.Source = New BitmapImage(New Uri(Oggetto.Thumbs))
                    'Else
                    '    Me.Img_Sender.Source = Nothing
                    'End If

                    Dim _OggettoInvio As New Funzioni_Dati.File_da_Inviare
                    _OggettoInvio.ID_Send = Funzioni_Generali.MaxId(App.Oggetti_da_Inviare, "Id_Send") + 1
                    _OggettoInvio.File_da_Inviare = Oggetto.File_Reale
                    _OggettoInvio.File_Thumbs = Oggetto.Thumbs
                    _OggettoInvio.ID_Object = Oggetto.ID
                    _OggettoInvio.ID_Section = Oggetto.IDSection
                    _OggettoInvio.Title = Oggetto.Title
                    App.Oggetti_da_Inviare.Add(_OggettoInvio)

                    Funzioni_Dati.File_To_Send.Aggiungi_Un_Elemento_to_Send(_OggettoInvio)

                Else

                    For Each DaInviare As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare
                        If DaInviare.ID_Section = Oggetto.IDSection AndAlso DaInviare.ID_Object = Oggetto.ID Then

                            Funzioni_Dati.File_To_Send.Cancella_Un_Elemento_to_Send(DaInviare)
                            App.Oggetti_da_Inviare.Remove(DaInviare)

                            Exit For
                        End If
                    Next

                    'Metti_a_Video_Immagine_Sent()
                    Oggetto.Testo_Bottone = App.Bottone_Add

                End If

                Metti_a_Video_Immagine_Sent()
                Exit For
            End If

        Next

    End Sub


    Private Sub Metti_a_Video_Immagine_Sent()
        Dim NomeFile As String = ""
        For Each DaInviare As Funzioni_Dati.File_da_Inviare In App.Oggetti_da_Inviare
            If DaInviare.File_Thumbs.Trim.Length > 0 Then
                NomeFile = DaInviare.File_Thumbs
            End If
        Next

        If NomeFile.Trim.Length > 0 Then
            Me.Pnl_Image.Visibility = Visibility.Visible
            'Me.Btn_Send.Visibility = Visibility.Visible
            Me.Img_Sender.Visibility = Visibility.Visible
            Me.Img_Sender.Source = New BitmapImage(New Uri(NomeFile))
            Me.Lbl_ContatoreSend.Text = "( " & CStr(App.Oggetti_da_Inviare.Count).Trim & " )"
        Else
            Me.Pnl_Image.Visibility = Visibility.Collapsed
            Me.Img_Sender.Source = Nothing
            'Me.Btn_Send.Visibility = Visibility.Collapsed
            'Me.Img_Sender.Visibility = Visibility.Collapsed
        End If
    End Sub


    Private Sub Btn_Settings_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Settings.Click
        If Not App.MFly_Stats Is Nothing Then
            App.MFly_Stats.ShowAt(CType(sender, FrameworkElement))
        End If
    End Sub

    Private Sub Lnk_Lingue_Click(sender As Object, e As RoutedEventArgs) Handles Lnk_Lingue.Click
        If Not App.MFly_Languages Is Nothing Then
            App.MFly_Languages.ShowAt(CType(sender, FrameworkElement))
        End If
    End Sub

    Private Async Sub Statistica_Scelta(ByVal sender As Object, ByVal e As RoutedEventArgs)

        Dim NomeBottone As String = CType(sender, Control).Name

        If NomeBottone.Trim.ToUpper = "STATS_01" Then
            Me.Frame.Navigate(GetType(The_Home_Page))
        End If

        If NomeBottone.Trim.ToUpper = "STATS_02" Then
            Me.Frame.Navigate(GetType(Login_Page))
        End If

        If NomeBottone.Trim.ToUpper = "STATS_03" Then
            If Funzioni_Generali.Connessione_Internet_Attiva Then

                Dim _Utente As String = localSettings.Values("Login_NORete_UserName")
                Dim _Pwd As String = localSettings.Values("Login_NORete_Password")

                Dim uriToLaunch As String = "http://ws.just-send-it.com/controlPanel/Auth.aspx?u=" & _Utente & "&p=" & _Pwd
                Dim uri As Uri = New Uri(uriToLaunch)

                Dim success = Await Windows.System.Launcher.LaunchUriAsync(uri)
                ' URI launched
                If success Then
                    ' URI launch failed
                Else

                End If
            End If
        End If

    End Sub


    Private Async Sub Lingua_Scelta(ByVal sender As Object, ByVal e As RoutedEventArgs)

        If Funzioni_Generali.Connessione_Internet_Attiva Then

            Dim NomeBottone As String = CType(sender, Control).Name
            Dim Codice As String = NomeBottone.Substring(4, NomeBottone.Length - 4)

            For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1
                If App.Lingue_Diponibili(Indice).p_LanguageCode.Trim = Codice Then
                    App.m_Lang_NoRete_Code = Codice
                    App.m_Lang_NoRete_Id = App.Lingue_Diponibili(Indice).p_LanguageID

                    localSettings.Values("Lang_NoRete_Code") = Codice
                    localSettings.Values("Lang_NoRete_Id") = App.Lingue_Diponibili(Indice).p_LanguageID
                End If
            Next

            Await (Funzioni_Lingue.Setta_Controlli_Lingua(Codice))

            Await Funzioni_Dati.File_da_Scaricare.Carica_File_da_Scaricare_ALL(App.Da_Scaricare)

            App.MFly_Languages = Nothing
            App.MFly_Stats = Nothing
            App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Principali_Call(App.m_Lang_NoRete_Id)
            Me.Frame.Navigate(GetType(The_Home_Page))
        End If

    End Sub


    Private Sub The_Home_Page_SizeChanged(sender As Object, e As SizeChangedEventArgs) Handles Me.SizeChanged


        If Not sender Is Nothing Then
            Menu_Primo_Elemento = 0
            Menu_Primo_Elemento_Precedente = 0
            Menu_Ultimo_Elemento = 0
        End If

        Dim bounds As Rect = Window.Current.Bounds
        Dim Height As Double = bounds.Height
        Dim Width As Double = bounds.Width
        'Me.Larghezza.Text = Width
        'Me.Altezza.Text = Height

        PannelloMenu.Children.Clear()

        Dim GrandeZZamenu As Integer = Width - 85  'Tolgo i 2 bottoni di Inizioe  Fine'
        Dim LaPartenza_W As Integer = Width_InizioMenu
        Dim Not_Primo As Boolean = False


        If Menu_Primo_Elemento > 0 Then
            Me.Btn_TornaIndietro.Visibility = Visibility.Visible
            Me.Lbl_Indietro.Visibility = Visibility.Visible
        Else
            Me.Btn_TornaIndietro.Visibility = Visibility.Collapsed
            Me.Lbl_Indietro.Visibility = Visibility.Collapsed
        End If

        For Indice As Integer = Menu_Primo_Elemento To 20

            If IntestaMenu_Lunghezza(Indice) = 0 Then
                Me.Btn_VaiAvanti.Visibility = Visibility.Collapsed
                Me.Lbl_Avanti.Visibility = Visibility.Collapsed
                Exit For
            End If
            If LaPartenza_W + IntestaMenu_Lunghezza(Indice) < GrandeZZamenu Then

                If Not_Primo Then
                    Dim _Sbarra As New TextBlock
                    _Sbarra.Name = "Sba" & CStr(Indice).Trim
                    _Sbarra.Text = "|"
                    _Sbarra.FontSize = 24
                    _Sbarra.Foreground = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                    PannelloMenu.Children.Add(_Sbarra)
                End If


                Dim _Bottone As New TextBlock
                _Bottone.Name = "Btn_" & CStr(IntestaMenu_Codice(Indice)).Trim
                _Bottone.Text = IntestaMenu_Testo(Indice)
                _Bottone.Margin = New Thickness(5, 10, 5, 0)
                _Bottone.Foreground = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                AddHandler _Bottone.Tapped, AddressOf Lbl_MenuPrincipale_Tapped
                PannelloMenu.Children.Add(_Bottone)


                LaPartenza_W += IntestaMenu_Lunghezza(Indice)
                LaPartenza_W += 5
                Not_Primo = True

                Menu_Ultimo_Elemento = Indice
            Else
                Me.Btn_VaiAvanti.Visibility = Visibility.Visible
                Me.Lbl_Avanti.Visibility = Visibility.Visible
                Exit For
            End If
        Next

    End Sub

    Private Sub Btn_VaiAvanti_Click(sender As Object, e As RoutedEventArgs) Handles Btn_VaiAvanti.Click
        Menu_Primo_Elemento_Precedente = Menu_Primo_Elemento
        Menu_Primo_Elemento = Menu_Ultimo_Elemento + 1
        Me.The_Home_Page_SizeChanged(Nothing, Nothing)

        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
        End If
    End Sub

    Private Sub Btn_TornaIndietro_Click(sender As Object, e As RoutedEventArgs) Handles Btn_TornaIndietro.Click
        If Menu_Primo_Elemento = Menu_Primo_Elemento_Precedente Then
            Menu_Primo_Elemento = 0
            Menu_Primo_Elemento_Precedente = 0
        End If
        Menu_Primo_Elemento = Menu_Primo_Elemento_Precedente
        Me.The_Home_Page_SizeChanged(Nothing, Nothing)

        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
        End If
    End Sub


    Private Async Sub Lbl_MenuPrincipale_Tapped(sender As Object, e As TappedRoutedEventArgs)

        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
            Exit Sub
        End If


        Dim _IdSection As Integer = CType(sender, TextBlock).Name.Substring(4, CType(sender, TextBlock).Name.Length - 4)
        App.Sezioni_Secondarie = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Secondarie_Call(_IdSection, App.m_Lang_NoRete_Id)

        If App.Sezioni_Secondarie.Count > 0 Then

            MTest = New MenuFlyout()
            For Indice As Integer = 0 To App.Sezioni_Secondarie.Count - 1
                Dim mn As New MenuFlyoutItem()
                mn.Text = App.Sezioni_Secondarie(Indice).p_Title
                mn.Name = "Btn_" & CStr(App.Sezioni_Secondarie(Indice).p_ID).Trim

                mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

                AddHandler mn.Tapped, AddressOf Lbl_Secondario_Tapped
                MTest.Items.Add(mn)
            Next
            MTest.ShowAt(CType(sender, FrameworkElement))

            App.Title_SettorePrincipale = ""
            App.Title_SettoreSecondario = ""
            For Indici As Integer = 0 To App.LeSezioni_Da_DataBase.Count
                If App.LeSezioni_Da_DataBase(Indici).p_ID = _IdSection Then
                    App.Title_SettorePrincipale = App.LeSezioni_Da_DataBase(Indici).p_Title
                    Exit For
                End If
            Next


        Else
            For Indice = 0 To 99
                If App.LeSezioni_Da_DataBase(Indice).p_ID = _IdSection Then
                    localSettings.Values("Scelta_Id") = _IdSection
                    localSettings.Values("Scelta_Title") = App.LeSezioni_Da_DataBase(Indice).p_Title

                    App.Visualizzo_Grid_Ricerca = False
                    Me.Griglia_Centrale.Visibility = Visibility.Visible
                    Me.Griglia_Search.Visibility = Visibility.Collapsed

                    Me.TB_All.Visibility = Visibility.Visible
                    Me.TB_Immagini.Visibility = Visibility.Visible
                    Me.TB_Brochere.Visibility = Visibility.Visible
                    Me.TB_Video.Visibility = Visibility.Visible

                    App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(_IdSection, True, True, True, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

                    App._Immagini = True
                    App._Video = True
                    App._Brochures = True
                    App.Title_SettorePrincipale = App.LeSezioni_Da_DataBase(Indice).p_Title
                    App.Title_SettoreSecondario = ""
                    App.Testo_Oggetto_Search = ""

                    Me.Frame.Navigate(GetType(The_Global_Page))
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Async Sub Lbl_Secondario_Tapped(sender As Object, e As TappedRoutedEventArgs)

        Funzioni_Generali.Invia_Email_In_Waiting()

        Me.Griglia_Intestazione.Visibility = Visibility.Collapsed
        If App.Visualizzo_Grid_Ricerca = False Then
            Me.Griglia_Centrale.Visibility = Visibility.Visible
            Me.Griglia_Search.Visibility = Visibility.Collapsed
        Else
            Me.Griglia_Centrale.Visibility = Visibility.Collapsed
            Me.Griglia_Search.Visibility = Visibility.Visible
        End If
        Me.Pannello_SottoScelte.Visibility = Visibility.Visible

        Dim IndiceMenu As Integer = CType(sender, MenuFlyoutItem).Name.Substring(4, CType(sender, MenuFlyoutItem).Name.Length - 4)

        For Indice = 0 To 99
            If App.Sezioni_Secondarie(Indice).p_ID = IndiceMenu Then
                localSettings.Values("Scelta_Id") = App.Sezioni_Secondarie(Indice).p_ID
                localSettings.Values("Scelta_Title") = App.Sezioni_Secondarie(Indice).p_Title

                App.Visualizzo_Grid_Ricerca = False
                Me.Griglia_Centrale.Visibility = Visibility.Visible
                Me.Griglia_Search.Visibility = Visibility.Collapsed

                Me.TB_All.Visibility = Visibility.Visible
                Me.TB_Immagini.Visibility = Visibility.Visible
                Me.TB_Brochere.Visibility = Visibility.Visible
                Me.TB_Video.Visibility = Visibility.Visible

                App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(App.Sezioni_Secondarie(Indice).p_ID, True, True, True, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

                App._Immagini = True
                App._Video = True
                App._Brochures = True
                App.Title_SettoreSecondario = App.Sezioni_Secondarie(Indice).p_Title
                App.Testo_Oggetto_Search = ""

                Me.Frame.Navigate(GetType(The_Global_Page))
                Exit For
            End If
        Next

    End Sub

    Private Sub Btn_Aggiornamento_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Aggiornamento.Click

        If Funzioni_Generali.Connessione_Internet_Attiva Then
            If Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento Then
                Categorie_Down = New List(Of File_Down.Categorie_To_Down)
                For Each Categoria As File_Down.Categorie_To_Down In App.Categorie_da_Scaricare

                    If Funzioni_Dati.LocalDatabase.Sezioni.Is_Categoria_Attiva(Categoria.ID_Section) Then
                        Dim LaCategoria As New File_Down.Categorie_To_Down
                        LaCategoria = Categoria
                        Categorie_Down.Add(LaCategoria)
                    End If
                Next

                Me.Lista_Down.ItemsSource = Nothing
                Me.Lista_Down.ItemsSource = Categorie_Down
                Me.MySplit_Down.IsPaneOpen = Not Me.MySplit_Down.IsPaneOpen

                If App.Contatore_Generale >= App.Oggetti_in_Downloading.Count Then
                    App.Contatore_Generale = 0
                    App.Oggetti_in_Downloading.Clear()
                End If

                If Me.MySplit_Down.IsPaneOpen Then
                    If App.Contatore_Generale < App.Oggetti_in_Downloading.Count And App.Contatore_Generale >= 0 Then
                        Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count
                    Else
                        Me.Lbl_Down.Text = ""
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Btn_Send_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Send.Click

        Funzioni_Generali.Invia_Email_In_Waiting()

        If App.Oggetti_da_Inviare.Count > 0 Then
            Me.Frame.Navigate(GetType(Send_Page))
        End If
    End Sub

    Private Async Sub Button_Click_1(sender As Object, e As RoutedEventArgs)

        Dim Quanti As Integer = 0
        Dim _idAccesso As String = ""


        If Not sender Is Nothing Then
            'Dim _MioBottone As Button = CType(sender, Button)
            _idAccesso = e.OriginalSource.DataContext.Id_Section
            Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path & "\Data", "JSi_W10.sqlite")
            Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)


            Dim lancialo As Boolean = True
            Dim ListaFile As List(Of Funzioni_Dati.File_da_Scaricare) = (From p In Conn_SQL.Table(Of Funzioni_Dati.File_da_Scaricare) Where p.ID_Section = _idAccesso).ToList
            Dim SelezioneOggetti As New List(Of Funzioni_Dati.File_da_Scaricare)


            If App.Oggetti_in_Downloading.Count = 0 Then
                App.Contatore_Generale = -1
            End If

            For Each Lista As Funzioni_Dati.File_da_Scaricare In ListaFile
                Dim AggiungiFile As New Funzioni_Dati.File_da_Scaricare
                AggiungiFile = Lista
                AggiungiFile.p_Scaricato = False
                App.Oggetti_in_Downloading.Add(AggiungiFile)
                'Quanti += 1


                Dim AggiungiFile2 As New Funzioni_Dati.File_da_Scaricare
                AggiungiFile2 = Lista
                AggiungiFile2.p_Scaricato = False
                SelezioneOggetti.Add(AggiungiFile)
            Next


            If App.Contatore_Generale > -1 Then

                'Inserire  La Sceklta in una lista di appoggio
                Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count
                Exit Sub
            Else
                App.Contatore_Generale = 0
            End If

        End If

        Do

            Dim ListaDaeseguire As New List(Of Funzioni_Dati.File_da_Scaricare)
            For Each Lista As Funzioni_Dati.File_da_Scaricare In App.Oggetti_in_Downloading
                If Lista.p_Scaricato = False Then
                    Dim AggiungiFile As New Funzioni_Dati.File_da_Scaricare
                    AggiungiFile = Lista
                    AggiungiFile.p_Scaricato = False
                    ListaDaeseguire.Add(AggiungiFile)
                End If
            Next


            If ListaDaeseguire.Count > 0 Then
                '
                Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count
                '
                'Eseguire la Lista ....
                '
                Dim File_Scaricato As Boolean = False

                For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In ListaDaeseguire    'App.Oggetti_in_Downloading

                    File_Scaricato = False
                    If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "IMG" Then
                        File_Scaricato = Await Funzioni_Generali.Scarica_File(OggettoSelezionato, ImageFolder, _UrlBase)
                    End If
                    If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "VID" Then
                        File_Scaricato = Await Funzioni_Generali.Scarica_File(OggettoSelezionato, VideoFolder, _UrlBase)
                    End If
                    If OggettoSelezionato.p_Tipologia.Trim.ToUpper = "BRO" Then
                        File_Scaricato = Await Scarica_e_Unzippa_File(OggettoSelezionato, BrochureFolderZIP, _UrlBase)
                    End If

                    If File_Scaricato Then
                        'Diminuisci dalla Lista del Settore
                        '---------------------------------------------------
                        For Each Settore As File_Down.Categorie_To_Down In Me.Categorie_Down
                            If Settore.ID_Section = _idAccesso Then
                                If Settore.NumeroOggetti = 1 Then
                                    Me.Categorie_Down.Remove(Settore)

                                    'Elimina anche dal calcolo genrale
                                    For Each Settore_APP As File_Down.Categorie_To_Down In App.Categorie_da_Scaricare
                                        If Settore_APP.ID_Section = _idAccesso Then
                                            App.Categorie_da_Scaricare.Remove(Settore)
                                            Exit For
                                        End If
                                    Next


                                Else
                                    Settore.NumeroOggetti = Settore.NumeroOggetti - 1
                                End If
                                Exit For
                            End If
                        Next


                        Funzioni_Dati.Elimina_Oggetto_Da_Scaricare(OggettoSelezionato.ID_Object)
                    End If



                    Quanti += 1
                    App.Contatore_Generale += 1
                    Me.Lbl_Down.Text = "Downloading ... " & CStr(App.Contatore_Generale) & " / " & App.Oggetti_in_Downloading.Count

                Next
                ''Ricarica la lista
                ''---------------------------------------------------
                Me.Lista_Down.ItemsSource = Nothing
                Me.Lista_Down.ItemsSource = Categorie_Down


                'Setta gli ID Letti
                For Each OggettoSelezionato As Funzioni_Dati.File_da_Scaricare In ListaDaeseguire    'App.Oggetti_in_Downloading

                    For Each Oggetto In App.Oggetti_in_Downloading
                        If OggettoSelezionato.ID_Object = Oggetto.ID_Object Then
                            Oggetto.p_Scaricato = True
                        End If
                    Next
                Next


            Else

                'Se Vuota uscire
                Exit Do
            End If


        Loop
        Me.Lbl_Down.Text = ""
        App.Oggetti_in_Downloading.Clear()
        App.Contatore_Generale = -1

        Exit Sub

    End Sub




    Private Async Function Scarica_e_Unzippa_File(ByVal OggettoSelezionato As Funzioni_Dati.File_da_Scaricare,
                                                ByVal _TheFolder As StorageFolder,
                                                ByVal _UlrBase As String) As Task(Of Boolean)

        Dim Consideralo_Scaricato As Boolean = False

        'Scarica IMG
        Try
            ' Gestione del Download
            '------------------------------------------------------------
            Dim Stringa_Thumb As String = OggettoSelezionato.p_File_da_Scaricare
            Dim _InputUrl As String = _UrlBase & Stringa_Thumb

            Dim Inizio As Integer = Stringa_Thumb.IndexOf("/")
            Dim Lungo As Integer = Stringa_Thumb.Trim.Length
            Dim _Filename As String = Stringa_Thumb.Substring(Inizio + 1, Lungo - Inizio - 1)

            Consideralo_Scaricato = Await (Gestore_DownLoad.StartDownload(_InputUrl, _TheFolder, _Filename))

            If Consideralo_Scaricato = False Then
                Return Consideralo_Scaricato
            End If
            '------------------------------------------------------------
            ' FINE -  Gestione del Download
            '------------------------------------------------------------

            'Gestione dell'unzip
            '------------------------------------------------------------

            Using stream = Await (Await localFolder.GetFileAsync("Pages\7z\" & _Filename)).OpenStreamForReadAsync()
                Using archive = SharpCompress.Archives.SevenZip.SevenZipArchive.Open(stream)

                    For Each entry As SharpCompress.Archives.SevenZip.SevenZipArchiveEntry In archive.Entries
                        Using entryStream = entry.OpenEntryStream()
                            Dim file = Await ApplicationData.Current.LocalFolder.CreateFileAsync(entry.Key.Replace("/", "\"), CreationCollisionOption.OpenIfExists)
                            Using fileStream = Await file.OpenStreamForWriteAsync()
                                entryStream.CopyTo(fileStream)
                            End Using
                        End Using
                    Next
                End Using
            End Using

            ''------------------------------------------------------------
            '  FINE  Gestione dell'unzip
            '------------------------------------------------------------

            Consideralo_Scaricato = True
            Me.Lbl_Extract.Text = ""

        Catch ex As Exception
            Consideralo_Scaricato = False
        End Try

        Return Consideralo_Scaricato
    End Function


    Private Async Sub Filtra_Oggetti_QuerySubmitted(sender As AutoSuggestBox, args As AutoSuggestBoxQuerySubmittedEventArgs) Handles Filtra_Oggetti.QuerySubmitted
        App.Visualizzo_Grid_Ricerca = True

        Dim sqlpath = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "JSi_W10.sqlite")
        Dim Conn_SQL As New SQLite.Net.SQLiteConnection(New SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), sqlpath)

        '----------------------------------------------------------------
        ' Nel Caso siano presenti elimina gli elementi creati in precedenza per altra Serach
        '--------------------------------------------------------------------
        Dim Eliminato As Boolean = False
        Do
            Eliminato = False

            For Each Elemento As Object In Me.Griglia_Centrale_Vista.Items
                Me.Griglia_Centrale_Vista.Items.Remove(Elemento)
                Eliminato = True
                Exit For
            Next
            If Not Eliminato Then
                Exit Do
            End If
        Loop


        Search_List = New List(Of List(Of Lista_Oggetti.Lista_Oggetti_Visibili))
        App.Testo_Oggetto_Search = Me.Filtra_Oggetti.Text


        If Me.Filtra_Oggetti.Text.Trim.Length = 0 Then
            '----------------------------------------------------------------------------------
            '  Caso di una Search Vuota ... si intende tornare allo status precedente
            '----------------------------------------------------------------------------------
            'Me.TB_All.Visibility = Visibility.Visible
            'Me.TB_Immagini.Visibility = Visibility.Visible
            'Me.TB_Brochere.Visibility = Visibility.Visible
            'Me.TB_Video.Visibility = Visibility.Visible

            App.Visualizzo_Grid_Ricerca = False
            'Me.Griglia_Centrale.Visibility = Visibility.Visible
            'Me.Griglia_Search.Visibility = Visibility.Collapsed

            'Ricarica Ultima selezione
            App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(Windows.Storage.ApplicationData.Current.LocalSettings.Values("Scelta_Id"), App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))
            Me.Frame.Navigate(GetType(The_Global_Page))
        Else

            '----------------------------------------------------------------------------------
            '  Caso di una Search Piena ... si effettua la ricerca
            '----------------------------------------------------------------------------------

            'If Me.TB_Immagini.Visibility = Visibility.Collapsed Then
            '    Me.TB_All.Visibility = Visibility.Visible
            '    Me.TB_Immagini.Visibility = Visibility.Visible
            '    Me.TB_Brochere.Visibility = Visibility.Visible
            '    Me.TB_Video.Visibility = Visibility.Visible
            '    Me.TB_All.IsChecked = True
            '    Me.TB_Brochere.IsChecked = False
            '    Me.TB_Video.IsChecked = False
            '    Me.TB_Immagini.IsChecked = False
            '    App._Immagini = True
            '    App._Video = True
            '    App._Brochures = True
            'End If
            'Me.TB_All.Visibility = Visibility.Collapsed
            'Me.TB_Immagini.Visibility = Visibility.Collapsed
            'Me.TB_Brochere.Visibility = Visibility.Collapsed
            'Me.TB_Video.Visibility = Visibility.Collapsed

            If App._Immagini = False And App._Video = False And App._Brochures = False Then

                App._All = True
                App._Immagini = True
                App._Video = True
                App._Brochures = True

                Colora_Tb_Button(Me.TB_All, True)
                Colora_Tb_Button(Me.TB_Brochere, False)
                Colora_Tb_Button(Me.TB_Immagini, False)
                Colora_Tb_Button(Me.TB_Video, False)
            End If


            'Cerca in Immagini
            'Cerca in Video
            'Cerca on Brochure
            OggettiFiltrati = Await (Funzioni_TheHome.Seleziona_Per_Filtro(Me.Filtra_Oggetti.Text, App._Immagini, App._Video, App._Brochures, (App.Bottone_Aggiornamento = App.Bottone_Updating)))


            If OggettiFiltrati.Count = 0 Then
                '------------------------------------------------------------
                '  Richiede conferma per la cancellazione di un item dalla lista
                '------------------------------------------------------------
                Dim Content As ContentDialog = New ContentDialog
                Content.Content = m_Temp_No_Result.Replace("{{ serachText }}", Me.Filtra_Oggetti.Text)
                Content.PrimaryButtonText = m_Alert_OK
                Content.Title = m_Alert_Warning

                Dim Ritorno As ContentDialogResult = Await Content.ShowAsync()

                App.Dwonload_Massivo_dalla_Prima_Pagina = True
                Me.Frame.Navigate(GetType(The_Global_Page))

                Exit Sub
            End If

            Me.Lbl_SettorePrincipale.Text = m_Temp_Results_For
            Me.Lbl_SettoreSecondario.Text = Me.Filtra_Oggetti.Text


            'Crea le Sezioni
            '---------------------------------------------------------------------------
            Dim _ElencoSezioni As New List(Of Integer)

            For Each OggettoFiltrato As Lista_Oggetti_Visibili In OggettiFiltrati

                Dim Esiste As Boolean = (_ElencoSezioni.IndexOf(OggettoFiltrato.IDSection) > -1)
                If Not Esiste Then
                    _ElencoSezioni.Add(OggettoFiltrato.IDSection)
                End If
            Next

            'Creare tutto il resto
            '---------------------------------------------------------------------------
            Dim MyStack As New StackPanel

            MyStack.Orientation = Orientation.Vertical
            Me.Griglia_Centrale_Vista.Items.Add(MyStack)

            For Indice As Integer = 0 To _ElencoSezioni.Count - 1

                Search_List.Add(New List(Of Lista_Oggetti.Lista_Oggetti_Visibili))
                Dim ListaElenco As New List(Of Lista_Oggetti_Visibili)
                Dim LaLi As Integer = _ElencoSezioni(Indice)

                ListaElenco = (From p In OggettiFiltrati Where p.IDSection = LaLi).ToList  '(From p In Conn_SQL.Table(Of Funzioni_Dati.Immagini) Where p.Section = _ElencoSezioni(Indice) Order By p.Order).ToList

                For Each imma As Lista_Oggetti_Visibili In ListaElenco
                    Dim Ogge As New Lista_Oggetti.Lista_Oggetti_Visibili
                    Ogge = imma
                    Ogge.Nome_01 = "TXT" & CStr(imma.ID).Trim
                    Ogge.Nome_02 = "LBL" & CStr(imma.ID).Trim
                    Ogge.Nome_03 = "MSG" & CStr(imma.ID).Trim
                    Search_List(Indice).Add(Ogge)
                Next


                Dim LaSezione As List(Of Funzioni_Dati.Sezioni) = Funzioni_Dati.LocalDatabase.Sezioni.Sezione_Specifica_Call(LaLi, App.m_Lang_NoRete_Id)

                Dim MioMessaggio As New TextBlock
                MioMessaggio.Margin = New Thickness(10, 40, 0, 10)
                MioMessaggio.FontSize = 18
                MioMessaggio.FontWeight = Windows.UI.Text.FontWeights.ExtraBold
                MioMessaggio.Foreground = New SolidColorBrush(Windows.UI.Colors.DarkBlue)

                If LaSezione.Count = 0 Then
                    MioMessaggio.Text = " - " & LaLi & " - "
                Else
                    If LaSezione(0).p_FK = 0 Then
                        MioMessaggio.Text = LaSezione(0).p_Title
                    Else
                        MioMessaggio.Text = Funzioni_Dati.LocalDatabase.Sezioni.Dammi_Descrizione_Sezione(LaSezione(0).p_FK) & " - " & LaSezione(0).p_Title
                    End If

                End If
                MyStack.Children.Add(MioMessaggio)

                Dim MiaGriglia As New Grid
                MiaGriglia.BorderThickness = New Thickness(3)
                MiaGriglia.BorderBrush = New SolidColorBrush(Windows.UI.Colors.DarkBlue)

                Dim MyGriView As New GridView
                MyGriView.Name = "Vista_" & CStr(Indice)
                MyGriView.ItemsSource = Search_List(Indice)
                MyGriView.IsItemClickEnabled = "True"
                AddHandler MyGriView.ItemClick, AddressOf GridSource_ItemClick
                Dim template As DataTemplate

                ' Select one of the DataTemplate objects, based on the 
                ' value of the selected item in the ComboBox.
                template = CType(Griglia_Search.Resources("Griglia_DataTemplate"), DataTemplate)
                MyGriView.ItemTemplate = template

                MiaGriglia.Children.Add(MyGriView)

                MyStack.Children.Add(MiaGriglia)

            Next

        End If

        Me.Griglia_Centrale.Visibility = Visibility.Collapsed
        Me.Griglia_Search.Visibility = Visibility.Visible


    End Sub

    Private Sub GridSource_ItemClick(sender As Object, e As ItemClickEventArgs)


        Dim OggettoSelezionato As New Lista_Oggetti_Visibili
        OggettoSelezionato = e.ClickedItem()


        App.Oggetto_Search = New Lista_Oggetti.Lista_Oggetti_Visibili

        Dim Trovato As Boolean = False
        For Indice As Integer = 0 To Search_List.Count - 1
            For Each Oggetto As Lista_Oggetti_Visibili In Search_List(Indice)
                If Oggetto.ID = OggettoSelezionato.ID Then
                    App.Oggetto_Search = Oggetto
                    Trovato = True
                    Exit For
                End If
            Next
        Next

        If Trovato Then
            Me.GridView_ItemClick(Nothing, Nothing)
        End If

    End Sub

    Private Sub Btn_ClosePane_Click(sender As Object, e As RoutedEventArgs) Handles Btn_ClosePane.Click

        If App.Contatore_Generale >= App.Oggetti_in_Downloading.Count Then
            App.Contatore_Generale = 0
            App.Oggetti_in_Downloading.Clear()
        End If

        Me.MySplit_Down.IsPaneOpen = False
    End Sub


    Private Sub Lbl_ContatoreSend_Tapped(sender As Object, e As TappedRoutedEventArgs) Handles Lbl_ContatoreSend.Tapped
        If App.Oggetti_da_Inviare.Count > 0 Then
            Me.Frame.Navigate(GetType(Send_Page))
        End If
    End Sub

    Private Sub Img_Sender_Tapped(sender As Object, e As TappedRoutedEventArgs)
        If App.Oggetti_da_Inviare.Count > 0 Then
            Me.Frame.Navigate(GetType(Send_Page))
        End If
    End Sub

    Private Sub Img_Sender_Tapped_1(sender As Object, e As TappedRoutedEventArgs) Handles Img_Sender.Tapped
        If App.Oggetti_da_Inviare.Count > 0 Then
            Me.Frame.Navigate(GetType(Send_Page))
        End If
    End Sub

    Private Sub Pnl_Image_Tapped(sender As Object, e As TappedRoutedEventArgs) Handles Pnl_Image.Tapped
        If App.Oggetti_da_Inviare.Count > 0 Then
            Me.Frame.Navigate(GetType(Send_Page))
        End If
    End Sub
End Class
