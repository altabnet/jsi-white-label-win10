﻿Public Class Funzioni_Login

    Public Class Login

        Dim m_HashId As String = ""
        Dim m_rootHashID As String = ""
        Dim m_push_channel As String = ""
        Dim m_template As String = ""
        Dim m_EnableToSend As String = ""

        Public Property p_rootHashID As String
            Get
                Return m_rootHashID
            End Get
            Set(value As String)
                m_rootHashID = value
            End Set
        End Property

        Public Property p_push_channel As String
            Get
                Return m_push_channel
            End Get
            Set(value As String)
                m_push_channel = value
            End Set
        End Property

        Public Property p_template As String
            Get
                Return m_template
            End Get
            Set(value As String)
                m_template = value
            End Set
        End Property

        Public Property p_EnableToSend As String
            Get
                Return m_EnableToSend
            End Get
            Set(value As String)
                m_EnableToSend = value
            End Set
        End Property

        Public Property p_HashId As String
            Get
                Return m_HashId
            End Get
            Set(value As String)
                m_HashId = value
            End Set
        End Property

    End Class

    Public Shared Function Convert_JSON_ReturnedFrom_Login(jsonString As String) As Login
        ' Mi creo un Array  di tanti eventuali risultati (nel caso fosse una tabella)
        Dim jsonParts As String() = jsonString.Replace("[", "").Replace("]", "").Split("},{")

        'Mi carico la struttura del Login
        Dim RigaLogin As New Login


        For Each jp As String In jsonParts

            If jp.Trim.Length > 0 Then
                'Splitto ogni campo della stessa riga
                Dim propData As String() = jp.Replace("{", "").Replace("}", "").Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)


                For Each rowData As String In propData
                    Try
                        Dim idx As Integer = rowData.IndexOf(":")
                        Dim n As String = rowData.Substring(0, idx - 1).Replace("""", "")
                        Dim v As String = rowData.Substring(idx + 1).Replace("""", "")

                        If n.ToUpper = "HASHID" Then RigaLogin.p_HashId = v
                        If n.ToUpper = "ROOTHASHID" Then RigaLogin.p_rootHashID = v
                        If n.ToUpper = "PUSH_CHANNEL" Then RigaLogin.p_push_channel = v
                        If n.ToUpper = "TEMPLATE" Then RigaLogin.p_template = v
                        If n.ToUpper = "ENABLETOSEND" Then RigaLogin.p_EnableToSend = v

                    Catch ex As Exception
                        Continue For
                    End Try

                Next
            End If
        Next
        Return RigaLogin

    End Function


    Public Class Response_Message
        Dim m_Status As String = ""
        Dim m_Message As String = ""

        Public Property p_Message As String
            Get
                Return m_Message
            End Get
            Set(value As String)
                m_Message = value
            End Set
        End Property

        Public Property p_Status As String
            Get
                Return m_Status
            End Get
            Set(value As String)
                m_Status = value
            End Set
        End Property

    End Class


    Public Shared Function Convert_JSON_ReturnedFrom_ResetPassword(jsonString As String) As Response_Message
        ' Mi creo un Array  di tanti eventuali risultati (nel caso fosse una tabella)
        Dim jsonParts As String() = jsonString.Replace("[", "").Replace("]", "").Split("},{")

        'Mi carico la struttura del Login
        Dim RigaLogin As New Response_Message


        For Each jp As String In jsonParts

            If jp.Trim.Length > 0 Then
                'Splitto ogni campo della stessa riga
                Dim propData As String() = jp.Replace("{", "").Replace("}", "").Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)


                For Each rowData As String In propData
                    Try
                        Dim idx As Integer = rowData.IndexOf(":")
                        Dim n As String = rowData.Substring(0, idx - 1).Replace("""", "")
                        Dim v As String = rowData.Substring(idx + 1).Replace("""", "")

                        If n.ToUpper = "STATUS" Then RigaLogin.p_Status = v
                        If n.ToUpper = "MESSAGE" Then RigaLogin.p_Message = v

                    Catch ex As Exception
                        Continue For
                    End Try

                Next
            End If
        Next
        Return RigaLogin

    End Function



End Class




