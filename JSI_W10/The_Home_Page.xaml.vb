﻿' The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

Imports Newtonsoft.Json
Imports Windows.Storage
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class The_Home_Page
    Inherits Page

    Dim localSettings As Windows.Storage.ApplicationDataContainer = Windows.Storage.ApplicationData.Current.LocalSettings
    Dim localFolder As Windows.Storage.StorageFolder = Windows.Storage.ApplicationData.Current.LocalFolder

    Dim m_Temp_Languages As String = "Languages"
    Dim m_Temp_Results_For As String = "Results for"
    Dim m_Temp_No_Result As String = "Your Search '{{ serachText }}' did not match any documents. "
    Dim m_Temp_Home As String = "Homepage"
    Dim m_Temp_Logout As String = "Logout"
    Dim m_Temp_Stats As String = "Stats"
    Dim m_SecIDE_TopThank As String = ""

    Dim m_Alert_Warning As String = "Warning"
    Dim m_Alert_OK As String = "Ok"
    Dim m_Log_DataMissing As String = "You have to connect the app to the internet at the least one time before using it."

    Dim MStats_FlyOut(100) As Flyout
    Dim MStats_FlyOut_Codice(100) As String

    '------------------------------------------------------
    ' Variabili per la Gesione Dinamica del Menù
    '------------------------------------------------------
    Public IntestaMenu_Codice(100) As String
    Public IntestaMenu_Testo(100) As String
    Public IntestaMenu_Lunghezza(100) As Integer
    Public IntestaMenu_Partenza(100) As Integer
    Public Width_InizioMenu As Integer = 0

    Public IFlyOut(100) As Flyout
    Public IFlyOut_Codice(100) As String

    Public MTest As MenuFlyout

    Public Menu_Primo_Elemento As Integer = 0
    Public Menu_Primo_Elemento_Precedente As Integer = 0
    Public Menu_Ultimo_Elemento As Integer = 0
    '------------------------------------------------------
    ' Variabili per la Gesione Dinamica del Menù
    '------------------------------------------------------

    Public Categorie_Down As List(Of File_Down.Categorie_To_Down)


    Private Async Sub The_Home_Page_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded

        '.............................................................................
        '.............................................................................
        'Metto il Loading
        '.............................................................................
        '.............................................................................



        If Not App.Layout_Globale Is Nothing Then

            '-----------------------------
            ' Globale
            '-----------------------------
            Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Btn_Settings.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Stack_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Lbl_MessaggioGenerico_XXX.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.welcomeTxtColor)


            Me.Lbl_MessaggioGenerico_XXX.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionTitleTxtColor)
            Me.Btn_Settings.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuTxtColor)



            '------------------------------------------------------------------
            ' Griglie
            '------------------------------------------------------------------
            Me.Grd_Generale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Frame_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

            Me.Frame_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Btn_TornaIndietro.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
            Me.Btn_VaiAvanti.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
            Me.Stack_Generale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
            Me.Stack_Dx.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)

            Me.Grd_Intestazione.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.headerColor)

            'Me.Griglia_Centrale_Vista.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            'Me.Griglia_Intestazione.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            'Me.Griglia_OggettiSelezionati.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            'Me.Griglia_Search.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            'Me.Grigli_Mostra.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            'Me.Grd_SottoIntesta.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)


            Me.PannelloMenu.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)

            Me.Lnk_Lingue.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
            Me.Lnk_Lingue.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.languageTextColor)


            'Me.Lbl_SettorePrincipale.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionTitleTxtColor)
            'Me.Lbl_SettoreSecondario.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionDescrTxtColor)


            Me.Filtra_Oggetti.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.searchTxtColor)
            Me.Filtra_Oggetti.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.searchBkgColor)
            'Me.Filtra_Oggetti.pckground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)



            'Me.Lbl_ContatoreSend.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBackgroundColor)
            'Me.Pnl_Image_Piccolo.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.subMenuBkgColor)
            'Me.Btn_Send.BorderThickness = New Thickness(App.Layout_Globale.pageMain.mainSendButtonBorderWidth)
            'Me.Btn_Send.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBorderColor)
            'Me.Btn_Send.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonTextColor)
            'Me.Btn_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBackgroundColor)


        End If



        '-----------------------------------------------------------------
        'QUI_Carico_Dati_e_Salvo-E_Download
        '-----------------------------------------------------------------
        If Not App.Caricamento_DatiStruttura_Effettuto AndAlso Funzioni_Generali.Connessione_Internet_Attiva AndAlso App.Is_First_Connection = False Then

            '---------------------------------------------
            ' 1 - Carica i dati da JSON
            ' 2 - Verifica se Struttura del Menù è Cambiata
            '---------------------------------------------
            Funct_RecuperoDati.Recupero_e_Save_Dati_From_TheHome()

            'Scarica Layout
            If Funzioni_Generali.Connessione_Internet_Attiva() Then
                'Scarica Json layout 
                Await Funzioni_Layout.Scarica_Json_Layout(App.Struttura_Globale.BaseInfo.HqHashing)
            End If


            If App.Menu_Aggiornato Then
                'Esegui_RicaricamenU

            End If
        End If


        '--------------------------------------------------
        ' LOGO
        '--------------------------------------------------
        Try
            Me.Img_Logo.Source = Nothing
            Dim uriSource = New Uri("ms-appx://JSI_ATAB/Assets/StoreLogo.png")
            Me.Img_Logo.CacheMode = Nothing
            Me.Img_Logo.Source = New BitmapImage(uriSource)
            Me.Img_Logo.UpdateLayout()
        Catch ex As Exception
            Me.Img_Logo.Source = Nothing
        End Try


        Dim _FileName1 As String = App.Firm_HdHash.Trim & ".jpeg"  '  App.Layout_Globale.general.panelLogo
        'Dim _FileName1 As String = App.Layout_Globale.general.panelLogo
        Dim _FileControllo1 As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName1)
        Dim _MyFileInfo1 As FileInfo = New System.IO.FileInfo(_FileControllo1.Replace("/", "\"))
        Try
            Me.Img_Logo.Source = Nothing
            If _MyFileInfo1.Length > 0 Then
                Dim uriSource = New Uri(_FileControllo1)
                Me.Img_Logo.CacheMode = Nothing
                Me.Img_Logo.Source = New BitmapImage(uriSource)
                Me.Img_Logo.UpdateLayout()
            Else
                Me.Img_Logo.Source = Nothing
            End If


        Catch ex As Exception
            Me.Img_Logo.Source = Nothing
        End Try
        '--------------------------------------------------
        ' LOGO
        '--------------------------------------------------



        Dim Temp_Languages As Object = localSettings.Values("TemIDE_Languages")
        Dim Temp_Results_For As Object = localSettings.Values("TemIDE_ResultsFor")
        Dim Temp_No_Result As Object = localSettings.Values("TemIDE_NoResults")
        Dim Temp_Home As Object = localSettings.Values("TemIDE_Home")
        Dim Temp_Logout As Object = localSettings.Values("TemIDE_LogOut")
        Dim Temp_Stats As Object = localSettings.Values("TemIDE_Stats")
        Dim SecIDE_TopThank As Object = localSettings.Values("SecIDE_TopThank")

        If Not Temp_Languages Is Nothing Then m_Temp_Languages = Temp_Languages
        If Not Temp_Results_For Is Nothing Then m_Temp_Results_For = Temp_Results_For
        If Not Temp_No_Result Is Nothing Then m_Temp_No_Result = Temp_No_Result

        If Not Temp_Home Is Nothing Then m_Temp_Home = Temp_Home
        If Not Temp_Logout Is Nothing Then m_Temp_Logout = Temp_Logout
        If Not Temp_Stats Is Nothing Then m_Temp_Stats = Temp_Stats

        m_SecIDE_TopThank = ""
        If Not SecIDE_TopThank Is Nothing Then m_SecIDE_TopThank = SecIDE_TopThank


        Me.Lnk_Lingue.Content = m_Temp_Languages
        Me.Lbl_MessaggioGenerico_XXX.Text = m_SecIDE_TopThank


        Menu_Primo_Elemento = 0
        Menu_Primo_Elemento_Precedente = 0
        Menu_Ultimo_Elemento = 0


        Me.Lnk_Lingue.Visibility = Visibility.Visible
        Me.Btn_Settings.Visibility = Visibility.Visible


        Dim Bottone_01 As Object = localSettings.Values("SecIDE_Add")
        Dim Bottone_02 As Object = localSettings.Values("SecIDE_Saved")
        Dim Bottone_03 As Object = localSettings.Values("SecIDE_Send")
        Dim Bottone_04 As Object = localSettings.Values("DetIDE_Close")
        Dim Bottone_05 As Object = localSettings.Values("SecIDE_Updating")
        Dim Bottone_06 As Object = localSettings.Values("SecIDE_Updated")


        If Not Bottone_01 Is Nothing Then App.Bottone_Add = Bottone_01
        If Not Bottone_02 Is Nothing Then App.Bottone_Saved = Bottone_02
        If Not Bottone_03 Is Nothing Then App.Bottone_Send = Bottone_03
        If Not Bottone_04 Is Nothing Then App.Bottone_Close = Bottone_04
        If Not Bottone_05 Is Nothing Then App.Bottone_Updating = Bottone_05
        If Not Bottone_06 Is Nothing Then App.Bottone_Update = Bottone_06

        Me.Btn_Aggiornamento.Content = App.Bottone_Updating
        App.Bottone_Aggiornamento = App.Bottone_Updating


        Dim Alert_Warning As Object = localSettings.Values("AleIDE_Warning")
        Dim Alert_OK As Object = localSettings.Values("AleIDE_OK")
        Dim LOG_DataMissing As Object = localSettings.Values("AleIDE_NoConnection")


        If Not Alert_Warning Is Nothing Then m_Alert_Warning = Alert_Warning
        If Not Alert_OK Is Nothing Then m_Alert_OK = Alert_OK
        If Not LOG_DataMissing Is Nothing Then m_Log_DataMissing = LOG_DataMissing



        ''-------------------------------------------------------------------
        ''-------------------------------------------------------------------
        ''  Creazioen Menu STATS
        ''-------------------------------------------------------------------
        ''-------------------------------------------------------------------
        'If App.MFly_Stats Is Nothing Then
        '    App.MFly_Stats = New MenuFlyout()


        '    Dim mn As New MenuFlyoutItem()
        '    mn.Text = m_Temp_Home
        '    mn.Name = "Stats_01"
        '    AddHandler mn.Click, AddressOf Statistica_Scelta
        '    mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '    mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
        '    'mn.FocusVisualPrimaryBrush = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '    'mn.FocusVisualSecondaryBrush = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")


        '    App.MFly_Stats.Items.Add(mn)

        '    mn = New MenuFlyoutItem()
        '    mn.Text = m_Temp_Logout
        '    mn.Name = "Stats_02"
        '    AddHandler mn.Click, AddressOf Statistica_Scelta
        '    mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '    mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
        '    App.MFly_Stats.Items.Add(mn)

        '    mn = New MenuFlyoutItem()
        '    mn.Text = m_Temp_Stats
        '    mn.Name = "Stats_03"
        '    AddHandler mn.Click, AddressOf Statistica_Scelta
        '    mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '    mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
        '    App.MFly_Stats.Items.Add(mn)
        'End If

        'If Funzioni_Generali.Connessione_Internet_Attiva Then
        '    '-------------------------------------------------------------------
        '    '-------------------------------------------------------------------
        '    ' CREZIONE MENU LINGUE
        '    '-------------------------------------------------------------------
        '    '-------------------------------------------------------------------
        '    'If App.MFly_Languages Is Nothing Then
        '    If App.Lingue_Diponibili Is Nothing Then
        '        Funzioni_Lingue.Carica_Lingue(App.Lingue_Diponibili, localSettings.Values("Login_NoRete_HashID").ToString)
        '    End If

        '    'App.MFly_Languages = New MenuFlyout()
        '    'For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

        '    '    Dim mLang As New MenuFlyoutItem()
        '    '    mLang.Text = App.Lingue_Diponibili(Indice).p_DisplayName.Trim
        '    '    mLang.Name = "Btn_" & App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
        '    '    AddHandler mLang.Tapped, AddressOf Lingua_Scelta
        '    '    'mLang.Background = PannelloMenu.Background
        '    '    mLang.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '    '    mLang.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

        '    '    App.MFly_Languages.Items.Add(mLang)

        '    'Next
        '    'End If
        'Else

        '    'Carica_Le_Lingue
        '    If App.Lingue_Diponibili Is Nothing Then
        '        Funzioni_Lingue.Carica_Lingue_dal_DB()
        '    End If


        '    'Me.Lnk_Lingue.Visibility = Visibility.Collapsed

        'End If

        'If App.MFly_Languages Is Nothing Then

        '    App.MFly_Languages = New MenuFlyout()
        '    For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

        '        Dim mLang As New MenuFlyoutItem()
        '        mLang.Text = App.Lingue_Diponibili(Indice).p_DisplayName.Trim
        '        mLang.Name = "Btn_" & App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
        '        AddHandler mLang.Tapped, AddressOf Lingua_Scelta
        '        'mLang.Background = PannelloMenu.Background
        '        mLang.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
        '        mLang.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

        '        App.MFly_Languages.Items.Add(mLang)

        '    Next
        'End If

        'App.Visualizzo_Grid_Ricerca = False
        'Funzioni_TheHome.Carica_Menu(Me)


        '.............................................................................
        '.............................................................................
        'Metto il Loading
        '.............................................................................
        '.............................................................................



        '--------------------------------------
        ' TOGLIE la APP dal Full Screen
        '--------------------------------------
        'Dim MyView As ApplicationView = ApplicationView.GetForCurrentView
        'If MyView.IsFullScreenMode Then
        '    MyView.ExitFullScreenMode()
        'End If
        '--------------------------------------
        ' Metta la APP in Full Screen
        '--------------------------------------


        '-----------------------------------------------------------------
        'QUI_Carico_Dati_e_Salvo-E_Download
        '-----------------------------------------------------------------
        ' Spostato nel loading !!!
        '-------------------------------------------------------
        '-------------------------------------------------------
        'If Not App.Caricamento_DatiStruttura_Effettuto AndAlso Funzioni_Generali.Connessione_Internet_Attiva AndAlso App.Is_First_Connection = False Then

        '    '---------------------------------------------
        '    ' 1 - Carica i dati da JSON
        '    ' 2 - Verifica se Struttura del Menù è Cambiata
        '    '---------------------------------------------
        '    Funct_RecuperoDati.Recupero_e_Save_Dati_From_TheHome()


        '    If App.Menu_Aggiornato Then
        '        'Esegui_RicaricamenU

        '    End If
        'End If
        '-------------------------------------------------------
        '-------------------------------------------------------
        ' Spostato nel loading !!!
        '-------------------------------------------------------


        If Funzioni_Generali.Connessione_Internet_Attiva AndAlso App.Is_First_Connection = False Then

            App.Le_Mie_Relazioni_Page = New List(Of Funzioni_Dati.TEMP_Relazione_Brochure_Page)
            'App.Le_Mie_Relazioni_Brochure = New List(Of Funzioni_Dati.TEMP_Relazione_Brochure_ID)
            'App.Le_Mie_Relazioni_Immagini = New List(Of Funzioni_Dati.TEMP_Relazione_Immagini_ID)
            'App.Le_Mie_Relazioni_Video = New List(Of Funzioni_Dati.TEMP_Relazione_Video_ID)

            '---------------------------------------------
            ' Effettua Altri Aggiornamenti - Mettin nella corretta struttura
            '---------------------------------------------
            Dim Video_FileScaricato As List(Of Funzioni_Dati.Video) = Funzioni_Dati.LocalDatabase.Video.Video_Genera_da_NuovoStruttura()
            Dim Immagini_FileScaricato As List(Of Funzioni_Dati.Immagini) = Funzioni_Dati.LocalDatabase.Immagini.Immagini_Genera_da_NuovoStruttura()
            Dim Brochure_FileScaricato As List(Of Funzioni_Dati.Brochure) = Funzioni_Dati.LocalDatabase.Brochures.Brochures_Genera_da_NuovoStruttura()
            Dim SitiWeb_FileScaricato As List(Of Funzioni_Dati.SitiWeb) = Funzioni_Dati.LocalDatabase.SitiWeb.SitiWeb_Genera_da_NuovoStruttura()
            Dim InfoBase_FileScaricato As Funzioni_Dati.InfoBase = Funzioni_Dati.LocalDatabase.InfoBase.InfoBase_Genera_da_NuovoStruttura()

            'Aggiorna_le_Tab_TEMP
            ' LUNGHISSIMAAAAAAAAAA
            'Await Funzioni_Dati.LocalDatabase.Gestisci_Tabelle_TEMP()

            '---------------------------------------------
            ' Aggiorna e Scarica le Thumbs
            '---------------------------------------------
            App.Thumbs_Da_Scaricare = New List(Of String)

            Dim W_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Thumbs"))
            Await Funzioni_Dati.LocalDatabase.Immagini.Immagini_Aggiorna_Dati_e_Thumbs(Immagini_FileScaricato, W_DestinationFolder)

            Await Funzioni_Dati.LocalDatabase.Video.Video_Aggiorna_Dati_e_Thumbs(Video_FileScaricato, W_DestinationFolder)

            Await Funzioni_Dati.LocalDatabase.Brochures.Brochures_Aggiorna_Dati_e_Thumbs(Brochure_FileScaricato, W_DestinationFolder)

            'Dim W_DestinationFolder As StorageFolder = Await (StorageFolder.GetFolderFromPathAsync(ApplicationData.Current.LocalFolder.Path & "\Thumbs"))
            'Await Funzioni_Dati.LocalDatabase.SitiWeb.Bros_Aggiorna_Dati_e_Thumbs(Brochure_FileScaricato)


            'Salva il File degli Oggetti da Scaricare
            '---------------------------------------------
            Await Funzioni_Dati.File_da_Scaricare.Carica_File_da_Scaricare_ALL(App.Da_Scaricare)



            '-------------------------------------------------
            '  Gestione del Deleting
            '-------------------------------------------------


            Dim SexDel As List(Of UpdateSync.SectionsDeleted) = App.Struttura_Globale.SectionsDeleted
            Await Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Deleted(SexDel)

            Dim ImgDel As List(Of UpdateSync.ImagesDeleted) = App.Struttura_Globale.ImagesDeleted
            Await Funzioni_Dati.LocalDatabase.Immagini.Immagini_Deleted(ImgDel)


            Dim VidDel As List(Of UpdateSync.VideosDeleted) = App.Struttura_Globale.VideosDeleted
            Await Funzioni_Dati.LocalDatabase.Video.Video_Deleted(VidDel)

            Dim BroDel As List(Of UpdateSync.BrochuresDeleted) = App.Struttura_Globale.BrochuresDeleted
            Await Funzioni_Dati.LocalDatabase.Brochures.Brochure_Deleted(BroDel)


            'Dim WebDel As List(Of UpdateSync.WebsitesDeleted) = App.Struttura_Globale.WebSitesDeteted
            'Await Funzioni_Dati.LocalDatabase.SitiWeb.Siti_Deleted(WebDel)


            '---------------------------------------------
            ' Predisponi al Download  (Richiama da DB e crea anche per Categoria)
            '---------------------------------------------

            App.Caricamento_DatiStruttura_Effettuto = True
        End If


        '---------------------------------------------
        ' Predisponi al Download  (Richiama da DB e crea anche per Categoria)
        '---------------------------------------------
        Genera_Lista_DownLoad()

        Await Cambio_Messaggio()

        If App.Oggetti_da_Inviare.Count > 0 Then
            App.Oggetti_Da_Inviare_dalla_Prima_Pagina = True
            Me.Frame.Navigate(GetType(The_Global_Page))
        End If
        Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento
        Visualizzazione_Btn_Aggiornamento()

    End Sub

    Private Sub Visualizzazione_Btn_Aggiornamento()
        If Not Funzioni_Generali.Connessione_Internet_Attiva() Then
            Me.Btn_Aggiornamento.Visibility = Visibility.Collapsed
        Else
            Me.Btn_Aggiornamento.Visibility = Visibility.Visible
        End If
    End Sub

    Private Async Function Cambio_Messaggio() As Task
        App.Bottone_Aggiornamento = App.Bottone_Update
        Me.Btn_Aggiornamento.Content = App.Bottone_Update
        App.Testo_Bottone_Update = "PROVA"

        localSettings.Values("Login_PrimaInstallazione") = "NO"

        Dim Contali As Integer = 0
        For Each Categoria As File_Down.Categorie_To_Down In App.Categorie_da_Scaricare
            If Funzioni_Dati.LocalDatabase.Sezioni.Is_Categoria_Attiva(Categoria.ID_Section) Then
                Contali += 1
            End If
        Next

        If Contali = 0 Then
            App.Visualizza_Bottone_Aggiornamento = False
            Me.Btn_Aggiornamento.Content = ""
            Me.Btn_Aggiornamento.Background = Me.Background
            Await Task.Delay(10)
            Me.Btn_Aggiornamento.Visibility = Visibility.Collapsed
            Await Task.Delay(10)

        Else
            App.Visualizza_Bottone_Aggiornamento = True
            Me.Btn_Aggiornamento.Visibility = Visibility.Visible
            Await Task.Delay(10)
        End If
    End Function


    Private Sub Btn_Settings_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Settings.Click
        If Not App.MFly_Stats Is Nothing Then
            App.MFly_Stats.ShowAt(CType(sender, FrameworkElement))
        End If
    End Sub

    Private Sub Lnk_Lingue_Click(sender As Object, e As RoutedEventArgs) Handles Lnk_Lingue.Click
        If Not App.MFly_Languages Is Nothing Then
            App.MFly_Languages.ShowAt(CType(sender, FrameworkElement))
        End If
    End Sub

    Private Async Sub Statistica_Scelta(ByVal sender As Object, ByVal e As RoutedEventArgs)

        Dim NomeBottone As String = CType(sender, Control).Name

        If NomeBottone.Trim.ToUpper = "STATS_01" Then
            Me.Frame.Navigate(GetType(The_Home_Page))
        End If

        If NomeBottone.Trim.ToUpper = "STATS_02" Then
            Me.Frame.Navigate(GetType(Login_Page))
        End If

        If NomeBottone.Trim.ToUpper = "STATS_03" Then
            If Funzioni_Generali.Connessione_Internet_Attiva Then

                Dim _Utente As String = localSettings.Values("Login_NORete_UserName")
                Dim _Pwd As String = localSettings.Values("Login_NORete_Password")


                Dim MyUri As Uri
                _Utente = MyUri.EscapeDataString(_Utente)
                _Pwd = MyUri.EscapeDataString(_Pwd)

                Dim uriToLaunch As String = "http://ws.just-send-it.com/controlPanel/Auth.aspx?u=" & _Utente & "&p=" & _Pwd
                Dim uri As Uri = New Uri(uriToLaunch)

                Dim success = Await Windows.System.Launcher.LaunchUriAsync(uri)
                ' URI launched
                If success Then
                    ' URI launch failed
                Else

                End If
            End If
        End If

    End Sub


    Private Async Sub Lingua_Scelta(ByVal sender As Object, ByVal e As RoutedEventArgs)

        Dim NomeBottone As String = CType(sender, Control).Name
        Dim Codice As String = NomeBottone.Substring(4, NomeBottone.Length - 4)

        For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1
            If App.Lingue_Diponibili(Indice).p_LanguageCode.Trim = Codice Then
                App.m_Lang_NoRete_Code = Codice
                App.m_Lang_NoRete_Id = App.Lingue_Diponibili(Indice).p_LanguageID

                localSettings.Values("Lang_NoRete_Code") = Codice
                localSettings.Values("Lang_NoRete_Id") = App.Lingue_Diponibili(Indice).p_LanguageID
            End If
        Next


        If Funzioni_Generali.Connessione_Internet_Attiva Then

            Await (Funzioni_Lingue.Setta_Controlli_Lingua(Codice))

            'Await Funzioni_Dati.File_da_Scaricare.Carica_File_da_Scaricare_ALL(App.Da_Scaricare)
        Else

            Await Funzioni_Lingue.Setta_Controlli_Lingua_OffLine(Codice)
        End If

        App.MFly_Languages = Nothing
        App.MFly_Stats = Nothing
        App.LeSezioni_Da_DataBase = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Principali_Call(App.m_Lang_NoRete_Id)
        Me.Frame.Navigate(GetType(The_Home_Page))

    End Sub


    Private Sub The_Home_Page_SizeChanged(sender As Object, e As SizeChangedEventArgs) Handles Me.SizeChanged


        If Not sender Is Nothing Then
            Menu_Primo_Elemento = 0
            Menu_Primo_Elemento_Precedente = 0
            Menu_Ultimo_Elemento = 0
        End If

        Dim bounds As Rect = Window.Current.Bounds
        Dim Height As Double = bounds.Height
        Dim Width As Double = bounds.Width
        'Me.Larghezza.Text = Width
        'Me.Altezza.Text = Height

        PannelloMenu.Children.Clear()

        Dim GrandeZZamenu As Integer = Width - 85  'Tolgo i 2 bottoni di Inizioe  Fine'
        Dim LaPartenza_W As Integer = Width_InizioMenu
        Dim Not_Primo As Boolean = False


        If Menu_Primo_Elemento > 0 Then
            Me.Btn_TornaIndietro.Visibility = Visibility.Visible
            Me.Lbl_Indietro.Visibility = Visibility.Visible
        Else
            Me.Btn_TornaIndietro.Visibility = Visibility.Collapsed
            Me.Lbl_Indietro.Visibility = Visibility.Collapsed
        End If

        For Indice As Integer = Menu_Primo_Elemento To 20

            If IntestaMenu_Lunghezza(Indice) = 0 Then
                Me.Btn_VaiAvanti.Visibility = Visibility.Collapsed
                Me.Lbl_Avanti.Visibility = Visibility.Collapsed
                Exit For
            End If
            If LaPartenza_W + IntestaMenu_Lunghezza(Indice) < GrandeZZamenu Then

                If Not_Primo Then
                    Dim _Sbarra As New TextBlock
                    _Sbarra.Name = "Sba" & CStr(Indice).Trim
                    _Sbarra.Text = "|"
                    _Sbarra.FontSize = 24
                    _Sbarra.FontFamily = New FontFamily("Assets/DIN_Bold.otf#DIN Alternate")
                    _Sbarra.Foreground = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                    PannelloMenu.Children.Add(_Sbarra)

                End If

                '-----------------------------------------------------------------
                ' se usi il Bottone devi settare +20 dove c'e' adesso il +10  nel caricamneto array
                '-----------------------------------------------------------------
                'Dim _Bottone As New Button
                '_Bottone.Name = "Btn_" & CStr(Indice).Trim
                '_Bottone.Content = IntestaMenu_Testo(Indice)
                '_Bottone.Background = PannelloMenu.Background
                'AddHandler _Bottone.Click, AddressOf theButton_click
                'PannelloMenu.Children.Add(_Bottone)

                Dim _Bottone As New TextBlock
                _Bottone.Name = "Btn_" & CStr(IntestaMenu_Codice(Indice)).Trim
                _Bottone.Text = IntestaMenu_Testo(Indice)
                _Bottone.Margin = New Thickness(5, 10, 5, 0)
                _Bottone.Foreground = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                _Bottone.FontFamily = New FontFamily("Assets/DIN_Bold.otf#DIN Alternate")
                _Bottone.FontSize = 16
                AddHandler _Bottone.Tapped, AddressOf Lbl_MenuPrincipale_Tapped
                PannelloMenu.Children.Add(_Bottone)


                LaPartenza_W += IntestaMenu_Lunghezza(Indice)
                LaPartenza_W += 7
                Not_Primo = True

                Menu_Ultimo_Elemento = Indice
            Else
                Me.Btn_VaiAvanti.Visibility = Visibility.Visible
                Me.Lbl_Avanti.Visibility = Visibility.Visible
                Exit For
            End If
        Next

    End Sub

    Private Sub Btn_VaiAvanti_Click(sender As Object, e As RoutedEventArgs) Handles Btn_VaiAvanti.Click
        Menu_Primo_Elemento_Precedente = Menu_Primo_Elemento
        Menu_Primo_Elemento = Menu_Ultimo_Elemento + 1
        Me.The_Home_Page_SizeChanged(Nothing, Nothing)

        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
        End If
    End Sub

    Private Sub Btn_TornaIndietro_Click(sender As Object, e As RoutedEventArgs) Handles Btn_TornaIndietro.Click
        If Menu_Primo_Elemento = Menu_Primo_Elemento_Precedente Then
            Menu_Primo_Elemento = 0
            Menu_Primo_Elemento_Precedente = 0
        End If
        Menu_Primo_Elemento = Menu_Primo_Elemento_Precedente
        Me.The_Home_Page_SizeChanged(Nothing, Nothing)

        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
        End If
    End Sub

    Private Async Sub The_Home_Page_Loading(sender As FrameworkElement, args As Object) Handles Me.Loading



        Try
            Me.Img_Logo.Source = Nothing
            Dim uriSource = New Uri("ms-appx://JSI_ATAB/Assets/StoreLogo.png")
            Me.Img_Logo.CacheMode = Nothing
            Me.Img_Logo.Source = New BitmapImage(uriSource)
            Me.Img_Logo.UpdateLayout()
        Catch ex As Exception
            Me.Img_Logo.Source = Nothing
        End Try


        'If Not App.Layout_Globale Is Nothing Then

        '    '-----------------------------
        '    ' Globale
        '    '-----------------------------
        '    Me.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

        '    Me.Btn_Settings.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    Me.Stack_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)



        '    '------------------------------------------------------------------
        '    ' Griglie
        '    '------------------------------------------------------------------
        '    Me.Grd_Generale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    Me.Frame_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)

        '    Me.Frame_Centrale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    Me.Btn_TornaIndietro.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
        '    Me.Btn_VaiAvanti.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
        '    Me.Stack_Generale.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)
        '    Me.Stack_Dx.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)

        '    Me.Grd_Intestazione.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.headerColor)

        '    'Me.Griglia_Centrale_Vista.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    'Me.Griglia_Intestazione.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    'Me.Griglia_OggettiSelezionati.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    'Me.Griglia_Search.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    'Me.Grigli_Mostra.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    'Me.Grd_SottoIntesta.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)


        '    Me.PannelloMenu.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.menuBkgColor)

        '    Me.Lnk_Lingue.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)
        '    Me.Lnk_Lingue.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.languageTextColor)


        '    'Me.Lbl_SettorePrincipale.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionTitleTxtColor)
        '    'Me.Lbl_SettoreSecondario.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionDescrTxtColor)


        '    Me.Filtra_Oggetti.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.searchTxtColor)
        '    Me.Filtra_Oggetti.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.searchBkgColor)
        '    'Me.Filtra_Oggetti.pckground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.general.backgroundColor)



        '    'Me.Lbl_ContatoreSend.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBackgroundColor)
        '    'Me.Pnl_Image_Piccolo.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.subMenuBkgColor)
        '    'Me.Btn_Send.BorderThickness = New Thickness(App.Layout_Globale.pageMain.mainSendButtonBorderWidth)
        '    'Me.Btn_Send.BorderBrush = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBorderColor)
        '    'Me.Btn_Send.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonTextColor)
        '    'Me.Btn_Send.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.mainSendButtonBackgroundColor)


        '--------------------------------------------------
        ' LOGO
        '--------------------------------------------------
        Dim _FileName As String = App.Firm_HdHash.Trim & ".jpeg" 'App.Layout_Globale.general.panelLogo
        'Dim _FileName As String = App.Layout_Globale.general.panelLogo
        Dim _FileControllo As String = System.IO.Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, _FileName)
        Dim MyFileInfo As System.IO.FileInfo = New System.IO.FileInfo(_FileControllo)
        Try
            If MyFileInfo.Length > 0 Then
                Dim uriSource = New Uri(_FileControllo)
                Me.Img_Logo.Source = New BitmapImage(uriSource)
            Else
                Me.Img_Logo.Source = Nothing
            End If

        Catch ex As Exception
            Me.Img_Logo.Source = Nothing
        End Try
        '--------------------------------------------------
        ' LOGO
        '--------------------------------------------------

        'End If



        ''-----------------------------------------------------------------
        ''QUI_Carico_Dati_e_Salvo-E_Download
        ''-----------------------------------------------------------------
        'If Not App.Caricamento_DatiStruttura_Effettuto AndAlso Funzioni_Generali.Connessione_Internet_Attiva AndAlso App.Is_First_Connection = False Then

        '    '---------------------------------------------
        '    ' 1 - Carica i dati da JSON
        '    ' 2 - Verifica se Struttura del Menù è Cambiata
        '    '---------------------------------------------
        '    Funct_RecuperoDati.Recupero_e_Save_Dati_From_TheHome()

        '    'Scarica Layout
        '    If Funzioni_Generali.Connessione_Internet_Attiva() Then
        '        'Scarica Json layout 
        '        Await Funzioni_Layout.Scarica_Json_Layout(App.Struttura_Globale.BaseInfo.HqHashing)
        '    End If


        '    If App.Menu_Aggiornato Then
        '        'Esegui_RicaricamenU

        '    End If
        'End If




        'Dim Temp_Languages As Object = localSettings.Values("TemIDE_Languages")
        'Dim Temp_Results_For As Object = localSettings.Values("TemIDE_ResultsFor")
        'Dim Temp_No_Result As Object = localSettings.Values("TemIDE_NoResults")
        'Dim Temp_Home As Object = localSettings.Values("TemIDE_Home")
        'Dim Temp_Logout As Object = localSettings.Values("TemIDE_LogOut")
        'Dim Temp_Stats As Object = localSettings.Values("TemIDE_Stats")
        'Dim SecIDE_TopThank As Object = localSettings.Values("SecIDE_TopThank")

        'If Not Temp_Languages Is Nothing Then m_Temp_Languages = Temp_Languages
        'If Not Temp_Results_For Is Nothing Then m_Temp_Results_For = Temp_Results_For
        'If Not Temp_No_Result Is Nothing Then m_Temp_No_Result = Temp_No_Result

        'If Not Temp_Home Is Nothing Then m_Temp_Home = Temp_Home
        'If Not Temp_Logout Is Nothing Then m_Temp_Logout = Temp_Logout
        'If Not Temp_Stats Is Nothing Then m_Temp_Stats = Temp_Stats

        'If Not SecIDE_TopThank Is Nothing Then m_SecIDE_TopThank = SecIDE_TopThank


        Me.Lnk_Lingue.Content = ""
        'Me.Lbl_MessaggioGenerico_XXX.Text = m_SecIDE_TopThank


        'Menu_Primo_Elemento = 0
        'Menu_Primo_Elemento_Precedente = 0
        'Menu_Ultimo_Elemento = 0


        Me.Lnk_Lingue.Visibility = Visibility.Collapsed
        'Me.Btn_Settings.Visibility = Visibility.Visible


        'Dim Bottone_01 As Object = localSettings.Values("SecIDE_Add")
        'Dim Bottone_02 As Object = localSettings.Values("SecIDE_Saved")
        'Dim Bottone_03 As Object = localSettings.Values("SecIDE_Send")
        'Dim Bottone_04 As Object = localSettings.Values("DetIDE_Close")
        'Dim Bottone_05 As Object = localSettings.Values("SecIDE_Updating")
        'Dim Bottone_06 As Object = localSettings.Values("SecIDE_Updated")


        'If Not Bottone_01 Is Nothing Then App.Bottone_Add = Bottone_01
        'If Not Bottone_02 Is Nothing Then App.Bottone_Saved = Bottone_02
        'If Not Bottone_03 Is Nothing Then App.Bottone_Send = Bottone_03
        'If Not Bottone_04 Is Nothing Then App.Bottone_Close = Bottone_04
        'If Not Bottone_05 Is Nothing Then App.Bottone_Updating = Bottone_05
        'If Not Bottone_06 Is Nothing Then App.Bottone_Update = Bottone_06

        'Me.Btn_Aggiornamento.Content = App.Bottone_Updating
        'App.Bottone_Aggiornamento = App.Bottone_Updating


        'Dim Alert_Warning As Object = localSettings.Values("AleIDE_Warning")
        'Dim Alert_OK As Object = localSettings.Values("AleIDE_OK")
        'Dim LOG_DataMissing As Object = localSettings.Values("AleIDE_NoConnection")


        'If Not Alert_Warning Is Nothing Then m_Alert_Warning = Alert_Warning
        'If Not Alert_OK Is Nothing Then m_Alert_OK = Alert_OK
        'If Not LOG_DataMissing Is Nothing Then m_Log_DataMissing = LOG_DataMissing



        '-------------------------------------------------------------------
        '-------------------------------------------------------------------
        '  Creazioen Menu STATS
        '-------------------------------------------------------------------
        '-------------------------------------------------------------------
        If App.MFly_Stats Is Nothing Then
            App.MFly_Stats = New MenuFlyout()


            Dim mn As New MenuFlyoutItem()
            mn.Text = m_Temp_Home
            mn.Name = "Stats_01"
            AddHandler mn.Click, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuBkgColor)
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuTxtColor)   'Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageContent.sectionTitleTxtColor)  'Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            'mn.FocusVisualPrimaryBrush = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
            'mn.FocusVisualSecondaryBrush = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")


            App.MFly_Stats.Items.Add(mn)

            mn = New MenuFlyoutItem()
            mn.Text = m_Temp_Logout
            mn.Name = "Stats_02"
            AddHandler mn.Click, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuBkgColor)
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuTxtColor) 'Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            App.MFly_Stats.Items.Add(mn)

            mn = New MenuFlyoutItem()
            mn.Text = m_Temp_Stats
            mn.Name = "Stats_03"
            AddHandler mn.Click, AddressOf Statistica_Scelta
            mn.Background = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuBkgColor)
            mn.Foreground = Funzioni_Generali.GetSolidColorBrush(App.Layout_Globale.pageMain.settingsMenuTxtColor)   ' Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
            App.MFly_Stats.Items.Add(mn)
        End If

        If Funzioni_Generali.Connessione_Internet_Attiva Then
            '-------------------------------------------------------------------
            '-------------------------------------------------------------------
            ' CREZIONE MENU LINGUE
            '-------------------------------------------------------------------
            '-------------------------------------------------------------------
            'If App.MFly_Languages Is Nothing Then
            If App.Lingue_Diponibili Is Nothing Then
                Funzioni_Lingue.Carica_Lingue(App.Lingue_Diponibili, localSettings.Values("Login_NoRete_HashID").ToString)
            End If

            'App.MFly_Languages = New MenuFlyout()
            'For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

            '    Dim mLang As New MenuFlyoutItem()
            '    mLang.Text = App.Lingue_Diponibili(Indice).p_DisplayName.Trim
            '    mLang.Name = "Btn_" & App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
            '    AddHandler mLang.Tapped, AddressOf Lingua_Scelta
            '    'mLang.Background = PannelloMenu.Background
            '    mLang.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
            '    mLang.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

            '    App.MFly_Languages.Items.Add(mLang)

            'Next
            'End If
        Else

            'Carica_Le_Lingue
            If App.Lingue_Diponibili Is Nothing Then
                Funzioni_Lingue.Carica_Lingue_dal_DB()
            End If


            'Me.Lnk_Lingue.Visibility = Visibility.Collapsed

        End If

        If App.MFly_Languages Is Nothing Then

            App.MFly_Languages = New MenuFlyout()
            For Indice As Integer = 0 To App.Lingue_Diponibili.Count - 1

                Dim mLang As New MenuFlyoutItem()
                mLang.Text = App.Lingue_Diponibili(Indice).p_DisplayName.Trim
                mLang.Name = "Btn_" & App.Lingue_Diponibili(Indice).p_LanguageCode.Trim
                AddHandler mLang.Tapped, AddressOf Lingua_Scelta
                'mLang.Background = PannelloMenu.Background
                mLang.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                mLang.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")

                App.MFly_Languages.Items.Add(mLang)

            Next
        End If

        App.Visualizzo_Grid_Ricerca = False
        Funzioni_TheHome.Carica_Menu(Me)

        ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen


        Me.The_Home_Page_SizeChanged(Nothing, Nothing)
    End Sub


    Private Async Sub Lbl_MenuPrincipale_Tapped(sender As Object, e As RoutedEventArgs)
        'RoutedEventArgs
        'TappedRoutedEventArgs

        If App.Menu_Aggiornato Then
            App.Menu_Aggiornato = False
            Frame.Navigate(GetType(The_Home_Page))
            Exit Sub
        End If

        Funzioni_Generali.Invia_Email_In_Waiting()

        Dim _IdSection As Integer = CType(sender, TextBlock).Name.Substring(4, CType(sender, TextBlock).Name.Length - 4)
        App.Sezioni_Secondarie = Funzioni_Dati.LocalDatabase.Sezioni.Sezioni_Secondarie_Call(_IdSection, App.m_Lang_NoRete_Id)


        If App.Sezioni_Secondarie.Count > 0 Then

            MTest = New MenuFlyout()
            For Indice As Integer = 0 To App.Sezioni_Secondarie.Count - 1
                Dim mn As New MenuFlyoutItem()
                mn.Text = App.Sezioni_Secondarie(Indice).p_Title
                mn.Name = "Btn_" & CStr(App.Sezioni_Secondarie(Indice).p_ID).Trim

                mn.Background = Funzioni_Generali.GetSolidColorBrush("#FFFFFF")
                mn.Foreground = Funzioni_Generali.GetSolidColorBrush("#1F9CE3")
                AddHandler mn.Click, AddressOf Lbl_Secondario_Tapped
                MTest.Items.Add(mn)
            Next
            MTest.ShowAt(CType(sender, FrameworkElement))

            App.Title_SettorePrincipale = ""
            App.Title_SettoreSecondario = ""
            For Indici As Integer = 0 To App.LeSezioni_Da_DataBase.Count
                If App.LeSezioni_Da_DataBase(Indici).p_ID = _IdSection Then
                    App.Title_SettorePrincipale = App.LeSezioni_Da_DataBase(Indici).p_Title
                    Exit For
                End If
            Next

        Else
            For Indice = 0 To 99
                If App.LeSezioni_Da_DataBase(Indice).p_ID = _IdSection Then
                    localSettings.Values("Scelta_Id") = _IdSection
                    localSettings.Values("Scelta_Title") = App.LeSezioni_Da_DataBase(Indice).p_Title
                    localSettings.Values("Scelta_FK") = App.LeSezioni_Da_DataBase(Indice).p_FK

                    App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(_IdSection, True, True, True, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

                    App._Immagini = True
                    App._Video = True
                    App._Brochures = True
                    App.Title_SettorePrincipale = App.LeSezioni_Da_DataBase(Indice).p_Title

                    Me.Frame.Navigate(GetType(The_Global_Page))
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Async Sub Lbl_Secondario_Tapped(sender As Object, e As RoutedEventArgs)
        'RoutedEventArgs
        'TappedRoutedEventArgs

        Dim IndiceMenu As Integer = CType(sender, MenuFlyoutItem).Name.Substring(4, CType(sender, MenuFlyoutItem).Name.Length - 4)

        For Indice = 0 To 99
            If App.Sezioni_Secondarie(Indice).p_ID = IndiceMenu Then
                localSettings.Values("Scelta_Id") = App.Sezioni_Secondarie(Indice).p_ID
                localSettings.Values("Scelta_Title") = App.Sezioni_Secondarie(Indice).p_Title
                localSettings.Values("Scelta_FK") = App.Sezioni_Secondarie(Indice).p_FK


                App.Oggetti_Selezionati = Await (Funzioni_TheHome.Seleziona_Per_Categoria(App.Sezioni_Secondarie(Indice).p_ID, True, True, True, (App.Bottone_Aggiornamento = App.Bottone_Updating)))

                App._Immagini = True
                App._Video = True
                App._Brochures = True

                App.Title_SettoreSecondario = App.Sezioni_Secondarie(Indice).p_Title
                Me.Frame.Navigate(GetType(The_Global_Page))
                Exit For
            End If
        Next

    End Sub

    Private Sub Filtra_Oggetti_QuerySubmitted(sender As AutoSuggestBox, args As AutoSuggestBoxQuerySubmittedEventArgs) Handles Filtra_Oggetti.QuerySubmitted

        If Me.Filtra_Oggetti.Text.Trim.Length > 0 Then
            App.Visualizzo_Grid_Ricerca = True
            App.Testo_Oggetto_Search = Me.Filtra_Oggetti.Text


            If Not sender Is Nothing Then
                'GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(1, App.Selected_Object.Title)  'Titolo
                'GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(2, App.Selected_Object.ID)  'ID
                GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(3, App.m_Lang_NoRete_Id)  ' Lingua
                GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(4, App.m_Username)  'Username
                GoogleAnalytics.EasyTracker.GetTracker().SetCustomDimension(5, Me.Filtra_Oggetti.Text)  'Testo
                GoogleAnalytics.EasyTracker.GetTracker().SendView("Ricerca")

            End If


            Me.Frame.Navigate(GetType(The_Global_Page))
        End If

    End Sub

    Private Async Sub Btn_Aggiornamento_Click(sender As Object, e As RoutedEventArgs) Handles Btn_Aggiornamento.Click

        Visualizzazione_Btn_Aggiornamento()

        If Funzioni_Generali.Connessione_Internet_Attiva Then
            If Me.Btn_Aggiornamento.Content = App.Bottone_Aggiornamento Then

                App.Dwonload_Massivo_dalla_Prima_Pagina = True
                Me.Frame.Navigate(GetType(The_Global_Page))
            End If
        Else

            '-------------------------------------------------------------------
            ' Individuazione di un errore nell'invio
            '-------------------------------------------------------------------
            Dim Content As ContentDialog = New ContentDialog
            Content.Content = m_Log_DataMissing
            Content.PrimaryButtonText = m_Alert_OK
            Content.Title = m_Alert_Warning

            Await Content.ShowAsync
            Exit Sub
        End If
    End Sub

    Private Sub Btn_Aggiornamento_DragLeave(sender As Object, e As DragEventArgs) Handles Btn_Aggiornamento.DragLeave

    End Sub

    Private Sub Btn_Settings_Tapped(sender As Object, e As TappedRoutedEventArgs) Handles Btn_Settings.Tapped

    End Sub
End Class
